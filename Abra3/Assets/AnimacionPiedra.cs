﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimacionPiedra : MonoBehaviour {
    Transform[] piedras;
    public  Transform targert;
    public float velMov=1;
 
	// Use this for initialization
	void Start () {
        piedras = GetComponentsInChildren<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
        
            this.transform.position = Vector3.MoveTowards(transform.position,targert.position, velMov*Time.deltaTime);
       
	}
}
