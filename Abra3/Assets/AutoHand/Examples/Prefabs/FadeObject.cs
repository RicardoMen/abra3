﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeObject : MonoBehaviour
{
    [SerializeField] Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        //FadeIn();
    }

    /// <summary>
    /// Triggers fade in animation
    /// </summary>
    void FadeIn()
    {
        animator.SetBool("fadeIn", true);
    }

    /// <summary>
    /// Triggers fade out animation
    /// </summary>
    /// <param name="secs"></param>
    public void FadeOut(int secs)
    {
        switch (secs)
        {
            case 1: animator.SetBool("fadeOut1sec", true); break;
            case 2: animator.SetBool("fadeOut2sec", true); break;
        }
    }

    public void InitFade()
    {
        StartCoroutine(InitFadeCoroutine());
    }

    private IEnumerator InitFadeCoroutine()
    {
        animator.Play("fadeOutx1");
        yield return new WaitForSeconds(1);
    }

    public void FadeSlow() {
        StartCoroutine(InitFadeSlow());
    }

    private IEnumerator InitFadeSlow()
    {
        animator.Play("fadeOutSlow");
        yield return new WaitForSeconds(1);
    }
}
