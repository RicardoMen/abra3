﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CargarEscena : MonoBehaviour {

	public void cargarEscena(string nameEscena)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(nameEscena);
    }
}
