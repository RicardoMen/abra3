using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContadorFinal : MonoBehaviour
{
    public int ContadorRespuestas;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (ContadorRespuestas >= 17)
        {
            FindObjectOfType<FinishGame>().FinishCurrentGame();
        }
    }
    public void Suma() 
    {
        ContadorRespuestas++;
    
    }
}
