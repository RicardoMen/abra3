﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;
public class Console : MonoBehaviour
{
	struct Log
	{
		public string message;
		public string stackTrace;
		public LogType type;
	}
	List<Log> logs = new List<Log>();
	Vector2 scrollPosition;
	public bool show = true;
	public bool collapse;

	// Visual elements:
	void Start(){
		addTxtLog ("###################### " +System.DateTime.Now+" ####################");
	}
	static readonly Dictionary<LogType, Color> logTypeColors = new Dictionary<LogType, Color>()
	{
		{ LogType.Assert, Color.white },
		{ LogType.Error, Color.red },
		{ LogType.Exception, Color.red },
		{ LogType.Log, Color.white },
		{ LogType.Warning, Color.yellow },
	};

	const int margin = 20;

	Rect windowRect = new Rect(margin, margin, Screen.width - (margin * 2), Screen.height - (margin * 2));
	Rect titleBarRect = new Rect(0, 0, 10000, 20);
	GUIContent clearLabel = new GUIContent("Clear", "Clear the contents of the console.");
	GUIContent collapseLabel = new GUIContent("Collapse", "Hide repeated messages.");

	void OnEnable ()
	{
		Application.logMessageReceived+=HandleLog;
	}

	void OnDisable ()
	{
		Application.logMessageReceived-=HandleLog;
	}

	public void Show(){
		show = !show;
	}

	void OnGUI ()
	{
		if (!show) {
			return;
		}

		windowRect = GUILayout.Window(123456, windowRect, ConsoleWindow, "Console");
	}
	void ConsoleWindow (int windowID)
	{
		scrollPosition = GUILayout.BeginScrollView(scrollPosition);
		for (int i = 0; i < logs.Count; i++) {
			var log = logs[i];

			// Combine identical messages if collapse option is chosen.
			if (collapse) {
				var messageSameAsPrevious = i > 0 && log.message == logs[i - 1].message;

				if (messageSameAsPrevious) {
					continue;
				}
			}

			GUI.contentColor = logTypeColors[log.type];
			GUILayout.Label(log.message);
		}

		GUILayout.EndScrollView();

		GUI.contentColor = Color.white;

		GUILayout.BeginHorizontal();

		if (GUILayout.Button(clearLabel)) {
			logs.Clear();
		}

		collapse = GUILayout.Toggle(collapse, collapseLabel, GUILayout.ExpandWidth(false));

		GUILayout.EndHorizontal();
		GUI.DragWindow(titleBarRect);
	}
	void HandleLog (string message, string stackTrace, LogType type)
	{
		addTxtLog(message+" ## "+stackTrace);
		logs.Add(new Log() {
			message = message,
			stackTrace = stackTrace,
			type = type,
		});
	}
	void addTxtLog(string texto){
		string path;
		#if UNITY_ANDROID
		path = "/storage/emulated/0/LogConsole.txt";
		#endif
		#if UNITY_EDITOR ||UNITY_STANDALONE
		if(!Directory.Exists("C:/Reportes/")){
			Directory.CreateDirectory("C:/Reportes/");
		}
		path ="C:/Reportes/LogConsole.txt";
		#endif

		texto = texto + System.Environment.NewLine;
		if (!File.Exists(path))
			File.WriteAllText(path, texto);
		else
			File.AppendAllText(path, texto);
	}

}