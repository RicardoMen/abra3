using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class DisplayTimer: MonoBehaviour
{
    public Text textTimer;

    public float timer = 0.0f;
    public bool isTimer = false;

    private void Update()
    {
        if(isTimer)
        {
            timer += Time.deltaTime;
            DisplayTime();
        }
    }
    void DisplayTime() 
    {
        int minute = Mathf.FloorToInt(timer / 60.0f);
        int second = Mathf.FloorToInt(timer - minute * 60);
        textTimer.text = string.Format("{0:00}:{1:00}", minute, second);

    }
    public void StartTimer()
    {
        isTimer = true;
    }
    public void stopTimer()
    {
        isTimer = false;
    }
    public void RestTimer()
    {
        timer = 0.0f;
    }

    public float GetTotalTimer() {
        return timer;
    }
}
