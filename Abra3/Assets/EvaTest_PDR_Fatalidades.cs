using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvaTest_PDR_Fatalidades : MonoBehaviour
{
  
    //public Dictionary<string, string> parametros = new Dictionary<string, string>();
    public float time = 2f;

    public JSONObject infoJsonSimulacion = new JSONObject(JSONObject.Type.OBJECT);
    IEnumerator Start()
    {
        yield return new WaitForSeconds(5);


        //inicializaci�n de ficha
        EvaluacionVR.current.Nueva();

        //insertando datos manualmente a la ficha

        //EvaluacionVR.current.ficha.AddRUT("11.111.111-1");
        //EvaluacionVR.current.ficha.trabajador.nombre = "Usuario";
        //EvaluacionVR.current.ficha.trabajador.paterno = "Test";
        //EvaluacionVR.current.ficha.trabajador.materno = "Test";
        //EvaluacionVR.current.ficha.trabajador.mail = "test@test.com";
        //EvaluacionVR.current.ficha.AddEmpresa(68);
        //EvaluacionVR.current.ficha.disp_id = 2913;
        //EvaluacionVR.current.ficha.eva_id = 116;

        //tomando los datos desde el controlador de inscripciones
        EvaluacionVR.current.ficha.AddRUT(ControllerLoginUser.Instance.inscripcion.content.ficha.trabajador.rut);
        EvaluacionVR.current.ficha.trabajador.nombre = ControllerLoginUser.Instance.inscripcion.content.ficha.trabajador.nombre;
        EvaluacionVR.current.ficha.trabajador.paterno = ControllerLoginUser.Instance.inscripcion.content.ficha.trabajador.paterno;
        EvaluacionVR.current.ficha.trabajador.materno = ControllerLoginUser.Instance.inscripcion.content.ficha.trabajador.materno;
        EvaluacionVR.current.ficha.trabajador.mail = ControllerLoginUser.Instance.inscripcion.content.ficha.trabajador.mail;

        EvaluacionVR.current.ficha.AddEmpresa(68);//Empresa VRCLASS
        EvaluacionVR.current.ficha.eva_id = FindObjectOfType<GestorVR_WebGL>().eva_id_Request; //Evaluaci�n 116 instalaciones sanitarias
        EvaluacionVR.current.ficha.disp_id = int.Parse(ControllerLoginUser.Instance.inscripcion.content.ficha.disId);

        yield return new WaitForSeconds(time);

        int contador1=0, contador2=0, contador3=0, contador4=0;
        //destreza diagnostico
        for (int i = 2450; i <= 2466; i++)
        {
            int s = Random.Range(0, 2);
            if (s == 0)
            {
                EvaluacionVR.current.ficha.AddRespuesta(i, "SI");
            }
            else
            {
                EvaluacionVR.current.ficha.AddRespuesta(i, "NO");
            }
            contador1++;
        }

        //conocimiento diagnostico
        for (int i = 2432; i <= 2448; i++)
        {
            int s = Random.Range(0, 2);
            if (s == 0)
            {
                EvaluacionVR.current.ficha.AddRespuesta(i, "a");
            }
            else
            {
                EvaluacionVR.current.ficha.AddRespuesta(i, "b");
            }
            contador2++;
        }

        //destreza evaluacion
        for (int i = 2467; i <= 2483; i++)
        {
            int s = Random.Range(0, 2);
            if (s == 0)
            {
                EvaluacionVR.current.ficha.AddRespuesta(i, "SI");
            }
            else
            {
                EvaluacionVR.current.ficha.AddRespuesta(i, "NO");
            }
            contador3++;
        }

        //conocimiento evaluacion
        for (int i = 2415; i <= 2431; i++)
        {
            int s = Random.Range(0, 2);
            if (s == 0)
            {
                EvaluacionVR.current.ficha.AddRespuesta(i, "a");
            }
            else
            {
                EvaluacionVR.current.ficha.AddRespuesta(i, "b");
            }
            contador4++;
        }


        Debug.Log(contador1+" "+contador2+" "+contador3+" "+ contador1);

        //parametros de la inscripci�n, estos si son necesarios SIEMPRE


        //JSONObject parametrosInscripcion = new JSONObject();
        foreach (var item in ControllerLoginUser.Instance.inscripcion.content.ficha.parametros)
        {
            //parametrosInscripcion.AddField(item.Key, item.Value);
            EvaluacionVR.current.ficha.AddParametro(item.Key, item.Value);
        }
        //EvaluacionVR.current.ficha.AddParametro("data", parametrosInscripcion);




        JSONObject timeModule_1 = new JSONObject();
        timeModule_1.Add(Random.Range(400, 600));//tiempo en segundos diagnostico, cambiar por los reales
        timeModule_1.Add(Random.Range(300, 400));//tiempo en segundos entrenamiento,cambiar por los reales
        timeModule_1.Add(Random.Range(400, 600));//tiempo en segundos evaluacion,cambiar por los reales
        infoJsonSimulacion.AddField("Modulo_1", timeModule_1);//array de tiempos en segundos del modulo 1



        EvaluacionVR.current.ficha.AddParametro("Times", infoJsonSimulacion);//se a�ade los arrays almacenados en infojsonsimulacion al params

        yield return new WaitForSeconds(time);

        //Data extra de la simulaci�n
        //agregaremos las respuestas del usuario, que selecciones realiz� en cada situaci�n

        JSONObject situacion_1 = new JSONObject();
        situacion_1.Add("nombre_fatalidad");//indicar el string de la fatalidad seleccionada en el canvas
       
        
        infoJsonSimulacion.AddField("Situacion_1", situacion_1);//array de las selecciones en el canvas para la fatalidad detectada 1

        //..por cada una de ellas de debera agregar el bloque anterior , lo que construir� algo parecido a esto
/*        Situacion_1[3]
            0   :	"espacio confinado"
            1   :	"ninguna"
            2   :	"caida distinto nivel"
*/

        //ficha cerrada y validada
        EvaluacionVR.current.ficha.Cerrar();



    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //envio de la informaci�n a GestorVR
            StartCoroutine(Qualitat.GestorVR.Module.Evaluacion.Ficha.SaveAll());
        }
    }

}


