using Qualitat.Helpers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EvaluacionVRTest115: MonoBehaviour
{

    [System.Serializable]
    public class TestClass
    {
        public string texto;
        public int numero;
    }

    IEnumerator Start()
    {

        yield return new WaitForSeconds(3);

        //inicialización de ficha
        EvaluacionVR.current.Nueva();

        //insertando datos manualmente a la ficha
        EvaluacionVR.current.ficha.AddRUT("11.111.111-1");
        EvaluacionVR.current.ficha.trabajador.nombre = "Usuario";
        EvaluacionVR.current.ficha.trabajador.paterno = "Test";
        EvaluacionVR.current.ficha.trabajador.materno = "Test";
        EvaluacionVR.current.ficha.trabajador.mail = "test@test.com";

        EvaluacionVR.current.ficha.AddEmpresa(32);//Empresa ARAUCO
        EvaluacionVR.current.ficha.eva_id = 115;//Evaluaiocn TestGestorVR

        //respuestas de la evaluación
        EvaluacionVR.current.ficha.AddRespuesta(2182, "b");
        EvaluacionVR.current.ficha.AddRespuesta(2183, "e");
        EvaluacionVR.current.ficha.AddRespuesta(2184, "si");
        EvaluacionVR.current.ficha.AddRespuesta(2185, "no");



       
        //parametros extras a la evaluación, esto es un ejemplo de los tipos de datos que podria recibir exta la ficha del usuario para posterior uso
        EvaluacionVR.current.ficha.AddParametro("Boolean", true);
        EvaluacionVR.current.ficha.AddParametro("Numero", 2);
        EvaluacionVR.current.ficha.AddParametro("Flotante", 2.14F);
        EvaluacionVR.current.ficha.AddParametro("String", "Hola me llamo Test");
        EvaluacionVR.current.ficha.AddParametro("Date", DateTime.Now);

        JSONObject objetoAnidado = new JSONObject();
        objetoAnidado.AddField("atributo_1", "1");
        objetoAnidado.AddField("atributo2", 1);
        EvaluacionVR.current.ficha.AddParametro("Objeto", objetoAnidado);

        JSONObject arrayAnidado = new JSONObject();
        arrayAnidado.Add(1);
        arrayAnidado.Add(2);
        arrayAnidado.Add(3);
        EvaluacionVR.current.ficha.AddParametro("Arreglo", arrayAnidado);

        JSONObject objeto = new JSONObject();
        objeto.AddField("array", arrayAnidado);
        objeto.AddField("objeto", objetoAnidado);

        EvaluacionVR.current.ficha.AddParametro("ObjetoComplejo", objeto);

        TestClass testClass = new TestClass()
        {
            texto = "Texto prueba",
            numero = 2
        };

        EvaluacionVR.current.ficha.AddParametro("class", testClass);


        //ficha cerrada y validada
        EvaluacionVR.current.ficha.Cerrar();

        //StartCoroutine(Qualitat.GestorVR.Module.Evaluacion.Ficha.SaveAll());
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //envio de la información a GestorVR
            StartCoroutine(Qualitat.GestorVR.Module.Evaluacion.Ficha.SaveAll());
        }
    }
}
