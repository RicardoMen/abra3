using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FinishGame : MonoBehaviour
{
    [Header("Posicion y camara")]
    [SerializeField] Transform position;
    [SerializeField] Transform XRPlayer;

    [Header("Panel")]
    [SerializeField] Text message;
    [SerializeField] GameObject endPanel;

    [Header("Audio message")]
    [SerializeField] GameObject finishAudio;
    [SerializeField] GameObject timesUpAudio;
    public GameObject Grua;
    public GameObject GruaX;


    public GameObject TeleportSafe;
    public bool PrimerPiso;
    public bool SegundoPiso;

    [Header("Fade")]
    public FadeObject fade;

    bool timeIsUp;

    public bool TimeIsUp { get => timeIsUp; set => timeIsUp = value; }

    public bool finish = false;

    public void FinishCurrentGame() {
        StartCoroutine(Finish());
    }
     void Start()
    {
        TeleportSafe = GameObject.Find("TeleporterPointerSafe");
    }
    public void DetectorDepiso() {
        if (XRPlayer.transform.position.y <= 2) 
        {
            position.transform.position = new Vector3(-0.67f, -1.49f, -47.14f);
            PrimerPiso = true;
            SegundoPiso = false;
        }
        else 
            if (XRPlayer.transform.position.y >= 4) 
        {
             position.transform.position = new Vector3(-0.67f, 2.20f, -47.14f);
            PrimerPiso = false;
            SegundoPiso = true;
           
          
        }



    }

    public void Anima()
    {
        StartCoroutine(Finish2());
    }

    private void Update()

    {
        if (SceneManager.GetActiveScene().name == "Diagnostico" || SceneManager.GetActiveScene().name == "evaluacion")
        {
            DetectorDepiso();

        }

        if (Input.GetKeyDown(KeyCode.Space))
            Anima();
        if (Input.GetKeyDown(KeyCode.Return))
        {
            TimeIsUp = true;
            FinishCurrentGame();
        }
    }
    IEnumerator Finish() {
        SelectMessage();

        fade.FadeSlow();
        yield return new WaitForSeconds(1.0f);        

        XRPlayer.position = position.position;
        XRPlayer.rotation = position.rotation;

        yield return new WaitForSeconds(2.0f);

        endPanel.SetActive(true);
        FindObjectOfType<GestorVR_PDR_Fatalidades>().Respuestas();

        if (SceneManager.GetActiveScene().name == "Diagnostico" || SceneManager.GetActiveScene().name == "evaluacion")
        {
            TeleportSafe.SetActive(false);

        }
        
    }
    IEnumerator Finish2()
    {
        GameObject H = GameObject.Find("Horquillas");

        fade.FadeSlow();
        yield return new WaitForSeconds(1.0f);
        //XRPlayer.position = position.position;
       // XRPlayer.rotation = position.rotation;
        GruaX.GetComponent<Animator>().enabled=false;
        Grua.transform.position = new Vector3(-25.55f, 0, -18.5f);
        H.GetComponent<MoveThis>().enabled = false;
        Grua.transform.Rotate(0, -90f, 0);

        yield return new WaitForSeconds(2.0f);
    }


    void SelectMessage() {
        
        string scene = SceneManager.GetActiveScene().name.ToLower();

        if (!TimeIsUp)
        {
            switch (scene)
            {
                case "diagnostico":
                    message.text = "DIAGN�STICO\n\nUsted ha terminado esta etapa\nvolveremos al men�.";
                    SetDiagnosticoFinished();
                    break;

                case "formacion":
                    message.text = "FORMACI�N\n\nUsted ha terminado esta etapa\nvolveremos al men�.";
                    SetFormacionFinished();
                    break;

                case "evaluacion":
                    message.text = "EVALUACI�N\n\nUsted ha terminado esta etapa\nvolveremos al men�.";
                    break;

                default:
                    message.text = "FORMACI�N\n\nHas concluido el recorrido.";
                    break;
            }

            finishAudio.SetActive(true);

        }
        else {
            switch (scene)
            {
                case "diagnostico":
                    message.text = "DIAGN�STICO\n\nSe te ha agotado el tiempo.";
                    SetDiagnosticoFinished();
                    break;

                case "evaluacion":
                    message.text = "EVALUACI�N\n\nSe te ha agotado el tiempo.";
                    break;
            }

            timesUpAudio.SetActive(true);
        }
    }
   
    public void ReturnToMainHall() {

        if (!finish)
        {
            message.text = "Regresando al men� principal...";

            if (SceneManager.GetActiveScene().name.ToLower() == "evaluacion")
                // SceneManager.LoadScene("PrincipalScene_DATA_LEGACY_elabra");
                StartCoroutine(Load("PrincipalScene_DATA_LEGACY_elabra"));
            else
                SceneManager.LoadScene("MainHall");

            finish = true;
        }
        else { Debug.Log("ya se presiono el boton que activa ReturnToMainHall"); }
        
    }

    IEnumerator Load(string sceneName)
    {
      //  FindObjectOfType<GestorVR_PDR_Fatalidades>().Respuestas();

        Debug.Log("selecciono los 17 elementos, saliendo por el panel de salida");
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene(sceneName);
    }

    /// <summary>
    /// Finaliza diagnostico indicando que debe activarse el bot�n diagn�stico en la pr�xima carga
    /// </summary>
    public void SetDiagnosticoFinished()
    {
        try
        {
            GameControler.Instance.ActivarFormacion = true;
            GameControler.Instance.ActivarDiagnostico = false;
            GameControler.Instance.ActivarEvaluacion = false;
        }
        catch (System.Exception ex)
        {
            Debug.Log("FinishGame - No se pudo guardar estado de finalizaci�n ya que el objeto no existe. Debe iniciar la escena desde MainHall para evitar errores. " + ex.Message);
        }
    }

    /// <summary>
    /// Finaliza formacion indicando que debe activarse el bot�n diagn�stico en la pr�xima carga
    /// </summary>
    public void SetFormacionFinished()
    {
        try
        {
            GameControler.Instance.ActivarEvaluacion = true;
            GameControler.Instance.ActivarFormacion = false;
            GameControler.Instance.ActivarDiagnostico = false;
        }
        catch (System.Exception ex)
        {
            Debug.Log("FinishGame - No se pudo guardar estado de finalizaci�n ya que el objeto no existe. Debe iniciar la escena desde MainHall para evitar errores. " + ex.Message);
        }
    }

}
