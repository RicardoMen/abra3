﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class FlechaOcultar : MonoBehaviour {
	Toggle t;	

	// Use this for initialization
	void Start () {
		t = GetComponent<Toggle> ();
		t.onValueChanged.AddListener (delegate {
			transform.Rotate (new Vector3 (0, 0, 180));
		});
	}
	
	// Update is called once per frame

}
