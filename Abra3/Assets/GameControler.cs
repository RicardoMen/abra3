using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameControler : MonoBehaviour
{
    public static GameControler Instance;
    bool activarDiagnostico;
    bool activarFormacion;
    bool activarEvaluacion;

    public bool ActivarDiagnostico { get => activarDiagnostico; set => activarDiagnostico = value; }
    public bool ActivarFormacion { get => activarFormacion; set => activarFormacion = value; }
    public bool ActivarEvaluacion { get => activarEvaluacion; set => activarEvaluacion = value; }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            if (Instance != this)
            {
                Destroy(gameObject);
            }
        }
    }

    public void ResetButtonActivation() {
        ActivarDiagnostico = false;
        ActivarFormacion = false;
        ActivarEvaluacion = false;
    }
 
}
