using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestorInsSanitaria : MonoBehaviour
{
    //public Dictionary<string, string> parametros = new Dictionary<string, string>();
    public float time = 2f;

    public JSONObject infoJsonSimulacion = new JSONObject(JSONObject.Type.OBJECT);
    IEnumerator Start()
    {
        yield return new WaitForSeconds(5);

             
        //inicialización de ficha
        EvaluacionVR.current.Nueva();

        //insertando datos manualmente a la ficha

        //EvaluacionVR.current.ficha.AddRUT("11.111.111-1");
        //EvaluacionVR.current.ficha.trabajador.nombre = "Usuario";
        //EvaluacionVR.current.ficha.trabajador.paterno = "Test";
        //EvaluacionVR.current.ficha.trabajador.materno = "Test";
        //EvaluacionVR.current.ficha.trabajador.mail = "test@test.com";
        //EvaluacionVR.current.ficha.AddEmpresa(68);
        //EvaluacionVR.current.ficha.disp_id = 2913;
        //EvaluacionVR.current.ficha.eva_id = 116;

        //tomando los datos desde el controlador de inscripciones
        EvaluacionVR.current.ficha.AddRUT(ControllerLoginUser.Instance.inscripcion.content.ficha.trabajador.rut);
        EvaluacionVR.current.ficha.trabajador.nombre = ControllerLoginUser.Instance.inscripcion.content.ficha.trabajador.nombre;
        EvaluacionVR.current.ficha.trabajador.paterno = ControllerLoginUser.Instance.inscripcion.content.ficha.trabajador.paterno;
        EvaluacionVR.current.ficha.trabajador.materno = ControllerLoginUser.Instance.inscripcion.content.ficha.trabajador.materno;
        EvaluacionVR.current.ficha.trabajador.mail = ControllerLoginUser.Instance.inscripcion.content.ficha.trabajador.mail;

        EvaluacionVR.current.ficha.AddEmpresa(68);//Empresa VRCLASS
        EvaluacionVR.current.ficha.eva_id = FindObjectOfType<GestorVR_WebGL>().eva_id_Request; //Evaluación 116 instalaciones sanitarias
        EvaluacionVR.current.ficha.disp_id = 2913;

        yield return new WaitForSeconds(time);
        for (int i = 2248; i <= 2255; i++)
        {
            int s = Random.Range(0, 2);
            if (s == 0)
            {
                EvaluacionVR.current.ficha.AddRespuesta(i, "no");
            }
            else
            {
                EvaluacionVR.current.ficha.AddRespuesta(i, "si");
            }

        }
        yield return new WaitForSeconds(time);
        for (int i = 2186; i <= 2196; i++)
        {
            int s = Random.Range(0, 3);

            switch (s)
            {
                case 0:
                    EvaluacionVR.current.ficha.AddRespuesta(i, "a");
                    break;
                case 1:
                    EvaluacionVR.current.ficha.AddRespuesta(i, "b");
                    break;
                case 2:
                    EvaluacionVR.current.ficha.AddRespuesta(i, "c");
                    break;

                default:
                    Debug.Log("rango incorrecto");
                    break;
            }


        }
        yield return new WaitForSeconds(time);
        for (int i = 2197; i <= 2216; i++)
        {
            int s = Random.Range(0, 2);
            if (s == 0)
            {
                EvaluacionVR.current.ficha.AddRespuesta(i, "no");
            }
            else
            {
                EvaluacionVR.current.ficha.AddRespuesta(i, "si");
            }
        }

        yield return new WaitForSeconds(time);
        for (int i = 2256; i <= 2263; i++)
        {
            int s = Random.Range(0, 2);
            if (s == 0)
            {
                EvaluacionVR.current.ficha.AddRespuesta(i, "no");
            }
            else
            {
                EvaluacionVR.current.ficha.AddRespuesta(i, "si");
            }
        }
        yield return new WaitForSeconds(time);
        for (int i = 2217; i <= 2227; i++)
        {
            int s = Random.Range(0, 3);

            switch (s)
            {
                case 0:
                    EvaluacionVR.current.ficha.AddRespuesta(i, "a");
                    break;
                case 1:
                    EvaluacionVR.current.ficha.AddRespuesta(i, "b");
                    break;
                case 2:
                    EvaluacionVR.current.ficha.AddRespuesta(i, "c");
                    break;

                default:
                    Debug.Log("rango incorrecto");
                    break;
            }
        }
        yield return new WaitForSeconds(time);
        for (int i = 2228; i <= 2247; i++)
        {
            int s = Random.Range(0, 2);
            if (s == 0)
            {
                EvaluacionVR.current.ficha.AddRespuesta(i, "no");
            }
            else
            {
                EvaluacionVR.current.ficha.AddRespuesta(i, "si");
            }
        }



        //parametros de la inscripción, estos si son necesarios SIEMPRE


        //JSONObject parametrosInscripcion = new JSONObject();
        foreach (var item in ControllerLoginUser.Instance.inscripcion.content.ficha.parametros)
        {
            //parametrosInscripcion.AddField(item.Key, item.Value);
            EvaluacionVR.current.ficha.AddParametro(item.Key, item.Value);
        }
        //EvaluacionVR.current.ficha.AddParametro("data", parametrosInscripcion);




        JSONObject timeModule_1 = new JSONObject();
        timeModule_1.Add(Random.Range(400,600));//tiempo en segundos diagnostico
        timeModule_1.Add(Random.Range(300, 400));//tiempo en segundos entrenamiento
        timeModule_1.Add(Random.Range(400, 600));//tiempo en segundos evaluacion
        infoJsonSimulacion.AddField("Modulo_1", timeModule_1);//array de tiempos en segundos del modulo 1

        JSONObject timeModule_2 = new JSONObject();
        timeModule_2.Add(Random.Range(400, 600));//tiempo en segundos diagnostico
        timeModule_2.Add(Random.Range(300, 400));//tiempo en segundos entrenamiento
        timeModule_2.Add(Random.Range(400, 600));//tiempo en segundos evaluacion
        infoJsonSimulacion.AddField("Modulo_2", timeModule_2);//array de tiempos en segundos del modulo 2

        JSONObject timeModule_3 = new JSONObject();
        timeModule_3.Add(Random.Range(400, 600));//tiempo en segundos diagnostico
        timeModule_3.Add(Random.Range(300, 400));//tiempo en segundos entrenamiento
        timeModule_3.Add(Random.Range(400, 600));//tiempo en segundos evaluacion
        infoJsonSimulacion.AddField("Modulo_3", timeModule_3);//array de tiempos en segundos del modulo 3

        JSONObject timeModule_4 = new JSONObject();
        timeModule_4.Add(Random.Range(400, 600));//tiempo en segundos diagnostico
        timeModule_4.Add(Random.Range(300, 400));//tiempo en segundos entrenamiento
        timeModule_4.Add(Random.Range(400, 600));//tiempo en segundos evaluacion
        infoJsonSimulacion.AddField("Modulo_4", timeModule_4);//array de tiempos en segundos del modulo 4


        EvaluacionVR.current.ficha.AddParametro("Times", infoJsonSimulacion);//se añade los arrays almacenados en infojsonsimulacion al params


        yield return new WaitForSeconds(time);

        //ficha cerrada y validada
        EvaluacionVR.current.ficha.Cerrar();



    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            //envio de la información a GestorVR
            StartCoroutine(Qualitat.GestorVR.Module.Evaluacion.Ficha.SaveAll());
        }
    }

}
