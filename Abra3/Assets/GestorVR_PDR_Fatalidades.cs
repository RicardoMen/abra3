using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GestorVR_PDR_Fatalidades : MonoBehaviour
{
    public static GestorVR_PDR_Fatalidades current;
    //public Dictionary<string, string> parametros = new Dictionary<string, string>();
    public enum Modo  {Ninguno,Diagnostico,Formacion, Evaluacion}
    public Modo modo = Modo.Ninguno;
    public float time = 2f;
    int contador1 = 0, contador2 = 0, contador3 = 0, contador4 = 0;
    public bool inicializacion = false;
    CountDown timer;
    DisplayTimer timerFormacion;
    float segundosTotales;
    JSONObject timeModule_1 = new JSONObject();
    public JSONObject infoJsonFatalidadesSeleccionadas = new JSONObject(JSONObject.Type.OBJECT);
    public JSONObject infoJsonTimes = new JSONObject(JSONObject.Type.OBJECT);
 

    void Awake()
    {
        

        if (current == null)
        {
           
           // DontDestroyOnLoad(gameObject);
            current = this;
        }
        else
        {
            if (current != this)
            {
                Destroy(gameObject);
            }
        }
    }

    IEnumerator Start()
    {
        //timer = FindObjectOfType<CountDown>();
        //segundosTotales = timer.TimeLeft * 60;//conversion a segundos de los minutos en timeleft

        yield return new WaitForSeconds(time);

        switch (modo)
        {
            case Modo.Diagnostico:
                Debug.Log("DIAGNOSTICO");

                timer = FindObjectOfType<CountDown>();
                segundosTotales = timer.TimeLeft * 60;//conversion a segundos de los minutos en timeleft
                Debug.Log(segundosTotales);
                Diagnostico();
                
                break;
            case Modo.Formacion:
                Debug.Log("FORMACION");
                timerFormacion = FindObjectOfType<DisplayTimer>();
               // segundosTotales = timerFormacion.GetTotalTimer();

                break;
            case Modo.Evaluacion:
                Debug.Log("EVALUACION");
                timer = FindObjectOfType<CountDown>();
                segundosTotales = timer.TimeLeft * 60;//conversion a segundos de los minutos en timeleft
                Debug.Log(segundosTotales);
                Evaluacion();
                
                break;

            default:
                Debug.Log("NO HA SELECCIONADO MODO");
                break;
        }

        //JSONObject timeModule_1 = new JSONObject();
        //timeModule_1.Add(GetTime());//tiempo en segundos diagnostico
        //timeModule_1.Add(GetTime());//tiempo en segundos entrenamiento
        //timeModule_1.Add(GetTime());//tiempo en segundos evaluacion
        //infoJsonSimulacion.AddField("Modulo_1", timeModule_1);//array de tiempos en segundos del modulo 1




        yield return new WaitForSeconds(time);


        //ficha cerrada y validada
        //EvaluacionVR.current.ficha.Cerrar();

        //yield return new WaitForSeconds(5);
        //Debug.Log(GetTime());

    }

    //al inicio seran contestadas todas las preguntas como incorrectas en diagnostico
    public void Diagnostico()
    {
        //destreza diagnostico
        for (int i = 2450; i <= 2466; i++)
        {
            EvaluacionVR.current.ficha.AddRespuesta(i, "NO");
            contador1++;
        }

        //conocimiento diagnostico
        for (int i = 2432; i <= 2448; i++)
        {          
            EvaluacionVR.current.ficha.AddRespuesta(i, "b");
            contador2++;
        }
        Debug.Log("Preguntas contestadas en diagnostico al inicializar: "+contador1 + " + " + contador2);
    }
    //al inicio seran contestadas todas las preguntas como incorrectas en evaluacion
    public void Evaluacion()
    {
        //destreza evaluacion
        for (int i = 2467; i <= 2483; i++)
        {
            EvaluacionVR.current.ficha.AddRespuesta(i, "NO");
            contador3++;
        }

        //conocimiento evaluacion
        for (int i = 2415; i <= 2431; i++)
        {
            EvaluacionVR.current.ficha.AddRespuesta(i, "b");
            contador4++;
        }
        Debug.Log("Preguntas contestadas en evaluacion al inicializar: " + contador3 + " + " + contador4);
    }

    public int GetTime()
    {

        float segundos = 0;
        
        int restante = (int)timer.TimeLeft;
        Debug.Log(restante);


        segundos = 900 - restante;
        
        int s =(int)segundos;
        Debug.Log("el tiempo obtenido es: "+s);
        return s;

    }

    public int GetTimeFormacion()
    {
        //return (int)segundosTotales;
        float segundos = timerFormacion.timer;
        int s = Convert.ToInt32(segundos);
        Debug.Log("el tiempo obtenido es: " + s);
        return s;
    }


    public void SetRespuesta(bool respuesta, int id_conocimiento, List<int> lista , int id_destreza)
    {
        if (respuesta)
        {
            EvaluacionVR.current.ficha.AddRespuesta(id_conocimiento, "a");//match respuesta correcta
        }
        else
        {
            EvaluacionVR.current.ficha.AddRespuesta(id_conocimiento, "b");//match respuesta incorrecta
        }
        if (lista.Count != 0)
        {
            EvaluacionVR.current.ficha.AddRespuesta(id_destreza, "SI");//selecciono situacion y completo lista?
        }
        else
        {
            EvaluacionVR.current.ficha.AddRespuesta(id_destreza, "NO");//selecciono situacion y completo lista?
        }



    }
    public string GetFatalidadString(int num)
    {
        string nombre = "no existe";
        switch (num)
        {
            case 1:
                nombre = "COLISION O VOLCAMIENTO VEHICULAR / EQUIPO";
                 break;
            case 2:
                nombre = "IMPACTO DE VEHICULO A PERSONA";
                break;
            case 3:
                nombre = "IMPACTO DE TREN A PERSONA";
                break;
            case 4:
                nombre = "COLISION DE TREN CON VEHICULOS / EQUIPOS";
                break;
            case 5:
                nombre = "ESPACIOS CONFINADOS";
                break;
            case 6:
                nombre = "FALLAS DEL TERRENO, DERRUMBES";
                break;
            case 7:
                nombre = "EXPOSICION A SUSTANCIAS PELIGROSAS AGUDA";
                break;
            case 8:
                nombre = "INCENDIO";
                break;
            case 9:
                nombre = "AHOGAMIENTO";
                break;
            case 10:
                nombre = "ATRAPAMIENTO O APLASTAMIENTO";
                break;
            case 11:
                nombre = "CONTACTO CON ELECTRICIDAD";
                break;
            case 12:
                nombre = "EXPOSICION A SUSTANCIAS PELIGROSAS CRONICAS";
                break;
            case 13:
                nombre = "TRONADURA";
                break;
            case 14:
                nombre = "LIBERACION NO CONTROLADA DE ENERGIA";
                break;
            case 15:
                nombre = "OPERACIONES DE IZAJE / CARGAS SUSPENDIDAS";
                break;
            case 16:
                nombre = "CAIDA DE OBJETOS";
                break;
            case 17:
                nombre = "CAIDAS DE DISTINTO NIVEL";
                break;
            case 18:
                nombre = "NINGUNA";
                break;
            default:
                Debug.Log("Item Fatalidad no encontrado");
                break;
        }
        return nombre;
    }
   
    public void Respuestas()
    {
        if(modo == Modo.Diagnostico)
        {
            foreach (var item in FindObjectsOfType<CodigoToggle>())
            {
                switch (item.IdPregunta)
                {
                    case 1:
                        SetRespuesta(item.Respuesta,2432,item.RespuestasUsuario,2450);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Diagnostico");
                        break;
                    case 2:
                        SetRespuesta(item.Respuesta,2433, item.RespuestasUsuario,2451);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Diagnostico");
                        break;
                    case 3:
                        SetRespuesta(item.Respuesta,2434, item.RespuestasUsuario,2452);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Diagnostico");
                        break;
                    case 4:
                        SetRespuesta(item.Respuesta,2435, item.RespuestasUsuario,2453);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Diagnostico");
                        break;
                    case 5:
                        SetRespuesta(item.Respuesta,2436, item.RespuestasUsuario,2454);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Diagnostico");
                        break;
                    case 6:
                        SetRespuesta(item.Respuesta,2437, item.RespuestasUsuario,2455);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Diagnostico");
                        break;
                    case 7:
                        SetRespuesta(item.Respuesta,2438, item.RespuestasUsuario,2456);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Diagnostico");
                        break;
                    case 8:
                        SetRespuesta(item.Respuesta,2439, item.RespuestasUsuario,2457);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Diagnostico");
                        break;
                    case 9:
                        SetRespuesta(item.Respuesta,2440, item.RespuestasUsuario,2458);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Diagnostico");
                        break;
                    case 10:
                        SetRespuesta(item.Respuesta,2441, item.RespuestasUsuario,2459);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Diagnostico");
                        break;
                    case 11:
                        SetRespuesta(item.Respuesta,2442, item.RespuestasUsuario,2460);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Diagnostico");
                        break;
                    case 12:
                        SetRespuesta(item.Respuesta,2443, item.RespuestasUsuario,2461);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Diagnostico");
                        break;
                    case 13:
                        SetRespuesta(item.Respuesta,2444, item.RespuestasUsuario,2462);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Diagnostico");
                        break;
                    case 14:
                        SetRespuesta(item.Respuesta,2445, item.RespuestasUsuario,2463);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Diagnostico");
                        break;
                    case 15:
                        SetRespuesta(item.Respuesta,2446, item.RespuestasUsuario,2464);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Diagnostico");
                        break;
                    case 16:
                        SetRespuesta(item.Respuesta,2447, item.RespuestasUsuario,2465);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Diagnostico");
                        break;
                    case 17:
                        SetRespuesta(item.Respuesta,2448, item.RespuestasUsuario,2466);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Diagnostico");
                        break;
                    default:
                        Debug.Log("Item Fatalidad no encontrado");
                        break;
                }
            }

            // timeModule_1.Add(GetTime());//tiempo en segundos diagnostico
            InicializacionEvaluacion.current.tiempos.Add(GetTime());
        }
        if(modo == Modo.Formacion)
        {
            //timeModule_1.Add(GetTime());//tiempo en segundos formacion
            InicializacionEvaluacion.current.tiempos.Add(GetTimeFormacion());
        }
        if (modo == Modo.Evaluacion)
        {
            foreach (var item in FindObjectsOfType<CodigoToggle>())
            {
                switch (item.IdPregunta)
                {
                    case 1:
                        SetRespuesta(item.Respuesta,2415, item.RespuestasUsuario,2467);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Evaluacion");
                        break;
                    case 2:
                        SetRespuesta(item.Respuesta,2416, item.RespuestasUsuario,2468);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Evaluacion");
                        break;
                    case 3:
                        SetRespuesta(item.Respuesta,2417, item.RespuestasUsuario,2469);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Evaluacion");
                        break;
                    case 4:
                        SetRespuesta(item.Respuesta,2418, item.RespuestasUsuario,2470);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Evaluacion");
                        break;
                    case 5:
                        SetRespuesta(item.Respuesta,2419, item.RespuestasUsuario,2471);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Evaluacion");
                        break;
                    case 6:
                        SetRespuesta(item.Respuesta,2420, item.RespuestasUsuario,2472);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Evaluacion");
                        break;
                    case 7:
                        SetRespuesta(item.Respuesta,2421, item.RespuestasUsuario,2473);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Evaluacion");
                        break;
                    case 8:
                        SetRespuesta(item.Respuesta,2422, item.RespuestasUsuario,2474);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Evaluacion");
                        break;
                    case 9:
                        SetRespuesta(item.Respuesta,2423, item.RespuestasUsuario,2475);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Evaluacion");
                        break;
                    case 10:
                        SetRespuesta(item.Respuesta,2424, item.RespuestasUsuario,2476);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Evaluacion");
                        break;
                    case 11:
                        SetRespuesta(item.Respuesta,2425, item.RespuestasUsuario,2477);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Evaluacion");
                        break;
                    case 12:
                        SetRespuesta(item.Respuesta,2426, item.RespuestasUsuario,2478);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Evaluacion");
                        break;
                    case 13:
                        SetRespuesta(item.Respuesta,2427, item.RespuestasUsuario,2478);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Evaluacion");
                        break;
                    case 14:
                        SetRespuesta(item.Respuesta,2428, item.RespuestasUsuario,2480);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Evaluacion");
                        break;
                    case 15:
                        SetRespuesta(item.Respuesta,2429, item.RespuestasUsuario,2481);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Evaluacion");
                        break;
                    case 16:
                        SetRespuesta(item.Respuesta,2430, item.RespuestasUsuario,2482);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Evaluacion");
                        break;
                    case 17:
                        SetRespuesta(item.Respuesta,2431, item.RespuestasUsuario,2483);
                        ParamsFatalidadesSeleccionadas(item.IdPregunta, item.RespuestasUsuario, "Evaluacion");
                        break;
                    default:
                        Debug.Log("Item Fatalidad no encontrado");
                        break;
                }
            }
            //timeModule_1.Add(GetTime());//tiempo en segundos evaluacion
            InicializacionEvaluacion.current.tiempos.Add(GetTime());
            timeModule_1.Add(InicializacionEvaluacion.current.tiempos[0]);
            timeModule_1.Add(InicializacionEvaluacion.current.tiempos[1]);
            timeModule_1.Add(InicializacionEvaluacion.current.tiempos[2]);


            infoJsonTimes.AddField("Modulo_1", timeModule_1);//cerramos y apilamos array de tiempos en segundos del modulo 1

            EvaluacionVR.current.ficha.AddParametro("Times", infoJsonTimes);//se a�ade los arrays almacenados en infojsonsimulacion al params
            EvaluacionVR.current.ficha.AddParametro("FatalidadesSeleccionadasDiagnostico", InicializacionEvaluacion.current.infoJsonFatalidadesSeleccionadasDiagnostico);//se a�ade los arrays almacenados en infojsonsimulacion al params
            EvaluacionVR.current.ficha.AddParametro("FatalidadesSeleccionadasEvaluacion", InicializacionEvaluacion.current.infoJsonFatalidadesSeleccionadasEvaluacion);//se a�ade los arrays almacenados en infojsonsimulacion al params
            Debug.Log(infoJsonFatalidadesSeleccionadas.Print());

            EvaluacionVR.current.ficha.Cerrar();
        }


    }
    
    public void ParamsFatalidadesSeleccionadas(int id_situacion, List<int> lista, string tipo)
    {
        //Data extra de la simulaci�n
        //agregaremos las respuestas del usuario, que selecciones realiz� en cada situaci�n

        JSONObject situacion = new JSONObject();
        if (lista.Count != 0)
        {
            foreach (var item in lista)
            {
                situacion.Add(GetFatalidadString(item));//indicar el string de la fatalidad seleccionada en el canvas
            }

        }
        else { 
            Debug.Log("la lista no tiene datos");
            situacion.Add("NO CONTIENE DATOS");

             }


        string situacion_id = "Situacion_" + id_situacion.ToString();

        if(tipo == "Diagnostico")
        {
            InicializacionEvaluacion.current.infoJsonFatalidadesSeleccionadasDiagnostico.AddField(situacion_id, situacion);//array de las selecciones en el canvas para la fatalidad detectada 1

            Debug.Log(InicializacionEvaluacion.current.infoJsonFatalidadesSeleccionadasDiagnostico.Print());
        }
        if (tipo == "Evaluacion")
        {
            InicializacionEvaluacion.current.infoJsonFatalidadesSeleccionadasEvaluacion.AddField(situacion_id, situacion);//array de las selecciones en el canvas para la fatalidad detectada 1

            Debug.Log(InicializacionEvaluacion.current.infoJsonFatalidadesSeleccionadasEvaluacion.Print());
        }


        // infoJsonFatalidadesSeleccionadas.Clear();//se recicla el json para la siguiente iteracion
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GetTime();
           // Respuestas();
            //envio de la informaci�n a GestorVR
           // StartCoroutine(Qualitat.GestorVR.Module.Evaluacion.Ficha.SaveAll());
        }
    }
}
