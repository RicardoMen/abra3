using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InicializacionEvaluacion : MonoBehaviour
{
    public static InicializacionEvaluacion current;
    public bool completada = false;
    public List<int> tiempos = new List<int>();
    public JSONObject infoJsonFatalidadesSeleccionadasDiagnostico = new JSONObject(JSONObject.Type.OBJECT);
    public JSONObject infoJsonFatalidadesSeleccionadasEvaluacion = new JSONObject(JSONObject.Type.OBJECT);
    void Awake()
    {


        if (current == null)
        {

             DontDestroyOnLoad(gameObject);
            current = this;
        }
        else
        {
            if (current != this)
            {
                Destroy(gameObject);
            }
        }
    }

    IEnumerator Start()
    {
        yield return new WaitForSeconds(5);

        if (!completada)
        {
            Debug.Log("inicializando ficha");
            //inicialización de ficha
            EvaluacionVR.current.Nueva();

            //insertando datos manualmente a la ficha

            //tomando los datos desde el controlador de inscripciones
            EvaluacionVR.current.ficha.AddRUT(ControllerLoginUser.Instance.inscripcion.content.ficha.trabajador.rut);
            EvaluacionVR.current.ficha.trabajador.nombre = ControllerLoginUser.Instance.inscripcion.content.ficha.trabajador.nombre;
            EvaluacionVR.current.ficha.trabajador.paterno = ControllerLoginUser.Instance.inscripcion.content.ficha.trabajador.paterno;
            EvaluacionVR.current.ficha.trabajador.materno = ControllerLoginUser.Instance.inscripcion.content.ficha.trabajador.materno;
            EvaluacionVR.current.ficha.trabajador.mail = ControllerLoginUser.Instance.inscripcion.content.ficha.trabajador.mail;

            EvaluacionVR.current.ficha.AddEmpresa(68);//Empresa VRCLASS
            EvaluacionVR.current.ficha.eva_id = FindObjectOfType<GestorVR_WebGL>().eva_id_Request;
            EvaluacionVR.current.ficha.disp_id = int.Parse(ControllerLoginUser.Instance.inscripcion.content.ficha.disId);

            yield return new WaitForSeconds(2);



            //parametros de la inscripción, estos si son necesarios SIEMPRE


            //JSONObject parametrosInscripcion = new JSONObject();
            foreach (var item in ControllerLoginUser.Instance.inscripcion.content.ficha.parametros)
            {
                //parametrosInscripcion.AddField(item.Key, item.Value);
                EvaluacionVR.current.ficha.AddParametro(item.Key, item.Value);
            }
            //EvaluacionVR.current.ficha.AddParametro("data", parametrosInscripcion);



        }
        else
        {
            Debug.Log("la ficha fue inicializada");
        }

    }

}
