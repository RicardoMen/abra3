using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Autohand.Demo;
using Autohand;

public class ChangeRay : MonoBehaviour
{
    [SerializeField] XRControllerEvent controllerEvent;
    [SerializeField] HandCanvasPointer handCanvasPointer;

    //[SerializeField] RaycastNuevo raycastNuevo;






    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void RayAutoHands(bool state)
    {
        controllerEvent.enabled = state;
        handCanvasPointer.enabled = state;
        //raynew.enabled = !state;
        //linea.enabled = !state;
    }
}
