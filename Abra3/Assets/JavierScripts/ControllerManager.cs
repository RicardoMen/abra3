﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;
using UnityEngine.InputSystem;
using System;

public class ControllerManager : MonoBehaviour
{
    [Header("Menu")]
    [SerializeField] Menu zoneMenu;
    
    [Header("Tools")]
    [SerializeField] GameObject[] testLeadsHelp;

    [Header("Hands")]
    [SerializeField] MeshRenderer[] hands;

    [Header("Camera")]
    [SerializeField] Transform cameraObject;
    [SerializeField] Vector3 originalPosition;
    [SerializeField] Transform xrRig;
    [SerializeField] Transform[] cameraPositions;
    float thresholdPosition;

    [SerializeField] RaycastNuevo raycastNuevo;
   

    [Header("Managers")]
    //AudioManager audioManager;
    //[SerializeField] Manager manager;

    float timer;
    float timerLimit = 2.0f;
    bool exitSimulation;
    bool testLeadsActivated;
    bool isMenuActivated;
    bool isObjectivesActivated;
    int currentIndex;
    

    [Header("XR Inputs")]
    [SerializeField] InputActionAsset inputAction;
    InputAction thumbstickLeft;
    InputAction thumbstickRight;

    [Header("Current scenario")]
    [SerializeField] bool isFreeView;

    private void Start()
    {
        thresholdPosition = 1.3f;

        ChangeHandVisibility(true);

        InitializeVrControllers();

        //audioManager = FindObjectOfType<AudioManager>();
        raycastNuevo= FindObjectOfType<RaycastNuevo>();
        
    }

    public void ShowMenu(bool activate) {
        //zoneMenu.ActivateMenu(activate);
    }

    public void ShowObjectives(bool activate)
    {
        //zoneMenu.ActivateObjectives(activate);
    }

    /*void ShowTestLeads(bool activate) {

        manager.ActivateProbes(activate);

        if (activate)
        {
            ChangeHandVisibility(false);
            testLeadsActivated = true;
        }
        else if (!activate)
        {
            ChangeHandVisibility(true);
            testLeadsActivated = false;
        }

        //Activating helpers
        foreach (GameObject go in testLeadsHelp) {
            go.SetActive(activate);
        }
    }*/

    /// <summary>
    /// Changes hands materials
    /// </summary>
    /// <param name="visible">Material alpha</param>
    void ChangeHandVisibility(bool visible) {

        foreach (MeshRenderer mesh in hands)
        {
            mesh.enabled = visible;
        }
    }

    /// <summary>
    /// Resets current camera position
    /// </summary>
    void ResetCameraPosition() {
        cameraObject.localPosition = originalPosition;
    }

    /// <summary>
    /// Checks controller input in order to change camera position
    /// </summary>
    void ChangeCameraPosition() {
        //Right
        Vector2 primaryAxis = thumbstickRight.ReadValue<Vector2>();
        if (primaryAxis.y >= 0.8f)
        {
            Debug.Log("move up");
            cameraObject.localPosition =
                new Vector3(
                    cameraObject.localPosition.x,
                    cameraObject.localPosition.y + 0.001f,
                    cameraObject.localPosition.z);
        }
        else if (primaryAxis.y <= -0.8f)
        {
            Debug.Log("move down");

            if (cameraObject.localPosition.y > thresholdPosition)
            {
                cameraObject.localPosition =
                    new Vector3(
                        cameraObject.localPosition.x,
                        cameraObject.localPosition.y - 0.001f,
                        cameraObject.localPosition.z);
            }
        }
    }

    /*void ChangeVolume() {
        //Left
        Vector2 primaryAxis = thumbstickLeft.ReadValue<Vector2>();
        float currentVolume = audioManager.GetAudioSourceVolume();

        if (primaryAxis.y >= 0.8f)
        {
            if (currentVolume < 0.1f)
                audioManager.ChangeAudioSourceVolume(0.01f);

        }
        else if (primaryAxis.y <= -0.8f)
        {
            if (currentVolume > 0.0f)
                audioManager.ChangeAudioSourceVolume(-0.01f);
        }
    }*/

    /// <summary>
    /// Changes current camera position
    /// </summary>
    /// <param name="index">Amount of positions to advance/return</param>
    void ChangePosition(int index)
    {
        currentIndex = currentIndex + index;

        if (currentIndex >= cameraPositions.Length)
            currentIndex = 0;
        else if (currentIndex < 0)
            currentIndex = cameraPositions.Length - 1;

        //xrRig.localPosition = cameraPositions[currentIndex].localPosition;
    }

    #region XR INPUT CONTROLLERS
    void InitializeVrControllers() {
        //Right hand
        var buttonA = inputAction.FindActionMap("RightHand").FindAction("ButtonA");
        buttonA.Enable();
        buttonA.performed += OnButtonPressedA;

        var buttonB = inputAction.FindActionMap("RightHand").FindAction("ButtonB");
        buttonB.Enable();
        buttonB.performed += OnButtonPressedB;

        var triggerRight = inputAction.FindActionMap("RightHand").FindAction("Trigger");
        triggerRight.Enable();
        triggerRight.performed += OnRightTriggerPressed;

        var thumbButtonRight = inputAction.FindActionMap("RightHand").FindAction("Thumbstick");
        thumbButtonRight.Enable();
        thumbButtonRight.performed += OnRightThumbstickPressed;

        thumbstickRight = inputAction.FindActionMap("RightHand").FindAction("Axis");
        thumbstickRight.Enable();

        var gripRight = inputAction.FindActionMap("RightHand").FindAction("Grip");
        gripRight.Enable();
        gripRight.performed += OnRightGripPressed;

        //Left hand
        var buttonX = inputAction.FindActionMap("LeftHand").FindAction("ButtonX");
        buttonX.Enable();
        buttonX.performed += OnButtonPressedX;

        var buttonY = inputAction.FindActionMap("LeftHand").FindAction("ButtonY");
        buttonY.Enable();
        buttonY.performed += OnButtonPressedY;

        var triggerLeft = inputAction.FindActionMap("LeftHand").FindAction("Trigger");
        triggerLeft.Enable();
        triggerLeft.performed += OnLeftTriggerPressed;

        var thumbButtonLeft = inputAction.FindActionMap("LeftHand").FindAction("Thumbstick");
        thumbButtonLeft.Enable();
        thumbButtonLeft.performed += OnLeftThumbstickPressed;

        thumbstickLeft = inputAction.FindActionMap("LeftHand").FindAction("Axis");
        thumbstickLeft.Enable();

        var gripLeft = inputAction.FindActionMap("LeftHand").FindAction("Grip");
        gripLeft.Enable();
        gripLeft.performed += OnLeftGripPressed;
    }

    void OnButtonPressedA(InputAction.CallbackContext obj) {
        if (!isMenuActivated)
        {
            isMenuActivated = true;
            ShowMenu(true);
        }
        else if (isMenuActivated)
        {
            isMenuActivated = false;
            ShowMenu(false);
        }
    }

    void OnButtonPressedB(InputAction.CallbackContext obj)
    {
        if (!isFreeView)
        {
            if (PlayerPrefs.GetInt("exercise", 1) == 2)
            {
                if (!isObjectivesActivated)
                {
                    isObjectivesActivated = true;
                    ShowObjectives(true);
                }
                else if (isObjectivesActivated)
                {
                    isObjectivesActivated = false;
                    ShowObjectives(false);
                }
            }
        }
    }

    void OnButtonPressedX(InputAction.CallbackContext obj)
    {
        if (!isFreeView)
        {
           // ShowTestLeads(false);
        }        
    }

    void OnButtonPressedY(InputAction.CallbackContext obj) {
        if (!isFreeView)
        {
            //ShowTestLeads(true);
        }
    }

    void OnLeftThumbstickPressed(InputAction.CallbackContext obj)
    {
       // if (PlayerPrefs.GetInt("exercise", 1) == 2)
            //FindObjectOfType<StageManager>().PlayInstructionClip();
    }

    void OnRightThumbstickPressed(InputAction.CallbackContext obj)
    {
       // ResetCameraPosition();
    }

    void OnLeftTriggerPressed(InputAction.CallbackContext obj)
    {
        ChangePosition(-1);
        Debug.Log("gatillo izquierdo");
    }

    void OnRightTriggerPressed(InputAction.CallbackContext obj)
    {
        ChangePosition(1);
        //Debug.Log("gatillo derecho");
        raycastNuevo.activandoboton(true);
    }

    private void OnLeftGripPressed(InputAction.CallbackContext obj)
    {
        /*if (testLeadsActivated)
        {
            manager.ChangeProbeParent(-1);
        }*/
    }

    private void OnRightGripPressed(InputAction.CallbackContext obj)
    {
        /*if (testLeadsActivated)
        {
            manager.ChangeProbeParent(1);
        }*/
    }

    #endregion

    // Update is called once per frame
    void Update()
    {
        //AUDIO AND CAMERA
        /*if (!isFreeView)
            ChangeVolume();

        ChangeCameraPosition();*/
    }
}
