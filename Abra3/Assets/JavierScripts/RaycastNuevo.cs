using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Autohand.Demo;
using UnityEngine.UI;
using Autohand;

public class RaycastNuevo : MonoBehaviour
{
    
    [SerializeField] XRControllerEvent controllerEvent;
    [SerializeField] HandCanvasPointer handCanvasPointer;
    [SerializeField] ControllerManager controllerManager;
    [SerializeField] ChangeRay changeRay;
    public float rayDistance = 300f;
    public float maxDistance = 5f;
    private Ray ray;
    private Vector3 rayDirection;
    public bool botonon;
    public LineRenderer linea;
    public RaycastNuevo raynew;

    public GameObject canvasCamera;
    public LayerMask canvasLayerMask;
    public float canvasRayDistance= 3f;
    bool canvasRaycast;


    [SerializeField] Material closeMat;
    [SerializeField] Material farMat;
    private void Awake()
    {
        linea = GetComponent<LineRenderer>();
        raynew = GetComponent<RaycastNuevo>();
    }

    // Start is called before the first frame update
    void Start()
    {
        rayDirection = transform.rotation * Vector3.forward;
        
    }

    // Update is called once per frame
    void Update()
    {

        Createrayo();
        //Debug.Log("acceder pregunta"+ botonon);
        activandoboton(false);
    }

          
        public void RayAutoHands(bool state)
        {
            controllerEvent.enabled = state;
            handCanvasPointer.enabled = state;
            
            raynew.enabled = !state;
            linea.enabled = !state;        
        }

   
        
        void Createrayo() 
        { 
            rayDirection = transform.rotation* Vector3.forward;
            ray.origin = transform.position;
            //ray.direction = transform.forward;

            RaycastHit hit;
            
            if (Physics.Raycast(ray.origin, rayDirection, out hit, rayDistance/*, UILayer*/))
            {
                linea.SetPosition(0, ray.origin);
                linea.SetPosition(1, hit.point);
                //float actualDistance = Vector3.Distance(ray.origin, hit.point);
                //Debug.Log("distancia de rayo"+actualDistance);
                //float maxDistance = 1f; // Cambia este valor por la distancia m�xima deseada

                // Verificar si la distancia es menor o igual a la distancia m�xima deseada

                if (hit.collider !=null)
                {
                linea.SetPosition(1, hit.point);
                }

                Debug.DrawRay(transform.position, rayDirection * rayDistance, Color.green);
                linea.material = farMat;

                if (hit.transform.tag == "pregunta")
                {
                    //Debug.Log("aqui se debe activar la pregunta OJO");
                    Debug.DrawRay(transform.position, rayDirection * rayDistance, Color.red);
                    linea.material = closeMat;

                    Renderer renderer = hit.transform.GetComponent<Renderer>();
                    if (renderer != null)
                    {
                        renderer.material.color = Color.red; 
                    }else{
                    
                    if (renderer == null)
                    {
                        // Restaurar el color original del material
                        renderer.material.color = Color.green ;
                    }
                }
                float distanceToImpact = hit.distance;
                    //Debug.Log("Distancia al punto de impacto: " + distanceToImpact);

                    if (distanceToImpact <= maxDistance)
                    {
                       // Debug.Log("acciona el boton que activa la pregunta ");

                        if (botonon)
                        {
                        Debug.Log("activando pregunta "+ botonon + " "+ hit.collider.name);

                            if (hit.collider.GetComponent<Button>())
                            {

                                // Acceder al componente Button y activar su comportamiento       
                                hit.collider.GetComponent<Button>().onClick.Invoke();
                                //RayAutoHands(true);
                            //activandoboton(false);
                                //canvasRaycast=true;
                            }
                        }

                    }

                 

                
                }
            }
        // Raycast hacia el canvas
        if (canvasRaycast)
        {
            RaycastHit canvasHit;
            Vector3 canvasRayDirection = hit.point - canvasCamera.transform.position;

            if (Physics.Raycast(canvasCamera.transform.position, canvasRayDirection, out canvasHit, canvasRayDistance, canvasLayerMask))
            {
                // Realiza las acciones necesarias cuando el rayo del canvas colisiona con un objeto
                
            }
        }
    }

    public void activandoboton(bool state)
    {
        botonon = state;
    }
}
