using UnityEngine;
using UnityEngine.UI;

public class MainHallManager : MonoBehaviour
{
    [SerializeField] Button buttonDiagnostico;
    [SerializeField] Button buttonFormacion;
    [SerializeField] Button buttonEvaluacion;

    [SerializeField] bool isDebugMode;

    private void Start()
    {
        if (isDebugMode)
            SetAllButtonsTrue();
        else
            ActivateCurrentStageButton();
    }

    private void ActivateCurrentStageButton()
    {
        buttonDiagnostico.interactable = GameControler.Instance.ActivarDiagnostico;
        buttonFormacion.interactable = GameControler.Instance.ActivarFormacion;
        buttonEvaluacion.interactable = GameControler.Instance.ActivarEvaluacion;
    }

    void SetAllButtonsTrue() {
        buttonDiagnostico.interactable = true;
        buttonFormacion.interactable = true;
        buttonEvaluacion.interactable = true;
    }
}
