using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Menu : MonoBehaviour
{

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void IrTutorial()
    {
        //SceneManager.LoadScene(1);
        FindObjectOfType<FadeObject>().InitFade();
        SceneManager.LoadSceneAsync("LoadingTutorial");
    }
    public void IrDiagnostico()
    {
        //SceneManager.LoadScene(2);
        FindObjectOfType<FadeObject>().InitFade();
        SceneManager.LoadSceneAsync("LoadingDiagnostico");
    }
    public void IrFormacion()
    {
        //SceneManager.LoadScene(3);
        FindObjectOfType<FadeObject>().InitFade();
        SceneManager.LoadSceneAsync("LoadingFormacion");
    }
    public void IrEvaluacion()
    {
        //SceneManager.LoadScene(4);
        FindObjectOfType<FadeObject>().InitFade();
        SceneManager.LoadSceneAsync("LoadingEvaluacion");
    }
    void TerminoTutorial()
    {

    }
    void TerminoDiagnostico()
    {

    }

}
