using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionManager : MonoBehaviour
{
   // [SerializeField] GameObject canvasBotonInvisible;
    [SerializeField] Button continuar;
    [SerializeField] Button regresar;

    FormacionManager formacionManager;

    // Start is called before the first frame update
    void OnEnable()
    {
        SetListeners();
        StartCoroutine(ShowButtons());
    }

    void SetListeners() {

        formacionManager = FindObjectOfType<FormacionManager>();

        //Continuar
        continuar.onClick.RemoveAllListeners();
        continuar.onClick.AddListener(() => MoveToNextQuestion(true));
        
        //Regresar
        if (regresar != null) {
            regresar.onClick.RemoveAllListeners();
            regresar.onClick.AddListener(() => MoveToNextQuestion(false));
        }
    }

    /// <summary>
    /// Activa o desactiva la pregunta para ir a la siguiente, viceversa para el canvas invisible
    /// </summary>
    /// <param name="moveToNext"></param>
    void MoveToNextQuestion(bool moveToNext) {

        if (moveToNext)
            formacionManager.TeleportToNextPosition();
        else 
            formacionManager.TeleportToPreviousPosition();

        //canvasBotonInvisible.SetActive(true);
        gameObject.SetActive(false);
    }

    void ActivateButton(Button button, bool activate) {
        if (button != null)
            button.gameObject.SetActive(activate);
    }

    IEnumerator ShowButtons() {
       // canvasBotonInvisible.SetActive(false);
        ActivateButton(continuar, false);
        ActivateButton(regresar, false);

        yield return new WaitForSeconds(10.0f);

        ActivateButton(continuar, true);
        ActivateButton(regresar, true);
    }
}
