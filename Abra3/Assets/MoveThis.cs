using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveThis : MonoBehaviour
{
    public Transform PointA;
    public Transform PointB;
    public float speed;
    public bool Animate;
    // Start is called before the first frame update
    void Start()
    {
        
    }

  

    void Update()
    {
        Animation();
    }
    public void Animation() 
    {
        if (Animate == true)
        {

            transform.position = Vector3.Lerp(PointA.transform.position, PointB.transform.position, Mathf.PingPong(Time.time / speed, 1));
        }

    }
}

