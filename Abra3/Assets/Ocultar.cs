﻿using UnityEngine;
using System.Collections;

public class Ocultar : MonoBehaviour {
	Transform objeto;
	float lateral;
	public bool ocultar=false;
	public bool mostrar = false;
	// Use this for initialization
	void Start () {
		objeto = this.transform;
		lateral = this.transform.position.x;
	
	}
	public void esconder(bool activo)
	{	
		if (activo) {
			ocultar = true;
			mostrar = false;
		} else {
			mostrar = true;
			ocultar = false;
		}

	}
	// Update is called once per frame
	void Update () {
		if (ocultar) {
			if (objeto.position.x > lateral * -0.5f) {
				objeto.position = objeto.position + Vector3.left * Time.deltaTime * 80;
			} else {
				ocultar = false;
			}
		}
		if (mostrar) {
			if (objeto.position.x < lateral ) {
				objeto.position = objeto.position - Vector3.left * Time.deltaTime * 80;
			} else {
				mostrar = false;
			}
		}
	}
}
