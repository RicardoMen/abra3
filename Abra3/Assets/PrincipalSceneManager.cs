using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrincipalSceneManager : MonoBehaviour
{
    [SerializeField] GameObject panelWait;
    [SerializeField] GameObject panelLoading;

    public void DisplayLoadingPanel() {
        panelWait.SetActive(false);
        
        panelLoading.SetActive(true);
        ResetMainHallButtons();
    }

    void ResetMainHallButtons()
    {
        try
        {
            GameControler.Instance.ResetButtonActivation();
        }
        catch (System.Exception ex)
        {
            Debug.Log("No se pudo guardar estado de reinicio ya que el objeto no existe." + ex.Message);
        }
    }
}
