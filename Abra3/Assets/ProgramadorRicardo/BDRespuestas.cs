using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BDRespuestas : MonoBehaviour
{
    public static BDRespuestas Bdrespuesta;
    public enum Modo { DIAGNOSTICO, EVALUACION, FORMACION };
    public Modo modoActual;


    public List<int> PreguntasRespondidasCorrectasDiagnostico;
    public List<int> PreguntasRespondidasIncorrectasDiagnostico;
    public int CazaBobosDiagnostico;

    public List<int> PreguntasRespondidasCorrectasEvaluacion;
    public List<int> PreguntasRespondidasIncorrectasEvaluacion;
    public int CazaBobosEvaluacion;

    public List<int> PreguntasRespondidasCorrectasFormacion;
    public List<int> PreguntasRespondidasIncorrectasFormacion;
    public int CazaBobosFormacion;






    void Awake()
    {
        if (BDRespuestas.Bdrespuesta == null)
        {
            BDRespuestas.Bdrespuesta = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        SceneManager.sceneLoaded += SetCurrentMode;
    }

    private void SetCurrentMode(Scene current, LoadSceneMode arg1)
    {
        //throw new NotImplementedException();

        //Scene currentScene = SceneManager.GetActiveScene();
        string sceneName = current.name.ToLower();
        Debug.Log(sceneName);

        if (sceneName == "diagnostico")
        {
            modoActual = Modo.DIAGNOSTICO;
        }
        if (sceneName == "formacion")
        {
            modoActual = Modo.FORMACION;
        }
        if (sceneName == "evaluacion")
        {
            modoActual = Modo.EVALUACION;
        }
    }

    void Start()
    {
        
    }

    public void SwitchUIMode(int uiMode)
    {
        switch (uiMode)
        {
            case 0:
                {
                    modoActual = Modo.DIAGNOSTICO;

                    return;
                }

            case 1:
                {
                    modoActual = Modo.EVALUACION;
                    return;
                }

            case 2:
                {
                    modoActual = Modo.FORMACION;
                    return;
                }
            default:
                break;

        }
    }

 

}