using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class Comprobante : MonoBehaviour
{
    // Start is called before the first frame update
    public Animator anim;
    float prevSpeed;
    public MoveThis Move;
    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
        prevSpeed = anim.speed;
    }
    public void PausaAnimacion()
    {
        anim.speed = 0;
        Move.Animate = false;

    }
    public void ContinuarAnimacion()
    {
        anim.speed = prevSpeed;
        Move.Animate = true;

    }
}