using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class CountDown : MonoBehaviour
{
    public float TimeLeft;
    public bool TimerOn = false;
    public GestorVR_PDR_Fatalidades GestorVR;
    public TextMeshProUGUI TimerTxt;
    FinishGame finishGame;

    private void Start()
    {
        finishGame = FindObjectOfType<FinishGame>();

        TimerTxt.text = string.Format("{0:00}:{1:00}", TimeLeft, 0);
        TimeLeft = TimeLeft * 60.0f;
        StartTimer();
    }

    public void StartTimer()
    {
        TimerOn = true;
    }

    public void StopTimer()
    {
        TimerOn = false;
    }

    void Update()
    {
        if (TimerOn)
        {
            if (TimeLeft > 0)
            {
                TimeLeft -= Time.deltaTime;
                updateTimer(TimeLeft);
            }
            else
            {
                Debug.Log("Time is UP!");
                TimeLeft = 0;
                TimerOn = false;

               finishGame.TimeIsUp = true;
                GestorVR.Respuestas();
                finishGame.FinishCurrentGame();
            }
        }
    }

    void updateTimer(float currentTime)
    {
        currentTime += 1;

        float minutes = Mathf.FloorToInt(currentTime / 60);
        float seconds = Mathf.FloorToInt(currentTime % 60);

        TimerTxt.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}