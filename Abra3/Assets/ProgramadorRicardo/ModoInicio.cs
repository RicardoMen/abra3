using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModoInicio : MonoBehaviour
{
   public  enum Modo { DIAGNOSTICO, EVALUACION, FORMACION };
   public Modo modoActual;

    public int NumeroDePreguntasContestadas;
    public int NumeroActualDepreguntas;
    public List<GameObject> BotonesInvisibles;
    void Start()
    {
        modoActual = Modo.DIAGNOSTICO;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SwitchUIMode(int uiMode)
    {
        switch (uiMode)
        {
            case 0:
                {
                    modoActual = Modo.DIAGNOSTICO;
                    
                    return;
                }

            case 1:
                {
                    modoActual = Modo.EVALUACION;
                    return;
                }

            case 2:
                {
                    modoActual = Modo.FORMACION;
                    return;
                }
            default:
                break;

        }
    }
    public void AumentoPorPregunta()
    {

        NumeroDePreguntasContestadas++;
    
    }
    public void AumentoPorModo() 
    {
        if (modoActual == Modo.DIAGNOSTICO && NumeroDePreguntasContestadas== NumeroActualDepreguntas) 
        {
            modoActual = Modo.FORMACION;
            NumeroDePreguntasContestadas = 0;
            for (int i = 0; i < BotonesInvisibles.Count; i++)
            {
                BotonesInvisibles[i].SetActive(true);
            }
        }
        if (modoActual == Modo.FORMACION && NumeroDePreguntasContestadas == NumeroActualDepreguntas)
        {
            modoActual = Modo.EVALUACION;
            NumeroDePreguntasContestadas = 0;
            for (int i = 0; i < BotonesInvisibles.Count; i++)
            {
                BotonesInvisibles[i].SetActive(true);
            }
        }


    }

}
