﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ControlCheck : MonoBehaviour {
	[System.Serializable]
	public class Checker{
		public GameObject padre;
		public Text texto;
		public GameObject Cu_correcto;
		public GameObject Cu_incorrecto;
		public GameObject Cu_vacio;
		public GameObject CuadroTexto; 
		public GameObject Cu_correctoPrincipal;
	} 
	public Checker Item;
	public bool seleccionado= false;
	public bool seleccionadoPrincipal= false;
	public bool FuncionCheckbox=true;
	public bool correcta= false;
	public bool correctaPrincipal= false;
	//####### Public metodos  ######
	public void CheckOK()
	{
		seleccionado = true;
		Item.Cu_correcto.SetActive (true);
		Item.Cu_incorrecto.SetActive (false);
	}
	public void CheckOKPrincipal()
	{
		seleccionadoPrincipal = true;
		Item.Cu_correctoPrincipal.SetActive (true);
		Item.Cu_incorrecto.SetActive (false);
	}
	public void CheckError()
	{
		Item.Cu_correcto.SetActive (false);
		Item.Cu_incorrecto.SetActive (true);
        if(Item.Cu_correctoPrincipal!=null)
		    Item.Cu_correctoPrincipal.SetActive (false);
	}
	public void CheckVacio()
	{
		seleccionado = false;
		seleccionadoPrincipal = false;
		Item.Cu_correcto.SetActive (false);
		Item.Cu_incorrecto.SetActive (false);
		if( GameObject.Find(this.transform.parent.name+"/"+this.name+"/Cu_correctoPrincipal"))
			Item.Cu_correctoPrincipal.SetActive (false);
	}
	public void CheckCuadroTexto(bool estado){
		Item.CuadroTexto.SetActive (estado);
	}
	public void UIMarcar()
	{
		seleccionado =!Item.Cu_correcto.activeSelf;
		Item.Cu_correcto.SetActive (!Item.Cu_correcto.activeSelf);
		Item.Cu_incorrecto.SetActive (false);
        if (GameObject.Find(this.transform.parent.name + "/" + this.name + "/Cu_correctoPrincipal"))
            Item.Cu_correctoPrincipal.SetActive(buscarPrincipal());
        else
        {
            //if (FindObjectOfType<Instrucciones> ())
            //	FindObjectOfType<Instrucciones> ().siguienteInstruccionSecundaria ();
        }
	}
	bool buscarPrincipal(){
		ControlCheck[] contrl = FindObjectsOfType<ControlCheck> ();
		foreach(ControlCheck c in contrl){
			if (c.seleccionadoPrincipal) {
				seleccionadoPrincipal = false;
				//FindObjectOfType<Instrucciones> ().siguienteInstruccionSecundaria ();
				return false;
			}
		}
		seleccionadoPrincipal = true;
		//FindObjectOfType<Instrucciones> ().siguienteInstruccionTercero ();
		return true;
	}
	// Use this for initialization
	void Awake () {
		Item.padre  = this.gameObject;
        if (this.GetComponent<Text>()) 
		    Item.texto = this.GetComponent<Text> ();
		Item.Cu_correcto = GameObject.Find(this.transform.parent.name+"/"+this.name+"/Cu_correcto");
		Item.Cu_incorrecto = GameObject.Find(this.transform.parent.name+"/"+this.name+"/Cu_error");
		Item.Cu_vacio = GameObject.Find(this.transform.parent.name+"/"+this.name+"/Cu_vacio");
		Item.CuadroTexto = GameObject.Find(this.transform.parent.name+"/"+this.name+"/cuadrado_texto");
		if( GameObject.Find(this.transform.parent.name+"/"+this.name+"/Cu_correctoPrincipal"))
			Item.Cu_correctoPrincipal = GameObject.Find(this.transform.parent.name+"/"+this.name+"/Cu_correctoPrincipal");
	}
	void Start(){
		seleccionado = false;
		if (FuncionCheckbox) {
			CheckVacio ();
			CheckCuadroTexto (false);
			GetComponent<Button> ().onClick.AddListener (delegate() {
				UIMarcar ();
			});
		}
	}
	// Update is called once per frame
	void Update () {
	
	}

}
