﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
public class ControlPantalla : MonoBehaviour {
	
	[Header("Check Box")]
	public List<GameObject> Items;
	// Use this for initialization
	[System.Serializable]
	public class Checker{
		public GameObject padre;
		public Text texto;
		public GameObject Cu_correcto;
		public GameObject Cu_incorrecto;
		public GameObject Cu_vacio;
		public GameObject CuadroTexto; 

	}
	
	public List<Checker> CheckBox ;
	void Awake () {
		FindItems ();
		//instrucciones = FindObjectOfType<Instrucciones> ();
	}

	void FindItems () {
		int count = Items.Count;
		for (int i = 0; i < count; i++) {
			Checker nuevo = new Checker ();
			nuevo.padre  = Items [i];
			nuevo.texto = Items [i].GetComponent<Text> ();
			nuevo.Cu_correcto = GameObject.Find(Items[i].transform.parent.name+"/"+Items[i].name+"/Cu_correcto");
			nuevo.Cu_incorrecto = GameObject.Find(Items[i].transform.parent.name+"/"+Items[i].name+"/Cu_error");
			nuevo.Cu_vacio = GameObject.Find(Items[i].transform.parent.name+"/"+Items[i].name+"/Cu_vacio");
			nuevo.CuadroTexto = GameObject.Find(Items[i].transform.parent.name+"/"+Items[i].name+"/cuadrado_texto");
			CheckBox.Add (nuevo);
		}
	}

//####### Public metodos  ######
	public void CheckOK(int item)
	{
		CheckBox [item].Cu_correcto.SetActive (true);
		CheckBox [item].Cu_incorrecto.SetActive (false);
	}
	public void CheckError(int item)
	{
		CheckBox [item].Cu_correcto.SetActive (false);
		CheckBox [item].Cu_incorrecto.SetActive (true);
	}
	public void CheckVacio(int item)
	{print (CheckBox.Count);
		CheckBox [item].Cu_correcto.SetActive (false);
		CheckBox [item].Cu_incorrecto.SetActive (false);
	}
	public void CheckCuadroTexto(int item,bool estado){
		CheckBox [item].CuadroTexto.SetActive (estado);
	}

	//##############################################################################################################


	 public GameObject[] pantalla;
	public SpriteRenderer sprite;
	 public ControlCheck[] Estandares;
	 public ControlCheck[] Peligros;

	bool isFadeout;
	bool isFadein;
	bool isFadeFuncion;
	string funcionfade;
	int actual=0;
	void Start(){
		pantalla[1].SetActive (false);
		pantalla[2].SetActive (false);
		StartCoroutine (PasarDiapo ());
	}
	IEnumerator PasarDiapo(){
		pantalla[actual].SetActive (true);
		yield return new WaitForSeconds (3);
		Fade_inFuncion ("desactivar");
	}
	IEnumerator esperar(string fun,string texto){

		yield return new WaitForSeconds (3);
	//	instrucciones.siguienteInstruccion (actual);
		Fade_inFuncion (fun);
	}
    public void desactivar()
    {
        foreach (ControlCheck c in FindObjectsOfType<ControlCheck>())
            c.CheckVacio();
        pantalla[actual].SetActive(false);
        actual++;
        if (pantalla.Length > actual)
        {
            pantalla[actual].SetActive(true);
            sprite.transform.SetParent(pantalla[actual].transform);
        }
        else { 
        //FindObjectOfType<InstanceFade> ().fade_inLoadLevel ();
    }
	}
	public void ComprobarCorrectos()
	{
		bool resultado = true;
		foreach (ControlCheck c in Estandares) {
			c.CheckError ();
			if (c.correcta) {
				if (!c.seleccionado) {
					resultado = false;
					//instrucciones.Incorrecto ();
				}
				c.CheckOK ();
			}
			if (c.correctaPrincipal) {
				if (!c.seleccionadoPrincipal) {
					resultado = false;
					//instrucciones.Incorrecto ();
				}
				c.CheckOKPrincipal ();
			}
			if (!c.correcta)
			{
				if (c.seleccionado) {
					resultado = false;
					print ("resultado incorrecto");
					//instrucciones.Incorrecto ();
				}
				c.CheckError();
			}
		}
		if (!resultado) {
			StartCoroutine (esperar ("desactivar", "Error: Esta es la respuesta correcta intente otra vez"));
			actual--;
		} else {
			//instrucciones.Correcto ();
			StartCoroutine (esperar ("desactivar", "Correcto"));

		}
	}
	public void ComprobarCorrectos2()
	{
		bool resultado = true;
		foreach (ControlCheck c in Peligros) {
			
			if (c.correcta) {
				
				if (!c.seleccionado) {
					print ("resultado incorrecto");
				//	instrucciones.Incorrecto ();
					resultado = false;
				}
				c.CheckOK();
			}
            
			if (!c.correcta)
			{
				if (c.seleccionado) {
					print ("resultado incorrecto");
					//instrucciones.Incorrecto ();
					resultado = false;
				}
				c.CheckError();
			}
        }
		if (!resultado) {
			StartCoroutine (esperar ("desactivar", "Error: Esta es la respuesta correcta intente otra vez"));
			actual--;
		} else {
			//instrucciones.Correcto ();
			StartCoroutine (esperar ("desactivar", "Correcto"));
			//FindObjectOfType<InstanceFade> ().fade_inLoadLevel ();
		}
	}
	void Update(){
		if (isFadeout) {
			if (sprite.color.a > 0) {
				sprite.color = new Color (0, 0, 0, sprite.color.a - (1 * Time.deltaTime));
			} else {
				isFadeout = false;
			}
		}
		if (isFadein) {
			if (sprite.color.a < 1) {
				sprite.color = new Color (0, 0, 0, sprite.color.a + (1 * Time.deltaTime));
			} else {
				if (isFadeFuncion) {
					isFadeFuncion = false;
					foreach (GameObject go in FindObjectsOfType (typeof(GameObject))) {
						go.SendMessage (funcionfade, SendMessageOptions.DontRequireReceiver);
					}

				}
				fade_out ();
			}
		}
	}
	public void fade_out (){
		isFadein = false;
		isFadeout = true;
	}
	public void fade_in(){
		isFadein = true;
		isFadeout = false;
	}
	public void Fade_inFuncion(string str){
		funcionfade = str;
		isFadeFuncion = true;
		fade_in ();	
	}
}
