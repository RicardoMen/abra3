using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

namespace USING.TEAMMANAGEMENT
{
    public class SceneChangeTimer : MonoBehaviour
    {
        [SerializeField]
        private Animator fadeAnimator;
        [SerializeField]
        private float transitionDuration = 1.0f;
        private float activationDelay = 15.0f;
        [SerializeField]
        private List<string> sceneNames;

        private bool timerStarted = false;
        private bool DoubleSpeed = false;

        private void Start()
        {
            fadeAnimator = GetComponentInChildren<Animator>();
            // Inicia la cuenta regresiva para la transici�n despu�s de un tiempo determinado.
            StartTimer();
        }

        private void Update()
        {
            if (!timerStarted)
            {
                return;
            }


            if (DoubleSpeed == true)
            {

                activationDelay -= Time.deltaTime * 2;
            }
            else
            {
                if (DoubleSpeed == false)
                {
                    activationDelay -= Time.deltaTime;
                }
            }

            if (activationDelay <= 0)
            {
                StartTransition();
                timerStarted = false;
            }
        }

        private void StartTimer()
        {
            timerStarted = true;
        }

        public void StartTransition()
        {
            // Elegir una escena aleatoriamente de la lista de nombres.
            int randomIndex = UnityEngine.Random.Range(0, sceneNames.Count);
            string sceneName = sceneNames[randomIndex];

            // Disparar la animaci�n de "Start Transition".
            fadeAnimator.SetTrigger("StartTransition");

            // Retrasar la carga de la escena.
            StartCoroutine(LoadSceneWithDelay(sceneName));
        }

        private IEnumerator LoadSceneWithDelay(string sceneName)
        {
            yield return new WaitForSeconds(transitionDuration);
            // Cargar la escena aleatoria.
            SceneManager.LoadScene(sceneName);
        }
        public void DoubleSpe()
        {

            DoubleSpeed = true;
        }
    }
}
