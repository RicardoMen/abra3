﻿using UnityEngine;
using System.Collections;

public class Ayuda : MonoBehaviour {
	public GameObject canvasAyuda;
	// Use this for initialization
	void Start () {
		#if UNITY_STANDALONE
		canvasAyuda.SetActive(false);
		#endif
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.JoystickButton3)|| Input.GetKeyDown(KeyCode.RightAlt)) {
			canvasAyuda.SetActive (!canvasAyuda.activeSelf);
		}
		
	}
}
