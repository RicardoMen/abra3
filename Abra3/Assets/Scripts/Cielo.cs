﻿using UnityEngine;
using System.Collections;

public class Cielo : MonoBehaviour {

	public Material sky;
	float giro=0;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		giro = (giro + Time.deltaTime * 1) % 360;
		sky.SetFloat ("_Rotation", giro);
	}
}
