﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ControlRut : MonoBehaviour
{
    public InputField inputRut;
    public Button Confirmar;
    public Text Subir;
    public GameObject PanelRut;
    public GameObject PanelEscena;
    public Toggle Facil;
    public Toggle Dificil;
    public GameObject advertencia1;
    public GameObject advertencia2;
    bool isRutcorrecto=false;
    // Start is called before the first frame update
    void Start()
    {
        inputRut.onValueChanged.AddListener(delegate {
            if (Qualitat.Helpers.Rut.Validate(Qualitat.Helpers.Rut.Format(inputRut.text)))
            {
                inputRut.text = Qualitat.Helpers.Rut.Format(inputRut.text);
                //Confirmar.interactable = true;
                isRutcorrecto = true;
                advertencia1.SetActive(false);
            }
            else
            {
                // Confirmar.interactable = false;
                isRutcorrecto = false;
            }
        });
        StartCoroutine(validateDispositivo());
    }

    public void ConfirmarRut()
    {
        if (isRutcorrecto)
        {
            if (Facil.isOn || Dificil.isOn)
            {
                Confirmar.interactable = false;
                if (EvaluacionVR.current != null)
                {
                    EvaluacionVR.current.Nueva();
                    EvaluacionVR.current.ficha.AddRUT(inputRut.text);
                    EvaluacionVR.current.ficha.AddEmpresa(36);
                    
                }
                if (Facil.isOn)
                {
                    CargarEscena("Escena Facil 1");
                }
                if (Dificil.isOn)
                {
                    CargarEscena("Escena Dificil 1");
                }
            }
            else advertencia2.SetActive(true);
            /*PanelRut.SetActive(false);
            PanelEscena.SetActive(true);*/
        }
        else advertencia1.SetActive(true);
    }
    public void subirDatos()
    {
        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            StartCoroutine(Qualitat.GestorVR.Module.Evaluacion.Ficha.SaveAll());
            Subir.text = "Subir";
        }
        else
        {
            Debug.LogWarning("Se intento subir la evaluación, pero no hay conexión a internet.");
            Subir.text = "No hay conexión a internet" ;
            
        }
        
    }
    public void CargarEscena(string escena)
    {
        Button[] btns = PanelEscena.GetComponentsInChildren<Button>();
        foreach(Button btn in btns)
        {
            btn.interactable = false;
        }
        SceneManager.LoadScene(escena);
    }
    
    IEnumerator validateDispositivo()
    {
        yield return new WaitForSeconds(5);
        var device = Qualitat.GestorVR.Model.Dispositivo.Table.Find(x => x.emp_id == 3);

        if (device.isAvailable)
        {
            Debug.Log("si, dispositivo activado " + device.dis_id);

        }
        else
        {
            Debug.Log("no, dispositivo no activado aun " + device.dis_id);
            Subir.text = "Active el Dispositivo Número: " + device.dis_id;
            Subir.transform.parent.GetComponent<Button>().interactable = false;

        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
