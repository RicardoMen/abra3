﻿using UnityEngine;
using System.Collections;

public class EscalarMenu : MonoBehaviour {
	public float escalado=2;
	// Use this for initialization
	void Start () {
		#if UNITY_STANDALONE || UNITY_EDITOR
		this.transform.localScale = this.transform.localScale * escalado;
		#endif
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
