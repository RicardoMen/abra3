﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(InstanceFade))]
public class FadeButtonEditor : Editor
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		InstanceFade myScript = (InstanceFade)target;
		if(GUILayout.Button("Fade In"))
		{
			myScript.fade_in();
		}
		if(GUILayout.Button("Fade Out"))
		{
			myScript.fade_out();
		}
		if(GUILayout.Button("Siguiente Escena"))
		{
			myScript.fade_inLoadLevel();
		}
	}
}
#endif