﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{
    public Transform transformToFollow;
    public Moment moment;

    [Header("Constraint")]
    public bool frzPosX;
    public bool frzPosY;
    public bool frzPosZ;
    public bool frzRotX;
    public bool frzRotY;
    public bool delate;
    float difRotY;
    public bool frzRotZ;

    public bool retardo = false;

    public enum Moment { OnFixedUpdate, OnUpdate }

    // Update is called once per frame
    void Update()
    {
        if (moment == Moment.OnUpdate)
        {
            UpdateTransform();
        }
    }

    void FixedUpdate()
    {
        if (moment == Moment.OnFixedUpdate)
        {
            UpdateTransform();
        }
    }

    public void UpdateTransform()
    {
        if (retardo)
        {
            Mathf.SmoothDamp(transform.eulerAngles.y, PlusMinus180(transformToFollow.transform.eulerAngles.y, transform.eulerAngles.y), ref difRotY, 1);
            transform.position = new Vector3(frzPosX ? transform.position.x : transformToFollow.position.x, frzPosY ? transform.position.y : transformToFollow.position.y, frzPosZ ? transform.position.z : transformToFollow.position.z);
            transform.eulerAngles = new Vector3(frzRotX ? transform.eulerAngles.x : transformToFollow.eulerAngles.x,
                frzRotY ? transform.eulerAngles.y : difRotY < -40 || difRotY > 40 ? Mathf.SmoothDamp(transform.eulerAngles.y, PlusMinus180(transformToFollow.transform.eulerAngles.y, transform.eulerAngles.y), ref difRotY, 5) : transform.eulerAngles.y
                , frzRotZ ? transform.eulerAngles.z : transformToFollow.eulerAngles.z);
        }
        else
        {
            transform.position = new Vector3(frzPosX ? transform.position.x : transformToFollow.position.x, frzPosY ? transform.position.y : transformToFollow.position.y, frzPosZ ? transform.position.z : transformToFollow.position.z);
            transform.eulerAngles = new Vector3(frzRotX ? transform.eulerAngles.x : transformToFollow.eulerAngles.x,
               frzRotY ? transform.eulerAngles.y : transformToFollow.eulerAngles.y
               , frzRotZ ? transform.eulerAngles.z : transformToFollow.eulerAngles.z);
        }
      
    }

    float PlusMinus180(float angle, float target)
    {
        while (angle > target + 180f)
        {
            angle -= 360f;
        }
        while (angle <= target - 180f)
        {
            angle += 360f;
        }
        return angle;
    }
}
