﻿using UnityEngine;

public class GazeCursor : MonoBehaviour{
	public GameObject cursor;
	public bool showCursor = true;
	public bool scaleCursorSize = true;
	public Camera camara;
	void Start(){
		if (cursor == null)
			cursor = this.gameObject;
		if (camara == null)
			camara = this.transform.parent.GetComponent<Camera> ();
	}
	 
	void Update(){
		PlaceCursor ();
	}
	
	private void PlaceCursor() {
		if (cursor == null) {
			return;
		}
		//var go = pointerData.pointerCurrentRaycast.gameObject;
		Camera cam = camara;// pointerData.enterEventCamera;  // Will be null for overlay hits.
		RaycastHit Rhit;
		Ray ray = cam.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
		
		cursor.SetActive( cam != null && showCursor);
		if (cursor.activeInHierarchy && Physics.Raycast(ray, out Rhit) ) {
			// Note: rays through screen start at near clipping plane.
			float dist = Rhit.distance + cam.nearClipPlane;
			cursor.transform.position = cam.transform.position + cam.transform.forward * dist;
			if (scaleCursorSize) {
				cursor.transform.localScale = Vector3.one * dist;
			}
		}
	}
	
}