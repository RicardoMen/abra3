﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class InstanceFade : MonoBehaviour {
	public Transform camara;
	public GameObject FadeSprite;
	public float velocidadFade = 0.3f;
	GameObject fadeObjeto;
	SpriteRenderer sprite;
	bool isFadein =false;
	bool isFadeout = true;
	// Use this for initialization
	void Start () {
		if (camara == null)
			camara = Camera.main.transform;
		fadeObjeto =  (GameObject)Instantiate(FadeSprite,camara.position,camara.rotation) ;
		fadeObjeto.transform.SetParent (camara);
		sprite = fadeObjeto.GetComponentInChildren<SpriteRenderer> ();
	}

	public void fade_out (){
		isFadein = false;
		isFadeout = true;
	}


	public void fade_in(){
		isFadein = true;
		isFadeout = false;
	}
	bool isFadeLevel=false;
	///<summary>
	///Activa fade_in y pasa al nivel siguiente nvl+1
	///</summary>
	public void fade_inLoadLevel(){
		intNivel = SceneManager.GetActiveScene ().buildIndex + 1;
		isFadeLevel = true;
		fade_in ();
	}
	int intNivel;
	///<summary>
	///Activa fadeIn y cambia al nivel indicado
	///</summary>
	public void fade_inLoadLevel(int level){
		intNivel = level;
		isFadeLevel = true;
		fade_in ();
	}
	string funcionfade;
	bool isFadeFuncion=false;
	///<summary>
	///Activa fade_out e fade_in y llama a la funcion dada
	///</summary>
	public void Fade_inFuncion(string str){
		funcionfade = str;
		isFadeFuncion = true;
		fade_in ();	
	}
	// Update is called once per frames
	void Update () {
		if (isFadeout) {
			if (sprite.color.a > 0) {
				sprite.color = new Color (0, 0, 0, sprite.color.a - (1 * Time.deltaTime * velocidadFade));
			} else {

			}
		}

		if (isFadein) {
			if (sprite.color.a < 1) {
				sprite.color = new Color (0, 0, 0, sprite.color.a + (1 * Time.deltaTime * velocidadFade));
			} else {
				if (isFadein) {
					if (isFadeLevel) {
						isFadeLevel = false;
						SceneManager.LoadScene (intNivel);
					}
					if (isFadeFuncion) {
						isFadeFuncion = false;
						foreach (GameObject go in FindObjectsOfType (typeof(GameObject))) {
							go.SendMessage (funcionfade, SendMessageOptions.DontRequireReceiver);
						}
						fade_out ();
					}
				}
			}
		}
	}
}
