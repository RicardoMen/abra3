using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Questions : MonoBehaviour
{
    public GameObject Panel;
    public List<string> Preguntas;
    public List<string> PreguntasTemp= new List<string>();
    public Text PreguntaUI;
    public string PreguntaEscogida;

    public List<Text> SelectorAlternativas;
    public List<string> AlternativasP1;
    public List<string> AlternativasP2;

    public List<string> AlternativasP3;

    public List<string> AlternativasP4;

    public List<string> AlternativasP5 ;

    public List<string> AlternativasEscogidas;
    public List<string> RespuestasPreguntas;
    public string respuestaActual;


    void Start()
    {
        var listCopy = new List<string>(Preguntas);
        PreguntasTemp = listCopy;


        randomizeList(PreguntasTemp);
      // PreguntaEscogida = GetRandomItem(Preguntas);

        PreguntaUI.text = PreguntasTemp[0];

        OrganizarRespuestas();
    }
    public string GetRandomItem(List<string> listToRandomize)
    {
        int randomNum = Random.Range(0, listToRandomize.Count);
        print(randomNum);

        string printRandom = listToRandomize[randomNum];
        return printRandom;
    }
    // Update is called once per frame
    void Update()
    {
      // SiguientePregunta();
    }
    public void OrganizarRespuestas() 
    {

            for (int i = 0; i < Preguntas.Count; i++)
            {
                if (Preguntas[i] == PreguntasTemp[0])
                {
                    PreguntaEscogida = Preguntas[i];


                }
            }
        
        if (PreguntaEscogida == Preguntas[0]) 
        {
          
            AlternativasEscogidas = AlternativasP1;
            Cambio(AlternativasP1);

            SelectorAlternativas[0].text = AlternativasP1[0];
            SelectorAlternativas[1].text = AlternativasP1[1];
            SelectorAlternativas[2].text = AlternativasP1[2];
            SelectorAlternativas[3].text = AlternativasP1[3];

            respuestaActual = RespuestasPreguntas[0];


        }
        if (PreguntaEscogida == Preguntas[1])
        {
            
            AlternativasEscogidas = AlternativasP2;
            Cambio(AlternativasP2);

            SelectorAlternativas[0].text = AlternativasP2[0];
            SelectorAlternativas[1].text = AlternativasP2[1];
            SelectorAlternativas[2].text = AlternativasP2[2];
            SelectorAlternativas[3].text = AlternativasP2[3];
            respuestaActual = RespuestasPreguntas[1];
        }
        if (PreguntaEscogida == Preguntas[2])
        {
            
            AlternativasEscogidas = AlternativasP3;

            Cambio(AlternativasP3);

            SelectorAlternativas[0].text = AlternativasP3[0];
            SelectorAlternativas[1].text = AlternativasP3[1];
            SelectorAlternativas[2].text = AlternativasP3[2];
            SelectorAlternativas[3].text = AlternativasP3[3];
            respuestaActual = RespuestasPreguntas[2];
        }
        if (PreguntaEscogida == Preguntas[3])
        {
            
            AlternativasEscogidas = AlternativasP4;
            Cambio(AlternativasP4);


            SelectorAlternativas[0].text = AlternativasP4[0];
            SelectorAlternativas[1].text = AlternativasP4[1];
            SelectorAlternativas[2].text = AlternativasP4[2];
            SelectorAlternativas[3].text = AlternativasP4[3];
            respuestaActual = RespuestasPreguntas[3];
        }
        if (PreguntaEscogida == Preguntas[4])
        {
            
            AlternativasEscogidas = AlternativasP5;
            Cambio(AlternativasP5);

            SelectorAlternativas[0].text = AlternativasP5[0];
            SelectorAlternativas[1].text = AlternativasP5[1];
            SelectorAlternativas[2].text = AlternativasP5[2];
            SelectorAlternativas[3].text = AlternativasP5[3];
            respuestaActual = RespuestasPreguntas[4];
        }
    }
    public void Cambio<T>(IList<T> list)
    {
        for (int i = 0; i < AlternativasEscogidas.Count; i++)
        {
            string temp = AlternativasEscogidas[i];
            int randomIndex = Random.Range(i, AlternativasEscogidas.Count);
            AlternativasEscogidas[i] = AlternativasEscogidas[randomIndex];
            AlternativasEscogidas[randomIndex] = temp;
        }
    }
    public void SiguientePregunta()
    {
        if (PreguntasTemp.Count == 0)
        {

            Panel.SetActive(false);
        }
        if (PreguntasTemp.Count > 0)
        {




            PreguntasTemp.RemoveAt(0);
            if(PreguntasTemp.Count >= 1)
            OrganizarRespuestas();
            PreguntaUI.text = PreguntasTemp[0];

         

        }
       
            
      
    }
    // Input list, randomize order
    public static List<string> randomizeList(List<string> randList)
    {
        for (int i = 0; i < randList.Count; i++)
        {
            string temp = randList[i];
            int rand = Random.Range(i, randList.Count);
            randList[i] = randList[rand];
            randList[rand] = temp;
        }
        //Debug.Log("randList = " + string.Join(",", randList));
        return randList;
    }
    public void ConfirmarRespuesta(Text Texto) 
    {
        if (Texto.text == respuestaActual) 
        {
            SiguientePregunta();
        }
    }
    
}
