﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class SoyElMapa : MonoBehaviour {
	public Transform canvas;
	public Transform[] punto;
	public GameObject[] players;
//	public Transform[] Objetos;
//	public Transform[] ObjetosPuntos;
	float RazonW;
	float RazonH;
	public float AjusteW =0;
	public float ajusteEditorW=0;
	public float AjusteH =0;
	public float ajusteEditorH=0;
	public float GalponW = 29;
	public float GalponH = 9;

	[Header("soloEnPc")]
	public Toggle vistas;
	public Toggle mapa;
	public ToggleGroup trabajadores;
	// Use this for initialization
	void Start () {
		print (canvas.GetComponent<RectTransform> ().rect.x+" x - y "+canvas.GetComponent<RectTransform> ().rect.y);
//		AjusteW = AjusteW - ajusteEditorW - canvas.GetComponent<RectTransform> ().rect.x;
//		AjusteH = AjusteH + ajusteEditorH - canvas.GetComponent<RectTransform> ().rect.height;
		RazonW = canvas.GetComponent<RectTransform> ().rect.width / (GalponW);
		RazonH = -canvas.GetComponent<RectTransform> ().rect.height / (GalponH*2);

	}
	public void OcultarTodos(bool activo){
		foreach (GameObject go in FindObjectsOfType (typeof(GameObject))) {
			go.SendMessage ("OcultarSelf",activo , SendMessageOptions.DontRequireReceiver);
		}
	
	}
	public void Ocultar4pantallas(bool activo){
		mapa.isOn = false;
		foreach (GameObject go in FindObjectsOfType (typeof(GameObject))) {
			go.SendMessage ("OcultarSelf",false , SendMessageOptions.DontRequireReceiver);
		}
	}
	public void OcultarSelf(bool activo){
		canvas.gameObject.SetActive (activo);
	}
	public void Ocultar(bool activo){
		if (!vistas.isOn && !trabajadores.AnyTogglesOn())
			canvas.gameObject.SetActive (activo);
		else {
			OcultarTodos (activo);
			canvas.gameObject.SetActive (false);
		}
	}
	void Update () {
		
		/*if (Input.GetButtonDown ("Fire2"))
			canvas.gameObject.SetActive (!canvas.gameObject.activeSelf);*/

		players = GameObject.FindGameObjectsWithTag ("Player") ;
		foreach (Transform p in punto)
			p.gameObject.SetActive (false);
		
		for (int u=0; u<players.Length && u<punto.Length ;u++) {
			punto [u].gameObject.SetActive (true);
			punto[u].localPosition = new Vector3 (players[u].transform.position.z * RazonW + AjusteW, players[u].transform.position.x * RazonH + AjusteH, 0);

		}
//		for (int i=0; i < Objetos.Length && i < ObjetosPuntos.Length; i++) {
//			ObjetosPuntos[i].localPosition = new Vector3(Objetos[i].position.z * RazonW + AjusteW, Objetos[i].position.x*RazonH + AjusteH,0);
//		}
	}
}
