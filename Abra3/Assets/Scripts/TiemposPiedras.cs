﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiemposPiedras : MonoBehaviour {
    float tiempo = 0;
    float t = 0;
    public GameObject PA;
    public Vector3 trans;
    public Quaternion rota;
    public Transform inicio;
    public Transform fin;
    public float tiempoCreacion = 1;
    public float tiempoDestruccion= 6;
	// Use this for initialization
	void Awake () {
    /*    trans = PA.transform.position;
        rota = PA.transform.rotation;*/
	}
	
	// Update is called once per frame
	void Update () {
        t = t + 1 * Time.deltaTime;
        if (t > tiempo)
        {
            tiempo = tiempo + tiempoCreacion* Random.RandomRange(3, 10)/10;
            GameObject g = Instantiate(PA,inicio.position,inicio.rotation);
           /* Vector3 ras = new Vector3(inicio.localEulerAngles.x, Random.RandomRange(0, 360), inicio.localEulerAngles.z);
            g.transform.localEulerAngles = ras;*/
            g.GetComponent<AnimacionPiedra>().targert = fin;
            g.transform.SetParent(this.transform);
            Destroy(g, tiempoDestruccion);
        }
    }
}
