﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class reloj : MonoBehaviour {

	/// Tiempo maximo del reloj, en segundos;
	public float segundos;

	/// Tiempo actual del reloj, en segundos
	public float tiempo_actual;

	/// ¿El reloj va en cuenta regresiva?
	public bool cuenta_regresiva;

	/// Variable que da inicio a contar segundos
	private bool cuentaSegundos;

	/// Variable auxiliar de conteo de segundos (utilizada en cuenta regresiva)
	private float segundos_aux;

	/// Objeto de tipo 'Text' del canvas
	public Text texto_reloj;

	// Use this for initialization
	void Start ()
	{
		cuentaSegundos = true;

		//texto_reloj = this.GetComponentInChildren<Text> ();
		//texto_reloj.text = string.Format ("{0:00}:{1:00}", (int)(segundos / 60), segundos % 60);
	}
	
	// Update is called once per frame
	void Update () {
		// Si comienza a contar segundos
		if (cuentaSegundos == true)
		{
			// Si el reloj cuenta de forma creciente (no cuenta regresiva)
			if(cuenta_regresiva == false)
			{
				tiempo_actual = tiempo_actual + Time.deltaTime * 1;

				texto_reloj.text = string.Format ("{0:00}:{1:00}", (int)(tiempo_actual / 60), tiempo_actual % 60);

				if (tiempo_actual >= segundos)
				{
					texto_reloj.text = string.Format ("{0:00}:{1:00}", (int)(segundos / 60), segundos % 60);
					cuentaSegundos = false;
				}
			}
			// Si el reloj cuenta de forma decreciente (cuenta regresiva)
			else
			{
				texto_reloj.text = string.Format ("{0:00}:{1:00}", (int)(segundos / 60), segundos % 60);

				segundos_aux = segundos_aux + Time.deltaTime * 1;
				tiempo_actual = segundos - segundos_aux;

				texto_reloj.text = string.Format ("{0:00}:{1:00}", (int)(tiempo_actual / 60), tiempo_actual % 60);

				if (tiempo_actual <= 0)
				{
					texto_reloj.text = string.Format ("{0:00}:{1:00}", (int)(0f / 60), 0f % 60);
					cuentaSegundos = false;
				}
			}
		}
	}
}
