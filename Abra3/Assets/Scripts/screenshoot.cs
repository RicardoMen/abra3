﻿
using UnityEngine;
using System.Collections;
using System.IO;

public class screenshoot : MonoBehaviour {

	int i=1;
	public string nombre;
	int ancho = Screen.width;
	int alto = Screen.height;
	// Take a shot immediately


	void Update () {		
			
		if (Input.GetButton("Fire1")){
			GameObject.Find ("/Player/Main Camera/Gaze pointer").SetActive (false);
			StartCoroutine(JPG());
		}
	}

	IEnumerator JPG() {
		yield return new WaitForSeconds (0.1f);
		
		// We should only read the screen buffer after rendering is complete
		yield return new WaitForEndOfFrame();

		// Create a texture the size of the screen, RGB24 format

		Texture2D tex = new Texture2D(ancho, alto, TextureFormat.RGB24, false);

		// Read screen contents into the texture
		tex.ReadPixels(new Rect(0, 0, ancho, alto), 0, 0);
		tex.Apply();

		// Encode texture into PNG
		byte[] bytes = tex.EncodeToJPG();
		Object.Destroy(tex);

		// For testing purposes, also write to a file in the project folder
		File.WriteAllBytes(Application.dataPath + ("/../"+ nombre + "_" + i + ".jpg"), bytes);
		i++;

			}

}