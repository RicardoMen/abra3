﻿#pragma strict
function OnTriggerEnter(other: Collider){
  var t: float = 3;
  while (t > 0){
    t -= Time.deltaTime;
    if (!GetComponent.<AudioSource>().isPlaying){
      GetComponent.<AudioSource>().Play();
    }
    yield; // let Unity free till next frame
  }
}