using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.SceneManagement;

public class CodigoToggle : MonoBehaviour
{
    public List<int> RespuestasUsuario = new List<int>();
    public List<int> UltimasRespuestasUsuario;
    public List<int> RespuestasVerdadera;
    public bool Respuesta;
    public int IdPregunta;
    public BDRespuestas BaseDeDatos;
    GameObject Player;
    public GameObject ThisCanvas;
    public float DistanciaMaxima;
  
    public float distance;
    // Start is called before the first frame update
    void Start()
    {

        Player = GameObject.Find("TrackerOffsets");
        //RespuestasUsuario.Clear();
    }

   


bool Checkmatch() {
   if(RespuestasUsuario == null && RespuestasVerdadera == null)
   {
     return true;
   }
   else if(RespuestasUsuario == null || RespuestasVerdadera == null)
   {
     return false;
   }

  if (RespuestasUsuario.Count != RespuestasVerdadera.Count)
      return false;
  for (int i = 0; i < RespuestasUsuario.Count; i++) {
      if (RespuestasUsuario[i] != RespuestasVerdadera[i])
          return false;
  }
  return true;
  }

    void Update()
    {
       //distancias();
       //Comparando();
        if (Input.GetKeyDown(KeyCode.B))
        {
            SceneManager.LoadScene(0);
        }
    }
public void Comparando()
{
   // if (Input.GetKeyDown(KeyCode.A))
  //  {
        RespuestasUsuario.Sort();
        RespuestasVerdadera.Sort();
        UltimasRespuestasUsuario.Sort();



//DIAGNOSTICO
        if (Checkmatch() == true&&BaseDeDatos.modoActual==BDRespuestas.Modo.DIAGNOSTICO)
        {

            Debug.Log("SonIguales");
            Debug.Log(RespuestasUsuario.Count());
            Debug.Log(RespuestasVerdadera.Count());
                var listCopy = new List<int>(RespuestasUsuario);
                UltimasRespuestasUsuario = listCopy;
                Respuesta = true;
                BaseDeDatos.PreguntasRespondidasCorrectasDiagnostico.Add(IdPregunta);

            }
        if (Checkmatch() == false && BaseDeDatos.modoActual == BDRespuestas.Modo.DIAGNOSTICO)
        {
            Debug.Log("sonDiferentes");
            Debug.Log(RespuestasUsuario.Count());
            Debug.Log(RespuestasVerdadera.Count());
                var listCopy = new List<int>(RespuestasUsuario);
                UltimasRespuestasUsuario = listCopy;
                Respuesta = false;
                BaseDeDatos.PreguntasRespondidasIncorrectasDiagnostico.Add(IdPregunta);
            }
            //EVALUACION
            if (Checkmatch() == true && BaseDeDatos.modoActual == BDRespuestas.Modo.EVALUACION)
            {

                Debug.Log("SonIguales");
                Debug.Log(RespuestasUsuario.Count());
                Debug.Log(RespuestasVerdadera.Count());
                var listCopy = new List<int>(RespuestasUsuario);
                UltimasRespuestasUsuario = listCopy;
                Respuesta = true;
                BaseDeDatos.PreguntasRespondidasCorrectasEvaluacion.Add(IdPregunta);

            }
            if (Checkmatch() == false && BaseDeDatos.modoActual == BDRespuestas.Modo.EVALUACION)
            {
                Debug.Log("sonDiferentes");
                Debug.Log(RespuestasUsuario.Count());
                Debug.Log(RespuestasVerdadera.Count());
                var listCopy = new List<int>(RespuestasUsuario);
                UltimasRespuestasUsuario = listCopy;
                Respuesta = false;
                BaseDeDatos.PreguntasRespondidasIncorrectasEvaluacion.Add(IdPregunta);
            }
            //FORMACION
            if (Checkmatch() == true && BaseDeDatos.modoActual == BDRespuestas.Modo.FORMACION)
            {

                Debug.Log("SonIguales");
                Debug.Log(RespuestasUsuario.Count());
                Debug.Log(RespuestasVerdadera.Count());
                var listCopy = new List<int>(RespuestasUsuario);
                UltimasRespuestasUsuario = listCopy;
                Respuesta = true;
                BaseDeDatos.PreguntasRespondidasCorrectasFormacion.Add(IdPregunta);

            }
            if (Checkmatch() == false && BaseDeDatos.modoActual == BDRespuestas.Modo.FORMACION)
            {
                Debug.Log("sonDiferentes");
                Debug.Log(RespuestasUsuario.Count());
                Debug.Log(RespuestasVerdadera.Count());
                var listCopy = new List<int>(RespuestasUsuario);
                UltimasRespuestasUsuario = listCopy;
                Respuesta = false;
                BaseDeDatos.PreguntasRespondidasIncorrectasFormacion.Add(IdPregunta);
            }


     //   }



    }
public void MandarACorroborar() 
{



    var listCopy = new List<int>(RespuestasUsuario);
    UltimasRespuestasUsuario = listCopy;
}

    public void Cazabobos() 
    {
        if (BaseDeDatos.modoActual == BDRespuestas.Modo.FORMACION)
        {
            BaseDeDatos.CazaBobosFormacion++;
        }
        if (BaseDeDatos.modoActual == BDRespuestas.Modo.EVALUACION)
        {
            BaseDeDatos.CazaBobosEvaluacion++;
        }
        if (BaseDeDatos.modoActual == BDRespuestas.Modo.DIAGNOSTICO)
        {
            BaseDeDatos.CazaBobosDiagnostico++;
        }


    }
    public void distancias() 
    {
         distance = Vector3.Distance(Player.transform.position, gameObject.transform.position);
        

        if(this.gameObject.activeInHierarchy==true && distance > DistanciaMaxima) 
        {

            ThisCanvas.SetActive(false);
        
        }




    }
    public void DebugLogRespuestasusuario() 
    {
        for (int i = 0; i < RespuestasUsuario.Count; i++)
        {
            Debug.Log(RespuestasUsuario[i]+"Esta En La lista de esta Respuesta");
        }
 
    }
}

