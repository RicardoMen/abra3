using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Data : MonoBehaviour
{
    public static Data Instance { get; private set; }
    public Hands CurrentHand { get => currentHand; set => currentHand = value; }
    public string UserID { get => userID; set => userID = value; }
    public string UserName { get => userName; set => userName = value; }
    public float DiagnosisTime { get => diagnosisTime; set => diagnosisTime = value; }
    public float EvaluationTime { get => evaluationTime; set => evaluationTime = value; }
    public float FormationTime { get => formationTime; set => formationTime = value; }

    Hands currentHand;
    string userID, userName;

    float diagnosisTime, evaluationTime, formationTime;

    private void Awake()
    {
        // If there is an instance, and it's not me, delete myself.

        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }
}
