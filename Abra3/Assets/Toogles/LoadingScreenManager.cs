using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScreenManager : MonoBehaviour
{
    [SerializeField] Image bar;
    [SerializeField] string targetScene;

    AsyncOperation loadingOperation;
    bool isFadingOut;

    void Start()
    {
        StartCoroutine(Load());
        
    }
    
    IEnumerator Load() {

        loadingOperation = SceneManager.LoadSceneAsync(targetScene);

        while (!loadingOperation.isDone) {
            bar.fillAmount = Mathf.Clamp01(loadingOperation.progress / 0.9f);

            if (!isFadingOut && loadingOperation.progress >= 40.0f) {
                FindObjectOfType<FadeObject>().InitFade();
                isFadingOut = true;
            }

            yield return null;
        }

        if (loadingOperation.isDone) {
            //FindObjectOfType<FadeObject>().InitFade();
            Debug.Log("Loaded");
        }

    }

}
