using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PutMyIDQuestion : MonoBehaviour
{
    public int IdQuestion;
    public CodigoToggle CodigoToggle;
    public Toggle ThisToogle;
 


    public void CheckMyAnswer() 
    {
        if (ThisToogle.isOn == true)
        {
            CodigoToggle.RespuestasUsuario.Add(IdQuestion);
            CodigoToggle.RespuestasUsuario.Sort();

        }
        if (ThisToogle.isOn == false)
        {
            CodigoToggle.RespuestasUsuario.Remove(IdQuestion);
            CodigoToggle.RespuestasUsuario.Sort();
        }
    }

  
}
