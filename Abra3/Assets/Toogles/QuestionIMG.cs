using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class QuestionIMG : MonoBehaviour
{
    public GameObject PanelPregunta;
    public GameObject PanelInformativo;
    public List<RectTransform> SelectorAlternativas;
    public List<Vector2> Alternativas;
    public RectTransform respuestaActual;
   
    public ModoInicio ModoActual;
    public Text TextoPregunta;
    string PrimerTexto;
    public AudioClip Correcto;
    public AudioClip Falso;
    public AudioSource AudioSonidos;
    public Text TextoDeRespuesta;
    public GameObject BotonContinuar;
    public List<GameObject> AlternativasEnPregunta;

    // public List<GameObject> AlternativasdeGame;


    void Start()
    {
        OrganizarRespuestas();
        ModoActual.GetComponent<ModoInicio>();
        PrimerTexto = "Cual es la fatalidad asociada a esta situaci�n";
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A)) 
        {
           // OrganizarRespuestas();
        }
    }
    public void OrganizarRespuestas()
    {
        randomizeList(Alternativas);
        for (int i = 0; i < SelectorAlternativas.Count; i++)
        {
            SelectorAlternativas[i].anchoredPosition = new Vector2(Alternativas[i].x, Alternativas[i].y);
        }
    }

    public static List<Vector2> randomizeList(List<Vector2> randList)
    {
        for (int i = 0; i < randList.Count; i++)
        {
            Vector2 temp = randList[i];
            int rand = Random.Range(i, randList.Count);
            randList[i] = randList[rand];
            randList[rand] = temp;
        }
        return randList;
    }
    public void SeleccionRespuesta(RectTransform ElRect) 
    {
        if (ModoActual.modoActual == ModoInicio.Modo.DIAGNOSTICO || ModoActual.modoActual == ModoInicio.Modo.EVALUACION)
        {
            if (ElRect == respuestaActual)
            {
               // Panel.SetActive(false);
                Debug.Log("Fue correcto");
                AudioSonidos.clip = Correcto;
                AudioSonidos.PlayOneShot(Correcto);

            }
            else
            {
                //Aca se coloca Ordenes si es que cometio un error
                PanelPregunta.SetActive(false);
                Debug.Log("Fue Erroneo");
                AudioSonidos.clip = Falso;
                AudioSonidos.PlayOneShot(Falso);

            }
        }
        if(ModoActual.modoActual == ModoInicio.Modo.FORMACION) 
        {

            if (ElRect == respuestaActual)
            {
                // Panel.SetActive(false);
                TextoPregunta.text = "Felicidades acertaste con la Respuesta";
                BotonContinuar.SetActive(true);
                Debug.Log("Fue correcto");
                AudioSonidos.clip = Correcto;
                AudioSonidos.PlayOneShot(Correcto);
                for (int i = 0; i < SelectorAlternativas.Count; i++)
                {
                    // if (SelectorAlternativas[i] != respuestaActual)
                    // {
                    SelectorAlternativas[i].gameObject.SetActive(false);

                    //  }
                }
            }
            else 
            {
                BotonContinuar.SetActive(true);
                TextoPregunta.text = "La Fatalidad asociada era "+ TextoDeRespuesta.text + " y recuerdalo no lo olvides";
                AudioSonidos.clip = Falso;
                AudioSonidos.PlayOneShot(Falso);
                for (int i = 0; i < SelectorAlternativas.Count; i++)
                {
                   // if (SelectorAlternativas[i] != respuestaActual)
                   // {
                        SelectorAlternativas[i].gameObject.SetActive(false);

                  //  }
                }
          
            }
        }
        }
    public void Depende() 
    {
        if (ModoActual.modoActual == ModoInicio.Modo.DIAGNOSTICO || ModoActual.modoActual == ModoInicio.Modo.EVALUACION) 
        {

            PanelPregunta.SetActive(true);
            for (int i = 0; i < AlternativasEnPregunta.Count; i++)
            {
                AlternativasEnPregunta[i].SetActive(true);
            }
        
        }
        if(ModoActual.modoActual == ModoInicio.Modo.FORMACION) 
        {

            PanelInformativo.SetActive(true);
        
        }





    }
    
    
    
    }

    
  



