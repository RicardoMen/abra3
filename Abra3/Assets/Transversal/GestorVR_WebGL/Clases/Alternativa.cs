﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Alternativa
{
    public string alt_alt_id;
    public string alt_pre_id;
    public string alt_alternativa;
    public string alt_descripcion;
    public string alt_ponderacion;
    public string alt_correcta;
    public string alt_creado;
    public string alt_modificado;
}

