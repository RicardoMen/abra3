﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class FichaResultado : MonoBehaviour
{


    public string fic_id;
    public string eva_id;
    public string trab_id;
    public string con_id;
    public string pro_id;
    public string disp_id;
    public string calificacion;
    public string pais_id;
    public string creado;
    public string sincronizado;


    public static FichaResultado current;



    void Awake()
    {
        if (current == null)
        {
            DontDestroyOnLoad(gameObject);
            current = this;
        }
        else if (current != this)
        {
            Destroy(gameObject);
        }
    }


    public void LoadFichaResultado(string request)
    {
        string stringData = request;
        JSONObject jsonData = new JSONObject(stringData);

        fic_id = jsonData.GetField("fic_id").Print();
        eva_id = jsonData.GetField("eva_id").Print();
        trab_id = jsonData.GetField("trab_id").Print();
        con_id = jsonData.GetField("con_id").Print();
        pro_id = jsonData.GetField("pro_id").Print();
        disp_id = jsonData.GetField("disp_id").Print();
        calificacion = jsonData.GetField("calificacion").Print();
        pais_id = jsonData.GetField("pais_id").Print();
        creado = jsonData.GetField("creado").str;
        sincronizado = jsonData.GetField("sincronizado").Print();

    }
}
