﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Inscripcion
{
    public string expira;
    public string id;
    public string proID;
    public string empId;
    public string status;
    public Content content = new Content();
    public string creado;
    public string modificado;

    [System.Serializable]
    public class User
    {
        public string name;
        public string pass;
    }
    [System.Serializable]
    public class Content
    {
        // public string uuid;
        public Ficha ficha = new Ficha();
        public List<History> history = new List<History>();
    }
    [System.Serializable]
    public class Ficha
    {
       // public string evaId;
        public string disId;
        // public Params parametros = new Params();
        public Dictionary<string, string> parametros = new Dictionary<string, string>();
        public Trabajador trabajador = new Trabajador();
        public Contratista contratista = new Contratista();
        public string proId;
        public string expire;
        public string authCode;
    }
    [System.Serializable]
    public class Params
    {
        // public Data data= new Data();
        public Dictionary<string, string> param = new Dictionary<string, string>();
        // public string sede;
        // public string curso;
        // public string proId;

    }
    //[System.Serializable]
    //public class Data
    //{
    //    public string proId;
    //}
    [System.Serializable]
    public class Trabajador
    {
        public string rut;
        public string mail;
        public string nombre;
        public string materno;
        public string paterno;
    }
    [System.Serializable]
    public class Contratista
    {
        public string rut;
        public string nombreCorto;
        public string razonSocial;
    }
    [System.Serializable]
    public class History
    {
        public string type;
        public string evaId;
        public string ficId;
        public string creado;
        public string calificacion;
    }

}

