﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Pregunta
{
    public string pre_id;
    public string eva_id;
    public string ite_id;
    public string descripcion;
    public string comentario;
    public string ponderacion;
    public string imagen;
    public string creado;
    public string modificado;
    public string habilitado;
    public List<Alternativa> alternativas;
    // public List<Item> item;




}