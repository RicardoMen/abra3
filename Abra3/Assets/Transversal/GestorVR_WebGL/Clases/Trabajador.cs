﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Trabajador
{

    public int antiguedad;
    public int hijos;
    public string nombre;
    public string paterno;
    public string materno;
    public string sexo;
    public string nacimiento;
    public string fono;
    public string mail;
    public string gerencia;
    public string cargo;
    public string estado_civil;
    public string rut;

}