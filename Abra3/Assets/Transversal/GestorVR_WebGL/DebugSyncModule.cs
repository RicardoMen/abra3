﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace Qualitat.GestorVR.Module.Analytics
{
    class DebugSyncModule : MonoBehaviour
    {
        // private static readonly string endpoint = "https://debug.qualitat.cl/api/";
        private static readonly string endpoint = "https://gestorvr.qualitat.cl/api";
        private static readonly string accessToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjbGkiOjIsImlhdCI6MTYxNTA5ODg0Miwic3ViIjoiTDM5UiIsInR5cCI6InB3ZCIsInVpZCI6MywidW4iOiJBbmFsaXR5Y3MifQ.Ev0X0ugkcnIMeg1_ImrHyW36E6vrcjsDCA-8ePUkRyI";
        public string debugId;
        public List<LogClass> inLogs;
        public List<LogClass> outLogs;
        public DebugSyncModule current;
        void Awake()
        {
            if (current == null)
            {
                DontDestroyOnLoad(gameObject);
                current = this;
                inLogs = new List<LogClass>();
                outLogs = new List<LogClass>();
            }
            else if (current != this)
            {
                Destroy(gameObject);
            }
        }

        void OnEnable()
        {
            Application.logMessageReceived += HandleLog;
        }

        void OnDisable()
        {
            Application.logMessageReceived -= HandleLog;
        }

        void OnApplicationQuit()
        {
            Application.logMessageReceived -= HandleLog;
        }

        private void HandleLog(string condition, string stackTrace, LogType type)
        {
            inLogs.Add(new LogClass()
            {
                msg = condition,
                trc = stackTrace,
                type = type.ToString("g"),
                iat = System.DateTime.UtcNow.ToString("o")
            });
        }

        public IEnumerator Start()
        {
            Debug.Log("Initial Debug");

            WWWForm form = new WWWForm();
            form.AddField("createdAt", System.DateTime.UtcNow.ToString("o"));
            form.AddField("app[name]", Application.productName);
            form.AddField("app[version]", Application.version);
            if (!String.IsNullOrEmpty(Application.buildGUID))
            {
                form.AddField("app[build]", Application.buildGUID);
            }
            form.AddField("app[unityVersion]", Application.unityVersion);
            form.AddField("app[company]", Application.companyName);
            form.AddField("app[platform]", Application.platform.ToString("g"));
            form.AddField("device[name]", SystemInfo.deviceName);
            form.AddField("device[model]", SystemInfo.deviceModel);
            form.AddField("device[type]", SystemInfo.deviceType.ToString("g"));
            UnityWebRequest request = UnityWebRequest.Post(endpoint + "/v1/debug", form);
            request.SetRequestHeader("Authorization", "Bearer " + accessToken);
            yield return request.SendWebRequest();
            if (request.isNetworkError)
            {
                Debug.Log("No hay acceso a internet");
            }
            else if (request.isHttpError)
            {
                Debug.Log("Error debug http:" + request.responseCode);
            }
            else
            {
                JSONObject jsonResponse = new JSONObject(request.downloadHandler.text);
                debugId = jsonResponse["_id"].str;
                form = new WWWForm();
                while (true)
                {
                    yield return new WaitWhile(() => inLogs.Count == 0 && outLogs.Count == 0);
                    if (inLogs.Count > 0)
                    {
                        for (int i = 0; i < inLogs.Count; i++)
                        {
                            form.AddField(string.Format("logs[{0}][msg]", i + outLogs.Count), inLogs[i].msg);
                            if (!string.IsNullOrEmpty(inLogs[i].trc))
                            {
                                form.AddField(string.Format("logs[{0}][trc]", i + outLogs.Count), inLogs[i].trc);
                            }
                            form.AddField(string.Format("logs[{0}][type]", i + outLogs.Count), inLogs[i].type);
                            form.AddField(string.Format("logs[{0}][iat]", i + outLogs.Count), inLogs[i].iat);
                        }
                        outLogs.AddRange(inLogs);
                        inLogs.Clear();
                        request = UnityWebRequest.Post(endpoint + "/v1/debug/" + debugId + "/log", form);
                        request.SetRequestHeader("Authorization", "Bearer " + accessToken);
                    }
                    else
                    {
                        yield return request.SendWebRequest();
                        if (request.isNetworkError)
                        {
                            Debug.LogWarning("No hay acceso a internet");
                            yield return new WaitForSeconds(60f);
                        }
                        else if (request.isHttpError)
                        {
                            Debug.LogError("Error debug http:" + request.responseCode + request.downloadHandler.text);
                            yield return new WaitForSeconds(10f);
                        }
                        else
                        {
                            outLogs.Clear();
                            form = new WWWForm();
                        }
                    }
                }
            }
        }
    }

    [System.Serializable]
    public struct LogClass
    {
        public string msg;
        public string trc;
        public string type;
        public string iat;
    }
}