using UnityEditor;
using UnityEngine;
using System.IO;


public class Enviroment : MonoBehaviour
{
#if UNITY_EDITOR
    [MenuItem("GestorVR_WebGL/Enviroment/WebGL")]
    static void SetWebGL()
    {
        string host = "";
        string path = Application.dataPath + "/Transversal/GestorVR_WebGL/Enviroment/Enviroment.cs";
       
        if (UnityEditor.EditorUserBuildSettings.development)
        {
            Debug.Log("LA COMPILACIÓN ESTA EN MODO DEVELOPMENT");
            host = "\"/dev\"";
            Debug.Log("host -> " + host);
        }
        else
        {
            Debug.Log("LA COMPILACIÓN ESTA EN MODO PRODUCTION");
            host = "\"/api\"";
            Debug.Log("host -> " + host);
        }
        lineChanger("    public static string enviroment_app ="+host+";", path, 55);
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
    }

    [MenuItem("GestorVR_WebGL/Enviroment/Oculus and UnityEditor")]
    static void SetDevelopment()
    {
        string host = "";
        string path = Application.dataPath + "/Transversal/GestorVR_WebGL/Enviroment/Enviroment.cs";

        if (UnityEditor.EditorUserBuildSettings.development)
        {
            Debug.Log("LA COMPILACIÓN ESTA EN MODO DEVELOPMENT");
            host = "\"https://gestorvr.qualitat.cl/dev\"";
            Debug.Log("host -> " + host);
        }
        else
        {
            Debug.Log("LA COMPILACIÓN ESTA EN MODO PRODUCTION");
            host = "\"https://gestorvr.qualitat.cl/api\"";
            Debug.Log("host -> " + host);
        }
       

       lineChanger("    public static string enviroment_app = "+host+";", path, 55);
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
    }
#endif
    public static string enviroment_app = "https://gestorvr.qualitat.cl/api";


   
    static void lineChanger(string newText, string fileName, int line_to_edit)
    {
        string[] arrLine = File.ReadAllLines(fileName);
        arrLine[line_to_edit - 1] = newText;
        File.WriteAllLines(fileName, arrLine);
    }
}

