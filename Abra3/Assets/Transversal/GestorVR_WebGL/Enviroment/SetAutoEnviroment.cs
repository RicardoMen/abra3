﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;
using System.IO;

class SetAutoEnviroment : IPreprocessBuildWithReport
{
    public int callbackOrder { get { return 0; } }
    public void OnPreprocessBuild(BuildReport report)

    {
        Debug.Log("comenze a compilar");



#if UNITY_WEBGL
        string host = "";
        string path = Application.dataPath + "GestorVR_WebGL/Enviroment/Enviroment.cs";
        if (UnityEditor.EditorUserBuildSettings.development)
        {
               Debug.Log("LA COMPILACIÓN ESTA EN MODO DEVELOPMENT");
              host = "\"/dev\"";  
               Debug.Log("host -> "+host);
        }
        else
        {
              Debug.Log("LA COMPILACIÓN ESTA EN MODO PRODUCTION");
              host = "\"/api\"";
              Debug.Log("host -> "+host);
        }
            
        lineChanger("    public static string enviroment_app =" + host + ";", path, 55);
#endif

#if UNITY_ANDROID
        string host = "";
        string path = Application.dataPath + "Transversal/GestorVR_WebGL/Enviroment/Enviroment.cs";

        if (UnityEditor.EditorUserBuildSettings.development)
        {
            Debug.Log("LA COMPILACIÓN ESTA EN MODO DEVELOPMENT");
            host = "\"https://gestorvr.qualitat.cl/dev\"";
            Debug.Log("host -> "+host);
        }
        else
        {
            Debug.Log("LA COMPILACIÓN ESTA EN MODO PRODUCTION");
            host = "\"https://gestorvr.qualitat.cl/api\"";
            Debug.Log("host -> " + host);
        }


        lineChanger("    public static string enviroment_app = " + host + ";", path, 55);
#endif
        Debug.Log("SetAutoEnviroment.OnPreprocessBuild for target " + report.summary.platform + " at path " + report.summary.outputPath);


    }


    static void lineChanger(string newText, string fileName, int line_to_edit)
    {
        string[] arrLine = File.ReadAllLines(fileName);
        arrLine[line_to_edit - 1] = newText;
        File.WriteAllLines(fileName, arrLine);
    }


}
#endif