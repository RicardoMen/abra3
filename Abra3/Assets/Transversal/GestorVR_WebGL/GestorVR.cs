﻿//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using System;

//public class GestorVR : MonoBehaviour
//{
//	public delegate void upFicha(Ficha ficha);
//	public upFicha onUpFicha;
//	public enum MetodoEmpresa { RUT_EMPRESA, ID_EMPRESA, ID_DEVICE }
//	public enum MetodoTrabajador { RUT_TRABAJADOR, MAIL_TRABAJADOR, ID_TRABAJADOR, NOMBRE_TRABAJADOR }
//	/* Revisar pais https://datahub.io/es/dataset/iso-3166-1-alpha-2-country-codes/resource/9c3b30dd-f5f3-4bbe-a3cb-d7b2c21d66ce	*/
//	public enum Pais { NONE, AC, AD, AE, AF, AG, AI, AL, AM, AN, AO, AQ, AR, AS, AT, AU, AW, AX, AZ, BA, BB, BD, BE, BF, BG, BH, BI, BJ, BM, BN, BO, BR, BS, BT, BV, BW, BY, BZ, CA, CC, CD, CF, CG, CH, CI, CK, CL, CM, CN, CO, CR, CU, CV, CX, CY, CZ, DE, DJ, DK, DM, DO, DZ, EC, EE, EG, ER, ES, ET, FI, FJ, FK, FM, FO, FR, GA, GB, GD, GE, GF, GG, GH, GI, GL, GM, GN, GP, GQ, GR, GS, GT, GU, GW, GY, HK, HM, HN, HR, HT, HU, ID, IE, IL, IM, IN, IO, IQ, IR, IS, IT, JE, JM, JO, JP, KE, KG, KH, KI, KM, KN, KP, KR, KW, KY, KZ, LA, LB, LC, LI, LK, LR, LS, LT, LU, LV, LY, MA, MC, MD, ME, MG, MH, MK, ML, MM, MN, MO, MP, MQ, MR, MS, MT, MU, MV, MW, MX, MY, MZ, NA, NC, NE, NF, NG, NI, NL, NO, NP, NR, NU, NZ, OM, PA, PE, PF, PG, PH, PK, PL, PM, PN, PR, PT, PW, PY, QA, RE, RO, RS, RU, RW, SA, SB, SC, SD, SE, SG, SH, SI, SJ, SK, SL, SM, SN, SO, SR, ST, SV, SY, SZ, TA, TC, TD, TF, TG, TH, TJ, TK, TL, TM, TN, TO, TR, TT, TV, TW, TZ, UA, UG, UM, US, UY, UZ, VA, VC, VE, VG, VI, VN, VU, WF, WS, YE, YT, ZA, ZM, ZW }
//	public static GestorVR current;
//	[Header("Aplicacion")]
//	public string URL = "http://gestorvr.qualitatcorp.cl/sync/";
//	public string IDEmpresa;
//	public MetodoEmpresa metodoEmpresa;
//	public Pais pais = Pais.NONE;
//	public string Proyecto;
//	[Header("Trabajador")]
//	public MetodoTrabajador metodoTrabajador;
//	[Header("Evaluacion")]
//	public Ficha Evaluacion;
//	/*
//	 * Variables Dependientes
//	 */
//	public bool isEmpty
//	{
//		get
//		{
//			return (Evaluacion.Count == 0);
//		}
//	}
//	public string Keycode
//	{
//		get
//		{
//			switch (Application.platform)
//			{
//				case RuntimePlatform.WindowsEditor:
//					return "QUALITAT_TEST";
//				default:
//					return Proyecto;
//			}
//		}
//	}
//	public string Modelo
//	{
//		get
//		{
//			switch (Application.platform)
//			{
//				case RuntimePlatform.WindowsEditor:
//					return "QUALITAT_TEST";
//				default:
//					return "WEB_PLAYER";
//			}
//		}
//	}
//	public bool Habilitado
//	{
//		get
//		{
//			return (PlayerPrefs.GetInt("Habilitado") == 1);
//		}
//		set
//		{
//			PlayerPrefs.SetInt("Habilitado", (value) ? 1 : 0);
//		}
//	}
//	/*
//	 * Funciones de Evaluacion
//	 */
//	public void nuevaEvaluacion()
//	{
//		Evaluacion = new Ficha();
//		Debug.Log("Nueva evaluacion");
//	}
//	public void nuevaEvaluacion(string ID_TRABAJADOR)
//	{
//		nuevaEvaluacion();
//		Evaluacion.IDTrabajador = ID_TRABAJADOR;
//		Debug.Log("->" + ID_TRABAJADOR);
//	}
//	public void triggerSync()
//	{
//		StartCoroutine(SyncDispositivo());
//		StartCoroutine(SyncEvaluaciones());
//		Debug.Log("Syncronizacion Terminada");
//	}
//	/*public*/
//	IEnumerator SyncDispositivo()
//	{
//		WWWForm form = new WWWForm();
//		form.AddField("keycode", Keycode);
//		form.AddField("modelo", Modelo);
//		form.AddField("empresa", IDEmpresa);
//		form.AddField("metodo", metodoEmpresa.ToString());
//		WWW download = new WWW(URL + "dispositivo", form);
//		yield return download;
//		if (!string.IsNullOrEmpty(download.error))
//		{
//			Debug.LogError("Error downloading: " + download.error);
//			Debug.LogWarning("Error downloading: " + download.text);
//		}
//		else
//		{
//			Habilitado = bool.Parse(download.text);
//			if (Habilitado == false)
//				Debug.LogWarning("Dispositivo desactivado - no se sincronizara nada");
//		}
//		yield return new WaitForSeconds(2);
//	}
//	/*public*/
//	IEnumerator SyncEvaluaciones()
//	{
//		yield return new WaitForSeconds(1);
//		if (Habilitado)
//		{
//			WWWForm form = new WWWForm();
//			if (Evaluacion.Integridad && !Evaluacion.Sincronizado)
//			{
//				print(Evaluacion.IDTrabajador);
//				form.AddField("keycode", Keycode);
//				form.AddField("empresa", IDEmpresa);
//				form.AddField("metodo", metodoEmpresa.ToString());
//				form.AddField("metodoTrabajador", metodoTrabajador.ToString());
//				if (pais != Pais.NONE)
//					form.AddField("pais", pais.ToString());
//				if (!String.IsNullOrEmpty(Proyecto))
//					form.AddField("proyecto", Proyecto);
//				form.AddField("trabajador", Evaluacion.IDTrabajador);
//				for (int j = 0; j < Evaluacion.Count; j++)
//				{
//					form.AddField("Respuesta[" + j + "][Pregunta]", Evaluacion.IDPregunta[j]);
//					form.AddField("Respuesta[" + j + "][Alternativa]", Evaluacion.IDAlternativa[j]);
//					form.AddField("Respuesta[" + j + "][Creacion]", Evaluacion.DatePregunta[j]);
//				}
//				WWW download = new WWW(URL + "Evaluacion", form);
//				yield return download;
//				if (!string.IsNullOrEmpty(download.error))
//				{
//					Debug.LogError("Error downloading: " + download.error);
//					Debug.LogWarning(download.text);
//				}
//				else
//				{
//					Debug.Log(download.text);
//					Evaluacion.IDFicha = int.Parse(download.text);
//					if (onUpFicha != null)
//					{
//						onUpFicha(Evaluacion);
//					}
//				}
//			}
//		}
//	}
//	/*****************************
//	 * 		Clase Ficha
//	 *****************************/
//	[System.Serializable]
//	public class Ficha
//	{
//		public string IDTrabajador;
//		public List<int> IDPregunta;
//		public List<string> IDAlternativa;
//		public List<string> DatePregunta;
//		public int IDFicha;

//		public bool Integridad
//		{
//			get
//			{
//				return ((IDPregunta.Count == IDAlternativa.Count) && !string.IsNullOrEmpty(IDTrabajador) && Count > 0);
//			}
//		}
//		public bool Sincronizado { get { return (IDFicha != -1); } }
//		public int Count
//		{
//			get
//			{
//				return IDPregunta.Count;
//			}
//		}

//		public Ficha()
//		{
//			IDPregunta = new List<int>();
//			IDAlternativa = new List<string>();
//			DatePregunta = new List<string>();
//			IDFicha = -1;
//		}

//		public void addRespuesta(int IDpregunta, string IDalternativa)
//		{
//			if (existRespuesta(IDpregunta))
//			{
//				editRespuesta(IDpregunta, IDalternativa);
//			}
//			else
//			{
//				IDPregunta.Add(IDpregunta);
//				IDAlternativa.Add(IDalternativa);
//				DatePregunta.Add(DateTime.Now.ToString("yyyy-MM-dd h:mm:ss"));
//			}
//		}

//		public void editRespuesta(int IDpregunta, string IDalternativa)
//		{
//			if (existRespuesta(IDpregunta))
//			{
//				int index = IDPregunta.IndexOf(IDpregunta);
//				if (!IDAlternativa[index].Equals(IDalternativa))
//				{
//					DatePregunta[index] = DateTime.Now.ToString("yyyy-MM-dd h:mm:ss");
//					IDAlternativa[index] = IDalternativa;
//				}
//			}
//			else
//			{
//				Debug.LogError("No existe pregunta -> " + IDpregunta.ToString());
//			}
//		}

//		public bool delRespuesta(int IDpregunta)
//		{
//			if (existRespuesta(IDpregunta))
//			{
//				int index = IDPregunta.IndexOf(IDpregunta);
//				IDAlternativa.RemoveAt(index);
//				IDPregunta.RemoveAt(index);
//				DatePregunta.RemoveAt(index);
//				return true;
//			}
//			else
//			{
//				Debug.LogError("No existe pregunta -> " + IDpregunta.ToString());
//				return false;
//			}
//		}

//		public bool existRespuesta(int IDpregunta)
//		{
//			return IDPregunta.Contains(IDpregunta);
//		}
//	}
//	/********************************
//	 * 		Fin Clase Ficha
//	 ********************************/

//	/*
//	 * Funcion para dejar una unica instancia de Evaluacion 
//	 */
//	void Awake()
//	{
//		if (current == null)
//		{
//			DontDestroyOnLoad(gameObject);
//			current = this;
//		}
//		else if (current != this)
//		{
//			Destroy(gameObject);
//		}
//	}
//}