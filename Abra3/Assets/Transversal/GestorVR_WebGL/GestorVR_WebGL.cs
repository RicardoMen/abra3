﻿using Qualitat.GestorVR.Auth;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;



public class GestorVR_WebGL : MonoBehaviour
{
    private static readonly string host = Enviroment.enviroment_app;// "https://demo.qualitat.cl/api";
    /*****************************
	 * 		https ://gestorvr.qualitat.cl/api/v1/rvevaluacion/90?expand=preguntas.alternativas,preguntas.item// ejemplo para descargar preguntas y alternativas eva 90
	 * 	 private static readonly string eva = "https ://gestorvr.qualitat.cl/api/v1/rvevaluacion/1";//consulta eva 1
	 *****************************/
    [Header("Variables de Descarga y Subida a GestorVR")]

    private static readonly string expand = "?expand=preguntas.alternativas,preguntas.item";
    private static readonly string endpoint = "/v1/rvevaluacion/";
    //private static readonly string token = "/auth/token";

    // string requestUrlEvaluacion = $"{host}/v1/rvevaluacion/"+eva_id.ToString()+"90?expand=preguntas.alternativas,preguntas.item"; //
    private static readonly string keyEva = "eva_data";


    //"username":"qualitat",
    //"password":"qualitat.rv@408",
    //"grant_type":"password",
    //"client_id":"postman",
    //"client_secret":"AY0iVk6Vor"


    private static readonly string requestUrlLogin = $"{host}/auth/token";
    private static readonly string client = "postman";
    private static readonly string clientSecret = "AY0iVk6Vor";
    private static readonly string grantType = "password";

    private static readonly string keyUserAuth = "user_auth";

    public static Dictionary<string, string> parametros = new Dictionary<string, string>();

    [Header("JSONs")]
    static JSONObject fichaEvaluacion;
    public JSONObject jsonEva;
    public static JSONObject paramsSimulacion;
    public static JSONObject paramsJson = new JSONObject(JSONObject.Type.OBJECT);


    [Header("Descarga Evaluacion")]
    public int eva_id_Request;
    public Data_Evaluacion data_evaluacion;
    static string eva;

    [Header("Empresa")]
    public int id_empresa;

    //variables internas
    static int count;
    static int dispositivo;
    static int evaluacion;
    static int pais;


    IEnumerator Start()
    {
        //elimina playerprefs
        PlayerPrefs.DeleteAll();
        eva = eva_id_Request.ToString();




        yield return Login("qualitat", "qualitat.rv@408");
        yield return new WaitForSeconds(1);
        yield return GetEva();//obtiene y almacena evaluacion desde api
        yield return new WaitForSeconds(1);
        LoadEvaRuntime();//carga la evaluacion descargada


    }

    public static IEnumerator Login(string username, string password)
    {
        WWWForm form = new WWWForm();
        form.AddField("username", username);
        form.AddField("password", password);
        form.AddField("grant_type", grantType);
        form.AddField("client_id", client);
        form.AddField("client_secret", clientSecret);


        UnityWebRequest www = UnityWebRequest.Post(requestUrlLogin, form);

        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.LogError(www.error);
        }
        else if (www.isHttpError)
        {
            Debug.LogError(www.error);
            Debug.LogError(www.downloadHandler.text);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
            PlayerPrefs.SetString(keyUserAuth, www.downloadHandler.text);
            PlayerPrefs.Save();
            JSONObject userData = new JSONObject(www.downloadHandler.text);
            Debug.Log("Exito loggin : " + userData["scope"]["name"].str);
        }
    }

    private static void Sign(ref UnityWebRequest request)
    {
        string textUserAuth = PlayerPrefs.GetString(keyUserAuth);
        JSONObject userAuth = new JSONObject(textUserAuth);
        string token = userAuth.GetField("token").str;

        request.SetRequestHeader("Authorization", $"Bearer {token}");
    }
    public static bool isLogged()
    {
        return PlayerPrefs.HasKey(keyUserAuth);
    }

    public static IEnumerator GetEva()
    {
        if (isLogged())
        {

            string requestUrlEvaluacion = host + endpoint + eva + expand;
            UnityWebRequest www = UnityWebRequest.Get(requestUrlEvaluacion);

            Sign(ref www);

            yield return www.SendWebRequest();

            if (www.isNetworkError)
            {
                Debug.LogError(www.error);
            }
            else if (www.isHttpError)
            {
                Debug.LogError(www.error);
                Debug.LogError(www.downloadHandler.text);
            }
            else
            {
                PlayerPrefs.SetString(keyEva, www.downloadHandler.text);
                PlayerPrefs.Save();
                //  Debug.Log(PlayerPrefs.GetString(keyEva));
                // Qualitat.Helpers.File.Save(PlayerPrefs.GetString(keyEva), "as.txt");


            }
        }
        else
        {
            Debug.Log("Es nesesario estar logeado");
        }
    }



    public static bool HasData(string key)
    {
        return PlayerPrefs.HasKey(key);
    }
    public static JSONObject LoadEvaluacionJson()
    {
        if (HasData(keyEva))
        {
            string stringData = PlayerPrefs.GetString(keyEva);
            JSONObject jsonData = new JSONObject(stringData);
            return jsonData;
        }
        else
        {
            return new JSONObject();
        }
    }



    public int eva_id
    {
        get
        { return evaluacion; }
        set
        { evaluacion = value; }
    }

    public int disp_id
    {
        get
        { return dispositivo; }
        set
        { dispositivo = value; }
    }
    public int pais_id
    {
        get
        { return pais; }
        set
        { pais = value; }
    }


    public int Count
    {
        get
        {
            // Debug.Log(data_evaluacion.preguntas.Count);
            return data_evaluacion.preguntas.Count;
        }

    }

    public void LoadEvaRuntime()
    {
        jsonEva = LoadEvaluacionJson();
        //Debug.Log(jsonEva.Print());
        // Debug.Log(jsonEva.GetField("preguntas").Print());


        //EvaID_PreguntaxAlternativa evaPreAlt = new EvaID_PreguntaxAlternativa();

        data_evaluacion.eva_id = jsonEva.GetField("eva_id").ToString();
        data_evaluacion.tev_id = jsonEva.GetField("tev_id").ToString();
        data_evaluacion.nombre = jsonEva.GetField("nombre").str;
        data_evaluacion.descripcion = jsonEva.GetField("descripcion").str;
        data_evaluacion.nota = jsonEva.GetField("nota").str;
        data_evaluacion.orden = jsonEva.GetField("orden").str;
        data_evaluacion.reporte = jsonEva.GetField("reporte").str;
        data_evaluacion.creado = jsonEva.GetField("creado").str;
        data_evaluacion.modificado = jsonEva.GetField("modificado").str;
        data_evaluacion.habilitado = jsonEva.GetField("habilitado").str;
        List<Pregunta> preguntas = new List<Pregunta>();

        foreach (var j in jsonEva.GetField("preguntas").list)
        {
            string pre_id = j.GetField("pre_id").ToString();
            string eva_id = j.GetField("eva_id").ToString();
            string ite_id = j.GetField("ite_id").ToString();
            string descripcion = j.GetField("descripcion").str;
            string comentario = j.GetField("comentario").str;
            string ponderacion = j.GetField("ponderacion").ToString();
            string imagen = j.GetField("imagen").str;
            string creado = j.GetField("creado").str;
            string modificado = j.GetField("modificado").str;
            string habilitado = j.GetField("habilitado").str;
            List<Alternativa> Alt = new List<Alternativa>();
            //   List<Item> Itm = new List<Item>();

            //  Debug.Log(j.GetField("alternativas").Print());
            // Debug.Log(j.GetField("item").list.Count);

            foreach (var a in j.GetField("alternativas").list)
            {
                string alt_alt_id = a.GetField("alt_id").ToString();
                string alt_pre_id = a.GetField("pre_id").ToString();
                string alt_alternativa = a.GetField("alternativa").str;
                string alt_descripcion = a.GetField("descripcion").str;
                string alt_ponderacion = a.GetField("ponderacion").ToString();
                string alt_correcta = a.GetField("correcta").str;
                string alt_creado = a.GetField("creado").str;
                string alt_modificado = a.GetField("modificado").str;

                Alt.Add(new Alternativa
                {
                    alt_alt_id = alt_alt_id,
                    alt_pre_id = alt_pre_id,
                    alt_alternativa = alt_alternativa,
                    alt_descripcion = alt_descripcion,
                    alt_ponderacion = alt_ponderacion,
                    alt_correcta = alt_correcta,
                    alt_creado = alt_creado,
                    alt_modificado = alt_modificado
                });
            }


            //foreach (var a in j.GetField("item").list)
            //{
            //    string item_ite_id = a.GetField("ite_id").str;
            //    string item_nombre = a.GetField("nombre").str;


            //    Itm.Add(new Item
            //    {
            //        ite_id = item_ite_id,
            //        nombre = item_nombre

            //    });
            //}


            preguntas.Add(new Pregunta
            {
                pre_id = pre_id,
                eva_id = eva_id,
                ite_id = ite_id,
                descripcion = descripcion,
                comentario = comentario,
                ponderacion = ponderacion,
                imagen = imagen,
                creado = creado,
                modificado = modificado,
                habilitado = habilitado,
                alternativas = Alt,
                // item = Itm


            }
            );

        }

        data_evaluacion.preguntas = preguntas;

        count = Count;// almacena cantidad de preguntas en evaluacion descargada, esto es usaod para comprobar integridad de las respuestas cant bajado x cant recolectado
    }

    public static GestorVR_WebGL current;
    public string Proyecto;

    [Header("Ficha Evaluacion")]
    public Ficha Evaluacion;

    [Header("Resutado Ficha")]
    static int fichaRetornada;
    /*
	 * Variables Dependientes
	 */
    //public bool isEmpty
    //{
    //    get
    //    {
    //        return (Evaluacion.Count == 0);
    //    }
    //}



    /*
	 * Funciones de Evaluacion
	 */
    public void nuevaEvaluacion()
    {
        Evaluacion = new Ficha();
        Debug.Log("Nueva evaluacion");
    }
    public void nuevaEvaluacion(string ID_TRABAJADOR)
    {
        nuevaEvaluacion();
        Evaluacion.IDTrabajador = ID_TRABAJADOR;
        Debug.Log("->" + ID_TRABAJADOR);
    }

    /*****************************
	 * 		Clase Ficha
	 *****************************/

    [System.Serializable]
    public class Respuesta
    {
        public int ID_Pregunta;
        public string ID_Alternativa;
        public string Date_Pregrunta;
    }
    [System.Serializable]
    public class RespuestaGestorVR
    {
        public int alt_id;
        public string creado;
    }



    [System.Serializable]
    public class Ficha
    {
        public string IDTrabajador;
        public int trab_id;
        public int con_id;
        public int pro_id;
        public int disp_id;
        public int pais_id;
        public string calificacion;
        public string creado;

        public Trabajador trabajador = new Trabajador();
        public List<Respuesta> Respuestas = new List<Respuesta>();

        public bool Integridad
        {
            get
            {
                Debug.Log("Preguntas en Evaluacion -> " + count + " " + " Respondidas en la simulacion -> " + Respuestas.Count + " " + " Id del Trabajador -> " + IDTrabajador);
                return (count == Respuestas.Count) && !string.IsNullOrEmpty(IDTrabajador);
            }
        }


        public void addRespuesta(int IDpregunta, string IDalternativa)
        {
            if (existRespuesta(IDpregunta))
            {
                editRespuesta(IDpregunta, IDalternativa);
            }
            else
            {
                Respuesta resp = new Respuesta();
                resp.ID_Pregunta = IDpregunta;
                resp.ID_Alternativa = IDalternativa;
                resp.Date_Pregrunta = DateTime.Now.ToString("yyyy-MM-dd h:mm:ss");
                Respuestas.Add(resp);
            }
        }

        public void editRespuesta(int IDpregunta, string IDalternativa)
        {
            if (existRespuesta(IDpregunta))
            {
                foreach (var v in Respuestas)
                {
                    if (v.ID_Pregunta == IDpregunta)
                    {
                        Debug.LogWarning("Si existe, modificando respuesta a pregunta -> " + IDpregunta.ToString());
                        v.ID_Pregunta = IDpregunta;
                        v.ID_Alternativa = IDalternativa;
                        v.Date_Pregrunta = DateTime.Now.ToString("yyyy-MM-dd h:mm:ss");
                        break;
                    }
                }
            }
            else
            {
                Debug.LogError("No existe pregunta -> " + IDpregunta.ToString());
            }
        }

        public bool existRespuesta(int IDpregunta)
        {
            bool check = false;
            if (Respuestas.Count != 0)
            {
                if (Respuestas.Exists(x => x.ID_Pregunta == IDpregunta))
                {
                    check = true;
                }
                else { check = false; }
            }

            return check;
        }

        public void Cerrar()
        {

            if (Integridad)
            {
                trabajador.rut = IDTrabajador;//set identidad del trabajador

                //se crea el json para almacenar la inforamcion de la ficha evaluacion
                fichaEvaluacion = new JSONObject();

                /*datos ficha*/
                if (evaluacion > 0) fichaEvaluacion.AddField("eva_id", evaluacion);
                //  if (trab_id > 0) fichaEvaluacion.AddField("trab_id", trab_id);
                // if (con_id > 0) fichaEvaluacion.AddField("con_id", con_id);
                // if (pro_id > 0) fichaEvaluacion.AddField("pro_id", pro_id);
                if (dispositivo > 0) fichaEvaluacion.AddField("disp_id", dispositivo);
                if (pais > 0) fichaEvaluacion.AddField("pais_id", pais);
                // if (!string.IsNullOrEmpty(calificacion)) fichaEvaluacion.AddField("calificacion", calificacion);
                fichaEvaluacion.AddField("creado", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                /*datos trabajador*/
                JSONObject trabajadorJson = new JSONObject();
                if (trabajador.antiguedad > 0) trabajadorJson.AddField("antiguedad", trabajador.antiguedad);
                if (trabajador.hijos > 0) trabajadorJson.AddField("hijos", trabajador.hijos);
                if (!string.IsNullOrEmpty(trabajador.nombre)) trabajadorJson.AddField("nombre", trabajador.nombre);
                if (!string.IsNullOrEmpty(trabajador.paterno)) trabajadorJson.AddField("paterno", trabajador.paterno);
                if (!string.IsNullOrEmpty(trabajador.materno)) trabajadorJson.AddField("materno", trabajador.materno);
                if (!string.IsNullOrEmpty(trabajador.sexo)) trabajadorJson.AddField("sexo", trabajador.sexo);
                if (!string.IsNullOrEmpty(trabajador.nacimiento)) trabajadorJson.AddField("nacimiento", trabajador.nacimiento);
                if (!string.IsNullOrEmpty(trabajador.fono)) trabajadorJson.AddField("fono", trabajador.fono);
                if (!string.IsNullOrEmpty(trabajador.mail)) trabajadorJson.AddField("mail", trabajador.mail);
                if (!string.IsNullOrEmpty(trabajador.gerencia)) trabajadorJson.AddField("gerencia", trabajador.gerencia);
                if (!string.IsNullOrEmpty(trabajador.cargo)) trabajadorJson.AddField("cargo", trabajador.cargo);
                if (!string.IsNullOrEmpty(trabajador.estado_civil)) trabajadorJson.AddField("estado_civil", trabajador.estado_civil);
                if (!string.IsNullOrEmpty(trabajador.rut)) trabajadorJson.AddField("rut", trabajador.rut);


                fichaEvaluacion.AddField("trabajador", trabajadorJson);

                foreach (var v in Respuestas)
                {

                    current.Match_Preid_Altid(v.ID_Pregunta, v.ID_Alternativa);
                }

                /*datos respuestas*/
                if (current.respuestaGestor.Count > 0)
                {
                    JSONObject respuestasjson = new JSONObject(JSONObject.Type.ARRAY);
                    for (int i = 0; i < Respuestas.Count; i++)
                    {
                        JSONObject respuestaJson = new JSONObject();
                        respuestaJson.AddField("alt_id", current.respuestaGestor[i].alt_id);
                        respuestaJson.AddField("creado", current.respuestaGestor[i].creado);
                        respuestasjson.Add(respuestaJson);
                    }

                    fichaEvaluacion.AddField("respuestas", respuestasjson);
                }


                /*datos parametros*/
                ///////////////////////////////////////////////////////////////



                ////parametros de app

                //paramsJson.AddField("app", AppJson());

                ////parametros de device


                //paramsJson.AddField("device", DeviceJson());



                if (!paramsJson.IsNull)
                {
                    fichaEvaluacion.AddField("params", paramsJson);
                }
                else
                {
                    Debug.LogError("Los Parametros no pueden ser nulos");


                }

                ///////////////////////////////////////////////////////////////////


                //}
                //else
                //{
                //   //no hay parametros de simulacion pero agregarar un eobjeto vacio
                //    JSONObject paramsJson = new JSONObject(JSONObject.Type.OBJECT);
                //    paramsJson.AddField("data", dataJson);




                //    //si existen datos en params version json lo agrega al params principal
                //    if (!paramsSimulacion.IsNull)
                //    {
                //        Debug.Log("imprimiendo el params json " + paramsSimulacion.Print());
                //        paramsJson.Merge(paramsSimulacion);
                //    }



                //    //parametros de app

                //    JSONObject appJson = new JSONObject(JSONObject.Type.OBJECT);
                //    appJson.AddField("platform", Application.platform.ToString("g"));
                //    appJson.AddField("unityVersion", Application.unityVersion);
                //    appJson.AddField("build", Application.buildGUID);
                //    appJson.AddField("companyName", Application.companyName);
                //    appJson.AddField("version", Application.version);
                //    appJson.AddField("productName", Application.productName);

                //    paramsJson.AddField("app", appJson);

                //    //parametros de device
                //    JSONObject deviceJson = new JSONObject(JSONObject.Type.OBJECT);
                //    deviceJson.AddField("name", SystemInfo.deviceName);
                //    deviceJson.AddField("type", SystemInfo.deviceType.ToString("g"));
                //    deviceJson.AddField("model", SystemInfo.deviceModel);
                //    deviceJson.AddField("uid", SystemInfo.deviceUniqueIdentifier);

                //    paramsJson.AddField("device", deviceJson);

                //    fichaEvaluacion.AddField("params", paramsJson);
                //    ///////////////////////////////////////////////////////////////////////
                //}





                Debug.Log(fichaEvaluacion.Print());
            }
            else
            {
                Debug.LogError("No paso el control de integridad");
            }


            //foreach(var v in current.rgvr)//imprime datos
            //{
            //    Debug.Log("alt_id -> "+ v.alt_id + " con fecha -> " + v.creado);
            //}
        }


    }
    //public static JSONObject AppJson()
    //{
    //    //parametros de app
    //    JSONObject appJson = new JSONObject(JSONObject.Type.OBJECT);
    //    appJson.AddField("platform", Application.platform.ToString("g"));
    //    appJson.AddField("unityVersion", Application.unityVersion);
    //    appJson.AddField("build", Application.buildGUID);
    //    appJson.AddField("companyName", Application.companyName);
    //    appJson.AddField("version", Application.version);
    //    appJson.AddField("productName", Application.productName);

    //    return appJson;
    //}
    //public static JSONObject DeviceJson()
    //{
    //    //parametros de device
    //    JSONObject deviceJson = new JSONObject(JSONObject.Type.OBJECT);
    //    deviceJson.AddField("name", SystemInfo.deviceName);
    //    deviceJson.AddField("type", SystemInfo.deviceType.ToString("g"));
    //    deviceJson.AddField("model", SystemInfo.deviceModel);
    //    deviceJson.AddField("uid", SystemInfo.deviceUniqueIdentifier);

    //    return deviceJson;

    //}


    public void Parametros(Dictionary<string, string> p)
    {

        parametros = p;

        //añade parametro de tipo de compilacion ejecutada
        //RuntimePlatform rp = Application.platform;
        // rp.ToString();
        // parametros.Add("app",rp.ToString());

    }
    public void AddParametros(JSONObject j)
    {

        paramsJson.Merge(j);

        //añade parametro de tipo de compilacion ejecutada
        //RuntimePlatform rp = Application.platform;
        // rp.ToString();
        // parametros.Add("app",rp.ToString());

    }
    public void AddParamsSimulacion(JSONObject json)
    {

        paramsSimulacion = json;

        //añade parametro de tipo de compilacion ejecutada
        //RuntimePlatform rp = Application.platform;
        // rp.ToString();
        // parametros.Add("app",rp.ToString());

    }
    /********************************
	 * 		Fin Clase Ficha
	 ********************************/


    /********************************
    * 		Clase Data Evaluacion Preguntas y Alternativas
    ********************************/

    [System.Serializable]
    public class Data_Evaluacion
    {
        public string eva_id;
        public string tev_id;
        public string nombre;
        public string descripcion;
        public string nota;
        public string orden;
        public string reporte;
        public string creado;
        public string modificado;
        public string habilitado;
        public List<Pregunta> preguntas;


    }



    /********************************
    * 		Clase Evaluacion Preguntas y Alternativas
    ********************************/

    /*
	 * Funcion para dejar una unica instancia de Evaluacion 
	 */
    void Awake()
    {
        if (current == null)
        {
            DontDestroyOnLoad(gameObject);
            current = this;
        }
        else if (current != this)
        {
            Destroy(gameObject);
        }
    }

    public List<RespuestaGestorVR> respuestaGestor;

    public void Match_Preid_Altid(int IDpregunta, string IDalternativa)
    {
        //comparar la ficha evaluacion con evaluacion pregunta alternativas y realizar match en busqueda de los alt_id
        //finalmente añade a una lista temporal para rescate de datos


        foreach (var p in data_evaluacion.preguntas)
        {

            if (p.pre_id == IDpregunta.ToString())
            {

                foreach (var a in p.alternativas)
                {

                    if (a.alt_alternativa == IDalternativa)
                    {

                        respuestaGestor.Add(new RespuestaGestorVR { alt_id = Convert.ToInt32(a.alt_alt_id), creado = DateTime.Now.ToString("yyyy-MM-dd h:mm:ss") });
                    }
                }
            }
        }

    }


    public void SyncFicha()
    {
        StartCoroutine(Sync());
    }
    public static IEnumerator Sync()
    {
        if (isLogged())
        {
            //test
            //JSONObject json = new JSONObject("{\"eva_id\":2,\"disp_id\":2323,\"creado\":\"2020-10-13 17:59:04\",\"trabajador\":{\"rut\":\"66.666.666-6\"},\"respuestas\":[{\"alt_id\":6,\"creado\":\"2020-10-13 17:59:04\"},{\"alt_id\":8,\"creado\":\"2020-10-13 17:59:04\"}]}");

            string requestSendEvaluation = host + "/v1/rvficha/evaluacion"; //"https://demo.qualitat.cl/api/v1/rvficha/evaluacion";
            UnityWebRequest request = Http.Post(requestSendEvaluation, fichaEvaluacion.Print());

            Sign(ref request);

            yield return request.SendWebRequest();

            if (request.isNetworkError || request.isHttpError)
            {

                Debug.LogErrorFormat("[Ficha Error {0} request={1} {2} body={3} Error={4}]", request.responseCode, request.method, request.url, request.downloadHandler.text, request.error);

            }

            //    if (request.isNetworkError)
            //{
            //    Debug.LogError(request.error);
            //}
            //else if (request.isHttpError)
            //{
            //    Debug.LogError(request.error);
            //    Debug.LogError(request.downloadHandler.text);
            //}
            else
            {
                Debug.LogFormat("[Ficha Exito {0} request={1} {2} body={3}]", request.responseCode, request.method, request.url, request.downloadHandler.text);

                FichaResultado.current.LoadFichaResultado(request.downloadHandler.text);//enviamos el resultado de la ficha a la clase de resultado para manipulacion posterior
            }
        }
        else
        {
            Debug.Log("Es nesesario estar logeado");
        }
    }

    //private void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.S))
    //    {
    //        StartCoroutine(Sync());
    //    }
    //    if (Input.GetKeyDown(KeyCode.P))
    //    {

    //    }
    //}
}
