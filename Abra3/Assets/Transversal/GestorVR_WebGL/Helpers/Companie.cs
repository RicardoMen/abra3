﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Companie : MonoBehaviour
{
    public CompaniesManager.COMPANIES _companie;

    [MMEnumCondition("_companie", 0)] public CompaniesManager.SENA _sena;
    [MMEnumCondition("_companie", 1)] public CompaniesManager.MINING_CAP _mining_Cap;
   // [MMEnumCondition("_companie", 2)] public CompaniesManager.VRCLASS_TESTING _vrClass_Testing;
    [MMEnumCondition("_companie", 3)] public CompaniesManager.VRCLASS _vrClass;
   // [MMEnumCondition("_companie", 4)] public CompaniesManager.CANAL_DA_SEG _canal_da_Seg;
    [MMEnumCondition("_companie", 5)] public CompaniesManager.SENAI _senai;
    [MMEnumCondition("_companie", 6)] public CompaniesManager.GLOBAL _global;
  //  [MMEnumCondition("_companie", 7)] public CompaniesManager.SENATI _senati;



    public static Companie Instance;
    [MMReadOnly] public int _proID = 0;
    public int _pais;

    void Awake()//skeleton
    {
        if (Instance == null)
        {

            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else
        {
            if (Instance != this)
            {
                Destroy(gameObject);
            }
        }
        switch ((int)_companie)
        {
            case 0:
                _proID = (int)_sena;
                _pais = 49;//colombia
                break;
            case 1:
                _proID = (int)_mining_Cap;
                _pais = 46;//chile
                break;
            case 2:
              //  _proID = (int)_vrClass_Testing;
                _pais = 46;//chile
                break;
            case 3:
                _proID = (int)_vrClass;
                _pais = 46;//chile
                break;
            case 4:
              //  _proID = (int)_canal_da_Seg;
                _pais = 31;//brasil
                break;
            case 5:
                _proID = (int)_senai;
                _pais = 31;//brasil
                break;
            case 6:
                _proID = (int)_global;
                _pais = 245;//global
                break;
            case 7:
              //  _proID = (int)_senati;
                _pais = 171;//peru
                break;

        }
    }

}
