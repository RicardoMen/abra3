﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CompaniesManager
{
    public enum COMPANIES
    {
        SENA,
        MINING_CAP,
        VRCLAS,
        CANAL_DA_SEG,
        SENAI,
        GLOBAL,
        SENATI,
        VRCLASS
    }
    public enum SENA
    {
        None = 0,
        Espacios = 7,
        Altura = 9
    }
    public enum MINING_CAP
    {
        None = 0,
        Izaje = 10,
        Percepción = 11,
        Altura = 12,
        Espacios = 13
    }
    public enum VRCLAS
    {
        None = 0,
        Altura = 16,
        Percepción = 17,
        Espacios = 18,
        Izaje = 19,
        VrFirex_Oficina = 20,
        VrFirex_Bodega = 21,
        VrFirex_Concentradora = 22,
        Eléctrico = 24,
        Vial = 25
    }
    public enum CANAL_DA_SEG
    {
        Percepcion = 53,
        Electrico = 52,
        Altura = 51,
        Espacios = 54,
    }
    public enum SENAI
    {
        None = 0,
        Altura = 26,
        Izaje = 27,
        Electrico = 28,
        Espacios = 29,
        Vial = 30,
        Percepcion = 44
    }
    public enum GLOBAL
    {
        None = 0,
        Altura = 37,
        Espacios = 38,
        Izaje = 39,
        Percepcion = 40,
        Electrico = 41,
        Vial = 42
    }
   
    
    public enum SENATI
    {
        Altura = 48,
        Electrico = 49
    }
    public enum VRCLASS
    {
        None = 0,
        InstalacionesSanitarias = 50,
        Sanitarias_mod01 = 58,
        Sanitarias_mod02 = 59,
        Sanitarias_mod03 = 60,
        Sanitarias_mod04 = 61,
        PDR_Fatalidades = 62,
        PDR_FatalidadesV2 = 63,
        PDR_FatalidadesV3 = 64
    }

}
