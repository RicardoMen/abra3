﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Http : MonoBehaviour
{
    public static UnityWebRequest Post(string url, string data)
    {
        var request = Request("POST", url);
        request.uploadHandler = FormData(data);
        request.uploadHandler.contentType = "application/json";
        return request;
    }

    public static UnityWebRequest Request(string Verb, string url)
    {
        var request = new UnityWebRequest(url, "POST");
        request.downloadHandler = new DownloadHandlerBuffer();
        return request;
    }
    public static UploadHandlerRaw FormData(string data)
    {
        return new UploadHandlerRaw(ToRawBody(data));
    }
    public static byte[] ToRawBody(string body)
    {
        return System.Text.Encoding.UTF8.GetBytes(body);
    }
}