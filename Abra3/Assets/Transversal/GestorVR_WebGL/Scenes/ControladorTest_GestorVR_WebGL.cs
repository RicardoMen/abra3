﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ControladorTest_GestorVR_WebGL : MonoBehaviour
{
    //public Dictionary<string, string> parametros = new Dictionary<string, string>();
    public JSONObject infoJsonSimulacion = new JSONObject(JSONObject.Type.OBJECT);

    [System.Serializable]
    public class TestClass
    {
        public string texto;
        public int numero;
    }
    IEnumerator Start()
    {
        //RuntimePlatform rp = Application.platform;
        //rp.ToString();
        //Debug.Log(rp);
        // yield return new WaitForSeconds(5);

        yield return new WaitForSeconds(5);
        GestorVR_WebGL.current.nuevaEvaluacion("11.111.111-1");
        GestorVR_WebGL.current.Evaluacion.trabajador.mail = "test@test.com";
        GestorVR_WebGL.current.Evaluacion.trabajador.nombre = "Usuario";
        GestorVR_WebGL.current.Evaluacion.trabajador.paterno = "Test";
        GestorVR_WebGL.current.Evaluacion.trabajador.materno = "Test";
        GestorVR_WebGL.current.disp_id = 2531;
        GestorVR_WebGL.current.eva_id = 99;


        GestorVR_WebGL.current.Evaluacion.addRespuesta(2182, "b");
        GestorVR_WebGL.current.Evaluacion.addRespuesta(2183, "e");
        GestorVR_WebGL.current.Evaluacion.addRespuesta(2184, "si");
        GestorVR_WebGL.current.Evaluacion.addRespuesta(2185, "no");



        //parametros.Add("sede","Concepcion");
        //parametros.Add("curso", "VG002");
        //parametros.Add("proID", "7");


        //  GestorVR_WebGL.current.AddParams(parametros);

        // GestorVR_WebGL.current.AddParams(ControllerTransversal.current.Inscripcion.content.ficha.parametros);



        JSONObject objetoAnidado = new JSONObject();
        objetoAnidado.AddField("atributo_1", "1");
        objetoAnidado.AddField("atributo2", 1);
        infoJsonSimulacion.AddField("Objeto",objetoAnidado);
        


        JSONObject arrayAnidado = new JSONObject();
        arrayAnidado.Add(1);
        arrayAnidado.Add(2);
        arrayAnidado.Add(3);
        infoJsonSimulacion.AddField("Arreglo", arrayAnidado);
        

        JSONObject objeto = new JSONObject();
        objeto.AddField("array", arrayAnidado);
        objeto.AddField("objeto", objetoAnidado);
        infoJsonSimulacion.AddField("ObjetoCompleto", objeto);


      

       

        TestClass testClass = new TestClass()
        {
            texto = "Texto prueba",
            numero = 2
        };
        JSONObject test_class = new JSONObject();

        infoJsonSimulacion.AddField("class", test_class);

        GestorVR_WebGL.current.AddParametros(infoJsonSimulacion);

        GestorVR_WebGL.current.Evaluacion.Cerrar();
        GestorVR_WebGL.current.SyncFicha();




    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {

            GestorVR_WebGL.current.SyncFicha();
        }
    }

}
