﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;
public class ControladorVR : MonoBehaviour
{
    [Header("Configuración GestorVR")]
    public EvaluacionVR evaluacionvr;
    [Tooltip("Se debe ingresar la identificación de las evaluaciónes")]
    public List<int> evaluaciones;

    void Awake()
    {
        evaluacionvr = gameObject.GetComponent<EvaluacionVR>();
        //evaluacionvr.enabled = false;
    }


    public IEnumerator ControllerCorountine()
    {
        yield return new WaitForSeconds(2);
        StartCoroutine(Qualitat.GestorVR.App.AutoLogin());
        yield return new WaitUntil(Qualitat.GestorVR.App.session.isValid);
        StartCoroutine(Qualitat.GestorVR.Module.Schema.Evaluacion.Resolve(evaluaciones));
    }
}