﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Qualitat.Transversal;
public class ControllerLoginUser : MonoBehaviour
{

    public CompaniesManager.COMPANIES _companie;

    [MMEnumCondition("_companie", 0)] public CompaniesManager.SENA _sena;
    [MMEnumCondition("_companie", 1)] public CompaniesManager.MINING_CAP _mining_Cap;
    [MMEnumCondition("_companie", 2)] public CompaniesManager.VRCLAS _vrClas;
    [MMEnumCondition("_companie", 3)] public CompaniesManager.CANAL_DA_SEG _canaldaSeg;
    [MMEnumCondition("_companie", 4)] public CompaniesManager.SENAI _senai;
    [MMEnumCondition("_companie", 5)] public CompaniesManager.GLOBAL _global;
    [MMEnumCondition("_companie", 6)] public CompaniesManager.SENATI _senati;
    [MMEnumCondition("_companie", 7)] public CompaniesManager.VRCLASS _VRClass;
    public Inscripcion inscripcion;
    public static ControllerLoginUser Instance;
    public bool _withoutData;

    [MMReadOnly] public int _proID = 0;

    void Awake()//skeleton
    {
        if (Instance == null)
        {

            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else
        {
            if (Instance != this)
            {
                Destroy(gameObject);
            }
        }
        switch ((int)_companie)
        {
            case 0:
                _proID = (int)_sena;
                break;
            case 1:
                _proID = (int)_mining_Cap;
                break;
            case 2:
                _proID = (int)_vrClas;
                break;
            case 3:
                _proID = (int)_canaldaSeg;
                break;
            case 4:
                _proID = (int)_senai;
                break;
            case 5:
                _proID = (int)_global;
                break;
            case 6:
                _proID = (int)_senati;
                break;
            case 7:
                // Debug.Log("sdfdfsdf");
                _proID = (int)_VRClass;
                break;
        }
    }


    private IEnumerator Start()
    {
        PlayerPrefs.DeleteAll();
        yield return Qualitat.Transversal.GetData.Login("GestorVR_Unity", "nnE3de0XEKwOTVja5OeAohhbpOpoQxMd");
    }

    public IEnumerator Login(string user, string pw)
    {
        Debug.Log("usuario -> " + user + " contraseña -> " + pw + " proyecto -> " + _proID);
        yield return Qualitat.Transversal.GetData.LoginUser(user, pw, _proID);

        Debug.Log(GetData.responseCode);
        if (GetData.responseCode == 200)//verifica si el token es valido y tiene autorizacion
        {
            inscripcion = UserData.LoadDataInscription(GetData.LoadData());
        }
    }
    public void HasData()
    {
        if (_withoutData)
        {
            PlayerPrefs.SetString("_withoutData", "No Subira Datos a GestorVR");
        }
        else
        {
            PlayerPrefs.DeleteKey("_withoutData");

        }
    }
}