﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qualitat.Transversal;
using System;
using UnityEngine.UI;

public class ControllerTokenUser : MonoBehaviour
{

    public SceneField menuScene;
    public Text token_expire;
    public string access_token;
    public Inscripcion inscripcion;
    public static ControllerTokenUser Instance;
    void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else
        {
            if (Instance != this)
            {
                Destroy(gameObject);
            }
        }
    }


    public bool useLocal;
    IEnumerator Start()
    {

        PlayerPrefs.DeleteAll();//linea para comprobar funcionamiento, comentar en produccion

        yield return GetData.Login("GestorVR_Unity", "nnE3de0XEKwOTVja5OeAohhbpOpoQxMd");//login a gestorvr
        yield return new WaitForSeconds(5);
        if (!useLocal)
        {
            access_token = URLParameters.GetSearchParameters().GetString("access-token", "vacio");
        }
        if (access_token != "vacio")
        {
            Debug.Log("access-token: " + access_token);
            yield return new WaitForSeconds(2);
            //yield return GetData.InscriptionData(access_token);
            yield return GetData.TokenUser(access_token);
            Debug.Log(GetData.responseCode);
            switch (GetData.responseCode)
            {
                case 200:
                    Debug.Log("Token valido");
                    token_expire.text = "Ingresando al sistema...";
                    inscripcion = UserData.LoadDataInscription(GetData.LoadData());
                    StopAnimation();
                    Transition.LoadLevel(menuScene, 1f, Color.black);
                    break;
                case 403:
                    Debug.Log("No esta autorizado, token expirado");
                    token_expire.text = "Usted no esta Autorizado";
                    StopAnimation();
                    break;
                default:
                    Debug.LogError("ERROR EN EL SISTEMA.");
                    token_expire.text = "Error desconocido.";
                    StopAnimation();
                    break;
            }
        }
        else
        {
            Debug.LogError("No viene el token de autorización");
            Transition.LoadLevel(menuScene, 1f, Color.black);
            Destroy(this);
        }

    }
    private Animation[] loaderAnim;
    void StopAnimation()
    {
        loaderAnim = FindObjectsOfType(typeof(Animation)) as Animation[];
        foreach (Animation item in loaderAnim)
        {
            item.Stop();
        }
    }

}
