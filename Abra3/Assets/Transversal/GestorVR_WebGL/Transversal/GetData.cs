﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Qualitat.Transversal
{


public class GetData : MonoBehaviour
{

        // private static readonly string host = "/api";
        private static string host = Enviroment.enviroment_app; // "https://gestorvr.qualitat.cl/api"; //activar para pruebas en editor local
        public static long responseCode;


    [Header("Variables del conexion y consulta a gestorvr")]
    private static readonly string keyInscription = "inscription_data";

    //"username":"qualitat",
    //"password":"qualitat.rv@408",
    //"grant_type":"password",
    //"client_id":"postman",
    //"client_secret":"AY0iVk6Vor"


    private static readonly string requestUrlLogin = $"{host}/auth/token";
    private static readonly string client = "postman";
    private static readonly string clientSecret = "AY0iVk6Vor";
    private static readonly string grantType = "password";

    private static readonly string keyUserAuth = "user_auth";
    private static readonly string keyUserLogin = "user_login";


   // private static readonly string token_endpoint = "/v1/transversal/token?access-token=";
   // private static readonly string token_test = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJ1aWQiOjEsInN1YiI6Il9zb00iLCJpYXQiOjE2MDk3ODg3NzIsImluc0lkIjoxLCJwcm9JZCI6NywiZW1wSWQiOjF9.okS_tt_ZfdVecj84unCt-cmiTaMj06UDdySrxHHdbNYlksnUS1S4HKyFa1LIKtq8tN9rwcUkkdzzSzdfZ18oOUfLEdZqkPpIpGEZzQUhtvSZTy_URy14WlRP5Cpxjh4Isw5FcYDNr0Ifooah6PPKtjcuSDzN5hGIS9W83ZXd0pADCF9tzkS5095cgInWwafrJ6x1h26G_YdClOgOcyF8Nv9fBzlH36rJrqWBizH5D95ow4qkPnDaLYvxUqcF738-w3_dnl72Mvf93YI2621goEYc4mZOnMyfyxHPqPmGl-_I0K5W8Zlt3sq-LpvJNvRvJEjFCH2alEmNBUX_bRtO1lNGRyihecQZ3N5zlcTZS8-SrF2TiEq-iH0sW9TRljAPWpnR6O0P3QOEF2XFHUWfSRpr0yL0EXg7bagkxHpoY-sVfdnT00HnQ9lCGf_AknFAi5m2PfN7Z9UUwfUGndRzMfD2f-LcYZUcXXabGfFTmC3WtcrInchsNq6I9ytTgaag8VaQMIpnKU-x7fr4UtcaF4C7QIJF7PFlEGPd_VfK7oc3DodQRJCDAqQ-cXnvotZtk4DGQqoovjmeF8D7bzeUYLpN8aonm-ke891ST5laM90MlcF2575OjxOiec4De2q_S596Og1XqgeFE5M5o0CpJW-6fIeWDgdfUYgC7RuUziQ";
   // private static readonly string inscription_endpoint = "&expand=inscription";


    
    IEnumerator Start()
    {
            yield return 0;

            Debug.Log("host: "+host);

            //PlayerPrefs.DeleteAll();

            //yield return Login("GestorVR_Unity", "nnE3de0XEKwOTVja5OeAohhbpOpoQxMd");
            //yield return new WaitForSeconds(2);
            //yield return InscriptionData(token_test);
            //yield return new WaitForSeconds(2);
            //yield return LoadData();



        }

        public static IEnumerator Login(string username, string password)
    {
        WWWForm form = new WWWForm();
        form.AddField("username", username);
        form.AddField("password", password);
        form.AddField("grant_type", grantType);
        form.AddField("client_id", client);
        form.AddField("client_secret", clientSecret);


        UnityWebRequest www = UnityWebRequest.Post(requestUrlLogin, form);

        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            Debug.LogError(www.error);
        }
        else if (www.isHttpError)
        {
            Debug.LogError(www.error);
            Debug.LogError(www.downloadHandler.text);
        }
        else
        {
            Debug.Log(www.downloadHandler.text);
            PlayerPrefs.SetString(keyUserAuth, www.downloadHandler.text);
            PlayerPrefs.Save();
            JSONObject userData = new JSONObject(www.downloadHandler.text);
            Debug.Log("Exito loggin : " + userData["scope"]["name"].str);
        }
    }

    private static void Sign(ref UnityWebRequest request)
    {
        string textUserAuth = PlayerPrefs.GetString(keyUserAuth);
        JSONObject userAuth = new JSONObject(textUserAuth);
        string token = userAuth.GetField("token").str;

        request.SetRequestHeader("Authorization", $"Bearer {token}");
    }
    public static bool isLogged()
    {
        return PlayerPrefs.HasKey(keyUserAuth);
    }
        
    //public static IEnumerator InscriptionData(string token)
    //{
    //    if (isLogged())
    //    {
           

    //            // string requestUrl = host + token_endpoint + token_test + inscription_endpoint;
    //            string requestUrl = host + token_endpoint + token + inscription_endpoint;


    //           // Debug.Log(requestUrl);

    //        UnityWebRequest www = UnityWebRequest.Get(requestUrl);

    //        Sign(ref www);

    //        yield return www.SendWebRequest();

    //        if (www.isNetworkError)
    //        {
    //            Debug.LogError(www.error);
    //            responseCode = www.responseCode;
    //            }
    //        else if (www.isHttpError)
    //        {
    //            Debug.LogError(www.error);
    //            Debug.LogError(www.downloadHandler.text);
    //            responseCode = www.responseCode;
                    
    //        }
    //        else
    //        {
    //            PlayerPrefs.SetString(keyInscription, www.downloadHandler.text);
    //            PlayerPrefs.Save();
    //            responseCode = www.responseCode;
    //                // Debug.Log(PlayerPrefs.GetString(keyInscription));
    //                // Qualitat.Helpers.File.Save(PlayerPrefs.GetString(keyInscription), "as.txt");


    //            }
    //    }
    //    else
    //    {
    //        Debug.Log("Es nesesario estar logeado");
    //    }
    //}



    public static bool HasData(string key)
    {
        return PlayerPrefs.HasKey(key);
    }
    public static JSONObject LoadData()
    {
            
            if (HasData(keyInscription))
        {
            string stringData = PlayerPrefs.GetString(keyInscription);
             
           // Debug.Log("intentando imprimir playerpref json bajado "+stringData);
            JSONObject jsonData = new JSONObject(stringData);
            Debug.Log(jsonData.Print());
            return jsonData;
        }
        else
        {
            Debug.Log("json vacio");
            return new JSONObject();
        }
    }

        public static JSONObject LoadDataLogin()
        {

            if (HasData(keyUserLogin))
            {
                string stringData = PlayerPrefs.GetString(keyUserLogin);

                // Debug.Log("intentando imprimir playerpref json bajado "+stringData);
                JSONObject jsonData = new JSONObject(stringData);
                Debug.Log(jsonData.Print());
                return jsonData;
            }
            else
            {
                Debug.Log("json vacio");
                return new JSONObject();
            }
        }


        public static IEnumerator TokenUser(string token)
        {
            if (isLogged())
            {
                Debug.Log("Obteniendo información de la inscripción");
                string endpoint_tokenuser = "/v1/transversal/inscriptionByToken";


                string requestUrl = host + endpoint_tokenuser;

                WWWForm form = new WWWForm();
                form.AddField("access-token", token);
                




                UnityWebRequest www = UnityWebRequest.Post(requestUrl, form);

                Sign(ref www);

                yield return www.SendWebRequest();

                if (www.isNetworkError)
                {
                    Debug.LogError(www.error);
                    responseCode = www.responseCode;
                }
                else if (www.isHttpError)
                {
                    Debug.LogError(www.error);
                    Debug.LogError(www.downloadHandler.text);
                    responseCode = www.responseCode;
                }
                else
                {
                    Debug.Log(www.downloadHandler.text);
                    PlayerPrefs.SetString(keyInscription, www.downloadHandler.text);
                    PlayerPrefs.Save();
                    JSONObject userData = new JSONObject(www.downloadHandler.text);
                    responseCode = www.responseCode;
                    // Debug.Log(userData.Print());
                    //    Debug.Log("Exito loggin : " + userData["scope"]["name"].str);
                }
            }
            else
            {
                Debug.Log("Es nesesario estar logeado");
            }
        }

        public static IEnumerator LoginUser(string username, string password, int proId)//, int empId
        {
            if (isLogged())
            {
                Debug.Log("Obteniendo información de la inscripción");
                string endpoint_loginuser = "/v1/transversal/inscriptionByUser";

              
                string requestUrl = host + endpoint_loginuser;
                Debug.Log(requestUrl);
                WWWForm form = new WWWForm();
                form.AddField("username", username);
                form.AddField("password", password);
                form.AddField("proId", proId);
                //form.AddField("empId", empId);




                UnityWebRequest www = UnityWebRequest.Post(requestUrl, form);

                Sign(ref www);

                yield return www.SendWebRequest();

                if (www.isNetworkError)
                {
                    Debug.LogError(www.error);
                    responseCode = www.responseCode;
                }
                else if (www.isHttpError)
                {
                    Debug.LogError(www.error);
                    Debug.LogError(www.downloadHandler.text);
                    responseCode = www.responseCode;
                }
                else
                {
                    Debug.Log(www.downloadHandler.text);
                    PlayerPrefs.SetString(keyInscription, www.downloadHandler.text);
                    PlayerPrefs.Save();
                    JSONObject userData = new JSONObject(www.downloadHandler.text);
                    responseCode = www.responseCode;
                    // Debug.Log(userData.Print());
                    //    Debug.Log("Exito loggin : " + userData["scope"]["name"].str);
                }
            }
            else
            {
                Debug.Log("Es nesesario estar logeado");
            }
        }
    }


}