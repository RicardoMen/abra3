﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Qualitat.Transversal
{
    public class UserData : MonoBehaviour
    {
        // public string jsondata= "{\"id\": 1,\"proId\": 1,\"empId\": 1,\"status\": 1,\"content\": {\"uuid\": 1,\"ficha\": {\"evaId\": \"98\",\"params\": {\"data\": {\"proId\": 1} },\"trabajador\": {\"rut\": \"18.108.559-2\",\"nombre\": \"Ruben\", \"materno\": \"Roa\",\"paterno\": \"Tejeda\"},\"contratista\": {\"rut\":\"11.111.111-1\",\"nombreCorto\":\"Qualitat\",\"razonSocial\":\"Qualitatcorp LTDA\"}},\"proId\": 1,\"expire\": 1608375600,\"authCode\": \"ACCD3\"},\"creado\":\"2020-12-18 17:35:07\",\"modificado\": null }";

        //  public JSONObject json = new JSONObject("{\"sub\":\"_soM\",\"iat\":1609788772,\"exp\":null,\"inscription\":{\"id\": 1,\"proId\": 2,\"empId\": 3,\"status\": 4,\"content\": {\"uuid\": 5,\"ficha\": {\"evaId\": \"98\",\"params\": {\"data\": {\"proId\": 6} },\"trabajador\": {\"rut\": \"18.108.559-2\",\"nombre\": \"Ruben\", \"materno\": \"Roa\",\"paterno\": \"Tejeda\"},\"contratista\": {\"rut\":\"11.111.111-1\",\"nombreCorto\":\"Qualitat\",\"razonSocial\":\"Qualitatcorp LTDA\"}},\"proId\": 1,\"expire\": 1608375600,\"authCode\": \"ACCD3\"},\"creado\":\"2020-12-18 17:35:07\",\"modificado\": null }}");


        public Inscripcion Inscription_User = new Inscripcion();

        public static Inscripcion LoadDataInscription(JSONObject json)
        {



            //     Debug.Log(json.Print());
            Inscripcion Inscription_User = new Inscripcion();


            //expiracion del token?


            //Inscription_User.expira = SetString(json.GetField("exp").Print());
            //Debug.Log("token expira?: " + Inscription_User.expira);

            //datos de usuario

            Inscription_User.id = SetString(Exist(json.GetField("id")));
            Inscription_User.proID = SetString(Exist(json.GetField("proId")));
            Inscription_User.empId = SetString(Exist(json.GetField("empId")));
            Inscription_User.status = SetString(Exist(json.GetField("status")));
            Inscription_User.creado = SetString(Exist(json.GetField("creado")));
            Inscription_User.modificado = SetString(Exist(json.GetField("modificado")));

            //content

            // Inscription_User.content.uuid = json.GetField("content").GetField("uuid").Print();
            // Inscription_User.content.ficha.proId = json.GetField("content").GetField("proId").Print();
            //  Inscription_User.content.ficha.expire = json.GetField("content").GetField("expire").Print();
            // Inscription_User.content.ficha.authCode = json.GetField("content").GetField("authCode").Print();

            //info de ficha
            Inscription_User.content.ficha.disId = SetString(Exist(json.GetField("content").GetField("ficha").GetField("disId")));
            //  Inscription_User.content.ficha.evaId = json.GetField("content").GetField("ficha").GetField("evaId").Print();


            //parametros

            // Debug.Log(json.GetField("content").GetField("ficha").HasField("params"));

            if (json.GetField("content").GetField("ficha").HasField("params"))
            {
                Debug.LogWarning("La inscripcion si trae params, se agregaran automaticamente");

                for (int i = 0; i < json.GetField("content").GetField("ficha").GetField("params").keys.Count; i++)
                {
                    Inscription_User.content.ficha.parametros.Add(json.GetField("content").GetField("ficha").GetField("params").keys[i], Exist(json.GetField("content").GetField("ficha").GetField("params").list[i]));
                }

                if (!Inscription_User.content.ficha.parametros.ContainsKey("insId"))
                {
                    Debug.Log("no esta clave insId, rescatando y agregando al params");
                    Inscription_User.content.ficha.parametros.Add("insId", Exist(json.GetField("id")));
                }
                if (!Inscription_User.content.ficha.parametros.ContainsKey("proId"))
                {
                    Debug.Log("no esta clave insId, rescatando y agregando al params");
                    Inscription_User.content.ficha.parametros.Add("proId", Exist(json.GetField("proId")));
                }

                foreach (var item in Inscription_User.content.ficha.parametros)
                {
                    Debug.Log("parametros ->" + item);
                }
            }
            else
            {
                Debug.LogWarning("La inscripcion no trae params, se agregaran automaticamente");

                if (!Inscription_User.content.ficha.parametros.ContainsKey("insId"))
                {
                    Debug.Log("no esta clave insId, rescatando y agregando al params");
                    Inscription_User.content.ficha.parametros.Add("insId", Exist(json.GetField("id")));
                }
                if (!Inscription_User.content.ficha.parametros.ContainsKey("proId"))
                {
                    Debug.Log("no esta clave insId, rescatando y agregando al params");
                    Inscription_User.content.ficha.parametros.Add("proId", Exist(json.GetField("proId")));
                }

                foreach (var item in Inscription_User.content.ficha.parametros)
                {
                    Debug.Log("parametros ->" + item);
                }
            }



            //identidad trabajador
            if (!json.GetField("content").GetField("ficha").GetField("trabajador").IsNull)
            {

                Inscription_User.content.ficha.trabajador.rut = json.GetField("content").GetField("ficha").GetField("trabajador").HasField("rut") ? SetString(Exist(json.GetField("content").GetField("ficha").GetField("trabajador").GetField("rut"))) : " ";

                Inscription_User.content.ficha.trabajador.mail = json.GetField("content").GetField("ficha").GetField("trabajador").HasField("mail") ? SetString(Exist(json.GetField("content").GetField("ficha").GetField("trabajador").GetField("mail"))) : " ";

                Inscription_User.content.ficha.trabajador.nombre = json.GetField("content").GetField("ficha").GetField("trabajador").HasField("nombre") ? SetString(Exist(json.GetField("content").GetField("ficha").GetField("trabajador").GetField("nombre"))) : " ";

                Inscription_User.content.ficha.trabajador.materno = json.GetField("content").GetField("ficha").GetField("trabajador").HasField("materno") ? SetString(Exist(json.GetField("content").GetField("ficha").GetField("trabajador").GetField("materno"))) : " ";

                Inscription_User.content.ficha.trabajador.paterno = json.GetField("content").GetField("ficha").GetField("trabajador").HasField("paterno") ? SetString(Exist(json.GetField("content").GetField("ficha").GetField("trabajador").GetField("paterno"))) : " ";


            }

            ////identidad contratista empresa
            //if (!json.GetField("content").GetField("ficha").GetField("contratista").IsNull)
            //{
            //    Inscription_User.content.ficha.contratista.rut = SetString(Exist(json.GetField("content").GetField("ficha").GetField("contratista").GetField("rut")));
            //    Inscription_User.content.ficha.contratista.nombreCorto = SetString(Exist(json.GetField("content").GetField("ficha").GetField("nombreCorto")));
            //    Inscription_User.content.ficha.contratista.razonSocial = SetString(Exist(json.GetField("content").GetField("ficha").GetField("razonSocial")));
            //}

            try
            {
                //history de usuario
                if (!json.GetField("content").GetField("history").IsNull)
                {
                    for (int i = 0; i < json.GetField("content").GetField("history").list.Count; i++)
                    {
                        Debug.Log(i);
                        Inscripcion.History hl = new Inscripcion.History();
                        hl.type = SetString(Exist(json.GetField("content").GetField("history").list[i].GetField("type")));
                        hl.evaId = SetString(Exist(json.GetField("content").GetField("history").list[i].GetField("evaId")));
                        hl.ficId = SetString(Exist(json.GetField("content").GetField("history").list[i].GetField("ficId")));
                        hl.creado = SetString(Exist(json.GetField("content").GetField("history").list[i].GetField("creado")));
                        hl.calificacion = SetString(Exist(json.GetField("content").GetField("history").list[i].GetField("calificacion")));

                        Inscription_User.content.history.Add(hl);
                    }
                }
            }
            catch (System.Exception e)
            {
                Debug.LogWarning("No viene History en el json ->" + e);
            }

            return Inscription_User;


        }

        public static string Exist(JSONObject j)
        {
            var s = "";
            if (j.IsNull)
            {
                    Debug.Log("es null");

                s = j.str;
            }
            else
            {
                //  Debug.Log("no es null");

                //Debug.Log(j.type.ToString());

                if (j.type.ToString() == "STRING")
                {
                    s = j.str;
                }
                else if (j.type.ToString() == "NUMBER")
                {
                    s = j.Print();
                }

            }
            return s;
        }

        public static string SetString(string valor)
        {
            string text = "";


            if (string.IsNullOrEmpty(valor))
            {
                text = "";

            }
            else
            {
                text = valor;
            }

            return text;
        }

    }
}
