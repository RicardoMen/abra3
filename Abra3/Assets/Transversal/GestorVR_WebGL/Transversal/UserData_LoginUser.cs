﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Qualitat.Transversal
{
    public class UserData_loginUser : MonoBehaviour
    {
       

        public InscriptionLogin Inscription_Login = new InscriptionLogin();

        public static InscriptionLogin LoadDataLogin(JSONObject json)
        {



            //     Debug.Log(json.Print());
            InscriptionLogin Inscription_Login = new InscriptionLogin();


            //datos de usuario

            Inscription_Login.id = SetStringLogin(json.GetField("id").Print());
            Inscription_Login.proID = SetStringLogin(json.GetField("proId").Print());
            Inscription_Login.empId = SetStringLogin(json.GetField("empId").Print());
            Inscription_Login.status = SetStringLogin(json.GetField("status").Print());
            Inscription_Login.creado = SetStringLogin(json.GetField("creado").str);
            Inscription_Login.modificado = SetStringLogin(json.GetField("modificado").str);

            //user
            Inscription_Login.content.user.name = SetStringLogin(json.GetField("content").GetField("user").GetField("name").str);
            Inscription_Login.content.user.pass = SetStringLogin(json.GetField("content").GetField("user").GetField("pass").str);

            //info de ficha
            Inscription_Login.content.ficha.disId = SetStringLogin(json.GetField("content").GetField("ficha").GetField("disId").Print());
            //  Inscription_Login.content.ficha.evaId = json.GetField("inscription").GetField("content").GetField("ficha").GetField("evaId").Print();


            ////parametros


            //for (int i = 0; i < json.GetField("inscription").GetField("content").GetField("ficha").GetField("params").keys.Count; i++)
            //{
            //    Inscription_Login.content.ficha.parametros.Add(json.GetField("inscription").GetField("content").GetField("ficha").GetField("params").keys[i], json.GetField("inscription").GetField("content").GetField("ficha").GetField("params").list[i].str);
            //}
            //foreach (var item in Inscription_Login.content.ficha.parametros)
            //{
            //    Debug.Log("parametros ->" + item);
            //}

            //identidad trabajador
            if (json.GetField("content").GetField("ficha").GetField("trabajador").list.Count != 0)
            {

                Inscription_Login.content.ficha.trabajador.rut = SetStringLogin(json.GetField("content").GetField("ficha").GetField("trabajador").GetField("rut").str);
                Inscription_Login.content.ficha.trabajador.nombre = SetStringLogin(json.GetField("content").GetField("ficha").GetField("trabajador").GetField("nombre").str);
                Inscription_Login.content.ficha.trabajador.materno = SetStringLogin(json.GetField("content").GetField("ficha").GetField("trabajador").GetField("materno").str);
                Inscription_Login.content.ficha.trabajador.paterno = SetStringLogin(json.GetField("content").GetField("ficha").GetField("trabajador").GetField("paterno").str);
            }

            Debug.Log(json.GetField("content").GetField("history").list[0].Print());

            //historial fichas del trabajador
            if(json.GetField("content").GetField("history").list.Count != 0)
            {
                for(int i =0; i < json.GetField("content").GetField("history").list.Count ; i++)
                {
                    Debug.Log(i);
                    HistoryLogin hl = new HistoryLogin();
                    hl.type = SetStringLogin(json.GetField("content").GetField("history").list[i].GetField("type").str);
                    hl.evaId = SetStringLogin(json.GetField("content").GetField("history").list[i].GetField("evaId").Print());
                    hl.ficId= SetStringLogin(json.GetField("content").GetField("history").list[i].GetField("ficId").Print());
                    hl.creado = SetStringLogin(json.GetField("content").GetField("history").list[i].GetField("creado").str);
                    hl.calificacion = SetStringLogin(json.GetField("content").GetField("history").list[i].GetField("calificacion").Print());

                    Inscription_Login.content.history.Add(hl);
                }
            }
           

            return Inscription_Login;

        }

        public static string SetStringLogin(string valor)
        {
            string text = "";


            if (string.IsNullOrEmpty(valor))
            {
                text = "";

            }
            else
            {
                text = valor;
            }

            return text;
        }
        [System.Serializable]
        public class InscriptionLogin
        {
            
            public string id;
            public string proID;
            public string empId;
            public string status;
            public ContentLogin content = new ContentLogin();
            public string creado;
            public string modificado;
        }
        [System.Serializable]
        public class ContentLogin
        {
            
            public UserLogin user = new UserLogin();
            public FichaLogin ficha = new FichaLogin();
            public List<HistoryLogin> history = new List<HistoryLogin>();
        }
        [System.Serializable]
        public class FichaLogin
        {
            
            public string disId;
          
          //  public Dictionary<string, string> parametros = new Dictionary<string, string>();
            public TrabajadorLogin trabajador = new TrabajadorLogin();
            

        }
        [System.Serializable]
        public class UserLogin
        {
            public string name;
            public string pass;
        }

        [System.Serializable]
        public class HistoryLogin
        {
            public string type;
            public string evaId;
            public string ficId;
            public string creado;
            public string calificacion;
        }
        [System.Serializable]
        public class TrabajadorLogin
        {
            public string rut;
            public string nombre;
            public string materno;
            public string paterno;
        }
       

    }
}
