﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Qualitat.CheckpointManager
{
    public enum Mano { Izquierda, Derecha }
    [System.Serializable]
    public class Checkpoint
    {

        public int current_escena;
        public List<int> orden_scenas;
        public Mano mano = Mano.Derecha;

        public Checkpoint(int current_escena, List<int> orden_scenas, Mano mano)
        {
            this.current_escena = current_escena;
            this.orden_scenas = orden_scenas;
            this.mano = mano;
        }

        // Cambiar en caso de no compartir el mismo archivo
        //public static readonly string path = Helpers.File.MakeDir("checkpoint.json");

        //public static void Save(Checkpoint checkpoint)
        //{
        //    Helpers.File.SaveJson(checkpoint, path);
        //}

        //public static Checkpoint Load()
        //{
        //    return Helpers.File.LoadJson<Checkpoint>(path);
        //}

        //public static bool Exist()
        //{
        //    return Helpers.File.ExistsFile(path);
        //}
    }
}
