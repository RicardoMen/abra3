﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Qualitat.CheckpointManager
{
    public class CheckpointCtrl : MonoBehaviour
    {
        public Checkpoint checkpoint;

        //cambiar en caso de que no compartan los mismo archivos
        public static readonly string path_ficha_temporal = "ficha_temporal.json";
        public static readonly string path_checkpoint = "checkpoint.json";

        void Start()
        {
            //Helpers.File.CreateIfNotExistsDirectory(Application.productName);
            //checkpoint = new Checkpoint(0, new List<int> { 1, 2, 4, 5 }, Mano.Derecha);
            //Save();
            Load();
        }

        public void Save()
        {
            Helpers.File.SaveJson(checkpoint, path_checkpoint);
            Helpers.File.SaveJson(EvaluacionVR.current.ficha,path_ficha_temporal);
        }

        public void Load()
        {
            if (Helpers.File.ExistsFile(path_checkpoint))
            {
                checkpoint = Helpers.File.LoadJson<Checkpoint>(path_checkpoint);
                EvaluacionVR.current.ficha = Helpers.File.LoadJson<GestorVR.Module.Evaluacion.Ficha>(path_ficha_temporal);
            }
        }

        public static CheckpointCtrl current;

        void Awake()
        {
            if (current == null)
            {
                DontDestroyOnLoad(gameObject);
                current = this;
            }
            else
            {
                if (current != this)
                {
                    Destroy(gameObject);
                }
            }
        }
    }
}