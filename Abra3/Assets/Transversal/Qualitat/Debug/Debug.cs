﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;

namespace QualitatDev
{
    [System.Serializable]
    public class Device : WebServices
    {
        public string keycode;
        public string type;
        public string name;
        public int debug;
        public int collapse;
        public int creado;
        public int modificado;

        public WWWForm Attributes
        {
            get
            {
                var www = new WWWForm();
                if (!string.IsNullOrEmpty(keycode)) www.AddField("keycode", keycode);
                if (!string.IsNullOrEmpty(type)) www.AddField("type", type);
                if (!string.IsNullOrEmpty(name)) www.AddField("name", name);
                www.AddField("debug", debug);
                www.AddField("collapse", collapse);
                www.AddField("creado", creado);
                if (modificado > 0) www.AddField("modificado", modificado);
                return www;
            }
        }

        public bool isCollapse
        {
            get
            {
                return collapse == 1;
            }
        }

        public const string fileName = "debug_device.json";

        public static bool existFile
        {
            get
            {
                return Qualitat.Helpers.File.ExistsFile(fileName);
            }
        }

        public static Device Data
        {
            get
            {
                if (existFile)
                {
                    return Qualitat.Helpers.File.LoadJson<Device>(fileName);
                }
                else
                {
                    var data = new Device();
                    data.Current();
                    return data;
                }
            }
            set
            {
                Qualitat.Helpers.File.SaveJson(value, fileName);
            }
        }

        public Device()
        {
            debug = 0;
            creado = Qualitat.Helpers.Date.TimeUnix.Now;
        }

        public void Current()
        {
            keycode = SystemInfo.deviceUniqueIdentifier;
            type = SystemInfo.deviceType.ToString("g");
            name = SystemInfo.deviceName;
        }

        public UnityWebRequest requestGet()
        {
            return UnityWebRequest.Get(Session.ResourceUrl("device/" + id));
        }

        public IEnumerator Get()
        {
            return Execute(requestGet());
        }

        public UnityWebRequest requestGetIdentity()
        {
            return UnityWebRequest.Get(Session.ResourceUrl("device/identity?keycode=" + keycode));
        }

        public IEnumerator GetIdentity()
        {
            return Execute(requestGetIdentity());
        }

        public UnityWebRequest requestSave
        {
            get
            {
                UnityWebRequest request;
                if (isSync)
                {
                    request = UnityWebRequest.Put(Session.ResourceUrl("device/" + id), Attributes.data);
                }
                else
                {
                    request = UnityWebRequest.Post(Session.ResourceUrl("device"), Attributes);
                }
                return request;
            }
        }

        public IEnumerator Save()
        {
            return Execute(requestSave);
        }
    }

    [System.Serializable]
    public class Debug : WebServices
    {
        public int dev_id;
        public string app_name;
        public string app_version;
        public string app_platform;
        public string app_company;
        public string unity_version;
        public List<string> tags;
        public int creado;

        public Debug()
        {
            tags = new List<string>();
            creado = Qualitat.Helpers.Date.TimeUnix.Now;
        }

        public void Current()
        {
            app_name = Application.productName;
            app_version = Application.version;
            app_platform = Application.platform.ToString("g");
            app_company = Application.companyName;
            unity_version = Application.unityVersion;
        }

        public WWWForm Attributes
        {
            get
            {
                var www = new WWWForm();
                www.AddField("dev_id", dev_id);
                www.AddField("app_name", app_name);
                www.AddField("app_version", app_version);
                www.AddField("app_platform", app_platform);
                www.AddField("app_company", app_company);
                www.AddField("unity_version", unity_version);
                www.AddField("creado", creado);
                for (int i = 0; i < tags.Count; i++)
                {
                    www.AddField(string.Format("tags[{0}]",i),tags[i]);
                }
                return www;
            }
        }

        public UnityWebRequest requestSave
        {
            get
            {
                return UnityWebRequest.Post(Session.ResourceUrl("debug"), Attributes);
            }
        }

        public IEnumerator Save()
        {
            return Execute(requestSave);
        }

        public IEnumerator SaveLogs(List<Log> logs)
        {
            WWWForm www = new WWWForm();
            for (int i = 0; i < logs.Count; i++)
            {
                www.AddField(string.Format("logs[{0}][msg]", i), logs[i].msg);
                www.AddField(string.Format("logs[{0}][trc]", i), logs[i].trc);
                www.AddField(string.Format("logs[{0}][type]", i), logs[i].type.ToString("g"));
                www.AddField(string.Format("logs[{0}][creado]", i), logs[i].creado);
            }
            var request = UnityWebRequest.Post(Session.ResourceUrl(string.Format("debug/{0}/addlogs", id)), www);
            Session.Sign(ref request);
            yield return request.SendWebRequest();
            if (request.isNetworkError || request.isHttpError)
            {
                status = Status.Error;
                //UnityEngine.Debug.LogErrorFormat("[{0}: msg={1} status={4} request={2} {3} debug={5} Error={6} ]", this.GetType().Name, "Error", request.method, request.url, request.responseCode, request.downloadHandler.text, request.error);
            }
            else
            {
                status = Status.Ok;
                //UnityEngine.Debug.LogFormat("[{0}: msg={1} status={4} request={2} {3} debug={5} Error={6} ]", this.GetType().Name, "Exito", request.method, request.url, request.responseCode, request.downloadHandler.text, request.error);
            }
        }
    }

    [System.Serializable]
    public class Log
    {
        public LogType type;
        public string msg;
        public string trc;
        public int creado;

        public Log()
        {
            creado = Qualitat.Helpers.Date.TimeUnix.Now;
        }

        public Log(string logString,string stackTrace,LogType logType)
        {
            msg = logString;
            trc = stackTrace;
            type = logType;
            creado = Qualitat.Helpers.Date.TimeUnix.Now;
            //creado = DateTime.Now.ToUniversalTime().ToString("O");
        }
    }

    public static class Session
    {
        public static string host { get { return "https://debug.qualitat.cl/api/"; } }
        /*
         * public static string host { get { return "http://localhost/QualiDev-Debug-Backend/web/"; } }
         */
        public static string ResourceUrl(string src) { return host + src; }

        public static void Sign(ref UnityWebRequest www)
        {
            www.SetRequestHeader("Authorization", "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYW1lIjoiUnViZW4gRWR1YXJkbyBUZWplZGEgUm9hIiwidWlkIjoiMSIsInN1YiI6Ikt5aHkifQ.uf-O_UthTVS6RAoRkMlXOtI-ptE2Ka8PLbaHyGSDQok");
        }
    }

    [System.Serializable]
    public class WebServices
    {
        public enum Status { None, Error, Ok }
        public Status status;
        public int id;

        public bool isSync { get { return id!=0; } }

        public IEnumerator Execute(UnityWebRequest request)
        {
            Session.Sign(ref request);
            yield return request.SendWebRequest();
            if (request.isNetworkError || request.isHttpError)
            {

                status = Status.Error;
                //UnityEngine.Debug.LogErrorFormat("[{0}: msg={1} status={4} request={2} {3} debug={5} Error={6} ]", this.GetType().Name, "Error", request.method, request.url, request.responseCode, request.downloadHandler.text, request.error);
            }
            else
            {
                status = Status.Ok;
                //UnityEngine.Debug.LogFormat("[{0}: msg={1} status={4} request={2} {3} debug={5} ]", this.GetType().Name, "Exito", request.method, request.url, request.responseCode, request.downloadHandler.text);
                JsonUtility.FromJsonOverwrite(request.downloadHandler.text, this);
            }
        }
    }
}