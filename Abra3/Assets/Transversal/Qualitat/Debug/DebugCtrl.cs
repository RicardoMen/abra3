﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qualitat.Helpers;

namespace QualitatDev.DebugModule
{
    public class DebugCtrl : MonoBehaviour
    {
        [Header("Servicio")]
        public Device dispositivo;
        public Debug debug;
        public List<Log> logs;
        public List<Log> inSync;

        public Coroutine rutina;

        [Header("Local")]
        public bool LogLocal;
        public bool encriptar;
        public List<string> tagEcriptar;

        public DateTime tiempoLog;
        private string rutaLogs;
        public readonly string hashEncriptar = "[#]";


        void Start()
        {
            logs = new List<Log>();
            inSync = new List<Log>();

            rutina = StartCoroutine(DebugRoutine());

            tiempoLog = DateTime.Now;

            rutaLogs = Qualitat.Helpers.File.MakeDir("Logs", Application.productName, tiempoLog.ToString("yyyy-MM-dd"));

            Qualitat.Helpers.File.CreateIfNotExistsDirectory(rutaLogs);

            //StartCoroutine(ImprimePrueba());
        }

        //private IEnumerator ImprimePrueba()
        //{
        //    yield return new WaitForEndOfFrame();
        //    int i = 0;
        //    while (true)
        //    {
        //        UnityEngine.Debug.Log("hola"+i++);
        //        yield return new WaitForSeconds(1f);
        //    }
        //}

        private IEnumerator DebugRoutine()
        {
            yield return ResolveDispositivo();
            if (dispositivo.status == WebServices.Status.Ok && dispositivo.debug == 1)
            {
                yield return BeginDebug();
            }
        }

        public IEnumerator BeginDebug()
        {
            /*
             * Creacion de Debug
             */
            var tags = debug.tags;
            debug = new Debug()
            {
                dev_id = dispositivo.id
            };
            debug.Current();
            debug.tags = tags;
            yield return debug.Save();
            /*
             * Añadir Logs
             */
            while (dispositivo.debug == 1)
            {
                yield return new WaitWhile(() => logs.Count == 0);
                inSync.AddRange(logs);
                logs.Clear();
                yield return debug.SaveLogs(inSync);
                if (debug.status == WebServices.Status.Ok)
                {
                    inSync.Clear();
                }
            }
        }

        public IEnumerator ResolveDispositivo()
        {
            if (!Device.existFile)
            {
                dispositivo.Current();
                yield return dispositivo.GetIdentity();
                if (dispositivo.status == WebServices.Status.Ok)
                {
                    Device.Data = dispositivo;
                }
                else
                {
                    dispositivo.creado = Date.TimeUnix.Now;
                    dispositivo.debug = 1;
                    yield return dispositivo.Save();
                    if (dispositivo.status == WebServices.Status.Ok)
                    {
                        Device.Data = dispositivo;
                    }
                }
            }
            else
            {

                dispositivo = Device.Data;
                if (!dispositivo.isSync)
                {
                    yield return dispositivo.Save();
                }
                else
                {
                    var dispositivoTemp = new Device() { id = dispositivo.id };
                    yield return dispositivoTemp.Get();
                    if (dispositivo.modificado == 0 && dispositivoTemp.modificado != 0)
                    {
                        dispositivo = dispositivoTemp;
                    }
                    else
                    {
                        if (dispositivo.modificado > dispositivoTemp.modificado)
                        {
                            yield return dispositivo.Save();

                        }
                        else
                        {
                            dispositivo = dispositivoTemp;
                        }
                    }
                }
                Device.Data = dispositivo;
            }
        }

        private void HandleLog(string condition, string stackTrace, LogType type)
        {
            var log = new Log(condition, stackTrace, type);
            if (LogLocal)
            {
                var logCopy = new Log(log.msg,log.trc,log.type);
                WriteLocal(logCopy);
            }
            logs.Add(log);
        }

        private void WriteLocal(Log log)
        {
            if (encriptar)
            {
                foreach (var tag in tagEcriptar)
                {
                    if (log.msg.Contains(tag))
                    {
                        log.msg = hashEncriptar + "(" + tag + ")" + log.msg.EncodeBase64();
                        break;
                    }
                }
            }

            File.AddLine(
                string.Format("{0} {1} {2}", DateTime.Now.ToString("s"), log.type.ToString("g"), log.msg),
                rutaLogs, string.Format("{0}.log", tiempoLog.ToString("HH.mm.ss"))
            );
        }

        void OnEnable()
        {
            Application.logMessageReceived += HandleLog;
        }

        void OnDisable()
        {
            Application.logMessageReceived -= HandleLog;
        }

        public static DebugCtrl current;

        void Awake()
        {
            if (current == null)
            {
                DontDestroyOnLoad(gameObject);
                current = this;
            }
            else if (current != this)
            {
                Destroy(gameObject);
            }
        }
    }
}