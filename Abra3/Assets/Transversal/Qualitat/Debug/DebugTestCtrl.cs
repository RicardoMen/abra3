﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugTestCtrl : MonoBehaviour {
    public InputField inputField;
    public Dropdown dropdown;
    public Button button;

    public void OnEnable()
    {
        button.onClick.AddListener(OnClick);
    }

    public void OnDisable()
    {
        button.onClick.RemoveListener(OnClick);
    }

    public void OnClick()
    {
        switch (dropdown.value)
        {
            case 0:
                Debug.LogError(inputField.text);
                break;
            case 1:
                Debug.LogAssertion(inputField.text);
                break;
            case 2:
                Debug.LogWarning(inputField.text);
                break;
            case 3:
                Debug.Log(inputField.text);
                break;
            case 4:
                Debug.LogException(new System.Exception(inputField.text));
                break;
        }

        //inputField.text = string.Empty;
    }
}
