﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Qualitat.GestorVR.Auth
{    
    public class Client
    {
        public string client;
        public string secret;

        public Client(string client, string secret)
        {
            this.client = client;
            this.secret = secret;
        }
    }
}