﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Qualitat.GestorVR.Auth
{
    [System.Serializable]
    public class Credentials
    {
        public string token;
        public string token_type;
        public int expire_in;
        public string expire_str;
        public string scope;

        public DateTime expire { get { return DateTime.Parse(expire_str).AddSeconds(expire_in); } }

        public bool isValid { get { return !string.IsNullOrEmpty(token) && expire.CompareTo(DateTime.Now) == 1; } }

        public int secondForExpire { get { return (int)(expire - DateTime.Now).TotalSeconds; } }
    }
}