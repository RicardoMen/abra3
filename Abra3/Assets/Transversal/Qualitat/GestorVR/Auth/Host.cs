﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Qualitat.GestorVR.Auth
{
    public class Host
    {
        public Uri url;
        public string Resource(params string[] src)
        {
            return Helpers.Url.Make(url.ToString(), Helpers.Url.Make(src));
        }

        public Host(string url)
        {
            this.url = new Uri(url);
        }
    }
}