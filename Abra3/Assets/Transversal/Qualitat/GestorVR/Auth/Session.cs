﻿using System;
using UnityEngine.Networking;
using System.Collections;
using UnityEngine;

/*
 * Codigo para autentificación
 * 
 * by rbn 2018
 */

namespace Qualitat.GestorVR.Auth
{
    public class Session
    {
        public enum GrantType { password }

        private readonly string endpoint;
        private readonly string path;

        /// <summary>
        /// Almacena la informacion de sesion de un servicio
        /// </summary>
        /// <param name="endpoint">uri de autentificación</param>
        /// <param name="path">archivo donde se almacenara la información</param>
        public Session(string endpoint,params string[] path)
        {
            this.endpoint = endpoint;
            this.path = Helpers.File.MakeDir(path);
        }

        /// <summary>
        /// Funcion que firma las request para el servicio
        /// </summary>
        /// <param name="request"></param>
        /// <param name="credentials"></param>
        public static void Sign(ref UnityWebRequest request, Credentials credentials)
        {
            request.SetRequestHeader("Authorization", string.Format("{0} {1}", credentials.token_type, credentials.token));
        }

        public void Sign(ref UnityWebRequest request)
        {
            Sign(ref request,credentials);
        }

        private static Credentials _CurentCredentials;

        public Credentials credentials
        {
            get
            {
                if (_CurentCredentials != null)
                {
                    return _CurentCredentials;
                }
                else
                {
                    if (Helpers.File.ExistsFile(path))
                    {
                        _CurentCredentials = Helpers.File.LoadJson<Credentials>(path);
                        return _CurentCredentials;
                    }
                    else
                    {
                        return new Credentials();
                    }
                }
            }
            set
            {
                Helpers.File.CreateIfNotExistsDirectory();
                Helpers.File.SaveJson(value, path);
                _CurentCredentials = value;
            }
        }

        public bool isValid()
        {
            return credentials.isValid;
        }

        public UnityWebRequest RequestLogin(Host host,User user, Client client, GrantType grantType = GrantType.password)
        {
            WWWForm form = new WWWForm();
            form.AddField("username", user.username);
            form.AddField("password", user.password);
            form.AddField("grant_type", grantType.ToString("g"));
            form.AddField("client_id", client.client);
            form.AddField("client_secret", client.secret);
            return UnityWebRequest.Post(host.Resource(endpoint), form);
        }

        public IEnumerator Login(Host host,User user, Client client, GrantType grantType = GrantType.password)
        {
            UnityWebRequest request = RequestLogin(host, user, client, grantType);
            yield return request.SendWebRequest();
            if (request.isNetworkError || request.responseCode == 500)
            {
                if (App.debug)
                {
                    Debug.LogErrorFormat("[Session msg={0} status={3} request={1} {2} debug={4} Error={5} ]", "Error", request.method, request.url, request.responseCode, request.downloadHandler.text, request.error);
                }
            }
            else
            {
                _CurentCredentials = new Credentials();
                if (App.debug)
                {
                    Debug.LogFormat("[Session: msg={0} status={3} request={1} {2} debug={4} ]","Exito", request.method, request.url, request.responseCode, request.downloadHandler.text);
                }
                JsonUtility.FromJsonOverwrite(request.downloadHandler.text, _CurentCredentials);
                _CurentCredentials.expire_str = DateTime.Now.ToString();
                credentials = _CurentCredentials;
            }
        }

        public IEnumerator AutoLogin(Host host, User user, Client client)
        {
            while (true)
            {
                if (!credentials.isValid || credentials.secondForExpire < 60)
                {
                    do
                    {
                        if (App.debug)
                        {
                            Debug.Log("[Session Relogeando... ]");
                        }
                        yield return Login(host, user, client);
                        if (!credentials.isValid)
                        {
                            yield return new WaitForSeconds(10);
                        }
                    } while (!credentials.isValid);
                }
                if (App.debug)
                {
                    Debug.LogFormat("[Session Se relogea en : {0} Segundos ]", credentials.secondForExpire - 30);
                }
                yield return new WaitForSecondsRealtime(credentials.secondForExpire - 30);
            }
        }
    }
}