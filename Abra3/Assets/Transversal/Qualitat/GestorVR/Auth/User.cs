﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Qualitat.GestorVR.Auth
{
    public class User
    {
        public string username;
        public string password;

        public User(string username, string password)
        {
            this.username = username;
            this.password = password;
        }
    }
}