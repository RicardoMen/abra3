﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;

namespace Qualitat.GestorVR
{
    public class ActiveRecords
    {
        public static readonly string endpoint = "v1";

        public static string Resource(params string[] src)
        {
            var lista = new List<string>(src);
            lista.Insert(0, endpoint);
            return App.Host.Resource(lista.ToArray());
        }

        public static string Resource(WWWForm get_params,params string[] src)
        {
            return Resource(src) + '?' + WWW.UnEscapeURL(System.Text.Encoding.UTF8.GetString(get_params.data));
        }


        public static string pathBase
        {
            get
            {
                Helpers.File.CreateIfNotExistsDirectory("GestorVR");
                return "GestorVR";
            }
        }

        public static string PathResource(string file)
        {
            return Helpers.File.MakeDir(pathBase,file);
        }

        public enum StatusCode { NONE, OK, ERROR, UPDATE, REMOVED, IDENTITY, CLOSE, EXISTS, NOT_EXISTS, FORBIDDEN }

        public StatusCode status = StatusCode.NONE;

        public IEnumerator Execute(UnityWebRequest request)
        {
            yield return new WaitUntil(()=> App.session.credentials.isValid);
            App.session.Sign(ref request);   
            yield return request.SendWebRequest();
            if (request.isNetworkError || request.isHttpError)
            {
                if (App.debug)
                {
                    Debug.LogAssertionFormat("[{0}: msg={1} status={4} request={2} {3} debug={5} Error={6} ]", GetType().Name, "Error", request.method, request.url, request.responseCode, request.downloadHandler.text, request.error);
                }
                status = StatusCode.ERROR;
            }
            else
            {
                if (App.debug)
                {
                    Debug.LogFormat("[{0}: msg={1} status={4} request={2} {3} debug={5} ]", GetType().Name, "Exito", request.method, request.url, request.responseCode, request.downloadHandler.text);
                }
                JsonUtility.FromJsonOverwrite(request.downloadHandler.text, this);
                status = StatusCode.OK;
            }
        }
    }
}
