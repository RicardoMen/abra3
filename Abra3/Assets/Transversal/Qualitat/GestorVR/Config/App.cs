﻿using System.Collections;
using UnityEngine;

namespace Qualitat.GestorVR
{
    public static class App
    {
        public static readonly string version = "3.9.3";
        public enum Environment { Production, Development, Local, Testing };

        public static Environment environment = Environment.Production;
        private static readonly string host = Enviroment.enviroment_app;

        public static readonly bool debug = true;

        public static readonly Auth.Session session = new Auth.Session("auth/token","session.json");
        public static readonly Auth.User user = new Auth.User("GestorVR_Unity", "nnE3de0XEKwOTVja5OeAohhbpOpoQxMd");
        public static readonly Auth.Client client = new Auth.Client("unity", "uXPuUsuSbj");

        public static Auth.Host Host {
            get
            {
               
                //switch (environment)
                //{
                //    case Environment.Production:
                //        return new Auth.Host("https://gestorvr.qualitatcorp.cl/api");

                //    case Environment.Development:
                //    case Environment.Testing:
                //        return new Auth.Host("https://gestorvr.qualitat.cl/dev");

                //    case Environment.Local:
                //    default:
                //        return new Auth.Host("http://localhost/GestorVR-Backend/web");
                //}
                return new Auth.Host(host);
            }
        }

        private static bool lockLogin; //Deadlock login
        public static IEnumerator AutoLogin()
        {
            if (!lockLogin)
            {
                lockLogin = true;
                yield return session.AutoLogin(Host ,user, client);
            }
            else
            {
                yield break;
            }
        }
    }
}