﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Qualitat.GestorVR.Model
{
    [System.Serializable]
    public class Device //: ActiveRecords
    {
        //public int id;
        public string keycode;
        public string type;
        public string name;
        public string alter_name;
        public int creado;
        //public int modificado;

        public void Current()
        {
            if(Application.platform ==  RuntimePlatform.Android)
            {
                AndroidJavaObject jo = new AndroidJavaObject("android.os.Build");
                keycode = jo.GetStatic<string>("SERIAL");
            }
            else
            {
                keycode = SystemInfo.deviceUniqueIdentifier;
            }
            type = SystemInfo.deviceType.ToString("g");
            name = SystemInfo.deviceName;
            creado = Helpers.Date.TimeUnix.Now;
        }

        public override string ToString()
        {
            return JsonUtility.ToJson(this);
        }
    }
}