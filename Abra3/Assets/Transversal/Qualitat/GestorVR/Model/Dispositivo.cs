﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using System;

namespace Qualitat.GestorVR.Model
{
    [System.Serializable]
    public class Dispositivo : ActiveRecords
    {
        public int dis_id;
        public int emp_id;
        public int dit_id;
        public string nombre;
        public string habilitado;
        public string activado;
        public string keycode;
        public string serial;
        public string creado;
        public string modificado;

        public static readonly string ResourceName = "dispositivo";

        public int primaryKey
        {
            get
            {
                return dis_id;
            }
            set
            {
                dis_id = value;
            }
        }

        public bool isSync
        {
            get
            {
                return dis_id > 0;
            }
        }

        public bool isAvailable
        {
            get
            {
                if (habilitado == "SI" && activado == "SI")
                    return true;
                else
                {
                    return false;
                }
            }
        }

        public WWWForm Attributes
        {
            get
            {
                WWWForm f = new WWWForm();
                if (emp_id > 0) f.AddField("emp_id", emp_id);
                if (dit_id > 0) f.AddField("dit_id", dit_id);
                if (!string.IsNullOrEmpty(nombre)) f.AddField("nombre", nombre);
                if (!string.IsNullOrEmpty(habilitado)) f.AddField("habilitado", habilitado);
                if (!string.IsNullOrEmpty(activado)) f.AddField("activado", activado);
                if (!string.IsNullOrEmpty(keycode)) f.AddField("keycode", keycode);
                if (!string.IsNullOrEmpty(serial)) f.AddField("serial", serial);
                return f;
            }
        }

        /*
         * Requesst
         */
        public UnityWebRequest requestSave { get { return isSync ? UnityWebRequest.Put(Resource(ResourceName, primaryKey.ToString()), Attributes.data) : UnityWebRequest.Post(Resource(ResourceName), Attributes); } }

        public UnityWebRequest requestGet { get { return UnityWebRequest.Get(Resource(ResourceName, primaryKey.ToString())); } }

        public UnityWebRequest RequestDelete { get { return UnityWebRequest.Delete(Resource(ResourceName, primaryKey.ToString())); } }

        public IEnumerator Save() { return Execute(requestSave); }

        public IEnumerator GetByPK() { return Execute(requestGet); }

        public IEnumerator GetByPK(int pk)
        {
            primaryKey = pk;
            return Execute(requestGet);
        }


        public IEnumerator GetByPK(string parametros)
        {
            var request = requestGet;
            request.url += parametros;
            return Execute(request);
        }

        public IEnumerator Delete()
        {
            yield return Execute(RequestDelete);
            if (status == StatusCode.OK)
            {
                status = StatusCode.REMOVED;
            }
            else
            {
                status = StatusCode.ERROR;
            }
        }

        /*
         *  Guardar Informacion en un archivo
         */
        public static string[] pathResources
        {
            get
            {
                return new string[] { pathBase, ResourceName + ".json"};
            }
        }

        private static List<Dispositivo> _table;
        public static List<Dispositivo> Table
        {
            get
            {
                if (_table != null)
                {
                    return _table;
                }
                else
                {
                    if (Helpers.File.ExistsFile(pathResources))
                    {
                        _table = Helpers.File.LoadArrayJson<Dispositivo>(pathResources);
                        return _table;
                    }
                    else
                    {
                        return new List<Dispositivo>();
                    }
                }
            }
            set
            {
                Helpers.File.SaveJson(value, pathResources);
                _table = value;
            }
        }
    }
}