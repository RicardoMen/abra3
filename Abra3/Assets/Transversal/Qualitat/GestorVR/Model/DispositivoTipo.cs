﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

namespace Qualitat.GestorVR.Model
{
    [System.Serializable]
    public class DispositivoTipo : ActiveRecords
    {
        public int dit_id;
        public string nombre, modelo, descripcion;

        public static string ResourceName { get { return "dispositivotipo"; } }

        public int primaryKey { get { return dit_id; } set { dit_id = value; } }
        public bool isSync { get { return dit_id > 0; } }

        public WWWForm Attributes
        {
            get
            {
                WWWForm f = new WWWForm();
                if (!string.IsNullOrEmpty(nombre)) f.AddField("nombre", nombre);
                if (!string.IsNullOrEmpty(modelo)) f.AddField("modelo", modelo);
                if (!string.IsNullOrEmpty(descripcion)) f.AddField("descripcion", descripcion);
                return f;
            }
        }

        public UnityWebRequest requestSave
        {
            get
            {
                UnityWebRequest request;
                if (isSync)
                {
                    request = UnityWebRequest.Put(Resource(ResourceName, primaryKey.ToString()), Attributes.data);
                }
                else
                {
                    request = UnityWebRequest.Post(Resource(ResourceName), Attributes);
                }
                return request;
            }
        }

        public UnityWebRequest requestGet
        {
            get
            {
                UnityWebRequest request = UnityWebRequest.Get(Resource(ResourceName, primaryKey.ToString()));
                return request;
            }
        }

        public UnityWebRequest RequestDelete
        {
            get
            {
                UnityWebRequest request = UnityWebRequest.Delete(Resource(ResourceName, primaryKey.ToString()));
                return request;
            }
        }

        public IEnumerator Save()
        {
            return Execute(requestSave);
        }

        public IEnumerator GetByPK()
        {
            return Execute(requestGet);
        }

        public IEnumerator GetByPK(int pk)
        {
            primaryKey = pk;
            return Execute(requestGet);
        }

        public IEnumerator Delete()
        {
            yield return Execute(RequestDelete);
            if (status == StatusCode.OK)
            {
                status = StatusCode.REMOVED;
            }
            else
            {
                status = StatusCode.ERROR;
            }
        }

        public UnityWebRequest requestIdentity
        {
            get
            {
                UnityWebRequest request;
                if (isSync)
                {
                    request = UnityWebRequest.Get(Resource(ResourceName, primaryKey.ToString()));
                }
                else
                {
                    WWWForm form = new WWWForm();
                    if (SystemInfo.deviceName != SystemInfo.unsupportedIdentifier)
                    {
                        form.AddField("nombre", SystemInfo.deviceName);
                    }
                    else
                    {
                        form.AddField("nombre", SystemInfo.deviceModel);
                    }
                    form.AddField("modelo",SystemInfo.deviceModel);
                    request = UnityWebRequest.Post(Resource(ResourceName,"identity"),form);
                }
                return request;
            }
        }

        public IEnumerator GetIdentity()
        {
            return Execute(requestIdentity);
        }

        /*
        *  Guardar Informacion en un archivo
        */
        private static List<DispositivoTipo> _table;
        public static List<DispositivoTipo> Table
        {
            get
            {
                if (_table != null)
                {
                    return _table;
                }
                else
                {
                    if (Helpers.File.ExistsFile(pathBase,ResourceName))
                    {
                        _table = Helpers.File.LoadArrayJson<DispositivoTipo>(pathBase, ResourceName);
                        return _table;
                    }
                    else
                    {
                        return new List<DispositivoTipo>();
                    }
                }
            }
            set
            {
                Helpers.File.SaveJson(value, pathBase, ResourceName);
                _table = value;
            }
        }
    }
}