﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

namespace Qualitat.GestorVR.Model
{
    [System.Serializable]
    public class Empresa : ActiveRecords
    {
        public int emp_id, com_id;
        public string nombre, rut, razon_social, giro, fono, mail, clasificacion;

        public static string ResourceName { get { return "empresa"; } }

        public int primaryKey { get { return emp_id; } set { emp_id = value; } }
        public bool isSync { get { return emp_id > 0; } }

        public WWWForm Attributes
        {
            get
            {
                WWWForm f = new WWWForm();
                if (com_id > 0) f.AddField("com_id", com_id);
                if (!string.IsNullOrEmpty(nombre)) f.AddField("nombre", nombre);
                if (!string.IsNullOrEmpty(rut)) f.AddField("rut", rut);
                if (!string.IsNullOrEmpty(razon_social)) f.AddField("razon_social", razon_social);
                if (!string.IsNullOrEmpty(giro)) f.AddField("giro", giro);
                if (!string.IsNullOrEmpty(fono)) f.AddField("fono", fono);
                if (!string.IsNullOrEmpty(mail)) f.AddField("mail", mail);
                if (!string.IsNullOrEmpty(clasificacion)) f.AddField("clasificacion", clasificacion);
                return f;
            }
        }

        public UnityWebRequest requestSave { get { return isSync? UnityWebRequest.Put(Resource(ResourceName, primaryKey.ToString()), Attributes.data):UnityWebRequest.Post(Resource(ResourceName), Attributes); } }

        public UnityWebRequest requestGet { get { return UnityWebRequest.Get(Resource(ResourceName, primaryKey.ToString())); } }

        public UnityWebRequest RequestDelete { get { return UnityWebRequest.Delete(Resource(ResourceName, primaryKey.ToString())); } }

        public IEnumerator Save() { return Execute(requestSave); }

        public IEnumerator GetByPK() { return Execute(requestGet); }
        
        public IEnumerator GetByPK(int pk)
        {
            primaryKey = pk;
            return Execute(requestGet);
        }

        public IEnumerator GetByPK(string parametros)
        {
            var request = requestGet;
            request.url += parametros;
            return Execute(request);
        }

        public IEnumerator Delete()
        {
            yield return Execute(RequestDelete);
            if (status == StatusCode.OK)
            {
                status = StatusCode.REMOVED;
            }
            else
            {
                status = StatusCode.REMOVED;
            }
        }

        public UnityWebRequest requestIdentity
        {
            get
            {
                UnityWebRequest request;
                if (isSync)
                {
                    request = UnityWebRequest.Get(Resource(ResourceName, primaryKey.ToString()));
                }
                else
                {
                    request = UnityWebRequest.Post(Resource(ResourceName,"identity"), Attributes);
                }
                return request;
            }
        }

        public IEnumerator GetIdentity()
        {
            return Execute(requestIdentity);
        }

        /*
         *  Guardar Informacion en un archivo
         */
        public static string pathResources { get { return string.Format("{0}\\{1}.json", pathBase, ResourceName); } }
        private static List<Empresa> _table;
        public static List<Empresa> Table
        {
            get
            {
                if (_table != null)
                {
                    return _table;
                }
                else
                {
                    if (Helpers.File.ExistsFile(string.Format(pathResources)))
                    {
                        _table = Helpers.File.LoadArrayJson<Empresa>(pathResources);
                        return _table;
                    }
                    else
                    {
                        return new List<Empresa>();
                    }
                }
            }
            set
            {
                Helpers.File.SaveJson(value, pathResources);
                _table = value;
            }
        }

        /*
         * Filtros
         */

        public static Empresa FindByPk(int primaryKey)
        {
            return Table.Find(x => x.primaryKey.ToString() == primaryKey.ToString());
        }

        public static Empresa FindByRUT(string RUT)
        {
            return Table.Find(x => x.rut == RUT);
        }

    }
}