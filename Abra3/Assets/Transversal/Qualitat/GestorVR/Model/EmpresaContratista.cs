﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Qualitat.GestorVR.Model
{
    [System.Serializable]
    public class EmpresaContratista
    {
        public int id;
        public int com_id;
        public string nombre_corto;
        public string rut;
        public string razon_social;
        public string direccion;
        public string fono;
        public string mail;
        public string web;
        public string creado;
        public string modificado;
    }
}