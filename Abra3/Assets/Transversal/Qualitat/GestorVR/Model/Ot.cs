﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
namespace Qualitat.GestorVR.Model
{
    public class Ot
    {
        public enum Estado { ABIERTO, CERRADO, EXTENSION }
        public int id;
        public int emp_id;
        public string nombre;
        public int lista;
        public string direccion;
        public DateTime inicio;
        public DateTime termino;
        public DateTime extencion;
        public Estado estado;
        public DateTime creado;
        public DateTime modificado;
    }
}
