﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using System.IO;

namespace Qualitat.GestorVR.Model
{
    [System.Serializable]
    public class RecursosSources : ActiveRecords
    {
        public int id;
        public string title, mimeType, _file;

        public static string ResourceName { get { return "recursossources"; } }
        public static string SubDIR { get { return "Sources"; } }
        

        public int primaryKey { get { return id; } set { id = value; } }
        public bool isSync { get { return id > 0; } }

        public string pathFile
        {
            get
            {
                if (string.IsNullOrEmpty(_file))
                {
                    do
                    {
                        _file = Path.Combine(SubDIR,Path.GetRandomFileName().Replace(".", string.Empty) + Path.GetRandomFileName().Replace(".", string.Empty) + ".dat");
                    } while (File.Exists(_file));
                }
                return _file;
            }
        }

        public bool ExistFile { get { return File.Exists(pathFile); } }

        public byte[] file
        {
            get
            {
                return Helpers.File.LoadBinary(pathFile);
            }
            set
            {
                Debug.Log(pathFile);
                Helpers.File.CreateIfNotExistsDirectory(SubDIR);
                Helpers.File.Save(value,pathFile);
            }
        }

        public WWWForm Attributes
        {
            get
            {
                WWWForm f = new WWWForm();
                if (!string.IsNullOrEmpty(title)) f.AddField("title", title);
                if (!string.IsNullOrEmpty(_file))
                {
                    if (string.IsNullOrEmpty(title))
                    {
                        title = Helpers.File.GetFileName(_file);
                    }
                    f.AddBinaryData("file", file, title, mimeType);
                }
                //if (file.Length > 0) f.AddBinaryData("file", file, title, mimeType);
                return f;
            }
        }

        public UnityWebRequest requestSave
        {
            get
            {
                UnityWebRequest request;
                if (isSync)
                {
                    request = UnityWebRequest.Put(Resource(ResourceName, primaryKey.ToString()), Attributes.data);
                }
                else
                {
                    request = UnityWebRequest.Post(Resource(ResourceName), Attributes);
                }
                return request;
            }
        }

        public UnityWebRequest requestGet
        {
            get
            {
                UnityWebRequest request = UnityWebRequest.Get(Resource(ResourceName, primaryKey.ToString()));
                return request;
            }
        }

        public UnityWebRequest RequestDelete
        {
            get
            {
                UnityWebRequest request = UnityWebRequest.Delete(Resource(ResourceName, primaryKey.ToString()));
                return request;
            }
        }

        public IEnumerator Save()
        {
            return Execute(requestSave);
        }

        public IEnumerator GetByPK()
        {
            return Execute(requestGet);
        }

        public IEnumerator GetByPK(int pk)
        {
            primaryKey = pk;
            return Execute(requestGet);
        }

        public IEnumerator Delete
        {
            get
            {
                yield return Execute(RequestDelete);
                if (status == StatusCode.OK)
                {
                    status = StatusCode.REMOVED;
                }
                else
                {
                    status = StatusCode.ERROR;
                }
            }
        }
        
        /*
         *  Guardar Informacion en un archivo
         */
        public static string pathResources { get { return string.Format("{0}\\{1}.json", pathBase, ResourceName); } }
        private static List<RecursosSources> _table;
        public static List<RecursosSources> Table
        {
            get
            {
                if (_table != null)
                {
                    return _table;
                }
                else
                {
                    if (File.Exists(string.Format(pathResources)))
                    {
                        _table = Helpers.File.LoadArrayJson<RecursosSources>(pathResources);
                        return _table;
                    }
                    else
                    {
                        return new List<RecursosSources>();
                    }
                }
            }
            set
            {
                Helpers.File.SaveJson(value, pathResources);
                _table = value;
            }
        }
    }
}