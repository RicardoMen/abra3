﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

namespace Qualitat.GestorVR.Model
{
    [System.Serializable]
    public class RvAlternativa : ActiveRecords
    {
        public int alt_id, pre_id;
        public float ponderacion;
        public string alternativa;
        public string descripcion;
        public string correcta;
        public string creado;
        public string modificado;

        public static readonly string ResourceName = "rvalternativa";

        public int primaryKey
        {
            get
            {
                return alt_id;
            }
            set
            {
                alt_id = value;
            }
        }

        public bool isSync
        {
            get
            {
                return alt_id > 0;
            }
        }

        public bool isCorrect
        {
            get
            {
                return correcta == "SI";
            }
        }

        public WWWForm Attributes
        {
            get
            {
                WWWForm f = new WWWForm();
                if (pre_id > 0) f.AddField("pre_id", pre_id);
                if (ponderacion > 0) f.AddField("ponderacion", ponderacion.ToString("F2"));
                if (!string.IsNullOrEmpty(alternativa)) f.AddField("alternativa", alternativa);
                if (!string.IsNullOrEmpty(descripcion)) f.AddField("descripcion", descripcion);
                if (!string.IsNullOrEmpty(correcta)) f.AddField("correcta", correcta);
                return f;
            }
        }

        public UnityWebRequest requestSave
        {
            get
            {
                UnityWebRequest request;
                if (isSync)
                {
                    request = UnityWebRequest.Put(Resource(ResourceName, primaryKey.ToString()), Attributes.data);
                }
                else
                {
                    request = UnityWebRequest.Post(Resource(ResourceName), Attributes);
                }
                return request;
            }
        }

        public UnityWebRequest requestGet
        {
            get
            {
                UnityWebRequest request = UnityWebRequest.Get(Resource(ResourceName, primaryKey.ToString()));
                return request;
            }
        }

        public UnityWebRequest RequestDelete
        {
            get
            {
                UnityWebRequest request = UnityWebRequest.Delete(Resource(ResourceName, primaryKey.ToString()));
                return request;
            }
        }

        public IEnumerator Save()
        {
            return Execute(requestSave);
        }

        public IEnumerator GetByPK()
        {
            return Execute(requestGet);
        }

        public IEnumerator GetByPK(int pk)
        {
            primaryKey = pk;
            return Execute(requestGet);
        }

        public IEnumerator Delete()
        {
            yield return Execute(RequestDelete);
            if (status == StatusCode.OK)
            {
                status = StatusCode.REMOVED;
            }
            else
            {
                status = StatusCode.ERROR;
            }
        }

        /*
         *  Guardar Informacion en un archivo
         */
        public static string pathResources { get { return Helpers.File.MakeDir(pathBase, ResourceName+".json"); } }
        private static List<RvAlternativa> _table;
        public static List<RvAlternativa> Table
        {
            get
            {
                if (_table != null)
                {
                    return _table;
                }
                else
                {
                    if (Helpers.File.ExistsFile(string.Format(pathResources)))
                    {
                        _table = Helpers.File.LoadArrayJson<RvAlternativa>(pathResources);
                        return _table;
                    }
                    else
                    {
                        return new List<RvAlternativa>();
                    }
                }
            }
            set
            {
                Helpers.File.SaveJson(value, pathResources);
                _table = value;
            }
        }
        /*
         * Definicion de Relaciones
         */
        public RvPregunta pregunta { get { return RvPregunta.Table.Find(x => x.primaryKey == pre_id); } }

        public static void Write(RvAlternativa alternativa)
        {
            if (alternativa.isSync)
            {
                var table = Table;
                if (table.Exists(x => x.primaryKey == alternativa.primaryKey))
                {
                    table[table.FindIndex(x => x.primaryKey == alternativa.primaryKey)] = alternativa;
                }
                else
                {
                    table.Add(alternativa);
                }
                Table = table;
            }
        }

        public static void Write(List<RvAlternativa> alternativas)
        {
            var table = Table;
            foreach (var alternativa in alternativas)
            {
                if (alternativa.isSync)
                {
                    if (table.Exists(x => x.primaryKey == alternativa.primaryKey))
                    {
                        table[table.FindIndex(x => x.primaryKey == alternativa.primaryKey)] = alternativa;
                    }
                    else
                    {
                        table.Add(alternativa);
                    }
                }
            }
            Table = table;
        }
    }
}