﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using System;

namespace Qualitat.GestorVR.Model
{
    [System.Serializable]
    public class RvEvaluacion : ActiveRecords
    {
        public int eva_id;
        public int tev_id;
        public string nombre;
        public string descripcion;
        public string nota;
        public string reporte;
        public string orden;
        public string creado;
        public string modificado;

        public static readonly string ResourceName = "rvevaluacion";

        public int primaryKey { get { return eva_id; } set { eva_id = value; } }
        public bool isSync { get { return eva_id > 0; } }

        public WWWForm Attributes
        {
            get
            {
                WWWForm f = new WWWForm();
                if (tev_id > 0) f.AddField("tev_id", tev_id);
                if (!string.IsNullOrEmpty(nombre)) f.AddField("nombre", nombre);
                if (!string.IsNullOrEmpty(descripcion)) f.AddField("descripcion", descripcion);
                if (!string.IsNullOrEmpty(orden)) f.AddField("orden", orden);
                return f;
            }
        }

        public UnityWebRequest requestSave
        {
            get
            {
                UnityWebRequest request;
                if (isSync)
                {
                    request = UnityWebRequest.Put(Resource(ResourceName, primaryKey.ToString()), Attributes.data);
                }
                else
                {
                    request = UnityWebRequest.Post(Resource(ResourceName), Attributes);
                }
                return request;
            }
        }

        public UnityWebRequest requestGet
        {
            get
            {
                UnityWebRequest request = UnityWebRequest.Get(Resource(ResourceName, primaryKey.ToString()));
                return request;
            }
        }

        public UnityWebRequest RequestDelete
        {
            get
            {
                UnityWebRequest request = UnityWebRequest.Delete(Resource(ResourceName, primaryKey.ToString()));
                return request;
            }
        }

        public IEnumerator Save()
        {
            return Execute(requestSave);
        }

        public IEnumerator GetByPK()
        {
            return Execute(requestGet);
        }

        public IEnumerator GetByPK(int pk)
        {
            primaryKey = pk;
            return Execute(requestGet);
        }

        public IEnumerator GetByPK(string parametros)
        {
            var request = requestGet;
            request.url += parametros;
            return Execute(request);
        }

        public IEnumerator GetByPK(int pk,string parametros)
        {
            primaryKey = pk;
            return GetByPK(parametros);
        }

        public IEnumerator Delete()
        {
            yield return Execute(RequestDelete);
            if (status == StatusCode.OK)
            {
                status = StatusCode.REMOVED;
            }
            else
            {
                status = StatusCode.ERROR;
            }
        }

        /*
         *  Guardar Informacion en un archivo
         */
        public readonly static string pathResources = Helpers.File.MakeDir(pathBase, ResourceName+".json");

        private static List<RvEvaluacion> _table;

        public static List<RvEvaluacion> Table
        {
            get
            {
                if (_table != null)
                {
                    return _table;
                }
                else
                {
                    if (Helpers.File.ExistsFile(string.Format(pathResources)))
                    {
                        _table = Helpers.File.LoadArrayJson<RvEvaluacion>(pathResources);
                        return _table;
                    }
                    else
                    {
                        return new List<RvEvaluacion>();
                    }
                }
            }
            set
            {
                Helpers.File.SaveJson(value, pathResources);
                _table = value;
            }
        }

        /*
         * Definicion de Relaciones
         */

        public List<RvPregunta> GetPreguntas()
        {
            return RvPregunta.Table.FindAll(x => x.eva_id == primaryKey);
        }

        public List<RvAlternativa> GetAlternativas()
        {
            return RvAlternativa.Table.FindAll(x => x.pregunta.eva_id == primaryKey);
        }

        public static void Write(RvEvaluacion evaluacion)
        {
            if (evaluacion.isSync)
            {
                var table = Table;
                if (table.Exists(x => x.primaryKey == evaluacion.primaryKey))
                {
                    table[table.FindIndex(x => x.primaryKey == evaluacion.primaryKey)] = evaluacion;
                }
                else
                {
                    table.Add(evaluacion);
                }
                Table = table;
            }
        }
    }
}