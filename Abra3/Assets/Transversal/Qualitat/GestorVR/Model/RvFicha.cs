﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using System;

namespace Qualitat.GestorVR.Model
{
    [System.Serializable]
    public class RvFicha : ActiveRecords
    {
        public static readonly string ResourceName = "rvficha";

        public int fic_id;
        public int eva_id;
        public int trab_id;
        public int con_id;
        public int pro_id;
        public int disp_id;
        public int pais_id;
        public string calificacion;
        public string creado;
        
        public RvFicha()
        {
            creado = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public int primaryKey
        {
            get
            {
                return fic_id;
            }
            set
            {
                fic_id = value;
            }
        }

        public bool isSync
        {
            get
            {
                if(fic_id > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public bool Integrity
        {
            get
            {
                bool flag = true;
                if (eva_id <= 0)
                {
                    Debug.LogError("Debes asignar una evaluacion a la ficha.");
                    flag = false;
                }
                if (disp_id <= 0)
                {
                    Debug.LogError("Debes asignarle una empresa a la ficha de evaluacion.");
                    flag = false;
                }
                return flag;
            }
        }
                
        public WWWForm Attributes
        {
            get
            {
                WWWForm form = new WWWForm();
                if (eva_id > 0)
                {
                    form.AddField("eva_id", eva_id);
                }
                if (trab_id > 0)
                {
                    form.AddField("trab_id", trab_id);
                }
                if (con_id > 0)
                {
                    form.AddField("con_id", con_id);
                }
                if (pro_id > 0)
                {
                    form.AddField("pro_id", pro_id);
                }
                if (disp_id > 0)
                {
                    form.AddField("disp_id", disp_id);
                }
                if (pais_id > 0)
                {
                    form.AddField("pais_id", pais_id);
                }
                if (!string.IsNullOrEmpty(calificacion))
                {
                    form.AddField("calificacion", calificacion);
                }
                if (!string.IsNullOrEmpty(creado))
                {
                    form.AddField("creado", creado);
                }
                return form;
            }
        }

        public RvEvaluacion evaluacion
        {
            get
            {
                return RvEvaluacion.Table.Find(x => x.eva_id == eva_id);
            }
        }
 
        public UnityWebRequest requestSave
        {
            get
            {
                UnityWebRequest request;
                if (isSync)
                {
                    request = UnityWebRequest.Put(Resource(ResourceName, primaryKey.ToString()), Attributes.data);
                }
                else
                {
                    request = UnityWebRequest.Post(Resource(ResourceName), Attributes);
                }
                return request;
            }
        }

        public UnityWebRequest requestGet
        {
            get
            {
                return UnityWebRequest.Get(Resource(ResourceName, primaryKey.ToString()));
            }
        }

        public UnityWebRequest RequestDelete
        {
            get
            {
                return UnityWebRequest.Delete(Resource(ResourceName, primaryKey.ToString()));
            }
        }

        public IEnumerator Save()
        {
            return Execute(requestSave);
        }


        public IEnumerator GetByPK()
        {
            return Execute(requestGet);
        }

        public IEnumerator GetByPK(int pk)
        {
            primaryKey = pk;
            return Execute(requestGet);
        }

        public IEnumerator Delete
        {
            get
            {
                yield return Execute(RequestDelete);
                if (status == StatusCode.OK)
                {
                    status = StatusCode.REMOVED;
                }
                else
                {
                    status = StatusCode.ERROR;
                }
            }
        }

        public static string pathResources
        {
            get
            {
                return PathResource(ResourceName + ".json");
            }
        }

        private static List<RvFicha> _table;

        public static List<RvFicha> Table
        {
            get
            {
                if (_table != null)
                {
                    return _table;
                }
                else
                {
                    if (Helpers.File.ExistsFile(string.Format(pathResources)))
                    {
                        _table = Helpers.File.LoadArrayJson<RvFicha>(pathResources);
                        return _table;
                    }
                    else
                    {
                        return new List<RvFicha>();
                    }
                }
            }
            set
            {
                Helpers.File.SaveJson(value, pathResources);
                _table = value;
            }
        }
    }
}