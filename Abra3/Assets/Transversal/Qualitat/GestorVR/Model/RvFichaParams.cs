﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using System;

namespace Qualitat.GestorVR.Model
{
    [System.Serializable]
    public class RvFichaParams : ActiveRecords
    {
        public static readonly string ResourceName = "rvfichaparams";

        public int id;
        public int fic_id;
        public string type;
        public string content;
        public string creado;
        public string modificado;

        public T GetData<T>()
        {
            if (Table.Exists(x => x.primaryKey.ToString() == primaryKey.ToString()))
            {
                return JsonUtility.FromJson<T>(Table.Find(x => x.primaryKey.ToString() == primaryKey.ToString()).content);
            }
            else
            {
                return JsonUtility.FromJson<T>(content);
            }
        }

        public void SetData<T>(T parametro)
        {
            type = "object";
            content = JsonUtility.ToJson(parametro);
        }

        public JSONObject toJSONObject()
        {
            return new JSONObject(content);
        }

        public List<T> GetlistData<T>()
        {
            if (Table.Exists(x => x.primaryKey.ToString() == primaryKey.ToString()))
            {
                return Helpers.Json.ToArrayList<T>(Table.Find(x => x.primaryKey.ToString() == primaryKey.ToString()).content);
            }
            else
            {
                return Helpers.Json.ToArrayList<T>(content);
            }
        }

        public void SetData<T>(List<T> parametro)
        {
            type = "array";
            content = JsonUtility.ToJson(parametro);
        }
        
        public bool isCreado
        {
            get
            {
                return !string.IsNullOrEmpty(creado);
            }
        }

        public bool isModificado
        {
            get
            {
                return !string.IsNullOrEmpty(modificado);
            }
        }

        public bool isValid
        {
            get
            {
                return !string.IsNullOrEmpty(content);
            }
        }

        public DateTime Creado
        {
            set
            {
                creado = value.ToString("yyyy-MM-dd HH:mm:ss");
            }
            get
            {
                return DateTime.Parse(creado);
            }
        }

        public DateTime Modificado
        {
            set
            {
                modificado = value.ToString("yyyy-MM-dd HH:mm:ss");
            }
            get
            {
                return DateTime.Parse(modificado);
            }
        }

        public int primaryKey
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public bool isSync
        {
            get
            {
                return id > 0;
            }
        }

        public RvFichaParams()
        {
            Creado = DateTime.Now;
        }

        /*
         * Funciones de Asignacion
         */

        public WWWForm Attributes
        {
            get
            {
                WWWForm f = new WWWForm();
                if (fic_id > 0) f.AddField("fic_id", fic_id);
                if (!string.IsNullOrEmpty(type)) f.AddField("type", type);
                if (!string.IsNullOrEmpty(content)) f.AddField("content", content);
                if (isCreado) f.AddField("creado", creado);
                if (isModificado) f.AddField("modificado", modificado);
                return f;
            }
        }

        /*
         * Request WEB
         */

        public UnityWebRequest requestSave
        {
            get
            {
                UnityWebRequest request;
                if (isSync)
                {
                    request = UnityWebRequest.Put(Resource(ResourceName, primaryKey.ToString()), Attributes.data);
                }
                else
                {
                    request = UnityWebRequest.Post(Resource(ResourceName), Attributes);
                }
                return request;
            }
        }

        public UnityWebRequest requestGet
        {
            get
            {
                return UnityWebRequest.Get(Resource(ResourceName, primaryKey.ToString()));
            }
        }

        public UnityWebRequest RequestDelete
        {
            get
            {
                UnityWebRequest request = UnityWebRequest.Delete(Resource(ResourceName, primaryKey.ToString()));
                return request;
            }
        }

        /*
         * Execute
         */

        public IEnumerator Save()
        {
            return Execute(requestSave);
        }

        public IEnumerator GetByPK()
        {
            return Execute(requestGet);
        }

        public IEnumerator GetByPK(int pk)
        {
            primaryKey = pk;
            return Execute(requestGet);
        }

        public IEnumerator Delete
        {
            get
            {
                yield return Execute(RequestDelete);
                if (status == StatusCode.OK)
                {
                    status = StatusCode.REMOVED;
                }
                else
                {
                    status = StatusCode.ERROR;
                }
            }
        }

        /*
         *  Guardar Informacion en un archivo
         */

        public static string pathResources
        {
            get
            {
                return PathResource(ResourceName + ".json");
            }
        }

        private static List<RvFichaParams> _table;

        public static List<RvFichaParams> Table
        {
            get
            {
                if (_table != null)
                {
                    return _table;
                }
                else
                {
                    if (Helpers.File.ExistsFile(pathResources))
                    {
                        _table = Helpers.File.LoadArrayJson<RvFichaParams>(pathResources);
                        return _table;
                    }
                    else
                    {
                        return new List<RvFichaParams>();
                    }
                }
            }
            set
            {
                Helpers.File.SaveJson(value, pathResources);
                _table = value;
            }
        }
    }
}