﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

namespace Qualitat.GestorVR.Model
{
    [System.Serializable]
    public class RvFichaRecurso : ActiveRecords
    {
        public int id, fic_id, src_id;
        public string name,tipo;

        public WWWForm Attributes
        {
            get
            {
                WWWForm f = new WWWForm();
                if (fic_id > 0) f.AddField("fic_id", fic_id);
                if (src_id > 0) f.AddField("src_id", src_id);
                if (!string.IsNullOrEmpty(name)) f.AddField("name", name);
                if (!string.IsNullOrEmpty(tipo)) f.AddField("tipo", tipo);
                return f;
            }
        }

        /*
         * Relaciones para cargar al sistema
         */
         

        public static string ResourceName { get { return "rvficharecursos"; } }

        public int primaryKey { get { return id; } set { id = value; } }
        public bool isSync { get { return id > 0; } }

        public UnityWebRequest requestSave
        {
            get
            {
                UnityWebRequest request;
                if (isSync)
                {
                    request = UnityWebRequest.Put(Resource(ResourceName, primaryKey.ToString()), Attributes.data);
                }
                else
                {
                    request = UnityWebRequest.Post(Resource(ResourceName), Attributes);
                }
                return request;
            }
        }

        public UnityWebRequest requestGet
        {
            get
            {
                UnityWebRequest request = UnityWebRequest.Get(Resource(ResourceName, primaryKey.ToString()));
                return request;
            }
        }

        public UnityWebRequest RequestDelete
        {
            get
            {
                UnityWebRequest request = UnityWebRequest.Delete(Resource(ResourceName, primaryKey.ToString()));
                return request;
            }
        }

        public IEnumerator Save()
        {
            return Execute(requestSave);
        }

        public IEnumerator GetByPK()
        {
            return Execute(requestGet);
        }

        public IEnumerator GetByPK(int pk)
        {
            primaryKey = pk;
            return Execute(requestGet);
        }

        public IEnumerator Delete
        {
            get
            {
                yield return Execute(RequestDelete);
                if (status == StatusCode.OK)
                {
                    status = StatusCode.REMOVED;
                }
                else
                {
                    status = StatusCode.ERROR;
                }
            }
        }
    }
}