﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;
using System;

namespace Qualitat.GestorVR.Model
{
    [System.Serializable]
    public class RvPregunta : ActiveRecords
    {
        public int pre_id;
        public int eva_id;
        public string descripcion;
        public string comentario;
        public string creado;
        public string modificado;
        public string habilitado;

        public static readonly string ResourceName = "rvpregunta";

        public int primaryKey { get { return pre_id; } set { pre_id = value; } }
        public bool isSync { get { return pre_id > 0; } }

        public WWWForm Attributes
        {
            get
            {
                WWWForm f = new WWWForm();
                if (eva_id > 0) f.AddField("eva_id", eva_id);
                if (!string.IsNullOrEmpty(descripcion)) f.AddField("descripcion", descripcion);
                if (!string.IsNullOrEmpty(comentario)) f.AddField("comentario", comentario);
                return f;
            }
        }

        public UnityWebRequest requestSave
        {
            get
            {
                UnityWebRequest request;
                if (isSync)
                {
                    request = UnityWebRequest.Put(Resource(ResourceName, primaryKey.ToString()), Attributes.data);
                }
                else
                {
                    request = UnityWebRequest.Post(Resource(ResourceName), Attributes);
                }
                return request;
            }
        }

        public UnityWebRequest requestGet
        {
            get
            {
                UnityWebRequest request = UnityWebRequest.Get(Resource(ResourceName, primaryKey.ToString()));
                return request;
            }
        }

        public UnityWebRequest RequestDelete
        {
            get
            {
                UnityWebRequest request = UnityWebRequest.Delete(Resource(ResourceName, primaryKey.ToString()));
                return request;
            }
        }

        public IEnumerator Save()
        {
            return Execute(requestSave);
        }

        public IEnumerator GetByPK()
        {
            return Execute(requestGet);
        }

        public IEnumerator GetByPK(int pk)
        {
            primaryKey = pk;
            return Execute(requestGet);
        }

        public IEnumerator Delete()
        {
            yield return Execute(RequestDelete);
            if (status == StatusCode.OK)
            {
                status = StatusCode.REMOVED;
            }
            else
            {
                status = StatusCode.ERROR;
            }
        }

        /*
         *  Guardar Informacion en un archivo
         */
        public static string pathResources { get { return string.Format("{0}\\{1}.json", pathBase, ResourceName); } }
        private static List<RvPregunta> _table;
        public static List<RvPregunta> Table
        {
            get
            {
                if (_table != null)
                {
                    return _table;
                }
                else
                {
                    if (Helpers.File.ExistsFile(string.Format(pathResources)))
                    {
                        _table = Helpers.File.LoadArrayJson<RvPregunta>(pathResources);
                        return _table;
                    }
                    else
                    {
                        return new List<RvPregunta>();
                    }
                }
            }
            set
            {
                Helpers.File.SaveJson(value, pathResources);
                _table = value;
            }
        }
        /*
         * Definicion de Relaciones
         */
        public RvEvaluacion evaluacion { get { return RvEvaluacion.Table.Find(x => x.primaryKey == eva_id); } }

        public List<RvAlternativa> GetAlternativas() { return RvAlternativa.Table.FindAll(x => x.pre_id == primaryKey); }

        public static void Write(RvPregunta pregunta)
        {
            if (pregunta.isSync)
            {
                var table = Table;
                if (table.Exists(x => x.primaryKey == pregunta.primaryKey))
                {
                    table[table.FindIndex(x => x.primaryKey == pregunta.primaryKey)] = pregunta;
                }
                else
                {
                    table.Add(pregunta);
                }
                Table = table;
            }
        }

        public static void Write(List<RvPregunta> preguntas)
        {
            var table = Table;
            foreach (var pregunta in preguntas)
            {
                if (pregunta.isSync)
                {
                    if (table.Exists(x => x.primaryKey == pregunta.primaryKey))
                    {
                        table[table.FindIndex(x => x.primaryKey == pregunta.primaryKey)] = pregunta;
                    }
                    else
                    {
                        table.Add(pregunta);
                    }
                }
            }
            Table = table;
        }
    }
}