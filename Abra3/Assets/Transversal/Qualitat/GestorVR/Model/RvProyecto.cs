﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

namespace Qualitat.GestorVR.Model
{
    [System.Serializable]
    public class RvProyecto : ActiveRecords
    {
        public static string ResourceName { get { return "rvproyecto"; } }

        public int pro_id;
        public string nombre,descripcion;
        
        public WWWForm Attributes
        {
            get
            {
                WWWForm f = new WWWForm();
                if (!string.IsNullOrEmpty(nombre)) f.AddField("nombre", nombre);
                if (!string.IsNullOrEmpty(descripcion)) f.AddField("descripcion", descripcion);
                return f;
            }
        }

        public int primaryKey { get { return pro_id; } set { pro_id = value; } }
        public bool isSync { get { return pro_id > 0; } }

        public UnityWebRequest requestSave
        {
            get
            {
                UnityWebRequest request;
                if (isSync)
                {
                    request = UnityWebRequest.Put(Resource(ResourceName, primaryKey.ToString()), Attributes.data);
                }
                else
                {
                    request = UnityWebRequest.Post(Resource(ResourceName), Attributes);
                }
                return request;
            }
        }

        public UnityWebRequest requestGet
        {
            get
            {
                UnityWebRequest request = UnityWebRequest.Get(Resource(ResourceName, primaryKey.ToString()));
                return request;
            }
        }

        public UnityWebRequest RequestDelete
        {
            get
            {
                UnityWebRequest request = UnityWebRequest.Delete(Resource(ResourceName, primaryKey.ToString()));
                return request;
            }
        }

        public IEnumerator Save()
        {
            return Execute(requestSave);
        }

        public IEnumerator GetByPK()
        {
            return Execute(requestGet);
        }

        public IEnumerator GetByPK(int pk)
        {
            primaryKey = pk;
            return Execute(requestGet);
        }

        public IEnumerator Delete
        {
            get
            {
                yield return Execute(RequestDelete);
                if (status == StatusCode.OK)
                {
                    status = StatusCode.REMOVED;
                }
                else
                {
                    status = StatusCode.ERROR;
                }
            }
        }

        public UnityWebRequest requestIdentity
        {
            get
            {
                UnityWebRequest request;
                if (isSync)
                {
                    request = UnityWebRequest.Get(Resource(ResourceName, primaryKey.ToString()));
                }
                else
                {
                    WWWForm form = Attributes;
                    request = UnityWebRequest.Post(Resource(ResourceName,"identity"), form);
                }
                return request;
            }
        }

        public IEnumerator GetIdentity()
        {
            return Execute(requestIdentity);
        }

        /*
         *  Guardar Informacion en un archivo
         */
        public static string pathResources { get { return string.Format("{0}\\{1}.json", pathBase, ResourceName); } }
        private static List<RvProyecto> _table;
        public static List<RvProyecto> Table
        {
            get
            {
                if (_table != null)
                {
                    return _table;
                }
                else
                {
                    if (Helpers.File.ExistsFile(string.Format(pathResources)))
                    {
                        _table = Helpers.File.LoadArrayJson<RvProyecto>(pathResources);
                        return _table;
                    }
                    else
                    {
                        return new List<RvProyecto>();
                    }
                }
            }
            set
            {
                Helpers.File.SaveJson(value, pathResources);
                _table = value;
            }
        }
        /*
         * Definicion de Relaciones
         */
        //public RvPregunta pregunta { get { return RvPregunta.Table.Find(x => x.primaryKey.ToString() == pre_id); } }
    }
}