﻿using System.Collections;
using UnityEngine.Networking;
using UnityEngine;

namespace Qualitat.GestorVR.Model
{
    [System.Serializable]
    public class RvRespuesta : ActiveRecords
    {
        public int res_id, alt_id, fic_id;
        public string creado;

        public static string ResourceName { get { return "rvrespuesta"; } }

        public int primaryKey { get { return res_id; } set { res_id = value; } }
        public bool isSync { get { return res_id > 0; } }
        
        public WWWForm Attributes
        {
            get
            {
                WWWForm f = new WWWForm();
                if (alt_id > 0) f.AddField("alt_id", alt_id);
                if (fic_id > 0) f.AddField("fic_id", fic_id);
                return f;
            }
        }

        public UnityWebRequest requestSave
        {
            get
            {
                UnityWebRequest request;
                if (isSync)
                {
                    request = UnityWebRequest.Put(Resource(ResourceName, primaryKey.ToString()), Attributes.data);
                }
                else
                {
                    request = UnityWebRequest.Post(Resource(ResourceName), Attributes);
                }
                return request;
            }
        }

        public UnityWebRequest requestGet
        {
            get
            {
                UnityWebRequest request = UnityWebRequest.Get(Resource(ResourceName, primaryKey.ToString()));
                return request;
            }
        }

        public UnityWebRequest RequestDelete
        {
            get
            {
                UnityWebRequest request = UnityWebRequest.Delete(Resource(ResourceName, primaryKey.ToString()));
                return request;
            }
        }

        public IEnumerator Save()
        {
            return Execute(requestSave);
        }

        public IEnumerator GetByPK()
        {
            return Execute(requestGet);
        }

        public IEnumerator GetByPK(int pk)
        {
            primaryKey = pk;
            return Execute(requestGet);
        }

        public IEnumerator Delete()
        {
            yield return Execute(RequestDelete);
            if (status == StatusCode.OK)
            {
                status = StatusCode.REMOVED;
            }
            else
            {
                status = StatusCode.ERROR;
            }
        }

        /*
         * Relaciones
         */

        public RvAlternativa alternativa { get { return RvAlternativa.Table.Find(x => x.primaryKey == alt_id); } }
    }
}