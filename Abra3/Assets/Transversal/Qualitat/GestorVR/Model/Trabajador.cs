﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine;

namespace Qualitat.GestorVR.Model
{
    [System.Serializable]
    public class Trabajador : ActiveRecords
    {
        public int tra_id,antiguedad,hijos;
        public string rut,nombre,paterno,materno,sexo,nacimiento,fono,mail,gerencia,cargo,estado_civil,creacion,modificacion;

        public static string ResourceName { get { return "trabajador"; } }

        public int primaryKey { get { return tra_id; } set { tra_id = value; } }
        public bool isSync { get { return tra_id > 0; } }

        public WWWForm Attributes
        {
            get
            {
                WWWForm f = new WWWForm();
                if (antiguedad > 0) f.AddField("antiguedad", antiguedad);
                if (hijos > 0) f.AddField("hijos", hijos);
                if (!string.IsNullOrEmpty(nombre)) f.AddField("nombre", nombre);
                if (!string.IsNullOrEmpty(paterno)) f.AddField("paterno", paterno);
                if (!string.IsNullOrEmpty(materno)) f.AddField("materno", materno);
                if (!string.IsNullOrEmpty(sexo)) f.AddField("sexo", sexo);
                if (!string.IsNullOrEmpty(nacimiento)) f.AddField("nacimiento", nacimiento);
                if (!string.IsNullOrEmpty(fono)) f.AddField("fono", fono);
                if (!string.IsNullOrEmpty(mail)) f.AddField("mail", mail);
                if (!string.IsNullOrEmpty(gerencia)) f.AddField("gerencia", gerencia);
                if (!string.IsNullOrEmpty(cargo)) f.AddField("cargo", cargo);
                if (!string.IsNullOrEmpty(estado_civil)) f.AddField("estado_civil", estado_civil);
                if (!string.IsNullOrEmpty(creacion)) f.AddField("creacion", creacion);
                if (!string.IsNullOrEmpty(modificacion)) f.AddField("modificacion", modificacion);
                if (!string.IsNullOrEmpty(rut)) f.AddField("rut", rut);
                return f;
            }
        }

        public UnityWebRequest requestSave
        {
            get
            {
                UnityWebRequest request;
                if (isSync)
                {
                    request = UnityWebRequest.Put(Resource(ResourceName, primaryKey.ToString()), Attributes.data);
                }
                else
                {
                    request = UnityWebRequest.Post(Resource(ResourceName), Attributes);
                }
                return request;
            }
        }

        public UnityWebRequest requestGet
        {
            get
            {
                UnityWebRequest request = UnityWebRequest.Get(Resource(ResourceName, primaryKey.ToString()));
                return request;
            }
        }

        public UnityWebRequest RequestDelete
        {
            get
            {
                UnityWebRequest request = UnityWebRequest.Delete(Resource(ResourceName, primaryKey.ToString()));
                return request;
            }
        }

        public IEnumerator Save()
        {
            return Execute(requestSave);
        }

        public IEnumerator GetByPK()
        {
            return Execute(requestGet);
        }

        public IEnumerator GetByPK(int pk)
        {
            primaryKey = pk;
            return Execute(requestGet);
        }

        public IEnumerator Delete()
        {
            yield return Execute(RequestDelete);
            if (status == StatusCode.OK)
            {
                status = StatusCode.REMOVED;
            }
            else
            {
                status = StatusCode.ERROR;
            }
        }

        public UnityWebRequest requestIdentity
        {
            get
            {
                UnityWebRequest request;
                if (isSync)
                {
                    request = UnityWebRequest.Get(Resource(ResourceName, primaryKey.ToString()));
                }
                else
                {
                    request = UnityWebRequest.Post(Resource(ResourceName,"identity"), Attributes);
                }
                return request;
            }
        }

        public IEnumerator GetIdentity()
        {
            return Execute(requestIdentity);
        }
        
        /*
         *  Guardar Informacion en un archivo
         */
        public static string pathResources { get { return string.Format("{0}\\{1}.json", pathBase, ResourceName); } }
        private static List<Trabajador> _table;
        public static List<Trabajador> Table
        {
            get
            {
                if (_table != null)
                {
                    return _table;
                }
                else
                {
                    if (Helpers.File.ExistsFile(string.Format(pathResources)))
                    {
                        _table = Helpers.File.LoadArrayJson<Trabajador>(pathResources);
                        return _table;
                    }
                    else
                    {
                        return new List<Trabajador>();
                    }
                }
            }
            set
            {
                Helpers.File.SaveJson(value, pathResources);
                _table = value;
            }
        }

        /*
         * Filtros
         */

        public static Trabajador FindRUT(string RUT)
        {
            return Table.Find(x => x.rut == RUT);
        }

        /*
         * CRUD
         */
        public static void Insert(Trabajador trabajador)
        {
            var t = Table;
            if (trabajador.isSync)
            {
                if (!t.Exists(x => x.primaryKey.ToString() == trabajador.primaryKey.ToString()))
                {
                    t.Add(trabajador);
                }
                else
                {
                    var indice = t.FindIndex(x => x.primaryKey.ToString() == trabajador.primaryKey.ToString());
                    t[indice] = trabajador;
                }
                Table = t;
            }
            else
            {
                if (!t.Exists(x => x.rut.Equals(trabajador.rut)))
                {
                    t.Add(trabajador);
                    Table = t;
                }
            }
        }
    }
}