﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using System;

public class EvaluacionVR : MonoBehaviour {
    public Qualitat.GestorVR.Module.Evaluacion.Ficha ficha;

    public static EvaluacionVR current;

    public void Nueva()
    {
        ficha = new Qualitat.GestorVR.Module.Evaluacion.Ficha();
    }

    void Awake()
    {
        DeleteFile();

        if (current == null)
        {
            StartCoroutine(Qualitat.GestorVR.App.AutoLogin());
            DontDestroyOnLoad(gameObject);
            current = this;
        }
        else
        {
            if (current != this)
            {
                Destroy(gameObject);
            }
        }
    }

    void DeleteFile()
    {
        if (Qualitat.Helpers.File.ExistsFile("session.json"))
        {
            Qualitat.Helpers.File.Remove("session.json");
        }
        else { Debug.Log("nada que eliminar"); }
    }
}
