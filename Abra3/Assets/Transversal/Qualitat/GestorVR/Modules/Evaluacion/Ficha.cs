﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Qualitat.GestorVR.Module.Evaluacion
{
    [System.Serializable]
    public class Ficha : Model.RvFicha
    {
        /*
         *  Relaciones para cargar al sistema
         */

        public Model.Trabajador trabajador;
        public List<Model.RvRespuesta> respuestas;
        public Model.RvFichaParams parametro;
        public Model.EmpresaContratista contratista;
        public Model.Ot ot;

        public Error error;

        /*
         * Constructor
         */

        public Ficha()
        {
            respuestas = new List<Model.RvRespuesta>();
            contratista = new Model.EmpresaContratista();
            parametro = new Model.RvFichaParams();
            ot = new Model.Ot();
            creado = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }

        /*
         * Eventos
         */

        public delegate void OnFichaEvent(Ficha ficha);

        /*
         * Comprobación
         */

        public bool IntegrityPrevio()
        {
            bool flag = true;
            if (string.IsNullOrEmpty(trabajador.rut))
            {
                Debug.LogError("[Ficha No registra un trabajador.]");
                flag = false;
            }
            if (disp_id <= 0)
            {
                Debug.LogError("[Ficha No tiene un dispositivo o empresa asignada.]");
                flag = false;
            }
            if (eva_id > 0)
            {
                if (evaluacion.nota != "SIN_NOTA")
                {
                    if (respuestas.Count == 0)
                    {
                        if (!parametro.isValid)
                        {
                            Debug.LogWarning("[Ficha No contiene respuestas o un parametros.]");
                            flag = false;
                        }
                    }
                }
            }
            else
            {
                Debug.LogWarning("[Ficha No tiene asignada una evaluacion.]");
                flag = false;
            }
            return flag;
        }

        /*
         * Forms
         */


        public JSONObject toJSONObject()
        {
            JSONObject ficha = new JSONObject();

            /*
             * Agrega Ficha
             */
            if (eva_id > 0)  ficha.AddField("eva_id", eva_id);
            if (trab_id > 0) ficha.AddField("trab_id", trab_id);
            if (con_id > 0) ficha.AddField("con_id", con_id);
            if (pro_id > 0) ficha.AddField("pro_id", pro_id);
            if (disp_id > 0) ficha.AddField("disp_id", disp_id);
            if (pais_id > 0) ficha.AddField("pais_id", pais_id);
            if (!string.IsNullOrEmpty(calificacion)) ficha.AddField("calificacion", calificacion);
            if (!string.IsNullOrEmpty(creado)) ficha.AddField("creado", creado);

            /*
             * Agrega Trabajador
             */
            JSONObject trabajadorJson = new JSONObject();
            if (trabajador.antiguedad > 0) trabajadorJson.AddField("antiguedad", trabajador.antiguedad);
            if (trabajador.hijos > 0) trabajadorJson.AddField("hijos", trabajador.hijos);
            if (!string.IsNullOrEmpty(trabajador.nombre)) trabajadorJson.AddField("nombre", trabajador.nombre);
            if (!string.IsNullOrEmpty(trabajador.paterno)) trabajadorJson.AddField("paterno", trabajador.paterno);
            if (!string.IsNullOrEmpty(trabajador.materno)) trabajadorJson.AddField("materno", trabajador.materno);
            if (!string.IsNullOrEmpty(trabajador.sexo)) trabajadorJson.AddField("sexo", trabajador.sexo);
            if (!string.IsNullOrEmpty(trabajador.nacimiento)) trabajadorJson.AddField("nacimiento", trabajador.nacimiento);
            if (!string.IsNullOrEmpty(trabajador.fono)) trabajadorJson.AddField("fono", trabajador.fono);
            if (!string.IsNullOrEmpty(trabajador.mail)) trabajadorJson.AddField("mail", trabajador.mail);
            if (!string.IsNullOrEmpty(trabajador.gerencia)) trabajadorJson.AddField("gerencia", trabajador.gerencia);
            if (!string.IsNullOrEmpty(trabajador.cargo)) trabajadorJson.AddField("cargo", trabajador.cargo);
            if (!string.IsNullOrEmpty(trabajador.estado_civil)) trabajadorJson.AddField("estado_civil", trabajador.estado_civil);
            if (!string.IsNullOrEmpty(trabajador.rut)) trabajadorJson.AddField("rut", trabajador.rut);
            ficha.AddField("trabajador", trabajadorJson);

            /*
             * Agrega Respuestas
             */
            if (respuestas.Count > 0)
            {
                JSONObject respuestasjson = new JSONObject(JSONObject.Type.ARRAY);
                for (int i = 0; i < respuestas.Count; i++)
                {
                    JSONObject respuestaJson = new JSONObject();
                    respuestaJson.AddField("alt_id", respuestas[i].alt_id);
                    respuestaJson.AddField("creado", respuestas[i].creado);
                    respuestasjson.Add(respuestaJson);
                }
                ficha.AddField("respuestas", respuestasjson);
            }

            /*
             * Agrega Parametros
             */
            if (parametro.isValid)
            {
                JSONObject parametroJson = new JSONObject();
                parametroJson.AddField("type", "Object");
                parametroJson.AddField("data", new JSONObject(parametro.content));
                parametroJson.AddField("creado", parametro.creado);
                ficha.AddField("params", parametroJson);
            }

            /*
             * Agrega Contratista
             */
            if (con_id == 0 && !string.IsNullOrEmpty(contratista.nombre_corto))
            {
                JSONObject contratistaJson = new JSONObject();
                contratistaJson.AddField("nombre_corto", contratista.nombre_corto);
                if (!string.IsNullOrEmpty(contratista.rut)) contratistaJson.AddField("rut", contratista.rut);
                if (!string.IsNullOrEmpty(contratista.razon_social)) contratistaJson.AddField("razon_social", contratista.razon_social);
                if (!string.IsNullOrEmpty(contratista.direccion)) contratistaJson.AddField("direccion", contratista.direccion);
                if (!string.IsNullOrEmpty(contratista.fono)) contratistaJson.AddField("fono", contratista.fono);
                if (!string.IsNullOrEmpty(contratista.mail)) contratistaJson.AddField("mail", contratista.mail);
                if (!string.IsNullOrEmpty(contratista.web)) contratistaJson.AddField("web", contratista.web);
                ficha.AddField("contratista", contratistaJson);
            }

            /*
             * Agrega Ot
             */
            if(ot.lista>0)
            {
                JSONObject otJson = new JSONObject();
                otJson.AddField("lista", ot.lista);
                otJson.AddField("emp_id", ot.emp_id);
                if (ot.creado != default(DateTime)) otJson.AddField("creado", ot.creado.ToString("yyyyy-MM-dd HH:mm"));
                if (ot.modificado != default(DateTime)) otJson.AddField("modificado", ot.modificado.ToString("yyyyy-MM-dd HH:mm"));
                ficha.AddField("ot", otJson);
            }

            return ficha;
        }

        public bool AddRUT(string rut)
        {
            var is_valid = Helpers.Rut.Validate(rut);
            if (is_valid)
            {
                AddID(rut);
            }
            return is_valid;
        }

        public void AddID(string identificador)
        {
            if (Model.Trabajador.Table.Exists(x => x.rut == identificador))
            {
                trabajador = Model.Trabajador.Table.Find(x => x.rut == identificador);
                trab_id = trabajador.primaryKey;
            }
            else
            {
                trabajador = new Model.Trabajador() { rut = identificador, status = StatusCode.IDENTITY };
            }
            if (App.debug)
            {
                Debug.LogFormat("[Ficha Identidad = {0} ]", trabajador.rut);
            }
        }

        public void AddRespuesta(int Pregunta, string Alternativa)
        {
            if (Model.RvAlternativa.Table.Exists(x => x.pre_id == Pregunta && x.alternativa == Alternativa))
            {
                var a = Model.RvAlternativa.Table.Find(x => x.pre_id == Pregunta && x.alternativa == Alternativa);
                if (respuestas.Exists(x => x.alternativa.pre_id == Pregunta))
                {
                    var indice = respuestas.FindIndex(x => x.alternativa.pre_id == Pregunta);
                    respuestas[indice] = new Model.RvRespuesta()
                    {
                        alt_id = a.primaryKey,
                        creado = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
                    };
                    Debug.LogFormat("[Ficha Respuesta Cambia ({0},{1})]", Pregunta, Alternativa);
                }
                else
                {
                    respuestas.Add(
                        new Model.RvRespuesta()
                        {
                            alt_id = a.primaryKey,
                            creado = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
                        }
                    );
                    if (App.debug)
                    {
                        Debug.LogFormat("[Ficha Respuesta Agrega ({0},{1})]", Pregunta, Alternativa);
                    }
                }
                if (eva_id == 0)
                {
                    eva_id = a.pregunta.eva_id;
                }
            }
            else
            {
                if (App.debug)
                {
                    Debug.LogErrorFormat("[Ficha Respuesta msg=La respuesta {0} - {1} No existe, verifique la evaluaciones asignadas.]", Pregunta, Alternativa);
                }
            }
        }

        public void AddParametro(object @class)
        {
            JSONObject json = new JSONObject(JsonUtility.ToJson(@class));
            AddParametro(json);
        }

        public void AddParametro(List<object> @class)
        {
            JSONObject json = new JSONObject(JsonUtility.ToJson(@class));
            AddParametro(json);
        }

        public void AddParametro(JSONObject json)
        {
            if(parametro.isValid)
            {
                JSONObject parametroJson = parametro.toJSONObject();
                parametroJson.Merge(json);
                parametro.content = parametroJson.Print();
            }
            else
            {
                parametro.type = "Object";
                parametro.content = json.Print();
            }
        }

        public void AddParametro(string key, string value)
        {
            JSONObject json = new JSONObject();
            json.AddField(key, value);
            AddParametro(json);
        }

        public void AddParametro(string key, int value)
        {
            JSONObject json = new JSONObject();
            json.AddField(key, value);
            AddParametro(json);
        }

        public void AddParametro(string key, float value)
        {
            JSONObject json = new JSONObject();
            json.AddField(key, value);
            AddParametro(json);
        }

        public void AddParametro(string key, bool value)
        {
            JSONObject json = new JSONObject();
            json.AddField(key, value);
            AddParametro(json);
        }

        public void AddParametro(string key, DateTime value)
        {
            JSONObject json = new JSONObject();
            json.AddField(key, value.ToString("yyyy-MM-dd HH:mm:ss"));
            AddParametro(json);
        }

        public void AddParametro(string key, JSONObject value)
        {
            JSONObject json = new JSONObject();
            json.AddField(key, value);
            AddParametro(json);
        }

        public void AddParametro(string key, object value)
        {
            JSONObject valueJson = new JSONObject(JsonUtility.ToJson(value));
            JSONObject json = new JSONObject();
            json.AddField(key, valueJson);
            AddParametro(json);
        }

        public void AddContratista(Model.EmpresaContratista contratista)
        {
            if (contratista.id > 0)
            {
                con_id = contratista.id;
            }
            this.contratista = contratista;
        }

        public void AddContratista(string nombre_corto)
        {
            if (disp_id > 0 && Schema.Dispositivo.ExistsDevice(disp_id))
            {
                var dispositivo = Schema.Dispositivo.Table.Find(x => x.dis_id == disp_id);
                if (Schema.Empresa.Table.Exists(x => x.emp_id == dispositivo.emp_id))
                {
                    var empresa = Schema.Empresa.Table.Find(x => x.emp_id == dispositivo.emp_id);
                    if (empresa.contratistas.Exists(x => x.nombre_corto == nombre_corto))
                    {
                        AddContratista(empresa.contratistas.Find(x => x.nombre_corto == nombre_corto));
                    }
                    else
                    {
                        AddContratista(new Model.EmpresaContratista() { nombre_corto = nombre_corto });
                    }
                }
            }
        }

        public void AddEmpresa(int PK)
        {
            if (Model.Dispositivo.Table.Exists(x => x.emp_id == PK))
            {
                disp_id = Model.Dispositivo.Table.Find(x => x.emp_id == PK).primaryKey;
                if (App.debug)
                {
                    Debug.LogFormat("[Ficha Se agrego la empresa : {0} Dispositivo : {1}]", PK, disp_id);
                }
            }
            else
            {
                if (App.debug)
                {
                    Debug.LogErrorFormat("[Ficha msg=El dispositivo no se encuentra registrado o no esta activado : {0}.]", PK);
                }
            }
        }

        public void AddProyecto(string proyecto)
        {
            pro_id = Model.RvProyecto.Table.Find(x => x.nombre == proyecto).primaryKey;
        }

        public void AddOt(int emp_id, int lista)
        {
            ot.lista = lista;
            ot.emp_id = emp_id;
        }

        public void Cerrar()
        {
            if (IntegrityPrevio())
            {
                if (status == StatusCode.CLOSE)
                {
                    if (App.debug)
                    {
                        Debug.Log("[Ficha La evaluacion ya fue cerrada.]");
                    }
                }
                else
                {
                    var table = Table;
                    table.Add(this);
                    Table = table;
                    status = StatusCode.CLOSE;

                    if (App.debug)
                    {
                        Debug.Log("[Ficha La evaluacion ha sido cerrada.]");
                    }
                    if (ExistTemporal())
                    {
                        RemoveTemporal();
                        if (App.debug)
                        {
                            Debug.Log("[Ficha Se elimina la evaluacion temporal.]");
                        }
                    }
                }
            }
            else
            {
                Debug.LogWarning("[Ficha Ha fallado el control de integridad.]");
            }
        }

        public UnityWebRequest requestSaveEvaluation
        {
            get
            {
                //var request = UnityWebRequest.Post(Resource(ResourceName + "/evaluacion"), AttributesWithRelations);
                UnityWebRequest request = Helpers.Http.Post(Resource(ResourceName + "/evaluacion"), toJSONObject().Print());
                App.session.Sign(ref request);
                return request;
            }
        }

        public IEnumerator SaveEvaluation()
        {
            return Execute(requestSaveEvaluation);
        }

        public static readonly string pathTemporal = ResourceName + "_temp.json";

        public static Ficha Temporal
        {
            get
            {
                return Helpers.File.LoadJson<Ficha>(pathTemporal);
            }
            set
            {
                Helpers.File.SaveJson(value, pathTemporal);
            }
        }

        public static bool ExistTemporal()
        {
            return Helpers.File.ExistsFile(pathTemporal);
        }

        public static void RemoveTemporal()
        {
            Helpers.File.Remove(pathTemporal);
        }

        public static readonly string pathBackup = string.Format("{0}_Backups_{1}.json", ResourceName, DateTime.Now.ToString("yyyy-MM-dd"));

        private static List<Ficha> _backup;

        public static List<Ficha> Backup
        {
            get
            {
                if (_backup != null)
                {
                    return _backup;
                }
                else
                {
                    if (Helpers.File.ExistsFile(pathBase, pathBackup))
                    {
                        _backup = Helpers.File.LoadArrayJson<Ficha>("GestorVR", pathBackup);
                        return _backup;
                    }
                    else
                    {
                        return new List<Ficha>();
                    }
                }
            }
            set
            {
                Helpers.File.SaveJson(value, "GestorVR", pathBackup);
                _backup = value;
            }
        }

        public static readonly string pathError = string.Format("{0}_Error_{1}.json", ResourceName, DateTime.Now.ToString("yyyy-MM-dd"));

        private static List<Ficha> _error;

        public static List<Ficha> Error
        {
            get
            {
                if (_error != null)
                {
                    return _error;
                }
                else
                {
                    if (Helpers.File.ExistsFile(pathBase, pathError))
                    {
                        _error = Helpers.File.LoadArrayJson<Ficha>("GestorVR", pathError);
                        return _error;
                    }
                    else
                    {
                        return new List<Ficha>();
                    }
                }
            }
            set
            {
                Helpers.File.SaveJson(value, "GestorVR", pathError);
                _error = value;
            }
        }

        private static List<Ficha> _table;

        public new static List<Ficha> Table
        {
            get
            {
                if (_table != null)
                {
                    return _table;
                }
                else
                {
                    if (Helpers.File.ExistsFile(pathResources))
                    {
                        _table = Helpers.File.LoadArrayJson<Ficha>(pathResources);
                        return _table;
                    }
                    else
                    {
                        return new List<Ficha>();
                    }
                }
            }
            set
            {
                Helpers.File.SaveJson(value, pathResources);
                _table = value;
            }
        }

        public static bool OnExistFichaForLoad()
        {
            if (Table.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static OnFichaEvent onFichaSync;

        private static bool lockSync = false;
        public static IEnumerator SaveAll()
        {
            if (!lockSync)
            {
                //Inicia Bloqueo
                lockSync = true;

                if (App.debug)
                {
                    Debug.Log("[Ficha msg = Inicio proceso de sincronización de fichas]");
                }

                var fichas_por_cargar = Table;

                if (fichas_por_cargar.Count > 0)
                {
                    yield return new WaitUntil(() => App.session.credentials.isValid);

                    //Resuelve dispositivos
                    var dispositivos = new List<int>();
                    foreach (var ficha in fichas_por_cargar)
                    {
                        if (!dispositivos.Contains(ficha.disp_id))
                        {
                            dispositivos.Add(ficha.disp_id);
                        }
                    }

                    yield return Schema.Dispositivo.ResolveDispositivos(dispositivos);

                    //Comienza a cargar evaluaciones
                    var fichas_cargadas = new List<Ficha>();
                    var fichas_no_cargadas = new List<Ficha>();
                    var fichas_con_error = new List<Ficha>();

                    foreach (var ficha in fichas_por_cargar)
                    {
                        /*
                         * Resuelve Fichas de Evaluacion
                         */
                        if (ficha.Integrity)
                        {
                            /*
                             * Crea ficha de evaluacion
                             */
                            var request = ficha.requestSaveEvaluation;
                            yield return request.SendWebRequest();
                            if (request.isNetworkError || request.isHttpError)
                            {
                                if (App.debug)
                                {
                                    Debug.LogErrorFormat("[Ficha Error {0} request={1} {2} body={3} Error={4}]", request.responseCode, request.method, request.url, request.downloadHandler.text, request.error);
                                }

                                if (request.isNetworkError || request.responseCode == 403)
                                {
                                    fichas_no_cargadas.Add(ficha);
                                }
                                else
                                {
                                    Debug.Log(request.downloadHandler.text);
                                    ficha.error.code = (int)request.responseCode;
                                    ficha.error.msg = request.downloadHandler.text;
                                    fichas_con_error.Add(ficha);
                                }
                                ficha.status = StatusCode.ERROR;
                            }
                            else
                            {
                                if (App.debug)
                                {
                                    Debug.LogFormat("[Ficha Exito {0} request={1} {2} body={3}]", request.responseCode, request.method, request.url, request.downloadHandler.text);
                                }

                                JsonUtility.FromJsonOverwrite(request.downloadHandler.text, ficha);
                                if (onFichaSync != null)
                                {
                                    onFichaSync.Invoke(ficha);
                                }
                                fichas_cargadas.Add(ficha);
                            }
                        }
                    }
                    var fichas_backup = Backup;
                    fichas_backup.AddRange(fichas_cargadas);
                    Backup = fichas_backup;

                    var fichas_error = Error;
                    fichas_error.AddRange(fichas_con_error);
                    Error = fichas_error;

                    Table = fichas_no_cargadas;

                    if (App.debug)
                    {
                        Debug.Log("[Ficha termino de sincronización ]");
                    }
                }
                else
                {
                    if (App.debug)
                        Debug.Log("[Ficha ninguna evaluacion a cargar. ]");
                }

                //Libera Bloqueo
                lockSync = false;
            }
            else
            {
                if (App.debug)
                {
                    Debug.LogWarning("[Ficha El sistemas ya se encuentra sincronizando las evaluaciones. ]");
                }
            }
        }
    }
}
