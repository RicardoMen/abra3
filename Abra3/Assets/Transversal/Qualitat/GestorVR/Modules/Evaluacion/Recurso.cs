﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Qualitat.GestorVR.Module.Evaluacion
{
    [System.Serializable]
    public class FichaRecurso : Model.RvFichaRecurso
    {
        public Model.RecursosSources recurso;
    }
}