﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Qualitat.GestorVR.Module.Schema
{
    [System.Serializable]
    public class Dispositivo : Model.Dispositivo
    {
        private static Model.Device _current;

        public static readonly string path = "gestorvr_device.json";

        public static Model.Device Current()
        {
            if (_current == null)
            {
                if (Helpers.File.ExistsFile(path))
                {
                    _current = Helpers.File.LoadJson<Model.Device>(path);
                }
                else
                {
                    _current = new Model.Device();
                    _current.Current();
                    Helpers.File.SaveJson(_current, path);
                }
            }
            return _current;
        }

        public Dispositivo(int empresa)
        {
            var c = Current();
            keycode = c.keycode;
            nombre = c.name;
            emp_id = empresa;
        }

        public static Model.Dispositivo FindByEmpresa(int emp_id)
        {
            return Table.Find(x => x.keycode == Current().keycode && x.emp_id == emp_id);
        }

        public static bool ExistsEmpresa(int emp_id)
        {
            if (Table.Exists(x => x.keycode == Current().keycode && x.emp_id == emp_id))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool ExistsDevice(int dis_id)
        {
            if (Table.Exists(x => x.dis_id == dis_id))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static IEnumerator ResolveEmpresas(List<int> empresas)
        {
            var devicesList = Table;

            foreach (var emp_id in empresas)
            {
                if (ExistsEmpresa(emp_id))
                {
                    var device = FindByEmpresa(emp_id);
                    var current_modified = device.modificado;
                    yield return device.GetByPK("?fields=modificado");
                    if (current_modified != device.modificado)
                    {
                        yield return device.GetByPK();
                        if (App.debug)
                        {
                            Debug.Log("[Dispositivo nuevo status = {1} " + (device.isAvailable ? "Habilitado" : "Desahbilitado"));
                        }
                    }
                    else
                    {
                        if (App.debug)
                        {
                            Debug.LogFormat("[Dispositivo [{0}] status = {1}", device.dis_id, device.isAvailable ? "Habilitado" : "Desahbilitado");
                        }
                    }
                }
                else
                {
                    if (App.debug)
                    {
                        Debug.Log("[Dispositivo msg = No se encuentra informacion del dispositivo, se resolvera identidad ... ]");
                    }
                    var device = new Dispositivo(emp_id);
                    var form = device.Attributes;

                    form.AddField("dipositivoTipo[nombre]", SystemInfo.deviceModel);
                    form.AddField("dipositivoTipo[modelo]", SystemInfo.deviceType.ToString("g"));

                    var request = UnityWebRequest.Post(Resource(ResourceName, "identity"), form);

                    App.session.Sign(ref request);

                    yield return request.SendWebRequest();

                    if (request.isNetworkError)
                    {
                        if (App.debug)
                        {
                            Debug.LogError("[Device msg = No tiene acceso a internet]" + request.downloadHandler.text);
                        }
                    }
                    else if (request.isHttpError)
                    {
                        if (App.debug)
                        {
                            Debug.LogError("[Device msg = Error al registrar dispositivo ]" + request.downloadHandler.text);
                        }
                    }
                    else
                    {
                        var dispositivo_nuevo = JsonUtility.FromJson<Model.Dispositivo>(request.downloadHandler.text);

                        devicesList.Add(dispositivo_nuevo);

                        if (App.debug)
                        {
                            Debug.Log("Se ha registrado el dispositivo :" + dispositivo_nuevo.dis_id + " Empresa [" + dispositivo_nuevo.emp_id + "] : " + (dispositivo_nuevo.isAvailable ? "Habilitado" : "Desahbilitado"));
                        }
                    }
                }
            }
            Table = devicesList;
        }

        public static IEnumerator ResolveDispositivos(List<int> dispositivos)
        {
            var devicesList = Table;

            foreach (var dis_id in dispositivos)
            {
                if (devicesList.Exists(x => x.dis_id == dis_id))
                {
                    var device = devicesList.Find(x => x.dis_id == dis_id);
                    var current_modified = device.modificado;
                    yield return device.GetByPK("?fields=modificado");
                    if (current_modified != device.modificado)
                    {
                        yield return device.GetByPK();
                        if (App.debug)
                        {
                            Debug.Log("[Dispositivo El dispositivo se actualizo : " + (device.isAvailable ? "Habilitado" : "Desahbilitado"));
                        }
                    }
                }
                else
                {
                    var device = new Model.Dispositivo()
                    {
                        primaryKey = dis_id
                    };

                    yield return device.GetByPK();
                    if (device.status != StatusCode.ERROR)
                    {
                        devicesList.Add(device);
                        if (App.debug)
                        {
                            Debug.LogError("[Dispositivo se añadio nuevo dispositivo");
                        }
                    }
                    else
                    {
                        if (App.debug)
                        {
                            Debug.LogError("[Dispositivo No se pudo verificar el dispositivo");
                        }
                    }
                }
            }

            Table = devicesList;
        }
    }
}
