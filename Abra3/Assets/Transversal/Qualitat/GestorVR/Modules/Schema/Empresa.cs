﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Qualitat.GestorVR.Module.Schema
{
    [System.Serializable]
    public class Empresa : Model.Empresa
    {
        public int cntCon;
        public List<Model.EmpresaContratista> contratistas;

        private new static readonly string pathResources = "GestorVR/schema_empresa.json";

        private static List<Empresa> _table;

        public Empresa()
        {
            contratistas = new List<Model.EmpresaContratista>();
        }

        public new static List<Empresa> Table
        {
            get
            {
                if (_table != null)
                {
                    return _table;
                }
                else
                {
                    if (Helpers.File.ExistsFile(pathResources))
                    {
                        _table = Helpers.File.LoadArrayJson<Empresa>(pathResources);
                        if(_table == null)
                        {
                            _table = new List<Empresa>();
                        }
                        return _table;
                    }
                    else
                    {
                        return new List<Empresa>();
                    }
                }
            }
            set
            {
                Helpers.File.SaveJson(value, pathResources);
                _table = value;
            }
        }

        public static IEnumerator Resolve(List<int> empresas)
        {            
            foreach (var emp_id in empresas)
            {
                var table = Table;
                if (table.Exists(x => x.emp_id == emp_id))
                {
                    var empresa = table.Find(x => x.emp_id == emp_id);
                    var cntCon = empresa.cntCon;
                    yield return empresa.GetByPK("?fields=0&expand=cntCon");
                    if(empresa.status == StatusCode.OK)
                    {
                        if(empresa.cntCon != cntCon)
                        {
                            yield return empresa.GetByPK("?fields=0&expand=contratistas");
                        }
                    }
                }
                else
                {
                    var empresa = new Empresa() { emp_id = emp_id };
                    yield return empresa.GetByPK("?fields=emp_id&expand=contratistas");
                    if(empresa.status == StatusCode.OK)
                    {
                        empresa.cntCon = empresa.contratistas.Count;
                        table.Add(empresa);
                    }
                }
                Table = table;
            }
        }
    }
}