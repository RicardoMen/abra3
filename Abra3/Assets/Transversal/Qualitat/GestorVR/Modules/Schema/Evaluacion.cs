﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Qualitat.GestorVR.Module.Schema
{
    [System.Serializable]
    public class Evaluacion : Model.RvEvaluacion
    {
        public List<Pregunta> preguntas;
        
        public Evaluacion(int eva_id)
        {
            this.eva_id = eva_id;
        }

        public static void Write(Evaluacion evaluacion)
        {
            var alternativas = new List<Model.RvAlternativa>();
            var items = new List<Model.RvItem>();
            var preguntas = new List<Model.RvPregunta>();
            foreach (var pregunta in evaluacion.preguntas)
            {
                if (!items.Exists(x => x.ite_id == pregunta.item.ite_id))
                {
                    items.Add(pregunta.item);
                }
                alternativas.AddRange(pregunta.alternativas);
                preguntas.Add(pregunta);
            }

            Model.RvEvaluacion.Write(evaluacion);
            Model.RvPregunta.Write(preguntas);
            Model.RvAlternativa.Write(alternativas);
            Model.RvItem.Write(items);
        }

        public static IEnumerator Resolve(List<int> evaluaciones)
        {
            foreach (var eva_id in evaluaciones)
            {
                if (Table.Exists(x => x.eva_id == eva_id))
                {
                    var eva_last_change = new Evaluacion(eva_id);
                    yield return eva_last_change.GetByPK("?fields=modificado");
                    if (eva_last_change.status == StatusCode.OK)
                    {
                        if (!string.IsNullOrEmpty(eva_last_change.modificado))
                        {
                            var actual = Table.Find(x => x.eva_id == eva_id);

                            if (!actual.modificado.Equals(eva_last_change.modificado))
                            {
                                if (App.debug)
                                {
                                    Debug.LogWarning("[Evaluacion La evaluacion ha cambiado, comenzando rescate.");
                                }
                                yield return eva_last_change.GetByPK("/full");
                                Write(eva_last_change);
                            }
                        }
                    }
                    else
                    {
                        if (App.debug)
                        {
                            Debug.LogError("[Evaluacion No se puede actualizar la evaluacion por algun error.");
                        }
                    }
                }
                else
                {
                    if (App.debug)
                    {
                        Debug.LogWarning("[Evaluacion La evaluacion no se encuentra almacenada, comenzando rescate.");
                    }
                    var eva = new Evaluacion(eva_id);
                    yield return eva.GetByPK("/full");
                    if (eva.status == StatusCode.OK)
                    {
                        if (App.debug)
                        {
                            Debug.Log("[Evaluacion La evaluacion se actualizo correctamente.");
                        }
                        Write(eva);
                    }
                    else
                    {
                        if (App.debug)
                        {
                            Debug.LogError("[Evaluacion No se puede actualizar la evaluacion por algun error.");
                        }
                    }
                }
            }
        }
    }
}