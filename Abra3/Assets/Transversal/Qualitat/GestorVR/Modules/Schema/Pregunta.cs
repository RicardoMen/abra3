﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Qualitat.GestorVR.Module.Schema
{
    [System.Serializable]
    public class Pregunta : Model.RvPregunta
    {
        public List<Model.RvAlternativa> alternativas;
        public Model.RvItem item;

        public static void Write(Pregunta pregunta)
        {
            Model.RvPregunta.Write(pregunta);
            Model.RvItem.Write(pregunta.item);
            foreach (var alt in pregunta.alternativas)
            {
                Model.RvAlternativa.Write(alt);
            }
        }
    }
}