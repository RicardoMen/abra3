﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;


namespace Qualitat.GestorVR.Module.Schema
{
    public class SchemaCtrl : MonoBehaviour
    {
        [Header("Configuración GestorVR")]
        [Tooltip("Se debe ingresar la identificación de las evaluaciónes")]
        public List<int> evaluaciones;
        [Tooltip("Se debe ingresar la identificación de las empresas que participan")]
        public List<int> empresas;
        [Tooltip("En caso de ocupar los contratistas de la empresa marcar esta opcion")]
        public bool SyncContratistas;

        private IEnumerator Start()
        {
            StartCoroutine(App.AutoLogin());

            yield return new WaitUntil(App.session.isValid);

            StartCoroutine(Evaluacion.Resolve(evaluaciones));
            StartCoroutine(Dispositivo.ResolveEmpresas(empresas));
            if (SyncContratistas)
            {
                StartCoroutine(Empresa.Resolve(empresas));
            }
        }
    }
}
