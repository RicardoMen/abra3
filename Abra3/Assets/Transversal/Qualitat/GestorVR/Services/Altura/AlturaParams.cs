﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AlturaParams
{
    public enum Nivel:int {Ejemplar,AltaCalidad,Promedio,Aceptable,Inaceptable }

    [System.Serializable]
    public class Objetivo
    {
        public Nivel objetivo;
    }

    [System.Serializable]
    public class Escena : Objetivo
    {
        public int intentos;
        public int tiempo;
    }

    [System.Serializable]
    public class Escena_2 : Objetivo
    {
        public int tiempo;
        public int caidas;
        public bool escotilla_1;
        public bool escotilla_2;
    }

    [System.Serializable]
    public class Escena_3 : Objetivo
    {
        public int intentos;
        public int tiempo;
        public int caidas;
    }

    [System.Serializable]
    public class Escena_4 : Escena_3
    {
        public Vector3 orientacion;
    }
    
    [System.Serializable]
    public class Escena_5 : Objetivo
    {
        public int tiempo;
        public Vector3 posicion;
        public List<Objeto> objetos;

        public Escena_5(int tiempo, Vector3 posicion, List<Objeto> objetos)
        {
            this.tiempo = tiempo;
            this.posicion = posicion;
            this.objetos = objetos;
        }
    }

    [System.Serializable]
    public class Objeto : Objetivo
    {
        public bool seleccionado;
        public bool visto;
        public float momento;
        public Vector3 observador;
    }

    public Escena esc_1_1;
    public Escena esc_1_2;
    public Escena esc_1_3;
    public Escena_2 esc_2_1;
    public Escena_2 esc_2_2;
    public Escena_3 esc_3_1;
    public Escena_3 esc_3_2;
    public Escena_4 esc_4_1;
    public Escena_4 esc_4_2;
    public Escena_5 esc_5;

    public new string ToString()
    {
        return JsonUtility.ToJson(this);
    }
}
