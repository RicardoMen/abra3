﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Qualitat.GestorVR.Service.AxaServices
{
    public static class Axa
    {
        /// <summary>
        /// Evaluaciones aplicables a Axa Colpatria
        /// </summary>
        public enum evaluacion { SinEvaluacion, Escalera = 86, Manlift = 87, Andamio = 88 }
        /// <summary>
        /// Posibles Perfiles de axa
        /// </summary>
        public enum perfil { SinPerfil, Trabajador, Coordinador }

        private static readonly Uri endpoint = new Uri(App.Host.url.ToString() + "/v1/axa");
        private static readonly string prefix = "statuspersona_";
        //private static JSONObject statusPersona = new JSONObject();
        /// <summary>
        /// Trae la información del los procesos relacionados con el trabajador de axa
        /// </summary>
        /// <param name="cedula_de_ciudadania"></param>
        /// <returns></returns>
        public static IEnumerator LoadStatusTrabajador(string cedula_de_ciudadania)
        {
            if (!App.session.isValid())
            {
                if (App.debug)
                    Debug.LogError("[Axa para el correcto funcionamiento de los servicios de axa, favor utilizar el schema para iniciar sesión");
                yield return new WaitUntil(() => App.session.isValid());
            }
            Uri url = new Uri(string.Format("{0}/statuspersona?cc={1}", endpoint.ToString(), cedula_de_ciudadania));
            UnityWebRequest request = UnityWebRequest.Get(url.ToString());
            App.session.Sign(ref request);
            yield return request.SendWebRequest();
            if (request.isNetworkError)
            {
                if (App.debug)
                    Debug.LogError("[Axa no se encuentra disponible la red");
            }
            else if (request.isHttpError)
            {
                if (App.debug)
                    Debug.LogWarning("[Axa ocurre un error con el servicio : " + request.downloadHandler.text);
            }
            else
            {
                if (App.debug)
                    Debug.Log("[Axa exito al traer la información del trabajador : " + cedula_de_ciudadania + " = " + request.downloadHandler.text);
            }
            JSONObject jsonBody = new JSONObject(request.downloadHandler.text);
            PlayerPrefs.SetString(prefix + cedula_de_ciudadania, jsonBody.Print());

        }


        /// <summary>
        /// Retorna un objeto con el contenido del trabajador
        /// </summary>
        /// <param name="cedula_de_ciudadania"></param>
        /// <returns></returns>
        public static JSONObject GetJsonTrabajador(string cedula_de_ciudadania)
        {
            if (PlayerPrefs.HasKey(prefix + cedula_de_ciudadania))
            {
                return new JSONObject(PlayerPrefs.GetString(prefix + cedula_de_ciudadania));
            }
            else
            {
                return new JSONObject(JSONObject.Type.NULL);
            }
        }

        /// <summary>
        /// Retorna un Objecto que contiene la información solicitada
        /// </summary>
        /// <param name="cedula_de_ciudadania"></param>
        /// <returns></returns>
        public static StatusPersona GetStatusPersona(string cedula_de_ciudadania)
        {
            StatusPersona persona = new StatusPersona();
            if(isExistsTrabajador(cedula_de_ciudadania))
            {
                var json = GetJsonTrabajador(cedula_de_ciudadania);
                JsonUtility.FromJsonOverwrite(json.Print(), persona);
                try
                {
                    persona.perfil = (perfil)Enum.Parse(typeof(perfil), json["perfil"].str);

                }
                catch (Exception)
                {
                    persona.perfil = perfil.SinPerfil;
                }
                try
                {
                    persona.lastEva = (evaluacion)json["lastEva"].n;
                }
                catch (Exception)
                {
                    persona.lastEva = evaluacion.SinEvaluacion;
                }
            }
            return persona;
        }

        /// <summary>
        /// Pregunta si la ultima solicitud fue correcta
        /// </summary>
        /// <param name="cedula_de_ciudadania"></param>
        /// <returns></returns>
        public static bool isSuccessRequest(string cedula_de_ciudadania)
        {
            if (PlayerPrefs.HasKey(prefix + cedula_de_ciudadania))
            {
                JSONObject jsonResponse = new JSONObject(PlayerPrefs.GetString(prefix + cedula_de_ciudadania));
                if (jsonResponse.type == JSONObject.Type.NULL)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Pregunta si existe el trabajador
        /// </summary>
        /// <param name="cedula_de_ciudadania"></param>
        /// <returns></returns>
        public static bool isExistsTrabajador(string cedula_de_ciudadania)
        {
            if (PlayerPrefs.HasKey(prefix + cedula_de_ciudadania))
            {
                JSONObject jsonResponse = new JSONObject(PlayerPrefs.GetString(prefix + cedula_de_ciudadania));
                return !jsonResponse.HasField("status");
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Responde la ultima evaluación realizada
        /// </summary>
        /// <param name="cedula_de_ciudadania"></param>
        /// <returns></returns>
        public static evaluacion GetLastEvaluationTrabajador(string cedula_de_ciudadania)
        {
            if (PlayerPrefs.HasKey(prefix + cedula_de_ciudadania))
            {
                JSONObject jsonResponse = new JSONObject(PlayerPrefs.GetString(prefix + cedula_de_ciudadania));
                try
                {
                    return (evaluacion)jsonResponse["lastEva"].n;

                }
                catch (Exception)
                {
                    return evaluacion.SinEvaluacion;
                }
            }
            else
            {
                return evaluacion.SinEvaluacion;
            }
        }

        /// <summary>
        /// Responde el ultimo perfil inscrito
        /// </summary>
        /// <param name="cedula_de_ciudadania"></param>
        /// <returns></returns>
        public static perfil GetPerfilTrabajador(string cedula_de_ciudadania)
        {
            if (PlayerPrefs.HasKey(prefix + cedula_de_ciudadania))
            {
                if (GetLastEvaluationTrabajador(cedula_de_ciudadania) == evaluacion.Andamio || GetLastEvaluationTrabajador(cedula_de_ciudadania) == evaluacion.SinEvaluacion)
                {
                    return perfil.SinPerfil;
                }
                else
                {
                    JSONObject jsonResponse = new JSONObject(PlayerPrefs.GetString(prefix + cedula_de_ciudadania));
                    try
                    {
                        return (perfil)Enum.Parse(typeof(perfil), jsonResponse["perfil"].str);
                    }
                    catch (Exception)
                    {
                        return perfil.SinPerfil;
                    }
                }
            }
            else
            {
                return perfil.SinPerfil;
            }
        }

        /// <summary>
        /// Da la orden de comenzar una nueva evaluación en caso de no empezar un proceso o tener pendiente la información 
        /// </summary>
        /// <param name="cedula_de_ciudadania"></param>
        /// <returns></returns>
        public static bool ComenzarNuevoProceso(string cedula_de_ciudadania)
        {
            return isSuccessRequest(cedula_de_ciudadania) && (!isExistsTrabajador(cedula_de_ciudadania) || GetLastEvaluationTrabajador(cedula_de_ciudadania) == evaluacion.Andamio) ;
        }
    }
}
