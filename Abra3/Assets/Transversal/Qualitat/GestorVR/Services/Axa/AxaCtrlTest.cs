﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qualitat.GestorVR.Service.AxaServices;

public class AxaCtrlTest : MonoBehaviour
{
    // Start is called before the first frame update
    IEnumerator Start()
    {
        /**
         * Esta consulta la información del trabajador, hacerlo antes de la evaluación y depues, y tener el schema funcionando
         */
        string cedula = "180686312";
        yield return Axa.LoadStatusTrabajador(cedula);

        Debug.Log($"Existe el trabajador : {Axa.isExistsTrabajador(cedula)}");
        Debug.Log($"La solicitud fue valida : {Axa.isSuccessRequest(cedula)}");
        Debug.Log($"La ultima evaluación que hizo fue : {Axa.GetLastEvaluationTrabajador(cedula)}");
        Debug.Log($"El Perfil del trabajador es : {Axa.GetPerfilTrabajador(cedula)}");
        Debug.Log($"Comenzar nueva evaluación : {Axa.ComenzarNuevoProceso(cedula)}");
        Debug.Log($"El data completa es : {Axa.GetJsonTrabajador(cedula).Print(true)}");
        Debug.Log($"El dato serializado es : {Axa.GetStatusPersona(cedula)} {JsonUtility.ToJson(Axa.GetStatusPersona(cedula))}");

    }
}
