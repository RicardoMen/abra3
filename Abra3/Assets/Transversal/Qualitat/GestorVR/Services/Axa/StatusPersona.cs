﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Qualitat.GestorVR.Service.AxaServices
{
    /**
     * 
    * {
    *  "ficId": 38506,
    *  "cc": "180686312",
    *  "nombre": "Juan Esteban",
    *  "paterno": "Veliz",
    *  "materno": "Viveros",
    *  "pefil": "coordinador",
    *  "empresa": "QUALITATCORP",
    *  "nit": "76.150.831-8",
    *  "lastEva": 88
    * }
    */
    public class StatusPersona
    {
        public int ficId;
        public string rut;
        public string nombre;
        public string paterno;
        public string materno;
        public Axa.perfil perfil;
        public string empresa;
        public string nit;
        public Axa.evaluacion lastEva; 
    }
}
