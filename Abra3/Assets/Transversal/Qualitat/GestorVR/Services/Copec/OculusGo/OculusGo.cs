﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;

namespace Qualitat.Copec
{
    public static class OculusGo
    {
        public enum Status
        {
            NETWORK_ERROR,
            EMPTY,
            ERROR,
            SUCCESS = 200,
            CREATED = 201,
            REMOVED = 204,
            NOT_MODIFIED = 304,
            UNPROCESABLE_ENTITY = 422,
            NOT_FOUND = 404,
            FORBIDDEN = 403,
            UNAUTHORIZED = 401,
            INTERNAL_SERVER = 500
        }

        private static readonly string token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJjbGkiOjIsImlhdCI6MTU1OTg1MDQ3Miwic3ViIjoiTGxzRyIsInR5cCI6InB3ZCIsInVpZCI6MiwidW4iOiJEaXNwb3NpdGl2byBPY3VsdXNHbyJ9.WLUOM-BAkZ2AsgTi_80zi3MAh1zyl_-0Ub7DyaZFJvw";
        private static readonly Uri host = new Uri("http://localhost/Copec-Backend/web/");
        //private static readonly Uri host = new Uri("https://academiacopec.qualitatcorp.cl/api-qualitat/");
        private static UnityWebRequest request;
        private static JSONObject persona = new JSONObject();

        public static IEnumerator GetPersona(string rut)
        {
            request = UnityWebRequest.Get(new Uri(host, string.Format("persona?filter[rut]={0}&expand=pm,cursos,cursosModulos,roles", rut)).ToString());
            request.SetRequestHeader("Authorization", token);
            yield return request.SendWebRequest();
            if (request.isNetworkError)
            {
                Debug.LogError("[Copec Error network");
            }
            else if (request.isHttpError)
            {
                Debug.LogErrorFormat("[Copec Error http code {0}", request.responseCode);
                Debug.LogError(request.downloadHandler.text);
            }
            else
            {
                Debug.Log("[Copec Exito al cargar la persona");
                persona = new JSONObject(request.downloadHandler.text)[0];
            }
        }

        public static bool IsSuccess()
        {
            if (request == null || request.isNetworkError || request.isHttpError)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static Status GetStatus()
        {
            if (request == null)
            {
                return Status.EMPTY;
            }
            else if (request.isNetworkError)
            {
                return Status.NETWORK_ERROR;
            }
            else
            {
                switch (request.responseCode)
                {
                    case 200: return Status.SUCCESS;
                    case 201: return Status.CREATED;
                    case 203: return Status.REMOVED;
                    case 401: return Status.UNAUTHORIZED;
                    case 403: return Status.FORBIDDEN;
                    case 404: return Status.NOT_FOUND;
                    case 422: return Status.UNPROCESABLE_ENTITY;
                    case 500: return Status.INTERNAL_SERVER;
                    default:
                        return Status.ERROR;
                }
            }
        }

        public static bool IsError()
        {
            if (request == null || request.isNetworkError || request.isHttpError)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool HasRol(string rol)
        {
            if (persona == null)
            {
                return false;
            }
            else
            {
                return persona["roles"].list.Exists(x => x["nombre"].str == rol);
            }
        }

        public static bool HasRol(int id)
        {
            if (persona == null)
            {
                return false;
            }
            else
            {
                return persona["roles"].list.Exists(x => x["id"].n == id);
            }
        }

        public static List<int> GetRoles()
        {
            List<int> lista = new List<int>();
            lista = persona["roles"].list.ConvertAll<int>(x => (int)x["id"].n);
            return lista;
        }

        public static List<int> GetModulos()
        {
            List<int> lista = new List<int>();
            lista = persona["cursosModulos"].list.ConvertAll(x => (int)x["id"].n);
            return lista;
        }

        public static List<int> GetCursos()
        {
            List<int> lista = new List<int>();
            lista = persona["cursos"].list.ConvertAll(x => (int)x["id"].n);
            return lista;
        }

        public static List<int> GetModuloPermission()
        {
            List<int> lista = new List<int>();
            lista = persona["pm"].list.ConvertAll(x => (int)x["mod_id"].n);
            return lista;
        }

        public static IEnumerator AllowModuloPemission(int id_modulo)
        {
            if (persona != null)
            {
                if(persona["pm"].IsArray)
                {
                    if(persona["pm"].list.Exists(x => x["mod_id"].n == id_modulo))
                    {
                        Debug.Log("[Copec Ya tiene permitido el modulo, no hacer nada");
                    }
                    else
                    {
                        WWWForm form = new WWWForm();
                        form.AddField("per_id",(int)persona["id"].n);
                        form.AddField("mod_id",id_modulo);
                        UnityWebRequest request = UnityWebRequest.Post(new Uri(host, "personamodulo").ToString(),form);
                        request.SetRequestHeader("Authorization", token);
                        yield return request.SendWebRequest();
                        if (request.isNetworkError)
                        {
                            Debug.LogError("[Copec Error network");
                        }
                        else if (request.isHttpError)
                        {
                            Debug.LogErrorFormat("[Copec Error http code {0}", request.responseCode);
                            Debug.LogError(request.downloadHandler.text);
                        }
                        else
                        {
                            Debug.Log("[Copec Exito al permite Modulo "+id_modulo);
                            yield return UpdatePersona();
                            Debug.Log("[Copec Termino de Actualizar");
                        }
                    }
                }
            }
        }

        public static IEnumerator UpdatePersona()
        {
            yield return GetPersona(persona["rut"].str);
        }

        public static IEnumerator DenyModuloPemission(int id_modulo)
        {

            if (persona != null)
            {
                if (persona["pm"].IsArray)
                {
                    if (persona["pm"].list.Exists(x => x["mod_id"].n == id_modulo))
                    {
                        JSONObject pm = persona["pm"].list.Find(x => x["mod_id"].ToString() == id_modulo.ToString());
                        UnityWebRequest request = UnityWebRequest.Delete(new Uri(host, string.Format("personamodulo/{0}", pm["id"].ToString())).ToString());
                        request.SetRequestHeader("Authorization", token);
                        yield return request.SendWebRequest();
                        if (request.isNetworkError)
                        {
                            Debug.LogError("[Copec Error network");
                        }
                        else if (request.isHttpError)
                        {
                            Debug.LogErrorFormat("[Copec Error http code {0}", request.responseCode);
                            Debug.LogError(request.downloadHandler.text);
                        }
                        else
                        {
                            Debug.Log("[Copec Exito al denegar modulo " + id_modulo);
                            yield return UpdatePersona();
                            Debug.Log("[Copec Termino de Actualizar");
                        }
                    }
                    else
                    {
                        Debug.Log("[Copec No tiene acceso al modulo, nada que eliminar"+persona["pm"]);
                    }
                }
            }
            else
            {
                Debug.Log("persona es nula");
            }
        }
    }
}
