﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qualitat.Copec;
public class OculusGoTestCtrl : MonoBehaviour {

    IEnumerator Start()
    {
        yield return OculusGo.GetPersona("5.215.762-5");
        if (OculusGo.IsSuccess())
        {
            Debug.Log(OculusGo.HasRol("Técnico atendedor de servicioa"));
            Debug.Log(OculusGo.HasRol(3));
            Debug.Log(OculusGo.GetRoles().Count);
            Debug.Log(OculusGo.GetModulos().Count);
            Debug.Log(OculusGo.GetCursos().Count);
            Debug.Log(OculusGo.GetModuloPermission()[0]);
            yield return OculusGo.AllowModuloPemission(3);
            Debug.Log("Termino Permitir");
            yield return OculusGo.DenyModuloPemission(3);
            Debug.Log("Termino Denegar");

        }
        else
        {
            Debug.Log("No logro o no existe");
        }
    }
}
