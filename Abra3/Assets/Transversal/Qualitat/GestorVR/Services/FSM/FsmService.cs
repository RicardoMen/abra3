﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Qualitat.GestorVR
{
    [System.Serializable]
    public class FsmService : ActiveRecords
    {
        public static readonly string ResourceName = "fsm";
       
        /// <summary>
        /// Funcion Encargada de averiguar si el trabajador ya se ha evaluado en el sistema, la variable status marca como EXISTS o NOT_EXISTS dependiendo si se encuentra, en caso de otro codigo dara error.
        /// </summary>
        /// <param name="rut">Debes ingresar el rut con formato valido ej : 11.111.111-1 </param>
        /// <param name="eva_id">Debe ingresar el codigo de evaluación1 </param>
        /// <returns></returns>
        public IEnumerator RevisarTrabajador(string rut,int eva_id)
        {
            WWWForm form = new WWWForm();
            form.AddField("trabajador[rut]", rut);
            form.AddField("evaluacion[eva_id]", eva_id);
            var request = UnityWebRequest.Get(Resource(form,ResourceName,"trabajador","estado"));
            App.session.Sign(ref request);
            yield return request.SendWebRequest();
            if(request.isNetworkError)
            {
                status = StatusCode.ERROR;
            }
            else
            {
                switch(request.responseCode)
                {
                    case 200:
                        status = StatusCode.EXISTS;
                        break;
                    case 404:
                        status = StatusCode.NOT_EXISTS;
                        break;
                    case 403:
                        status = StatusCode.FORBIDDEN;
                        break;
                    default:
                        status = StatusCode.ERROR;
                        break;
                }
            }
        }
    }
}