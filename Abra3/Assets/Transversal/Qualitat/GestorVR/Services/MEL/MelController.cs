﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MelController : MonoBehaviour {
    public string rut;
    public Qualitat.GestorVR.MelService mel;

	void Start () {
        StartCoroutine(Espera(rut));
	}

    IEnumerator Espera(string rut)
    {
        mel = new Qualitat.GestorVR.MelService();
        yield return mel.Rescue(rut);
        if(mel.status==Qualitat.GestorVR.ActiveRecords.StatusCode.OK)
        {
            Debug.Log("Hizo la evaluacion " + mel.eva_id);
            Saluda();
        }
        else
        {
            //No lo hizo o no lo pudo resolver
            Debug.Log("No pudo T.T");
        }
    }

    public void Saluda()
    {
        Debug.Log("Siguiente funcion...");
    }
}