﻿using System.Collections;
using UnityEngine.Networking;
using UnityEngine;

namespace Qualitat.GestorVR
{
    [System.Serializable]
    public class MelService : ActiveRecords
    {
        public int eva_id;

        public bool isValid
        {
            get
            {
                return eva_id != 0;
            }
        }

        public static readonly string ResourceName = "mel";

        public IEnumerator Rescue(string rut)
        {
            WWWForm form = new WWWForm();
            form.AddField("rut", rut);
            return Execute(UnityWebRequest.Post(Resource(ResourceName,"rescue"),form));            
        }
    }
}