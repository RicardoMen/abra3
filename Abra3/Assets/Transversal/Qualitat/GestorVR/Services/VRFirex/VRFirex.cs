﻿namespace VRFirex
{
    public enum Color : int
    {
        ROJO,
        AMARILLO,
        VERDE
    }

    public enum Resultado : int
    {
        REPRUEBA,
        APRUEBA
    }

    public enum Escapo: int
    {
        NO,
        SI
    }

    /*
     * Escenas
     * 
     * Las Nuevas Evaluaciones se iran agregando hacia abajo respetando el orden de ingreso.
     */

    public enum Escena : int
    {
        VRFirex = 0,
        PepsicoOficina = 1,
        PepsicoBodega = 2,
        MarruecosBodega = 3,
        CeimBodega = 4,
        CeimOficina = 5,
        FireControlBodega = 6,
        FireControlOficina = 7
    }
    
    [System.Serializable]
    public class Score
    {
        public int tie; // Tiempo en Segundos
        public int fue; // Fuegos Extingidos
        public Escapo esc; // Escapo de la Escena
        public int pun; // Puntaje de la escena
        public int tot; // Puntaje Total Maximo Posible
        public Color col; // Resultado de la Evaluacion

        public override string ToString()
        {
            return string.Format("[Tiempo = {0}, Fuegos = {1}, Escapa = {2}, Puntaje = {3}, Total = {4}, Color = {5}]",tie,fue,esc.ToString("g"),pun,tot,col.ToString("g")) ;
        }
    }
    /// <summary>
    /// Escenas
    /// </summary>
    [System.Serializable]
    public class Escenas
    {
        public Escena esc; // Tipo de escena 
        public Score fac; // Score Facil
        public Score med; // Score Medio
        public Score dif; // Score Dificil
        public int tot; // Puntaje Total
        public Resultado res; // Resultado de la evaluacion

        public override string ToString()
        {
            return string.Format("[VRFirex Escena={0} Facil={1} Medio={2} Dificil={3} Total = {4} Resultado={5}]",esc.ToString("g"),fac.ToString(),med.ToString(),dif.ToString(),tot,res.ToString("g"));
        }
    }
}