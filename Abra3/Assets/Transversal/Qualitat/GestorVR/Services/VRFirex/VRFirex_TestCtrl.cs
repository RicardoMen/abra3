﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRFirex_TestCtrl : MonoBehaviour
{
    public VRFirex.Escenas data;
    public Qualitat.GestorVR.Module.Evaluacion.Ficha ficha;
    // Use this for initialization
    void Start()
    {
        EvaluacionVR.current.ficha.AddParametro(data);
        ficha.AddRUT("18.108.559-2");
        Debug.Log("Existe paramatreo :"+EvaluacionVR.current.ficha.parametro.isValid);
        if (EvaluacionVR.current.ficha.parametro.isValid)
        {
            var dataPre = EvaluacionVR.current.ficha.parametro.GetData<VRFirex.Escenas>();
            Debug.Log("datapre : " + dataPre.ToString());
            Debug.Log(JsonUtility.ToJson( dataPre));
            EvaluacionVR.current.ficha.AddParametro(dataPre);
        }
        ficha.disp_id = 179;// 151 PUBLICO | 119 PEPSICO | 179 Marruecos Emp 20
        ficha.eva_id = 57; // 57 Evaluación de Extintores
        ficha.AddParametro(data);
        //StartCoroutine(ficha.CloseAndSave());
    }

    // Update is called once per frame
    void Update()
    {

    }
}
