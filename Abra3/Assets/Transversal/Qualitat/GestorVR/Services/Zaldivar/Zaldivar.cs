﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System;
namespace Qualitat.GestorVR.Module
{
    /// <summary>
    /// Servicio Zaldivar - Qdata
    /// </summary>
    public static class Zaldivar
    {
        public static ActiveRecords.StatusCode status;

        private static JSONObject trabajador;

        /// <summary>
        /// Prepara el trabajador para ser consultado, actualizando el {status} OK cuando la consulta es exitosa y error cuando falla
        /// </summary>
        /// <param name="rut"></param>
        /// <returns></returns>
        public static IEnumerator FindWorker(string rut)
        {
            UriBuilder uriBuilder = new UriBuilder(App.Host.url)
            {
                Query = string.Format( "fields=rut&expand=params&per-page=1&filter[rut]={0}",rut)
            };
            uriBuilder.Path += "/v1/zaldivar/trabajador";
            UnityWebRequest request = UnityWebRequest.Get(uriBuilder.Uri.ToString());
            App.session.Sign(ref request);
            yield return request.SendWebRequest();
            if(request.isNetworkError||request.isHttpError)
            {
                status = ActiveRecords.StatusCode.ERROR;
                if (App.debug)
                {
                    Debug.LogErrorFormat("[Zaldivar msg = No se puede traer la información del trabajador http = {0} network = {1} msg = {2}]",request.isHttpError,request.isNetworkError,request.downloadHandler.text);
                }
            }
            else
            {
                JSONObject arrayTrabajador = new JSONObject(request.downloadHandler.text);
                if(arrayTrabajador.IsArray&&arrayTrabajador.list.Count==1)
                {
                    trabajador = arrayTrabajador[0];
                }
                status = ActiveRecords.StatusCode.OK;

                if (App.debug)
                {
                    Debug.LogFormat("[Zaldivar msg = Busca la información del trabajador {0} con exito]",rut);
                }
            }
        }


        /// <summary>
        /// Actualiza la información de consulta para un trabajador
        /// </summary>
        /// <returns></returns>
        public static IEnumerator UpdateWorker()
        {
            if(trabajador!=null)
            {
                UriBuilder uriBuilder = new UriBuilder()
                {
                    Scheme = "https",
                    Host = "gestorvr.qualitat.cl",
                    Path = "dev/v1/zaldivar/trabajador",
                    Query = string.Format("fields=rut&expand=params&per-page=1&filter[rut]={0}", trabajador["rut"])
                };
                UnityWebRequest request = UnityWebRequest.Get(uriBuilder.Uri.ToString());
                App.session.Sign(ref request);
                yield return request.SendWebRequest();
                if (request.isNetworkError || request.isHttpError)
                {
                    status = ActiveRecords.StatusCode.ERROR;
                }
                else
                {
                    JSONObject arrayTrabajador = new JSONObject(request.downloadHandler.text);
                    if (arrayTrabajador.IsArray && arrayTrabajador.list.Count == 1)
                    {
                        trabajador = arrayTrabajador[0];
                        Debug.Log(trabajador);
                    }
                    status = ActiveRecords.StatusCode.OK;

                    if (App.debug)
                    {
                        Debug.LogFormat("[Zaldivar msg = Se actualizo la información del trabajador {0} con exito]",trabajador["rut"].str);
                    }
                }
            }
            else
            {
                if(App.debug)
                {
                    Debug.LogWarning("[Zaldivar msg = No se puede actualizar la información del trabajador porque no existe trabajador previo]");
                }
            }
        }

        /// <summary>
        /// Entrega un listado de las evaluaciones pendientes
        /// </summary>
        /// <returns></returns>
        public static List<int> EvaluacionesPendientes()
        {
            List<int> list = new List<int>();
            if(trabajador!=null)
            {
                var pendientes = trabajador["params"]["data"]["zaldivar"]["pendientes"];
                if (pendientes.IsArray)
                {
                    foreach (var item in pendientes.list)
                    {
                        if (item.IsNumber)
                        {
                            list.Add((int)item.n);
                        }
                    }
                }
            }
            return list;
        }

        /// <summary>
        /// Consulta si el trabajador tiene una evaluacion pendientes a traves de una id de evaluacion
        /// </summary>
        /// <param name="eva_id"></param>
        /// <returns></returns>
        public static bool EvaluacionPendiente(int eva_id)
        {
            List<int> list = EvaluacionesPendientes();
            return list.Exists(x => x == eva_id);
        }

        /// <summary>
        /// Consulta si el trabajador tiene evaluaciones pendientes
        /// </summary>
        /// <returns></returns>
        public static bool ExistenEvaluaciones()
        {
            return EvaluacionesPendientes().Count != 0;
        }
    }
}
