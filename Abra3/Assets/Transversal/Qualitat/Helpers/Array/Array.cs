﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace Qualitat.Helpers
{
    public static class Array
    {
        /// <summary>
        /// Baraja un array.
        /// </summary>
        /// <typeparam name="T">Array element type.</typeparam>
        /// <param name="array">Array to shuffle.</param>
        public static void Shuffle<T>(this T[] array)
        {
            var random = new System.Random();
            int n = array.Length;
            for (int i = 0; i < n; i++)
            {
                int r = i + random.Next(n - i);
                T t = array[r];
                array[r] = array[i];
                array[i] = t;
            }
        }

        /// <summary>
        /// Baraja un array.
        /// </summary>
        /// <typeparam name="T">Array element type.</typeparam>
        /// <param name="array">Array to shuffle.</param>
        public static void Shuffle<T>(this List<T> array)
        {
            var random = new System.Random();
            int n = array.Count;
            for (int i = 0; i < n; i++)
            {
                int r = i + random.Next(n - i);
                T t = array[r];
                array[r] = array[i];
                array[i] = t;
            }
        }

        /// <summary>
        /// Compara dos cadenas
        /// </summary>
        /// <param name="l1"></param>
        /// <param name="l2"></param>
        /// <returns></returns>
        public static bool Match(this List<string> l1, List<string> l2)
        {
            if (l1.Count != l2.Count)
            {
                return false;
            }
            for (int i = 0; i < l1.Count; i++)
            {
                if (l1[i].CompareTo(l2[i])!=0)
                {
                    return false;
                }
            }
            return true;
        }

    }
}