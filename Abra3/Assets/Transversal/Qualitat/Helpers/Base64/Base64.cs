﻿using System;
using System.Text;

namespace Qualitat.Helpers
{

    public static class Base64
    {
        /// <summary>
        /// Retorna una cadena de texto cifrada en norma base64
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string EncodeBase64(this string text)
        {
            var plainTextBytes = Encoding.UTF8.GetBytes(text);
            return Convert.ToBase64String(plainTextBytes);
        }

        /// <summary>
        /// Decifra una cadena de texto cifrada en base64
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string DecodeBase64(this string text)
        {
            var base64EncodedBytes = Convert.FromBase64String(text);
            return Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
