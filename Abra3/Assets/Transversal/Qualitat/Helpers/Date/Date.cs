﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Qualitat.Helpers
{

    public static class Date
    {
        public static class TimeUnix
        {
            private readonly static DateTime origin = new DateTime(1970, 1, 1);

            /// <summary>
            /// Segundos universar en timeunix
            /// </summary>
            public static int Now
            {
                get
                {
                    return Convert.ToInt32((DateTime.UtcNow.Subtract(origin)).TotalSeconds);
                }
            }

            /// <summary>
            /// Transforma una dato de tiempo en timeunix
            /// </summary>
            /// <param name="dateTime"></param>
            /// <returns></returns>
            public static int Parse(DateTime dateTime)
            {
                return Convert.ToInt32((dateTime.ToUniversalTime().Subtract(origin)).TotalSeconds);
            }
        }
    }
}
