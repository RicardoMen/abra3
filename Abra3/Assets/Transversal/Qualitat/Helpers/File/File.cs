﻿using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Qualitat.Helpers
{
    /// <summary>
    /// Modulo para manejar archivos 
    /// <para>Dependencias  Json,Base64</para>
    /// </summary>
    public static class File
    {
        private static readonly bool verbose = true; //Atributo que debuelve logs de carga de archivos false si no se requiere que se muestren 
        private static readonly bool shared = true; //Atributo cuando las aplicaciones comparten la carpeta de archivos falso para cuando ocupe la carpeta asignada en la aplicación
        private static readonly string project = "Qualitatcorp"; //Nombre del proyecto y la carpeta base a compartir

        /// <summary>
        /// Une una lista de string con directorio separador
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static string MakeDir(params string[] args)
        {
            string separator = Path.DirectorySeparatorChar.ToString();
            return string.Join(separator, args).Replace("/",separator).Replace("\\",separator);
        }

        /// <summary>
        /// Retorna el path base de cada plataforma
        /// </summary>
        /// <returns></returns>
        public static string GetBasePath()
        {
            if (shared)
            {
                switch (Application.platform)
                {
                    case RuntimePlatform.WindowsPlayer:
                    case RuntimePlatform.WindowsEditor:
                        return System.Environment.GetFolderPath(System.Environment.SpecialFolder.MyDocuments);
                    case RuntimePlatform.Android:
                       return Application.persistentDataPath;
                    default:
                        return Application.persistentDataPath;//antes /emulated/0/Qualitat
                }
            }
            else
            {
                return Application.dataPath;
            }
        }

        /// <summary>
        /// Construye una direccion a partir de la raiz de escritura
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string GetFullPath(params string[] path)
        {
            return MakeDir(GetBasePath(), project, MakeDir(path));
        }

        /// <summary>
        /// Retorna la direccion del directorio de un archivo
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string GetFileName(params string[] path)
        {
            return Path.GetFileName(GetFullPath(path));
        }

        /// <summary>
        /// Retorna si existe un archivo en el directorio base
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool ExistsFile(params string[] path)
        {
            return System.IO.File.Exists(GetFullPath(path));
        }

        /// <summary>
        /// Retorna si existe el directorio
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool ExistsDirectory(params string[] path)
        {
            return Directory.Exists(GetFullPath(path));
        }

        /// <summary>
        /// Crea si no existe un directorio
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static DirectoryInfo CreateIfNotExistsDirectory(params string[] path)
        {
            if (!Directory.Exists(GetFullPath(path)))
            {
                return Directory.CreateDirectory(GetFullPath(path));
            }
            return new DirectoryInfo(GetFullPath(path));
        }

        /// <summary>
        /// Almacena una cadena de texto en un path
        /// </summary>
        /// <param name="text"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static FileInfo Save(string text, params string[] path)
        {
            System.IO.File.WriteAllText(GetFullPath(path), text);
            return new FileInfo(GetFullPath(path));
        }

        /// <summary>
        /// Añade una cadena de texto en un archivo
        /// </summary>
        /// <param name="text"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static FileInfo AddLine(string text, params string[] path)
        {
            text += System.Environment.NewLine;
            
            if (ExistsFile(path))
            {
                System.IO.File.AppendAllText(GetFullPath(path), text);
            }
            else
            {
                System.IO.File.WriteAllText(GetFullPath(path), text);
            }
            return new FileInfo(GetFullPath(path));
        }

        /// <summary>
        /// Guarda un objeto en binatio en el path de destino
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static FileInfo Save(object obj, params string[] path)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = System.IO.File.Open(GetFullPath(path), FileMode.OpenOrCreate);
            bf.Serialize(file, obj);
            file.Close();
            if (verbose)
            {
                Debug.LogFormat("[{0}: action={1} path={2} ]", obj.GetType().Name, "saveBinary", GetFullPath(path));
            }
            return new FileInfo(GetFullPath(path));
        }
        
        /// <summary>
        /// Guarda una lista de objetos en el path de destino
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static FileInfo Save<T>(List<T> list, params string[] path)
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = System.IO.File.Open(GetFullPath(path), FileMode.OpenOrCreate);
            bf.Serialize(file, list);
            file.Close();
            if (verbose)
            {
                Debug.LogFormat("[{0}: action={1} path={2} ]", list.GetType().Name, "saveBinary", GetFullPath(path));
            }
            return new FileInfo(GetFullPath(path));
        }

        /// <summary>
        /// Almacena un arhchivo byte
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static FileInfo Save(byte[] bytes, params string[] path)
        {
            System.IO.File.WriteAllBytes(GetFullPath(path), bytes);
            if (verbose)
            {
                Debug.LogFormat("[{0}: action={1} path={2} ]", bytes.GetType().Name, "saveBinary", GetFullPath(path));
            }
            return new FileInfo(GetFullPath(path));
        }

        /// <summary>
        /// Guarda un archivo con un nombre unico
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="path"></param>
        /// <returns></returns>
        public static FileInfo SaveRandomFile(byte[] bytes, params string[] path)
        {
            string randomName = Path.GetRandomFileName();
            if (verbose)
            {
                Debug.LogFormat("[{0}: action={1} path={2} ]", bytes.GetType().Name, "saveRandomBinary", Path.Combine(GetFullPath(path), randomName));
            }
            return Save(bytes,MakeDir(MakeDir(path),randomName));
        }

        /// <summary>
        /// Carga un objeto en binario
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public static T Load<T>(params string[] path)
        {
            T t = default(T);
            if (ExistsFile(path))
            {
                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = System.IO.File.Open(GetFullPath(path), FileMode.Open);
                t = (T)bf.Deserialize(file);
                file.Close();
            }
            return t;
        }

        /// <summary>
        /// Carga un texto desde un archivo
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static string LoadText(params string[] path)
        {
            return System.IO.File.ReadAllText(GetFullPath(path));
        }

        /// <summary>
        /// Carga un archivo binario ej. Imagenes musica etc...
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static byte[] LoadBinary(params string[] path)
        {
            return System.IO.File.ReadAllBytes(GetFullPath(path));
        }

        /// <summary>
        /// Guarda un objeto en json
        /// </summary>
        /// <param name="objeto"></param>
        /// <param name="path"></param>
        public static void SaveJson(object objeto, params string[] path)
        {
            System.IO.File.WriteAllText(GetFullPath(path), JsonUtility.ToJson(objeto, true));
            if (verbose)
            {
                Debug.LogFormat("[File Object={0} action={1} path={2} ]", objeto.GetType().Name, "saveJson", GetFullPath(path));
            }
        }

        /// <summary>
        /// Almacena una lista de clases en un archivo
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <param name="path"></param>
        public static void SaveJson<T>(List<T> list, params string[] path)
        {
            System.IO.File.WriteAllText(GetFullPath(path), Json.Parse(list));
        }

        /// <summary>
        /// Carga un Objeto en Json
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public static T LoadJson<T>(params string[] path)
        {
            return JsonUtility.FromJson<T>(System.IO.File.ReadAllText(GetFullPath(path)));
        }

        /// <summary>
        /// Actualiza una objeto desde un json
        /// </summary>
        /// <param name="objeto"></param>
        /// <param name="path"></param>
        public static void LoadJsonOverwrite(ref object objeto, params string[] path)
        {
            JsonUtility.FromJsonOverwrite(System.IO.File.ReadAllText(GetFullPath(path)), objeto);
        }

        /// <summary>
        /// Carga una lista de objetos
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="path"></param>
        /// <returns></returns>
        public static List<T> LoadArrayJson<T>(params string[] path)
        {
            return Json.ToArray<T>(System.IO.File.ReadAllText(GetFullPath(path)));
        }
        
        /// <summary>
        /// Elimina un archivo
        /// </summary>
        /// <param name="path"></param>
        public static void Remove(params string[] path)
        {
            System.IO.File.Delete(GetFullPath(path));
        }
    }
}
