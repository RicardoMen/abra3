﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class FileCtrl_Test : MonoBehaviour {
    public Text UItext;

    private void Start()
    {
        UItext.text = Qualitat.Helpers.File.GetFullPath("hola.json")+"\n";
        UItext.text += Directory.Exists( Qualitat.Helpers.File.GetFullPath("hola"))+"\n";
        UItext.text += Qualitat.Helpers.File.GetFullPath("Session.json") + "\n";
        UItext.text += File.Exists(Qualitat.Helpers.File.GetFullPath("Session.json")) + "\n";
        UItext.text += Qualitat.Helpers.File.GetFullPath("GestorVR","database.json") + "\n";
        UItext.text += Qualitat.Helpers.File.GetFullPath("debug_device.json") + "\n";
        UItext.text += File.Exists(Qualitat.Helpers.File.GetFullPath("debug_device.json")) + "\n";

        Qualitat.Helpers.File.AddLine("Chao2asdsadasd", "hola.txt");
        Qualitat.Helpers.File.CreateIfNotExistsDirectory("Perro");
        Qualitat.Helpers.File.Save("hola", "Perro", "hola.txt");

        Debug.Log(Qualitat.Helpers.File.LoadText("hola.txt"));

        Debug.Log(Qualitat.Helpers.Rut.Validate("11.111.111-1"));
        Debug.Log(Qualitat.Helpers.Rut.Format("181085592"));
        Debug.Log(Qualitat.Helpers.Rut.Validate("18.108.559","2"));
        Debug.Log(Qualitat.Helpers.Rut.CalcDigit(18108559));
    }
}

