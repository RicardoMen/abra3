﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;

namespace Qualitat.Helpers.Forms
{
    public static class Validation
    {
        /// <summary>
        /// Retorna si un correo es valido
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static bool Email(string email)
        {
            Regex regex = new Regex("\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*");
            if (regex.IsMatch(email) && regex.Replace(email, string.Empty).Length == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
