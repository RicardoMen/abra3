﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Qualitat.Helpers
{
    public static class Json
    {
        /// <summary>
        /// Devuelve una lista de objetos desde un texto json wrapper
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public static List<T> ToArray<T>(string json)
        {
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
            return wrapper.content;
        }

        /// <summary>
        /// Devuelve una lista de objetos desde un texto array json
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public static List<T> ToArrayList<T>(string json)
        {
            string newJson = "{ \"content\": " + json + "}";
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(newJson);
            return wrapper.content;
        }

        /// <summary>
        /// Transforma una lista de objetos a json array wrapper
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <returns></returns>
        public static string Parse<T>(List<T> array)
        {
            Wrapper<T> wrapper = new Wrapper<T>(array);
            return JsonUtility.ToJson(wrapper);
        }

        [System.Serializable]
        private class Wrapper<T>
        {
            public List<T> content;

            public Wrapper(List<T> content)
            {
                this.content = content;
            }
        }
    }
}