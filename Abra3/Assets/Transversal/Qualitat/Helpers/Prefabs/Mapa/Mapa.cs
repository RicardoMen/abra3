﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mapa : MonoBehaviour
{
    [Header("Configuracion General")]
    public Transform playerSeguido;
    public Transform terreno;
    public List<Transform> objetosMapeados;

    [Header("Configuracion iconos")]
    public Image spriteMapa;
    public Image iconoPlayer;
    public Sprite iconoObjeto;
    public List<Image> listaIconos;
    [Range(0.1f, 1f)]
    public float propIconosObjeto = 1f;
    Vector3 escalaInicial;

    Vector3 tamañoTerreno;
    Vector3 tamañoImagen;
    Vector3 deltaZeroPlayer;
    Vector3 deltaZeroObjetos;

    void Start()
    {
        tamañoTerreno = GetMeshSize(terreno);
        tamañoImagen = GetMeshSize(spriteMapa.transform);

        listaIconos = new List<Image>();
        escalaInicial = new Vector3(0.1f, 0.1f, 0.1f);
        propIconosObjetoAnterior = propIconosObjeto;

        if (objetosMapeados.Count > 0)
        {
            for (int i = 0; i < objetosMapeados.Count; i++)
            {
                GameObject g;
                g = Instantiate(new GameObject(), spriteMapa.transform);
                g.name = "iconoObjeto" + (i < 10 ? "0" + i.ToString() : i.ToString());
                g.transform.localScale = escalaInicial * propIconosObjeto;

                g.AddComponent<Image>();
                g.GetComponent<Image>().sprite = iconoObjeto;

                listaIconos.Add(g.GetComponent<Image>());
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        PosicionaIconoPlayer();
        PosicionaIconosObjetos();
    }

    public void PosicionaIconoPlayer()
    {
        deltaZeroPlayer = playerSeguido.position - terreno.position;

        iconoPlayer.transform.localPosition = new Vector3(deltaZeroPlayer.x * tamañoImagen.x / tamañoTerreno.x, deltaZeroPlayer.z * tamañoImagen.y / tamañoTerreno.z, -1f);
        iconoPlayer.transform.localEulerAngles = new Vector3(0f, 0f, -playerSeguido.eulerAngles.y);
    }

    float propIconosObjetoAnterior;
    public void PosicionaIconosObjetos()
    {
        if (objetosMapeados.Count > 0)
        {
            for (int i = 0; i < listaIconos.Count; i++)
            {
                listaIconos[i].transform.localPosition = new Vector3((objetosMapeados[i].position.x - terreno.position.x) * tamañoImagen.x / tamañoTerreno.x, (objetosMapeados[i].position.z - terreno.position.z) * tamañoImagen.y / tamañoTerreno.z, -1f);
                if (propIconosObjeto != propIconosObjetoAnterior)
                {
                    listaIconos[i].transform.localScale = escalaInicial * propIconosObjeto;
                }
            }
            propIconosObjetoAnterior = propIconosObjeto;
        }
    }

    Vector3 GetMeshSize(Transform t)
    {
        Vector3 tamañoTransform = new Vector3(0f, 0f, 0f);

        if (t.GetComponent<MeshFilter>() != null)
            tamañoTransform = new Vector3(t.GetComponent<MeshFilter>().mesh.bounds.size.x * t.localScale.x, t.GetComponent<MeshFilter>().mesh.bounds.size.y * t.localScale.y, t.GetComponent<MeshFilter>().mesh.bounds.size.z * t.localScale.z); ;

        if (t.GetComponent<Image>() != null)
            tamañoTransform = new Vector3(t.GetComponent<RectTransform>().rect.width, t.GetComponent<RectTransform>().rect.height, 0f);

        Debug.Log("TAMAÑO: " + tamañoTransform);

        return tamañoTransform;
    }

    List<Vector2> GetLimitsVertex(Transform t, string ejeExcepcion)
    {
        List<Vector2> limites = new List<Vector2>();

        switch (ejeExcepcion)
        {
            case "x":
                limites.Add(new Vector2(t.localPosition.y - (GetMeshSize(t).y / 2), t.localPosition.z - (GetMeshSize(t).z / 2)));
                limites.Add(new Vector2(t.localPosition.y - (GetMeshSize(t).y / 2), t.localPosition.z + (GetMeshSize(t).z / 2)));
                limites.Add(new Vector2(t.localPosition.y + (GetMeshSize(t).y / 2), t.localPosition.z + (GetMeshSize(t).z / 2)));
                limites.Add(new Vector2(t.localPosition.y + (GetMeshSize(t).y / 2), t.localPosition.z - (GetMeshSize(t).z / 2)));
                break;
            case "y":
                limites.Add(new Vector2(t.localPosition.x - (GetMeshSize(t).x / 2), t.localPosition.z - (GetMeshSize(t).z / 2)));
                limites.Add(new Vector2(t.localPosition.x - (GetMeshSize(t).x / 2), t.localPosition.z + (GetMeshSize(t).z / 2)));
                limites.Add(new Vector2(t.localPosition.x + (GetMeshSize(t).x / 2), t.localPosition.z + (GetMeshSize(t).z / 2)));
                limites.Add(new Vector2(t.localPosition.x + (GetMeshSize(t).x / 2), t.localPosition.z - (GetMeshSize(t).z / 2)));
                break;
            case "z":
                limites.Add(new Vector2(t.localPosition.x - (GetMeshSize(t).x / 2), t.localPosition.y - (GetMeshSize(t).y / 2)));
                limites.Add(new Vector2(t.localPosition.x - (GetMeshSize(t).x / 2), t.localPosition.y + (GetMeshSize(t).y / 2)));
                limites.Add(new Vector2(t.localPosition.x + (GetMeshSize(t).x / 2), t.localPosition.y + (GetMeshSize(t).y / 2)));
                limites.Add(new Vector2(t.localPosition.x + (GetMeshSize(t).x / 2), t.localPosition.y - (GetMeshSize(t).y / 2)));
                break;
        }

        for (int i = 0; i < limites.Count; i++)
            Debug.Log("LIMITES[" + i + "] = " + limites[i]);

        return limites;
    }


}
