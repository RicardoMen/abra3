﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

namespace Qualitat.Helpers.Prefab
{
    [RequireComponent(typeof(Text))]
    public class VersionBuildController : MonoBehaviour
    {
        /// <summary>
        /// Rutina de Asignación de la version
        /// </summary>
        /// <returns></returns>
        public IEnumerator Start()
        {
            Text textVersion = GetComponent<Text>();
            textVersion.text = getVersion();
            StartCoroutine(GestorVR.App.AutoLogin());
            yield return new WaitUntil(GestorVR.App.session.isValid);
            yield return ResolveBuildApplication();
            textVersion.text = getVersion();
        }

        /// <summary>
        /// Endpoint de cada recurso
        /// </summary>
        public static readonly string resourceApp = GestorVR.App.Host.url.ToString() + "/v1/app";
        public static readonly string resourceBuild = GestorVR.App.Host.url.ToString() + "/v1/appbuild";

        /// <summary>
        /// Es la llave con la cual se almacena en el player preference
        /// </summary>
        private string keyName {
            get
            {
                return "appBuildVersion" + Application.buildGUID;
            }
        }

        /// <summary>
        /// Pregunta si tiene o no la version
        /// </summary>
        /// <returns></returns>
        public bool hasVersion()
        {
            return PlayerPrefs.HasKey(keyName);
        }

        /// <summary>
        /// Cadena de texto que define como se mostrara la version
        /// </summary>
        /// <returns></returns>
        public string getVersion()
        {
            if (hasVersion())
            {
                JSONObject build = new JSONObject(PlayerPrefs.GetString(keyName));
                return Application.productName + " " + Application.version + "." + build["version"].n + " " + build["environment"].str;
            }
            else
            {
                if(Application.isEditor)
                {
                    return Application.productName + " " + Application.version + "EDITOR";
                }
                else
                {
                    return Application.productName + " " + Application.version + "*";
                }
            }
        }
        
        /// <summary>
        /// Servicio que resuelve la version de la compilación
        /// </summary>
        /// <returns></returns>
        public IEnumerator ResolveBuildApplication()
        {
            if(!Application.isEditor)
            {
                if (hasVersion())
                {
                    /**
                     * Si tiene una identidad se debe traer sin hacer nada mas
                     */
                    JSONObject build = new JSONObject(PlayerPrefs.GetString(keyName));
                    UnityWebRequest request = UnityWebRequest.Get(resourceBuild + "/" + build["id"].n);
                    GestorVR.App.session.Sign(ref request);
                    yield return request.SendWebRequest();
                    if (request.isNetworkError)
                    {
                        Debug.LogError("[App No existe conexion");
                    }
                    else if (request.isHttpError)
                    {
                        Debug.LogError("[App existe algun problema : " + request.downloadHandler.text);
                    }
                    else
                    {
                        JSONObject responseBuild = new JSONObject(request.downloadHandler.text);

                        if (build.Print() != responseBuild.Print())
                        {
                            PlayerPrefs.SetString(keyName, responseBuild.Print());
                            PlayerPrefs.Save();
                            Debug.Log("[App ha cambiado el estado de la aplicación" + getVersion());
                        }
                        else
                        {
                            Debug.Log("[App sin novedad" + getVersion());
                        }
                    }
                }
                else
                {
                    /**
                     * En este caso hay que resolver la identidad
                     */
                    JSONObject jsonBuild = new JSONObject();
                    jsonBuild.AddField("guid", Application.buildGUID);
                    jsonBuild.AddField("deviceType", SystemInfo.deviceType.ToString("g"));
                    jsonBuild.AddField("environment", "DEVELOPMENT");

                    JSONObject jsonApp = new JSONObject();
                    jsonApp.AddField("name", Application.productName);
                    jsonApp.AddField("version", Application.version);
                    jsonApp.AddField("platform", Application.platform.ToString("g"));
                    jsonApp.AddField("company", Application.companyName);
                    jsonApp.AddField("unityVersion", Application.unityVersion);

                    JSONObject jsonRequest = new JSONObject();
                    jsonRequest.AddField("build", jsonBuild);
                    jsonRequest.AddField("app", jsonApp);

                    UnityWebRequest request = Http.Post(resourceBuild + "/identity", jsonRequest.Print());
                    GestorVR.App.session.Sign(ref request);
                    yield return request.SendWebRequest();
                    if (request.isNetworkError)
                    {
                        Debug.LogError("[App No existe conexion");
                    }
                    else if (request.isHttpError)
                    {
                        Debug.LogError("[App existe algun problema : " + request.downloadHandler.text);
                    }
                    else
                    {
                        JSONObject responseBuild = new JSONObject(request.downloadHandler.text);
                        PlayerPrefs.SetString(keyName, responseBuild.Print());
                        PlayerPrefs.Save();
                        Debug.Log("[App ha asignado el estado de la aplicación" + getVersion());
                    }
                }
            }
        }
    }
}

