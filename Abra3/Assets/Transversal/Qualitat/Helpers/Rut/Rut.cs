﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text.RegularExpressions;

namespace Qualitat.Helpers
{
    public static class Rut
    {
        /// <summary>
        /// Metodo de validación de rut con digito verificador separado por un guión
        /// dentro de la cadena
        /// </summary>
        /// <param name="rut">string</param>
        /// <returns>booleano</returns>
        public static bool Validate(string rut)
        {
            if (rut.Length >= 9)
            {
                rut = rut.Replace(".", "").ToUpper();
                string dv = rut.Substring(rut.Length - 1, 1);
                Regex expresion = new Regex("^([0-9]+-[0-9K])$");
                //valida contenido
                if (!expresion.IsMatch(rut))
                {
                    return false;
                }
                else
                {
                    //Validate codigo verificador
                    string[] rutTemp = rut.Split('-');
                    if (dv != CalcDigit(int.Parse(rutTemp[0])))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            else
            {
                return false;
            }

        }

        /// <summary>
        /// Método que valida el rut con el digito verificador
        /// por separado
        /// </summary>
        /// <param name="rut">integer</param>
        /// <param name="dv">char</param>
        /// <returns>booleano</returns>
        public static bool Validate(string rut, string dv)
        {
            return Validate(rut + "-" + dv);
        }

        /// <summary>
        /// Limpia una cadena de rut
        /// </summary>
        /// <param name="rut"></param>
        /// <returns></returns>
        public static string Clean(string rut)
        {
            return rut.Replace(".", "").Replace("-", "").ToUpper();
        }

        /// <summary>
        /// Método que calcula el digito verificador a partir
        /// de la mantisa del rut
        /// </summary>
        /// <param name="rut"></param>
        /// <returns></returns>
        public static string CalcDigit(int rut)
        {
            int suma = 0;
            int multiplicador = 1;
            while (rut != 0)
            {
                multiplicador++;
                if (multiplicador == 8)
                {
                    multiplicador = 2;
                }
                suma += (rut % 10) * multiplicador;
                rut = rut / 10;
            }
            suma = 11 - (suma % 11);
            if (suma == 11)
            {
                return "0";
            }
            else
            {
                if (suma == 10)
                {
                    return "K";
                }
                else
                {
                    return suma.ToString();
                }
            }
        }
        
        /// <summary>
        /// Da Formato de rut a un texto
        /// </summary>
        /// <param name="rut"></param>
        /// <returns></returns>
        public static string Format(string rut)
        {
            int cont = 0;
            string format;
            if (rut.Length == 0)
            {
                return "";
            }
            else
            {
                rut = Clean(rut);
                format = "-" + rut.Substring(rut.Length - 1);
                for (int i = rut.Length - 2; i >= 0; i--)
                {
                    format = rut.Substring(i, 1) + format;
                    cont++;
                    if (cont == 3 && i != 0)
                    {
                        format = "." + format;
                        cont = 0;
                    }
                }
                return format;
            }
        }
    }
}