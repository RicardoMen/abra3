﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

namespace Qualitat.Helpers
{
    public static class Url
    {
        /// <summary>
        /// Une cadenas de texto para crear una url
        /// </summary>
        /// <param name="src"></param>
        /// <returns></returns>
        public static string Make(params string[] src)
        {
            return string.Join("/", src);
        }

        /// <summary>
        /// Codifica un string a binario para hacer entregas de paquetes por post en un body
        /// </summary>
        /// <param name="str">cadeja de texto por ejemplo un json</param>
        /// <returns></returns>
        public static byte[] EncodeBody(string str)
        {
            return Encoding.UTF8.GetBytes(str);
        }
    }
}