﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckAuth : MonoBehaviour
{
    //public AudioQuestion _audioQuestion;
    public Text _text;
    public Button _button;
    private void Start()
    {
        _text.text = "";
    }
    public void CheckWhoAuthUse()
    {
        if (FindObjectOfType<ControllerTokenUser>() != null)
        {
            //if (_audioQuestion._initialChecker)
            {
                StartCoroutine(StartLevel());
            }
        }
        else
        {
            GetComponent<PopupOpener>().OpenPopup();
        }
    }
    IEnumerator StartLevel()
    {
        yield return new WaitForSeconds(1f);
        //_audioQuestion.PlayInitialAudio(_audioQuestion._audioStage[0].PhraseAsset[0]);
        //yield return new WaitWhile(() => _audioQuestion._subtitleManager.TextSynchronizer.Source.isPlaying);
        GetComponent<LoaderManager>().Load();
    }
}