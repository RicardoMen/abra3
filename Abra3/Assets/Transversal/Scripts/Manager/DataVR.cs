﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataVR : MonoBehaviour
{
    public JSONObject infoJson = new JSONObject(JSONObject.Type.OBJECT);
    public JSONObject paramsInscripcionJson = new JSONObject(JSONObject.Type.OBJECT);
    public JSONObject AppJson()
    {
        //parametros de app
        JSONObject appJson = new JSONObject(JSONObject.Type.OBJECT);
        appJson.AddField("platform", Application.platform.ToString("g"));
        appJson.AddField("unityVersion", Application.unityVersion);
        appJson.AddField("build", Application.buildGUID);
        appJson.AddField("companyName", Application.companyName);
        appJson.AddField("version", Application.version);
        appJson.AddField("productName", Application.productName);

        return appJson;
    }
    public JSONObject DeviceJson()
    {
        //parametros de device
        JSONObject deviceJson = new JSONObject(JSONObject.Type.OBJECT);
        deviceJson.AddField("name", SystemInfo.deviceName);
        deviceJson.AddField("type", SystemInfo.deviceType.ToString("g"));
        deviceJson.AddField("model", SystemInfo.deviceModel);
        deviceJson.AddField("uid", SystemInfo.deviceUniqueIdentifier);
        return deviceJson;
    }
    public IEnumerator SendDataToManager()
    {
        print("Using Loging");
        GestorVR_WebGL.current.nuevaEvaluacion(ControllerLoginUser.Instance.inscripcion.content.ficha.trabajador.rut);
        GestorVR_WebGL.current.Evaluacion.trabajador.mail = ControllerLoginUser.Instance.inscripcion.content.ficha.trabajador.mail;
        GestorVR_WebGL.current.Evaluacion.trabajador.nombre = ControllerLoginUser.Instance.inscripcion.content.ficha.trabajador.nombre;
        GestorVR_WebGL.current.Evaluacion.trabajador.paterno = ControllerLoginUser.Instance.inscripcion.content.ficha.trabajador.paterno;
        GestorVR_WebGL.current.Evaluacion.trabajador.materno = ControllerLoginUser.Instance.inscripcion.content.ficha.trabajador.materno;
        GestorVR_WebGL.current.disp_id = int.Parse(ControllerLoginUser.Instance.inscripcion.content.ficha.disId);
        GestorVR_WebGL.current.eva_id = FindObjectOfType<GestorVR_WebGL>().eva_id_Request;
        infoJson.AddField("app", AppJson());
        infoJson.AddField("device", DeviceJson());
        JSONObject j = new JSONObject(JSONObject.Type.OBJECT);
        foreach (var item in ControllerLoginUser.Instance.inscripcion.content.ficha.parametros)
        {
            j.AddField(item.Key, item.Value);
        }
        paramsInscripcionJson.AddField("data", j);
        GestorVR_WebGL.current.AddParametros(paramsInscripcionJson);
        GestorVR_WebGL.current.AddParametros(infoJson);
        foreach (var item in ScriptableDataManager.Instance.saveData.Answers)
        {
            GestorVR_WebGL.current.Evaluacion.addRespuesta(item.Key, item.Value);
            Debug.Log(item);
        }
        yield return new WaitForSeconds(1f);
        GestorVR_WebGL.current.Evaluacion.Cerrar();
        GestorVR_WebGL.current.SyncFicha();
        yield return new WaitForSeconds(2f);
        while (System.String.IsNullOrEmpty(FichaResultado.current.fic_id))
        {
            GestorVR_WebGL.current.SyncFicha();
            yield return new WaitForSeconds(4f);
        }
    }
}