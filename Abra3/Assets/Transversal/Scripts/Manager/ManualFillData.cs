﻿using System.Collections;
using System.Collections.Generic;
using Qualitat.Transversal;
using UnityEngine;
using UnityEngine.UI;
public class ManualFillData : MonoBehaviour
{
    public InputField user, pass;
    public Button _continue;
    public Text message;
    public GameObject loaderIcon;
    [Header("Setting for NO DATA")]
    public bool _withoutData;
    private void Start()
    {
        if (PlayerPrefs.HasKey("_withoutData"))
        {
            _withoutData = true;
            Debug.Log(PlayerPrefs.GetString("_withoutData"));
        }
        else
        {
            _withoutData = false;
            Debug.Log("Si Subira Datos a GestorVR");
        }

        
    }
    public void ManualFillUserQuest()
    {
        StartCoroutine(FillCorountineOculus());
    }
    public void ManualFillUserWebGL()
    {
        _continue.interactable = false;
        StartCoroutine(FillCorountineWeb());
    }
    IEnumerator FillCorountineWeb()
    {
        message.text = "";
        if (!_withoutData)
        {
            yield return (ControllerLoginUser.Instance.Login(user.text, pass.text));
            yield return new WaitForSeconds(1f);
            switch (GetData.responseCode)
            {
                case 200:
                    FindObjectOfType<LoaderManager>().Load();
                    GetComponent<Popup>().Close();
                    break;
                case 403:
                    message.text = ("Alert");//"El usuario y/o la contraseña no coinciden. \n O ya no posee más inscripciones vigentes.";
                    _continue.interactable = true;
                    break;
                case 404:
                    message.text = ("Alert");
                    _continue.interactable = true;
                    break;
                default:
                    message.text = ("Alert");
                    _continue.interactable = true;
                    break;
            }
        }
        else
        {
            yield return new WaitForSeconds(1f);
            //var tempAudio = FindObjectOfType<AudioQuestion>();
            //tempAudio.PlayInitialAudio(tempAudio._audioStage[0].PhraseAsset[0]);
            //yield return new WaitWhile(() => tempAudio._subtitleManager.TextSynchronizer.Source.isPlaying);
            
            FindObjectOfType<LoaderManager>().Load();
            GetComponent<Popup>().Close();
        }
    }
    IEnumerator FillCorountineOculus()
    {
        message.text = "";
        loaderIcon.SetActive(true);
        if (!_withoutData)
        {
            yield return (ControllerLoginUser.Instance.Login(user.text, pass.text));
            yield return new WaitForSeconds(2f);
            switch (GetData.responseCode)
            {
                case 200:
                    FindObjectOfType<Loader>().StartLevel();
                    break;
                case 403:
                    loaderIcon.SetActive(false);
                    message.text = ("Alert");
                    break;
                case 404:
                    message.text = ("Alert");
                    loaderIcon.SetActive(false);
                    break;
            }
        }
        else
        {
            yield return new WaitForSeconds(1f);
            FindObjectOfType<Loader>().StartLevel();
        }
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.S))
            ManualFillUserQuest();
    }
}