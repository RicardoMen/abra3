﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Question", menuName = "Questions/New Question")]
public class SOQuestion : ScriptableObject
{
    [TextArea]
    public string question;
    [TextArea]
    public string[] answers;
    public int _questionID;
    public string[] _answersID;
}