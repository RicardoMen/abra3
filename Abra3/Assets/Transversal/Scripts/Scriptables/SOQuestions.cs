﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
    [CreateAssetMenu(fileName = "New Module", menuName = "Questions/New Module")]
    public class SOQuestions : ScriptableObject
    {
        public SOQuestion[] questions;
    }
