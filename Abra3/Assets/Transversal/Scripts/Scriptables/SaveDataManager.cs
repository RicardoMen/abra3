﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Data", menuName = "Data/New Data")]
public class SaveDataManager : ScriptableObject
{
    public Dictionary<int, string> Answers = new Dictionary<int, string>();
}
