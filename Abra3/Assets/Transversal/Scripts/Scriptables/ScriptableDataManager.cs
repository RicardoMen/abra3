﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptableDataManager : MonoBehaviour
{
    public static ScriptableDataManager Instance;
    public SaveDataManager saveData;
    void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(this);
    }
}
