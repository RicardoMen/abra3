﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VersionManager : MonoBehaviour
{
    public string version;
    void OnGUI()
    {
        GUI.Label(new Rect(10, 10, 100, 20), version);
    }
}
