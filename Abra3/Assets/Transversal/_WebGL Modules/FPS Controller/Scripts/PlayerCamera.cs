﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCamera : MonoBehaviour
{

    [Header("Input Settings")]
    public string MouseXInput;
    public string MouseYInput;
    [Header("Common Camera Settings")]
    public float mouseSensitivity;
    [Header("FPS Camera Settings")]
    public Vector2 FPS_MinMaxAngles;
    public bool isCanvasOpen;
    public float zoom = 20, normal = 60, smoothTransition = 5;
    public bool isTutorial;
    Camera mainCamera;
    Transform FPSController;
    float xClamp;
    private void Awake()
    {
        xClamp = 0;
        FPSController = transform.parent;
    }
    void Start()
    {
        mainCamera = GetComponent<Camera>();
    }
    void Update()
    {
        if (!isCanvasOpen)
        {

            CameraZoom();
        }
    }
    private void FixedUpdate()
    {
        if (!isCanvasOpen)
        {
            RotateCamera();
        }
    }
    public void SetIE(bool state)
    {
        isCanvasOpen = state;
    }
    void CameraZoom()
    {
        if (Input.GetMouseButton(1) && !isTutorial)
        {
            mainCamera.fieldOfView = Mathf.Lerp(mainCamera.fieldOfView, zoom, Time.deltaTime * smoothTransition);
        }
        else
        {
            mainCamera.fieldOfView = Mathf.Lerp(mainCamera.fieldOfView, normal, Time.deltaTime * smoothTransition);
        }
    }
    void RotateCamera()
    {
        float mouseX = Input.GetAxis(MouseXInput) * (mouseSensitivity * Time.deltaTime);
        float mouseY = Input.GetAxis(MouseYInput) * (mouseSensitivity * Time.deltaTime);
        Vector3 eulerRotation = transform.eulerAngles;

        xClamp += mouseY;
        xClamp = Mathf.Clamp(xClamp, FPS_MinMaxAngles.x, FPS_MinMaxAngles.y);
        eulerRotation.x = -xClamp;
        transform.eulerAngles = eulerRotation;
        FPSController.Rotate(Vector3.up * mouseX);
    }
}