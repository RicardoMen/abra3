﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum PlayerStates
{
    Idle,
    Walking
}

public class PlayerMovement : MonoBehaviour
{
    public PlayerStates playerStates;

    [Header("Inputs")]
    public string HorizontalInput = "Horizontal";
    public string VerticalInput = "Vertical";

    [Header("Player Motor")]
    [Range(1f, 15f)]
    public float walkSpeed;

    [Header("Animator and Parameters")]
    public Animator CharacterAnimator;
    public float HorzAnimation;
    public float VertAnimation;
    public bool LandAnimation;

    [Header("Sounds")]
    public List<AudioClip> FootstepSounds;
    CharacterController characterController;
    float _footstepDelay;
    AudioSource _audioSource;
    float footstep_et = 0;
    void Start()
    {
        characterController = GetComponent<CharacterController>();
        _audioSource = gameObject.AddComponent<AudioSource>();
    }

    void Update()
    {
        if (!GetComponentInChildren<PlayerCamera>().isTutorial)
        {
            if (!GetComponentInChildren<PlayerCamera>().isCanvasOpen /*&& GameManager.Instance._audioQuestion._initialChecker*/)
            {
                //handle controller
                HandlePlayerControls();

                //sync animations with controller
                SetCharacterAnimations();
            }
        }
        else
        {
            if (!GetComponentInChildren<PlayerCamera>().isCanvasOpen)
            {
                //handle controller
                HandlePlayerControls();

                //sync animations with controller
                SetCharacterAnimations();
            }
        }

    }
    float vInput, hInput;
    void HandlePlayerControls()
    {
        hInput = Input.GetAxisRaw(HorizontalInput);
        vInput = Input.GetAxisRaw(VerticalInput);
        if (GetComponentInChildren<PlayerCamera>().isTutorial)
        {
            /*Vector3 fwdMovement = TutorialManager.Instance.teleport.disableTeleport == false ? transform.forward * vInput : Vector3.zero;
            Vector3 rightMovement = TutorialManager.Instance.teleport.disableTeleport == false ? transform.right * hInput : Vector3.zero;
            if (!TutorialManager.Instance.teleport.disableTeleport)
            {
                characterController.SimpleMove(Vector3.ClampMagnitude(fwdMovement + rightMovement, 1f) * walkSpeed);
                PlayFootstepSounds();
            }*/
        }
        else
        {
            Vector3 fwdMovement = GameManager.Instance.disableTeleport == false ? transform.forward * vInput : Vector3.zero;
            Vector3 rightMovement = GameManager.Instance.disableTeleport == false ? transform.right * hInput : Vector3.zero;
            if (!GameManager.Instance.disableTeleport)
            {
                characterController.SimpleMove(Vector3.ClampMagnitude(fwdMovement + rightMovement, 1f) * walkSpeed);
                PlayFootstepSounds();
            }
        }

        if (characterController.isGrounded)
        {
            if (hInput == 0 && vInput == 0)
                playerStates = PlayerStates.Idle;
            else
            {
                playerStates = PlayerStates.Walking;
                _footstepDelay = (2 / walkSpeed);
            }
        }
    }
    void SetCharacterAnimations()
    {
        if (!CharacterAnimator)
            return;

        switch (playerStates)
        {
            case PlayerStates.Idle:
                HorzAnimation = Mathf.Lerp(HorzAnimation, 0, 5 * Time.deltaTime);
                VertAnimation = Mathf.Lerp(VertAnimation, 0, 5 * Time.deltaTime);
                break;

            case PlayerStates.Walking:
                HorzAnimation = Mathf.Lerp(HorzAnimation, 1 * Input.GetAxis("Horizontal"), 5 * Time.deltaTime);
                VertAnimation = Mathf.Lerp(VertAnimation, 1 * Input.GetAxis("Vertical"), 5 * Time.deltaTime);
                break;
        }

        LandAnimation = characterController.isGrounded;
        CharacterAnimator.SetFloat("Horizontal", HorzAnimation);
        CharacterAnimator.SetFloat("Vertical", VertAnimation);
        CharacterAnimator.SetBool("isGrounded", LandAnimation);
    }
    void PlayFootstepSounds()
    {
        if (playerStates == PlayerStates.Idle)
            return;

        if (footstep_et < _footstepDelay)
        {
            footstep_et += Time.deltaTime;
        }
        else
        {
            footstep_et = 0;
            _audioSource.PlayOneShot(FootstepSounds[Random.Range(0, FootstepSounds.Count)]);
        }
    }
}
