﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoaderManager : MonoBehaviour
{
    public static LoaderManager Instance;
    public bl_SceneLoader sceneLoader;
    public SceneField scene;
    private void Awake()
    {
        Instance = this;
    }

    public void Load()
    {
        sceneLoader.LoadLevel(scene);
    }
}
