﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CallAction : MonoBehaviour
{
    OpenQuestion openQuestion;
    void Start()
    {
        openQuestion = FindObjectOfType<OpenQuestion>();
    }
    public void CallNextStage()
    {
        openQuestion.NextStage();
    }
    public void CallPersonalized()
    {
        openQuestion.StagePersonalized();
    }
    public void CallTeleport(int index)
    {
        GameManager.Instance.TeleportFunction(index);
        GameManager.Instance.teleportIndex = index;
    }
    public void UsePopUp()
    {
        GameManager.Instance.popUpIsOpen = false;
        GameManager.Instance.waitPopUp = false;
        GameManager.Instance.LockMouse();
    }
}