﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public int currentAnswer = 0;
    public GameObject _playerBody;
    public TeleportStages[] teleport;
    public int teleportIndex = 0;
    public Stages[] _stages;
    public Transform player;
    public KeyCode _openPanelKey, _teleportKey;
    [MMReadOnly] public bool popUpIsOpen;
    [MMReadOnly] public bool waitPopUp;
    [MMReadOnly] public bool disableTeleport;
    [Header("Scripts References")]
    public OpenQuestion _openQuestion;
    public PlayerCamera _playerCamera;
    public LoaderManager _loader;
    public DataWebGL _dataWebGL;
    //public AudioQuestion _audioQuestion;
    [Header("Setting for NO DATA")]
    public bool _withoutData;
    private void Awake()
    {
        Instance = this;
        player.position = teleport[0].tpAreas[0].position;
        player.rotation = Quaternion.Euler(0, teleport[0].tpAreas[0].eulerAngles.y, 0);
    }
    void Start()
    {
        if (PlayerPrefs.HasKey("_withoutData"))
        {
            _withoutData = true;
            Debug.Log(PlayerPrefs.GetString("_withoutData"));
        }
        else
        {
            _withoutData = false;
            Debug.Log("Si Subira Datos a GestorVR");
        }

        FadeCamera.Instance.FadeInStart(2f);
        ActivateEnviroment(0);
        LockMouse();
       // _audioQuestion.PlayInitialAudio(_audioQuestion._audioStage[currentAnswer].PhraseAsset[0]);
    }
    void Update()
    {
       /* if (_audioQuestion._initialChecker)
        {
            if (Input.GetKeyDown(_openPanelKey) && !popUpIsOpen && !waitPopUp)
            {
                _playerCamera.SetIE(false);
                _openQuestion.OpenPopup();
                UnlockMouse();
                waitPopUp = true;
                disableTeleport = true;
            }
            else if (Input.GetKeyDown(_openPanelKey) && waitPopUp && !popUpIsOpen)
            {
                try
                {
                    if (_openQuestion.currentPopUp.GetComponent<Popup>() != null)
                    {
                        _openQuestion.currentPopUp.GetComponent<Popup>().Close();
                    }
                }
                catch
                {
                    Debug.Log("Trying to open a nonexistent panel");
                }
                LockMouse();
                waitPopUp = false;
                disableTeleport = false;
            }

            if (Input.GetKeyDown(_teleportKey) && !disableTeleport)
            {
                disableTeleport = true;
                teleportIndex += 1;
                if (teleportIndex > teleport[currentAnswer].tpAreas.Length - 1)
                    teleportIndex = 0;
                StartCoroutine(FadingTeleport(teleportIndex));
            }

            HideBodyWorker();
        }*/

    }
    public void HideBodyWorker()
    {
        if (currentAnswer > _openQuestion.SO.questions.Length - 1)
        {
            return;
        }
        else
        {
            if (teleport[currentAnswer].hideWorker == true)
            {
                if (teleportIndex == 0)
                {
                    _playerBody.SetActive(false);
                }
                else
                {
                    StartCoroutine(ActivateWorker(.75f));
                }
            }
        }
    }
    private IEnumerator ActivateWorker(float time)
    {
        yield return new WaitForSeconds(time);
        _playerBody.SetActive(true);
    }

    public void TeleportFunction(int index)
    {
        StartCoroutine(FadingTeleport(index));
    }
    IEnumerator FadingTeleport(int index)
    {
        _playerCamera.enabled = false;
        FadeCamera.Instance.FadeOut(.5f);
        yield return new WaitWhile(() => FadeCamera.Instance.opacity > 0);
        player.position = teleport[currentAnswer].tpAreas[index].position;
        player.rotation = Quaternion.Euler(0, teleport[currentAnswer].tpAreas[index].eulerAngles.y, 0);
        _playerCamera.enabled = true;
        FadeCamera.Instance.FadeIn(1f);
        switch (currentAnswer)
        {
            case 6:
                if (index == 0)
                {
                    player.GetComponent<CharacterController>().enabled = false;
                }
                else
                {
                    player.GetComponent<CharacterController>().enabled = true;
                }
                break;
            case 7:
                if (index == 0)
                {
                    player.GetComponent<CharacterController>().enabled = false;
                }
                else
                {
                    player.GetComponent<CharacterController>().enabled = true;
                }
                break;
            case 8:
                if (index == 0)
                {
                    player.GetComponent<CharacterController>().enabled = false;
                }
                else
                {
                    player.GetComponent<CharacterController>().enabled = true;
                }
                break;
            case 9:
                player.GetComponent<CharacterController>().enabled = true;
                break;
        }
        yield return new WaitForSeconds(.75f);
        disableTeleport = false;
    }
    public IEnumerator PersonalizedTeleport()
    {
        disableTeleport = true;
        teleportIndex = 0;
        FadeCamera.Instance.FadeOut(.5f);
        yield return new WaitWhile(() => FadeCamera.Instance.opacity > 0);
        player.position = teleport[currentAnswer].tpAreas[teleportIndex].position;
        player.rotation = Quaternion.Euler(0, teleport[currentAnswer].tpAreas[teleportIndex].eulerAngles.y, 0);
        FadeCamera.Instance.FadeIn(1f);

    }
    public void ActivateEnviroment(int index)
    {
        StartCoroutine(ActivateArea(index));
        if (index > 0)
        {
            _stages[index - 1].stage.SetActive(false);
            _stages[index - 1].area.SetActive(false);
        }
    }
    private IEnumerator ActivateArea(int index)
    {
        _stages[index].stage.SetActive(true);
        _stages[index].area.SetActive(true);
        yield return new WaitForSeconds(.5f);
        _stages[index].area.GetComponent<BoxCollider>().enabled = true;
    }
    public void LockMouse()
    {
        _playerCamera.isCanvasOpen = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
    public void UnlockMouse()
    {
        _playerCamera.SetIE(true);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    private bool muted;
    private void SetAudioMute(bool mute)
    {
        AudioSource[] sources = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
        for (int index = 0; index < sources.Length; ++index)
        {
            sources[index].mute = mute;
        }
        muted = mute;
    }
    public IEnumerator EndSimulator(float time)
    {
        yield return new WaitForSeconds(time);
        SetAudioMute(true);
        _stages[9].stage.SetActive(false);
        FadeCamera.Instance.FadeOut();
        if (!_withoutData)
        {
            yield return _dataWebGL.SendDataToManager();
            _loader.Load();
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else
        {
            yield return new WaitForSeconds(1f);
            _loader.Load();
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }
}



