﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Stages
{
    public GameObject stage;
    public GameObject area;
    public GameObject[] phases;
}
