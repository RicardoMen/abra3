﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TeleportStages
{
    public Transform[] tpAreas;
    public bool hideWorker;
}
