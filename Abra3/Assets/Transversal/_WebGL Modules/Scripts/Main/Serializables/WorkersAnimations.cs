﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class WorkersAnimations
{
    public Animator[] animators;
    public bool onTeleport;
}