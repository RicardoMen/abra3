﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnEnterAreaQuest : MonoBehaviour
{
    public GameManager gameManager;
    Color color;
    private void OnEnable()
    {
        StartCoroutine(ActiveCollider());
    }
    private void OnDisable()
    {
        GetComponent<Collider>().enabled = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && !GameManager.Instance.popUpIsOpen/* && GameManager.Instance._audioQuestion._initialChecker*/)
        {
            Camera.main.GetComponent<PlayerCamera>().SetIE(true);
            gameManager._openQuestion.OpenPopup();
            gameManager.UnlockMouse();
            gameManager.waitPopUp = true;
        }
    }
    IEnumerator ActiveCollider()
    {
        yield return new WaitForSeconds(1f);
        GetComponent<Collider>().enabled = true;
    }

}
