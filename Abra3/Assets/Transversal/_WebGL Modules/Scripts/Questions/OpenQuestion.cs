﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
public class OpenQuestion : MonoBehaviour
{
    [Header("Scriptable")]
    public SOQuestions SO;
    [Header("References")]
    public GameObject[] popupPrefab;
    [MMReadOnly] public GameObject currentPopUp;
    protected Canvas m_canvas;
    private int oldIndex = 0;
    //[Header("Scripts References")]
    //public AudioQuestion _audioQuestion;
    protected void Start()
    {
        m_canvas = GameObject.Find("Canvas").GetComponent<Canvas>();
        oldIndex = 0;
        ScriptableDataManager.Instance.saveData.Answers.Clear();
    }
    void Update()
    {
        if (GameManager.Instance.currentAnswer > oldIndex)
        {
            StartCoroutine(FadeIn());
            oldIndex = GameManager.Instance.currentAnswer;
        }
    }
    IEnumerator FadeIn()
    {
        if (GameManager.Instance.currentAnswer > SO.questions.Length - 1)
        {
            Debug.Log("End all questions.");
            StartCoroutine(GameManager.Instance.EndSimulator(3f));
        }
        else
        {
            yield return new WaitForSeconds(1f);
            GameManager.Instance.TeleportFunction(0);
            GameManager.Instance.ActivateEnviroment(GameManager.Instance.currentAnswer);
            yield return new WaitForSeconds(2f);
            FadeCamera.Instance.FadeIn();
           // _audioQuestion.PlayInitialAudio(_audioQuestion._audioStage[GameManager.Instance.currentAnswer].PhraseAsset[0]);
            yield return new WaitForSeconds(1f);
            GameManager.Instance.popUpIsOpen = false;
            GameManager.Instance.waitPopUp = false;
            GameManager.Instance.teleportIndex = 0;
            GameManager.Instance.disableTeleport = false;
            GameManager.Instance._playerBody.SetActive(true);
        }
    }
    public virtual void OpenPopup()
    {
        foreach (var item in GameObject.FindGameObjectsWithTag("Panel"))
        {
            item.GetComponent<Popup>().Close();
        }
        var popup = Instantiate(popupPrefab[GameManager.Instance.currentAnswer]) as GameObject;
        currentPopUp = popup;
        popup.SetActive(true);
        popup.transform.localScale = Vector3.zero;
        popup.transform.SetParent(m_canvas.transform, false);
        popup.GetComponent<Popup>().Open();
        FillQuest();
    }
    private void FillQuest()
    {
        currentPopUp.GetComponent<PopUpQuestion>().question.text = (SO.questions[GameManager.Instance.currentAnswer].question);
        for (int i = 0; i < currentPopUp.GetComponent<PopUpQuestion>().answers.Length; i++)
        {
            currentPopUp.GetComponent<PopUpQuestion>().answers[i].GetComponentInChildren<Text>().text = (SO.questions[GameManager.Instance.currentAnswer].answers[i]);
        }
    }
    public void NextStage()
    {
        CheckAnswers();
        StartCoroutine(ClosePopUp(1f));
    }
    public void StagePersonalized()
    {
        CheckAnswers();
        StartCoroutine(Personalized(1f));
    }
    void CheckAnswers()
    {
        var name = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name;
        int tempcurrent = 0;
        tempcurrent = GameManager.Instance.currentAnswer + 1;
        switch (name)
        {
            case "Answer 1":
                ScriptableDataManager.Instance.saveData.Answers.Add(SO.questions[GameManager.Instance.currentAnswer]._questionID, SO.questions[GameManager.Instance.currentAnswer]._answersID[0]);
                //_audioQuestion.PlayAnswerAudio(_audioQuestion._audioStage[GameManager.Instance.currentAnswer].PhraseAsset[1]);
                Debug.Log("Pregunta: " + tempcurrent + ", el usuario respondio: " + SO.questions[GameManager.Instance.currentAnswer]._answersID[0]);
                break;
            case "Answer 2":
                ScriptableDataManager.Instance.saveData.Answers.Add(SO.questions[GameManager.Instance.currentAnswer]._questionID, SO.questions[GameManager.Instance.currentAnswer]._answersID[1]);
               // _audioQuestion.PlayAnswerAudio(_audioQuestion._audioStage[GameManager.Instance.currentAnswer].PhraseAsset[2]);
                Debug.Log("Pregunta: " + tempcurrent + ", el usuario respondio: " + SO.questions[GameManager.Instance.currentAnswer]._answersID[1]);
                break;
            case "Answer 3":
                ScriptableDataManager.Instance.saveData.Answers.Add(SO.questions[GameManager.Instance.currentAnswer]._questionID, SO.questions[GameManager.Instance.currentAnswer]._answersID[2]);
               // _audioQuestion.PlayAnswerAudio(_audioQuestion._audioStage[GameManager.Instance.currentAnswer].PhraseAsset[2]);
                Debug.Log("Pregunta: " + tempcurrent + ", el usuario respondio: " + SO.questions[GameManager.Instance.currentAnswer]._answersID[2]);
                break;
        }
        foreach (var item in currentPopUp.GetComponent<PopUpQuestion>().answers)
        {
            item.interactable = false;
        }
        GameManager.Instance.popUpIsOpen = true;
    }
    IEnumerator ClosePopUp(float time)
    {
        /* while (!_audioQuestion._answerChecker)
         {
             yield return null;
         }
         yield return new WaitForSeconds(time);
         currentPopUp.GetComponent<Popup>().Close();
         GameManager.Instance.LockMouse();
         GameManager.Instance.currentAnswer++;
         FadeCamera.Instance.FadeOut();
         currentPopUp = null;*/
        yield return null;
    }
    IEnumerator Personalized(float time)
    {
        // while (!_audioQuestion._answerChecker)
        // {
        //     yield return null;
        // }
        // yield return new WaitForSeconds(time);
        //yield return new WaitWhile(() => _audioQuestion._subtitleManager.TextSynchronizer.Source.isPlaying);
        currentPopUp.GetComponent<Popup>().Close();
        GameManager.Instance.LockMouse();
        FadeCamera.Instance.FadeOut();
        yield return new WaitWhile(() => FadeCamera.Instance.opacity > 0);
        FadeCamera.Instance.FadeIn();
        StartCoroutine(GameManager.Instance.PersonalizedTeleport());
        GameManager.Instance._stages[GameManager.Instance.currentAnswer].phases[0].SetActive(false);
        GameManager.Instance._stages[GameManager.Instance.currentAnswer].phases[1].SetActive(true);
        yield return new WaitForSeconds(time + 3);
        FadeCamera.Instance.FadeOut();
        GameManager.Instance.currentAnswer++;
    }
}