﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopUpQuestion : MonoBehaviour
{
    public Text question;
    public Button[] answers;
    void Start()
    {
        for (int i = 0; i < answers.Length; i++)
        {
            answers[i].transform.SetSiblingIndex(Random.Range(0, answers.Length));
        }
    }
}