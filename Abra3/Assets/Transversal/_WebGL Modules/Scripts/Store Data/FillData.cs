﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class FillData : MonoBehaviour
{
    public GameObject[] canvasAnswers;
    public Sprite good, wrong;
    public CanvasShow canvasShow;
    public string percentKey;
    public Text percentText;
    public Image percentImage;
    public Sprite[] percentSprites;
    public Button _button;
    private float correctCounter = 0;
    public bool isTest;
    public string[] strings;
    private List<string> listOfValues;
   // public AudioQuestion _audioQuestion;
    void Start()
    {
        //_audioQuestion.PlayInitialAudio(_audioQuestion._audioStage[0].PhraseAsset[0]);
        UnlockMouse();
        canvasShow.CanvasChange();
        if (isTest)
        {
            listOfValues = strings.ToList();
        }
        else
        {
            listOfValues = ScriptableDataManager.Instance.saveData.Answers.Values.ToList();
        }
        for (int i = 0; i < canvasAnswers.Length; i++)
        {
            int count = i + 1;
            if (listOfValues[i] == "A")
            {
                correctCounter++;
                canvasAnswers[i].GetComponentInChildren<Image>().sprite = good;
                canvasAnswers[i].GetComponentInChildren<Image>().color = Color.green;
            }
            else
            {
                canvasAnswers[i].GetComponentInChildren<Image>().sprite = wrong;
                canvasAnswers[i].GetComponentInChildren<Image>().color = Color.red;
            }
        }
        CalculatePercentage();
    }
    private void Update()
    {
        //if (_audioQuestion._initialChecker)
        {
            _button.interactable = true;
        }
    }
    private void CalculatePercentage()
    {
        percentText.text = (percentKey) + (correctCounter * 10) + "%";
        if (correctCounter <= 7)
        {
            percentImage.sprite = percentSprites[0];
        }
        else if (correctCounter >= 8 && correctCounter <= 9)
        {
            percentImage.sprite = percentSprites[1];
        }
        else if (correctCounter == 10)
        {
            percentImage.sprite = percentSprites[2];
        }
    }
    void UnlockMouse()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
}
