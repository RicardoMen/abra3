﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
public class Loader : MonoBehaviour
{
    public SceneField loadingScene;
    public SceneField levelScene;
    private AsyncOperation _loadingSceneAsync;
    public bool useCorountine;
    public int seconds;
    void Start()
    {
        if (useCorountine)
        {
            StartCoroutine(CorountineScene());
        }
    }
    virtual public void StartLevel()
    {
        _loadingSceneAsync = SceneManager.LoadSceneAsync(loadingScene);
        _loadingSceneAsync.allowSceneActivation = false;
        if (_loadingSceneAsync != null)
        {
            _loadingSceneAsync.allowSceneActivation = true;
        }
        SceneManager.LoadSceneAsync(levelScene);
    }
    public IEnumerator CorountineScene()
    {
        yield return new WaitForSeconds(seconds);
        FindObjectOfType<BNG.ScreenFader>().DoFadeIn();
        yield return new WaitForSeconds(2);
        StartLevel();
    }
}