﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
    public bool isHud, isTeleport, isEnable;
    private void OnEnable()
    {
        isEnable = true;
    }
    private void OnDisable()
    {
        isEnable = false;
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Finger" && isHud && isEnable)
        {
            GetComponent<Button>().onClick.Invoke();
            GetComponent<Image>().color = Color.gray;
            FillData();
            isHud = false;
        }
        if (other.tag == "Finger" && isTeleport && isEnable)
        {
            GetComponent<Button>().onClick.Invoke();
            isTeleport = false;
            StartCoroutine(returnTrue());
        }
    }
    IEnumerator returnTrue()
    {
        yield return new WaitForSeconds(.5f);
        isTeleport = true;
    }
    IEnumerator Answer()
    {
        GameManagerVR.Instance.answered = true;
        yield return new WaitForSeconds(1f);
        GameManagerVR.Instance.answered = false;
    }
    private void FillData()
    {
        StartCoroutine(Answer());
        var tempCurrentSO = GameManagerVR.Instance._questionManagerVR._scriptableQuestion.questions[GameManagerVR.Instance.currentQuestion - 1];
        switch (this.name)
        {
            case "Answer 1":
                GameManagerVR.Instance.answerCorrectly = true;
                ScriptableDataManager.Instance.saveData.Answers.Add(tempCurrentSO._questionID, tempCurrentSO._answersID[0]);
                Debug.Log("Pregunta: " + GameManagerVR.Instance.currentQuestion + " con ID: " + tempCurrentSO._questionID + ", se contesto como: " + tempCurrentSO._answersID[0]);
                break;
            case "Answer 2":
                ScriptableDataManager.Instance.saveData.Answers.Add(tempCurrentSO._questionID, tempCurrentSO._answersID[1]);
                Debug.Log("Pregunta: " + GameManagerVR.Instance.currentQuestion + " con ID: " + tempCurrentSO._questionID + ", se contesto como: " + tempCurrentSO._answersID[1]);
                break;
            case "Answer 3":
                ScriptableDataManager.Instance.saveData.Answers.Add(tempCurrentSO._questionID, tempCurrentSO._answersID[2]);
                Debug.Log("Pregunta: " + GameManagerVR.Instance.currentQuestion + " con ID: " + tempCurrentSO._questionID + ", se contesto como: " + tempCurrentSO._answersID[2]);
                break;
        }
        foreach (var item in ScriptableDataManager.Instance.saveData.Answers)
        {
            Debug.Log("Dictionary items: " + item);
        }
    }
}
