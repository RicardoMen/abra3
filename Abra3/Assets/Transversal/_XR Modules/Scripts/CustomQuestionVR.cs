﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomQuestionVR : MonoBehaviour
{
    QuestionManagerVR _questionManagerVR;
    private void Start()
    {
        _questionManagerVR = GetComponent<QuestionManagerVR>();
    }
    public IEnumerator CustomChangeQuestion()
    {
        GameManagerVR.Instance.fadeCamera.DoFadeIn();
        yield return new WaitForSeconds(1f);
        GameManagerVR.Instance.fadeCamera.DoFadeOut();
        if (GameManagerVR.Instance.answerCorrectly)
        {
            GameManagerVR.Instance.player.position = GameManagerVR.Instance.teleport[GameManagerVR.Instance.currentQuestion - 1].tpAreas[0].position;
            GameManagerVR.Instance.player.rotation = Quaternion.Euler(0, GameManagerVR.Instance.teleport[GameManagerVR.Instance.currentQuestion - 1].tpAreas[0].eulerAngles.y, 0);
            GameManagerVR.Instance._stages[GameManagerVR.Instance.currentQuestion - 1].phases[0].SetActive(false);
            GameManagerVR.Instance._stages[GameManagerVR.Instance.currentQuestion - 1].phases[1].SetActive(true);
            _questionManagerVR.StateButtonManager(false);
            yield return new WaitForSeconds(2);
            GameManagerVR.Instance.fadeCamera.DoFadeIn();
            yield return new WaitForSeconds(1f);
            GameManagerVR.Instance.fadeCamera.DoFadeOut();
            _questionManagerVR.FillQuestion();
        }
        else if (!GameManagerVR.Instance.answerCorrectly)
        {
            GameManagerVR.Instance.fadeCamera.DoFadeIn();
            yield return new WaitForSeconds(1f);
            GameManagerVR.Instance.fadeCamera.DoFadeOut();
            _questionManagerVR.FillQuestion();
        }


    }
}
