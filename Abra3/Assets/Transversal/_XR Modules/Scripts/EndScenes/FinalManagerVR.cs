﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BNG;
public class FinalManagerVR : MonoBehaviour
{
    public ScreenFader _screenFader;
    void Start()
    {
        StartCoroutine(EndSimulator());
    }

    private IEnumerator EndSimulator()
    {
        yield return new WaitForSeconds(5f);
        _screenFader.DoFadeIn();
        yield return new WaitForSeconds(2f);
        Application.Quit();
    }
}
