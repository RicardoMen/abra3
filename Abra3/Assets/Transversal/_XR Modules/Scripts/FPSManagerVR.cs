﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BNG;
public class FPSManagerVR : MonoBehaviour
{
    public CanvasShow _canvas;
    bool click;
    void Update()
    {
        if (InputBridge.Instance.BackButtonDown)
        {
            if (click)
            {
                _canvas.CanvasChange(true);
            }
            else
            {
                _canvas.CanvasChange(false);
            }
            click = !click;
        }

    }
}
