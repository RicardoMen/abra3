﻿using System.Collections;
using System.Collections.Generic;
using BNG;
using UnityEngine;
public class GameManagerVR : MonoBehaviour
{
    public static GameManagerVR Instance;

    [Header("Current Question")]
    public int currentQuestion = 0;
    [Header("Player Controller")]
    public Transform player;
    public ScreenFader fadeCamera;
    [Header("Position's Controller")]
    public TeleportStages[] teleport;
    [Header("Enviroment Controller")]
    public Stages[] _stages;
    public bool _changeEnviroment;
    public int _indexToChangeEnviroment;
    public GameObject oldEnviroment, newEnviroment;
    [HideInInspector]
    public bool answerCorrectly, answered;
    [Header("Scripts References")]
    public QuestionManagerVR _questionManagerVR;
    [Header("Data")]
    public bool _withoutData;
    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        if (PlayerPrefs.HasKey("_withoutData"))
        {
            _withoutData = true;
            Debug.Log(PlayerPrefs.GetString("_withoutData"));
        }
        else
        {
            _withoutData = false;
            Debug.Log("Si Subira Datos a GestorVR");
        }

        player.position = teleport[currentQuestion].tpAreas[0].position;
        player.rotation = Quaternion.Euler(0, teleport[currentQuestion].tpAreas[0].eulerAngles.y, 0);
    }
    public void PlayerPosRot(int index)
    {
        player.position = teleport[currentQuestion].tpAreas[index].position;
        player.rotation = Quaternion.Euler(0, teleport[currentQuestion].tpAreas[index].eulerAngles.y, 0);
    }
    public void TeleportFunction(int index)
    {
        StartCoroutine(FadeTeleport(index));
    }
    IEnumerator FadeTeleport(int index)
    {
        fadeCamera.DoFadeIn();
        yield return new WaitForSeconds(.75f);
        fadeCamera.DoFadeOut();
        PlayerPosRot(index);
    }
    public void ChangeEnviroment()
    {
        if (currentQuestion > _indexToChangeEnviroment)
        {
            oldEnviroment.SetActive(false);
            newEnviroment.SetActive(true);
        }
    }
    public void ChangeStage()
    {
        _stages[currentQuestion].stage.SetActive(true);
        if (currentQuestion > 0)
        {
            _stages[currentQuestion - 1].stage.SetActive(false);
        }
    }
    public IEnumerator EndModule()
    {
        yield return new WaitForSeconds(1f);
        fadeCamera.DoFadeIn();
        if (!_withoutData)
        {
            yield return (GetComponent<DataVR>().SendDataToManager());
        }
        else
        {
            yield return new WaitForSeconds(1.5f);
        }

        GetComponent<Loader>().StartLevel();
    }
}