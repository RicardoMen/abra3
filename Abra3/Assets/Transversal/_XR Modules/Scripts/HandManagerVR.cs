﻿using System;
using System.Collections;
using System.Collections.Generic;
using BNG;
using UnityEngine;

public class HandManagerVR : MonoBehaviour
{
    public PlayerTeleport playerTeleport;
    public Transform _leftTransfom, _rightTransform, _tablet;
    public Vector3 rightHand, leftHand;
    public float rotationInX;
    [SerializeField]
    private string mainHand;
    public bool test;
    void Awake()
    {
        if (!test)
        {
            mainHand = PlayerPrefs.GetString("DominantHand");
        }

        if (mainHand == "Left")
        {
            LeftHanded();
            PlayerPrefs.SetInt("HandSelection", 0);
            playerTeleport.HandSide = ControllerHand.Left;
        }
        else if (mainHand == "Right")
        {
            RightHandes();
            PlayerPrefs.SetInt("HandSelection", 1);
            playerTeleport.HandSide = ControllerHand.Right;
        }
    }
    private void LeftHanded()
    {
        _tablet.parent = _rightTransform;
        _tablet.localPosition = rightHand;
        _tablet.localRotation = Quaternion.Euler(rotationInX, 0, 0);
    }
    private void RightHandes()
    {
        _tablet.parent = _leftTransfom;
        _tablet.localPosition = leftHand;
        _tablet.localRotation = Quaternion.Euler(rotationInX, 0, 0);
    }
}
