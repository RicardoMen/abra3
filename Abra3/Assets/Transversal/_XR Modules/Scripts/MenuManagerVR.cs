﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class MenuManagerVR : MonoBehaviour
{
    public CanvasShow _handSelector;
    public InputField _nameField, _idField;
    public UnityEngine.UI.Button _startButton;
    public BNG.VRUISystem vRUI;
    public GameObject LeftPointer, RightPointer;
    void Start()
    {
        PlayerPrefs.DeleteAll();
        StartCoroutine(showinitialcanvas());
    }
    IEnumerator showinitialcanvas()
    {
        yield return new WaitForSeconds(1f);
        _handSelector.CanvasChange();
    }
    public void SetDominantHand(string _pref)
    {
        PlayerPrefs.SetString("DominantHand", _pref);
        if (_pref == "Left")
        {
            vRUI.SelectedHand = BNG.ControllerHand.Left;
            vRUI.ControllerInput[0] = BNG.ControllerBinding.LeftTrigger;
            vRUI.UpdateControllerHand(BNG.ControllerHand.Left);
            RightPointer.SetActive(false);
            LeftPointer.SetActive(true);
        }
    }
    public void CheckFillField()
    {
        if (_nameField.text.Length > 3 && _idField.text.Length > 3)
        {
            _startButton.interactable = true;
        }
        else
        {
            _startButton.interactable = false;
        }
    }

}
