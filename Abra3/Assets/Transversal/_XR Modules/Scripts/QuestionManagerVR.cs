﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestionManagerVR : MonoBehaviour
{
    public SOQuestions _scriptableQuestion;
    public Text questTxt;
    public Button[] answersHUD, teleportHUD;
    void Start()
    {
        FillQuestion();
        StartCoroutine(EnableOrDisableHUD(4f));
    }
    void NextQuestion()
    {
        if (_scriptableQuestion.questions.Length > 0)
        {
            GameManagerVR.Instance.currentQuestion += 1;
            if (GameManagerVR.Instance.currentQuestion > _scriptableQuestion.questions.Length - 1)
            {
                StartCoroutine(GameManagerVR.Instance.EndModule());
            }
            else
            {
                StartCoroutine(EnableOrDisableHUD(4f));
                if (GameManagerVR.Instance.teleport[GameManagerVR.Instance.currentQuestion - 1].hideWorker)
                {
                    StartCoroutine(GetComponent<CustomQuestionVR>().CustomChangeQuestion());
                }
                else
                {
                    StartCoroutine(changeQuest());
                }
            }
        }
    }
    public IEnumerator changeQuest()
    {
        GameManagerVR.Instance.fadeCamera.DoFadeIn();
        yield return new WaitForSeconds(1f);
        GameManagerVR.Instance.fadeCamera.DoFadeOut();
        FillQuestion();
    }
    public void FillQuestion()
    {
        questTxt.text = (_scriptableQuestion.questions[GameManagerVR.Instance.currentQuestion].question);
        for (int i = 0; i < answersHUD.Length; i++)
        {
            answersHUD[i].GetComponentInChildren<Text>().text = (_scriptableQuestion.questions[GameManagerVR.Instance.currentQuestion].answers[i]);
            answersHUD[i].transform.SetSiblingIndex(Random.Range(0, answersHUD.Length));
            answersHUD[i].GetComponentInChildren<Image>().color = Color.white;
            answersHUD[i].GetComponentInChildren<ButtonManager>().isHud = true;
        }
        GameManagerVR.Instance.PlayerPosRot(0);
        GameManagerVR.Instance.ChangeStage();
        if (GameManagerVR.Instance._changeEnviroment)
        {
            GameManagerVR.Instance.ChangeEnviroment();
        }
        FillLogic(() => NextQuestion());
        GameManagerVR.Instance.answerCorrectly = false;
    }
    IEnumerator EnableOrDisableHUD(float time)
    {
        StateButtonManager(false);
        yield return new WaitForSeconds(time);
        StateButtonManager(true);
    }
    public void StateButtonManager(bool state)
    {
        foreach (var item in answersHUD)
        {
            item.GetComponentInChildren<ButtonManager>().isEnable = state;
        }
        foreach (var item in teleportHUD)
        {
            item.GetComponentInChildren<ButtonManager>().isEnable = state;
        }
    }
    void FillLogic(UnityEngine.Events.UnityAction action)
    {
        for (int i = 0; i < answersHUD.Length; i++)
        {
            answersHUD[i].onClick.RemoveAllListeners();
            answersHUD[i].onClick.AddListener(action);
        }
    }
}