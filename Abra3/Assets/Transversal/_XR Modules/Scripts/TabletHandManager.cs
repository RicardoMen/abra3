﻿using BNG;
using UnityEngine;

public class TabletHandManager : MonoBehaviour
{
    bool leftTrigger, rightTrigger;
    void OnEnable()
    {
        // Subscribe to input events
        InputBridge.OnInputsUpdated += UpdateInputs;
    }
    private void UpdateInputs()
    {
        if (leftTrigger)
        {
            InputBridge.Instance.LeftGrip = 1f;
            InputBridge.Instance.LeftThumbNear = true;
        }
        else
        {
            InputBridge.Instance.LeftGrip = 0f;
            InputBridge.Instance.LeftThumbNear = false;
        }

        if (rightTrigger)
        {
            InputBridge.Instance.RightGrip = 1f;
            InputBridge.Instance.RightThumbNear = true;
        }
        else
        {
            InputBridge.Instance.RightGrip = 0f;
            InputBridge.Instance.RightThumbNear = false;
        }
        InputBridge.Instance.LeftTrigger = 0;
        InputBridge.Instance.LeftTriggerNear = false;
        InputBridge.Instance.LeftTriggerUp = false;
        InputBridge.Instance.LeftTriggerDown = false;
        InputBridge.Instance.RightTrigger = 0f;
        InputBridge.Instance.RightTriggerNear = false;
        InputBridge.Instance.RightTriggerUp = false;
        InputBridge.Instance.RightTriggerDown = false;
    }
    void Update()
    {
        UpdateInputs();
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Finger")
        {
            if (other.gameObject.name.Contains("Left"))
            {
                leftTrigger = true;
            }
            else if (other.gameObject.name.Contains("Right"))
            {
                rightTrigger = true;
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Finger")
        {
            if (other.gameObject.name.Contains("Left"))
            {
                leftTrigger = false;
            }
            else if (other.gameObject.name.Contains("Right"))
            {
                rightTrigger = false;
            }
        }
    }
}