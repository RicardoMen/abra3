﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
public class ButtonTutorialVR : MonoBehaviour
{
    public bool isHud, isTeleport, isEnable;
    private void OnEnable()
    {
        isEnable = true;
    }
    private void OnDisable()
    {
        isEnable = false;
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Finger" && isHud && isEnable)
        {
            GetComponent<Button>().onClick.Invoke();
            GetComponent<Image>().color = Color.gray;
            isHud = false;
        }
        if (other.tag == "Finger" && isTeleport && isEnable)
        {
            GetComponent<Button>().onClick.Invoke();
            isTeleport = false;
            StartCoroutine(returnTrue());
        }
        IEnumerator returnTrue()
        {
            yield return new WaitForSeconds(.5f);
            isTeleport = true;
        }
    }
}
