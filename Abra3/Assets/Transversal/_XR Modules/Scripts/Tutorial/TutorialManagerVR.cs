﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BNG;
public class TutorialManagerVR : MonoBehaviour
{
    public int _phaseTutorial = 0;
    public Transform _player;
    public ScreenFader _screenFader;
    public PlayerTeleport _playerTeleport;
    public CanvasShow _canvasShow;
    public Transform[] _tpAreas;
    [SerializeField]
    private bool _enableToTeleport;
    public GameObject _tabletGFX;
    public GameObject _questionGFX, _answersGFX, _teleportGFX;
    public Text _panelTxt;
    [TextArea]
    public string[] labels;
    public GameObject[] images;
    //Internal COunts
    int teleportCount, tpCount;
    void Start()
    {
        StartCoroutine(ChangePanelUI());
        StartCoroutine(FadeTeleport(0));
    }

    private void OnEnable()
    {
        PlayerTeleport.OnAfterTeleport += TeleportPhase;
    }
    IEnumerator ChangePanelUI()
    {
        yield return new WaitForSeconds(.5f);

        _panelTxt.text = labels[_phaseTutorial];
        foreach (var item in images)
        {
            item.SetActive(false);
        }
        images[_phaseTutorial].SetActive(true);
        switch (_phaseTutorial)
        {
            case 0:
                break;
            case 1:
                _tabletGFX.SetActive(true);
                _enableToTeleport = true;
                break;
            case 2:
                _questionGFX.SetActive(true);
                _answersGFX.SetActive(true);
                _teleportGFX.SetActive(false);
                break;
            case 3:
                break;
            case 4:
                break;
        }
        yield return new WaitForSeconds(1f);
        _canvasShow.CanvasChange(false);
    }
    IEnumerator EndTutorial()
    {
        yield return new WaitForSeconds(4f);
        _screenFader.DoFadeIn();
        yield return new WaitForSeconds(6f);
        GetComponent<Loader>().StartLevel();
    }
    public void AnswerQuestion()
    {
        foreach (var item in _answersGFX.GetComponentsInChildren<BoxCollider>())
        {
            item.enabled = false;
        }
        _canvasShow.CanvasChange(true);
        RestartQuestionPhase();
        StartCoroutine(ChangePanelUI());
        StartCoroutine(EndTutorial());
    }
    private IEnumerator EnableDisableTeleport(bool state)
    {
        yield return new WaitForSeconds(.25f);
        _playerTeleport.enabled = state;
        _enableToTeleport = state;
        if(GameObject.Find("LineRenderer")!=null)
             GameObject.Find("LineRenderer").SetActive(state);
        _playerTeleport.TeleportMarker.SetActive(state);
    }
    private void TeleportPhase()
    {
        teleportCount++;
        if (teleportCount >= 3)
        {
            _canvasShow.CanvasChange(true);
            _phaseTutorial++;
            StartCoroutine(FadeTeleport(0));
            StartCoroutine(EnableDisableTeleport(false));
            PlayerTeleport.OnAfterTeleport -= TeleportPhase;
            StartCoroutine(ChangePanelUI());
        }
    }
    public void TeleportFunction(int TPPos)
    {
        StartCoroutine(FadeTeleport(TPPos));
        tpCount++;
        if (tpCount >= 2)
        {
            StartCoroutine(FadeTeleport(0));
            _canvasShow.CanvasChange(true);
            StartCoroutine(EnableDisableTeleport(false));
            StartCoroutine(ChangePanelUI());
            _phaseTutorial++;
        }
    }
    public void RestartQuestionPhase()
    {
        _phaseTutorial++;
        StartCoroutine(FadeTeleport(0));
    }
    IEnumerator FadeTeleport(int TPPos)
    {
        if (_enableToTeleport)
        {
            _screenFader.DoFadeIn();
            yield return new WaitForSeconds(.75f);
            _screenFader.DoFadeOut();
            _player.position = _tpAreas[TPPos].position;
            _player.rotation = Quaternion.Euler(0, _tpAreas[TPPos].eulerAngles.y, 0);
        }

    }
}
