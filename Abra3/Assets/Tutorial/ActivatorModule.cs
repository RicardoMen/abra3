using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivatorModule : MonoBehaviour
{
    public delegate void ActiveModule();
    public event ActiveModule OnActiveModule;
    public delegate void DisableModule();
    public event DisableModule OnDisableModule;

    public bool isWorking = true;
    private void OnTriggerEnter(Collider other)
    {
        if (isWorking)
            if (other.CompareTag("Player"))
                OnActiveModule?.Invoke();
    }

    private void OnTriggerExit(Collider other)
    {
        if (isWorking)
            if (other.CompareTag("Player"))
                OnDisableModule?.Invoke();
    }

    public void Disable()
    {
        isWorking = false;
    }

    public void Enable()
    {
        isWorking = true;
    }
}
