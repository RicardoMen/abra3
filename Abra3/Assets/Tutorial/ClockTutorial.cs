using UnityEngine;
using UnityEngine.UI;

public class ClockTutorial : MechanicTutorial
{

    [Header("Clock Elements")]
    [SerializeField] GameObject clockScreenTutorial;
    [SerializeField] GameObject clockScreenRegular;
    [SerializeField] Button FinishTutorialBtn;

    // Start is called before the first frame update
    void Start()
    {
        AddListeners();
        clockScreenTutorial.SetActive(true);
    }

    void AddListeners() {
        FinishTutorialBtn.onClick.AddListener(() =>
        {
            clockScreenTutorial.SetActive(false);
            clockScreenRegular.SetActive(false);
            OnFinish?.Invoke();
        });
    }

    
}
