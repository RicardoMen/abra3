using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
//using EPOOutline;
using Autohand;

public class GrabTutorial : MechanicTutorial
{
    public GameObject PopupIntro;
    public GameObject PopupFinal;
    public Grabbable grabbableItem;
    public PlacePoint placePoint;
    public GameObject canvas;
    public ActivatorModule activator;

    public Button FinishBtn;

    [Header("Outline config")]
    public Color outlineColor;
    // Start is called before the first frame update
    void Start()
    {
        // activator.OnActiveModule += () =>
        // {
        //     canvas.SetActive(true);
        // };
        // activator.OnDisableModule += () =>
        // {
        //     canvas.SetActive(false);
        // };
        FinishBtn.gameObject.SetActive(false);
#if !UNITY_WEBGL
        placePoint.placeNames = new string[] { grabbableItem.name };

        placePoint.OnPlace.AddListener(() =>
        {
            PopupIntro.SetActive(false);
            PopupFinal.SetActive(true);
            FinishBtn.gameObject.SetActive(true);
        });

        placePoint.OnRemove.AddListener(() =>
        {
            PopupIntro.SetActive(true);
            PopupFinal.SetActive(false);
            FinishBtn.gameObject.SetActive(false);
        });
#else
        

#endif
        FinishBtn.onClick.AddListener(() =>
        {
            OnFinish?.Invoke();
        });

        //Outlinable _tempOutlinable = grabbableItem.gameObject.AddComponent<Outlinable>();
        //tempOutlinable.AddAllChildRenderersToRenderingList();
        //_tempOutlinable.OutlineParameters.Color = outlineColor;
       // _tempOutlinable.enabled = false;
        grabbableItem.onHighlight.AddListener((hand, grab) =>
        {
            //_tempOutlinable.enabled = true;

        });

        grabbableItem.onUnhighlight.AddListener((hand, grab) =>
        {
          //  _tempOutlinable.enabled = false;
        });
    }
}
