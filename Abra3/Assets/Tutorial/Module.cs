using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class Module : MonoBehaviour
{
    public UnityAction<ModuleState> ChangeState;
    public ModuleState moduleState;
    public GameObject ContainerModule;
    public GameObject ModelExample;
    
    public void DisableModule()
    {
        ContainerModule.SetActive(false);
        ModelExample.SetActive(true);
    }

    public void EnableModule()
    {
        ContainerModule.SetActive(true);
        ModelExample.SetActive(false);
    }

    public void StartModule()
    {
        moduleState = ModuleState.inProcess;
        ChangeState?.Invoke(moduleState);
    }

    public void SetInProcessModule()
    {
        moduleState = ModuleState.inProcess;
        ChangeState?.Invoke(moduleState);
    }

    public void FinishModule()
    {
        moduleState = ModuleState.finished;
        ChangeState?.Invoke(moduleState);
    }

    public enum ModuleState
    {
        ignored,
        inProcess,
        finished

    }

}
