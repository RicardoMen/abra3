using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ModulesController : MonoBehaviour
{
    public Transform modulesContainer;
    public List<Module> modules;

    // Start is called before the first frame update
    private void Awake()
    {
        // for (var i = 0; i < modulesContainer.childCount; i++)
        // {
        //     modules.Add(modulesContainer.GetChild(i).GetComponent<Module>());
        // }
        //DisableModules();


    }

    IEnumerator Start()
    {
        yield return new WaitForSeconds(0.55f);
        DisableModules();
    }

    public void EnableModules()
    {
        foreach (var item in modules)
        {
            item.EnableModule();
        }
    }

    public void DisableModules()
    {
        foreach (var item in modules)
        {
            item.DisableModule();
        }
    }
}
