using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TeleportTutorial : MechanicTutorial
{
    [Header("Elements")]
    public Transform player;
    public GameObject Teleport;
    public GameObject PopupIntro;
    public GameObject PopupFinal;
    public GameObject canvas;
    public TextMeshProUGUI timerText;
    public ActivatorModule correctArea;
    public bool state;

    private Vector3 relativePosition;


    // Start is called before the first frame update
    void Start()
    {
        relativePosition = canvas.transform.position - player.transform.position;
        PopupIntro.SetActive(true);
        PopupFinal.SetActive(false);

        correctArea.OnActiveModule += () =>
        {
            state = true;
            CheckState();
        };

        // correctArea.OnDisableModule += () =>
        // {
        //     state = false;
        //     CheckState();
        // };
    }

    private void CheckState()
    {
        if (state)
        {
            PopupIntro.SetActive(false);
            PopupFinal.SetActive(true);
            // StopAllCoroutines();
            StartCoroutine(FinishTutorial(5));
            state = false;
        }
        // else
        // {
        //     PopupIntro.SetActive(true);
        //     PopupFinal.SetActive(false);
        // }
    }

    private IEnumerator FinishTutorial(float time)
    {
        Teleport.gameObject.SetActive(false);
        for (var i = 0; i < time; i++)
        {
            timerText.text = "Saliendo en: " + (time - i);
            yield return new WaitForSeconds(1);
        }
        Teleport.gameObject.SetActive(true);
        OnFinish?.Invoke();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 position = player.position + relativePosition;
        canvas.transform.position = new Vector3(position.x, canvas.transform.position.y, canvas.transform.position.z);
    }
}
