using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Autohand;

public class TutorialController : MonoBehaviour
{
    [Header("UI Elements")]
    public EffectsManager effectsManager;
    public ModulesController modulesController;

    public GameObject limits;
    public GameObject IntroPopup;
    public GameObject FinalPopup;

    [SerializeField] Button EvaluationModeBtn;
    [SerializeField] Button FormationModeBtn;


    public List<MechanicTutorial> tutorials;

    public UnityAction OnEvaluationMode;
    public UnityAction OnFormationMode;

    public GameObject TeleportController;
    public GameObject InitialLimit;

    public bool executationFormation = false;

    // Start is called before the first frame update
    void Start()
    {
        TeleportController.SetActive(false);
        foreach (var item in tutorials)
        {
            item.gameObject.SetActive(false);
        }
        StartCoroutine(StartTutorials());
    }

    private IEnumerator StartTutorials()
    {
        IntroPopup.SetActive(true);
        FinalPopup.SetActive(false);
        //EvaluationModeBtn.gameObject.SetActive(false);
        //FormationModeBtn.gameObject.SetActive(false);
        yield return new WaitForSeconds(8);
        TeleportController.SetActive(true);
        InitialLimit.SetActive(false);
        IntroPopup.SetActive(false);
        int count = 0;
        tutorials[0].gameObject.SetActive(true);
        tutorials[0].OnFinish += () =>
        {
            if (count < tutorials.Count - 1)
            {

                tutorials[0].gameObject.SetActive(false);
                tutorials[1].gameObject.SetActive(true);
            }
        };
        tutorials[1].OnFinish += () =>
        {
            if (count < tutorials.Count - 1)
            {

                tutorials[1].gameObject.SetActive(false);
                tutorials[2].gameObject.SetActive(true);
                //LoadButton.SetActive(true);
                //FinalPopup.SetActive(true);
            }
        };        
        tutorials[2].OnFinish += () =>
        {
            tutorials[2].gameObject.SetActive(false);
            LoadButton.SetActive(true);
            FinalPopup.SetActive(true);
        };
        try
        {
            EvaluationModeBtn.onClick.AddListener(() =>
            {
                //effectsManager.Fade(1);
                executationFormation = false;
                EvaluationModeBtn.gameObject.SetActive(false);
                FormationModeBtn.gameObject.SetActive(false);
                modulesController.EnableModules();
                FinalPopup.SetActive(false);
                limits.SetActive(false);
                OnEvaluationMode?.Invoke();
            });

            FormationModeBtn.onClick.AddListener(() =>
            {
                //effectsManager.Fade(1);
                executationFormation = true;
                FormationModeBtn.gameObject.SetActive(false);
                EvaluationModeBtn.gameObject.SetActive(false);
                modulesController.DisableModules();
                FinalPopup.SetActive(false);
                limits.SetActive(false);
                OnFormationMode?.Invoke();
            });
        }
        catch (System.Exception ex) {
            Debug.Log("Button not found");
        }
    }

    private IEnumerator ChangeTutorial(MechanicTutorial prevTutorial, MechanicTutorial nextTutorial, float time)
    {
        // StartCoroutine(FinishTutorial(prevTutorial, time));
        prevTutorial.gameObject.SetActive(false);
        yield return new WaitForSeconds(time);
        nextTutorial.gameObject.SetActive(true);
    }

    private IEnumerator InitTutorial(MechanicTutorial tutorialObj, float time)
    {
        effectsManager.Fade(time);
        yield return new WaitForSeconds(time);
        tutorialObj.gameObject.SetActive(true);
        Debug.Log("tutorial iniciado");
    }

    private IEnumerator FinishTutorial(MechanicTutorial tutorialObj, float time)
    {
        effectsManager.Fade(time);
        yield return new WaitForSeconds(time / 2);
        tutorialObj.gameObject.SetActive(false);
        yield return new WaitForSeconds(time / 2);
        Debug.Log("tutorial finalizado");
    }

    private IEnumerator FinishTutorials(MechanicTutorial tutorialObj, float time)
    {
        effectsManager.Fade(time);
        yield return new WaitForSeconds(time / 2);
        tutorialObj.gameObject.SetActive(false);
        yield return new WaitForSeconds(time / 2);
        Debug.Log("tutoriales finalizado");

    }

    #region DLC QUALITAT
    [Header("UI Qualitat")]
    public GameObject LoadButton;

    public void LoadNextScene() {
        SetTutorialFinished();
        StartCoroutine(Load());    
    }

    IEnumerator Load() {
        FindObjectOfType<FadeObject>().InitFade();

        yield return new WaitForSeconds(1.5f);

        UnityEngine.SceneManagement.SceneManager.LoadScene("MainHall");
    }

    /// <summary>
    /// Finaliza tutorial indicando que debe activarse el bot�n diagn�stico en la pr�xima carga
    /// </summary>
    public void SetTutorialFinished()
    {
        try
        {
            GameControler.Instance.ActivarDiagnostico = true;
            GameControler.Instance.ActivarEvaluacion = false;
            GameControler.Instance.ActivarFormacion = false;
        }
        catch (System.Exception ex) {
            Debug.Log("No se pudo guardar estado de finalizaci�n ya que el objeto no existe. Debe iniciar la escena desde MainHall para evitar errores. " + ex.Message);
        }
    }

    #endregion
}
