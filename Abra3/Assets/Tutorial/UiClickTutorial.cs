using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UiClickTutorial : MechanicTutorial
{
    [Header("Elements")]
    public GameObject Popup;
    public GameObject canvas;
    public Button TestStateBtn;
    private TextMeshProUGUI StateText;
    private bool state;
    public Button FinishTutorialBtn;
    public ActivatorModule activatorModule;
    // Start is called before the first frame update
    void Start()
    {
        FinishTutorialBtn.gameObject.SetActive(false);
        StateText = TestStateBtn.GetComponentInChildren<TextMeshProUGUI>();
        TestStateBtn.onClick.AddListener(() =>
        {
            state = !state;

            if (state)
            {
                StateText.text = "ATR�S";
                FinishTutorialBtn.gameObject.SetActive(true);
            }
            else
            {
                StateText.text = "CONTINUAR";
                FinishTutorialBtn.gameObject.SetActive(false);
            }

        });

        FinishTutorialBtn.onClick.AddListener(() =>
        {
            if (state)
            {
                OnFinish?.Invoke();
            }
        });
    }
}
