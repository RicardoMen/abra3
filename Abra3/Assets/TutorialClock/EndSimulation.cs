using UnityEngine;
using UnityEngine.SceneManagement;

using System.Collections;


public class EndSimulation : MonoBehaviour
{
    public GestorVR_PDR_Fatalidades CapturarRespuesta;
    public void ReturnToMainHall() {
        //CapturarRespuesta.Respuestas(); //Omitido para testeo
        SetStageFinished();        
    }

    /// <summary>
    /// Indica qu� bot�n se debe activar seg�n la escena finalizada a trav�s del reloj de pulsera
    /// </summary>
    void SetStageFinished() {
        string scene = SceneManager.GetActiveScene().name.ToLower();
        
        switch (scene)
        {
            case "diagnostico":
                FindObjectOfType<FinishGame>().SetDiagnosticoFinished();
                //CapturarRespuesta.Respuestas();
                //SceneManager.LoadScene("MainHall");
                StartCoroutine(Load(true,"MainHall"));
                break;

            case "formacion":
                FindObjectOfType<FinishGame>().SetFormacionFinished();
                //CapturarRespuesta.Respuestas();
                // SceneManager.LoadScene("MainHall");
                StartCoroutine(Load(true, "MainHall"));
                break;

            case "tutorial":
                FindObjectOfType<TutorialController>().SetTutorialFinished();
                //SceneManager.LoadScene("MainHall");
                StartCoroutine(Load(false, "MainHall"));
                break;

            case "evaluacion":
                //CapturarRespuesta.Respuestas();
                // SceneManager.LoadScene("PrincipalScene_DATA_LEGACY_elabra");
                StartCoroutine(Load(true, "PrincipalScene_DATA_LEGACY_elabra"));
                break;
        }
    }

    IEnumerator Load(bool answers, string sceneName)
    {
        Debug.Log("Rescata Respuestas "+answers);
        if(answers)
            CapturarRespuesta.Respuestas();

        yield return new WaitForSeconds(5);
        SceneManager.LoadScene(sceneName);
    }
}
