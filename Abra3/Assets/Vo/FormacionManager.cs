using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class FormacionManager : MonoBehaviour
{
    
    public Transform[] post;
    public GameObject[] quest;
    public int nquest;
    public FadeObject fade;
    public Transform XRPlayer;

    // Start is called before the first frame update
    public void FirstPost()
    {
        TpNextPoint(0,2);
    }

    public void TpNextPoint(int n,float t)
    {        
        StartCoroutine(TpNextPointC(n,t));
    }
    IEnumerator TpNextPointC(int n, float t)
    {
        yield return new WaitForSeconds(t);
        fade.InitFade();

        if (nquest < post.Length)
        {
            XRPlayer.position = post[n].position;
            XRPlayer.rotation = post[n].rotation;
            yield return new WaitForSeconds(1.0f);

            ActivateCurrentQuestion(n);
        }
        else if (nquest >= post.Length)
        {
            //FindObjectOfType<GestorVR_PDR_Fatalidades>().Respuestas();
            FindObjectOfType<FinishGame>().FinishCurrentGame();
        }
    }

    public void TeleportToNextPosition()
    {
        nquest++;
        TpNextPoint(nquest, 2);
    }

    public void TeleportToPreviousPosition() 
    {
        nquest--;
        TpNextPoint(nquest, 2);
    }

    void ActivateCurrentQuestion(int index)
    {
        if (quest.Length > 0)
            quest[index].SetActive(true);
    }
}
