using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Autohand;
using Autohand.Demo;

public class CustomRaycastBehavior : MonoBehaviour
{
    public static CustomRaycastBehavior Instance { get; private set; }

    float currentPointerDistance;
    public float CurrentPointerDistance { get => currentPointerDistance; set => currentPointerDistance = value; }

    [SerializeField] XRControllerEvent controllerEvent;
    [SerializeField] LineRenderer lineRenderer;

    [SerializeField] Material closeMat;
    [SerializeField] Material farMat;

    [SerializeField] float maxDistance;

    private void Start()
    {
        if (maxDistance == 0)
            maxDistance = 5.5f;

        ActivateXRControllerEvent(true);
    }


    private void Update()
    {
        /*if (CurrentPointerDistance >= maxDistance)
        {
            ActivateXRControllerEvent(true);
            lineRenderer.material = closeMat;
        }*/
        //Debug.Log(CurrentPointerDistance);
    }

    public void ActivateXRControllerEvent(bool state)
    {
        controllerEvent.enabled = state;
    }

    public void CheckRaycastDistance()
    {

        if (CurrentPointerDistance <= maxDistance)
        {
            ActivateXRControllerEvent(true);
            //lineRenderer.material = closeMat;
        }
        else if (CurrentPointerDistance > maxDistance)
        {
            ActivateXRControllerEvent(false);
            //lineRenderer.material = farMat;
        }
    }

    /// <summary>
    /// Resets pointer values
    /// </summary>
    public void ResetPointerValues()
    {
        ActivateXRControllerEvent(false);
        lineRenderer.material = farMat;
    }

}