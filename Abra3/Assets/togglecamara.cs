﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class togglecamara : MonoBehaviour {
	bool on = false;
	public GameObject camara;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	public void onoff () {
		
		on = !on;
		if(on)
			camara.GetComponent<Camera>().enabled = true;
		else if(!on)
			camara.GetComponent<Camera>().enabled = false;
	}
}
